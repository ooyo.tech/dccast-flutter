[![style: effective dart](https://img.shields.io/badge/style-effective_dart-40c4ff.svg)](https://pub.dev/packages/effective_dart)

# DCCast Flutter

DCCast flutter development

## Getting Started

Хөгжүүлэлттэй холбоотой асуудалуудыг бүгдийг issue үүсгэн бүртгэж байгаа

[Coding guide](docs/style-guide.md)

Шинээр хувилбар гарах болгонд [энэхүү](docs/changelog.md) файлыг шинэчилж байна уу.