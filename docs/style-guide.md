# Style Guide 

tl;dr
-----

Optimize for readability. Write detailed documentation. Test everything. Avoid `is`, `print`, `part of`, `extension` and `_`. No timeouts or timers.

## Naming convention:
* Classes, enums, typedefs, and extensions name should in `UpperCamelCase`.
* Libraries, packages, directories, and source files name should be in `snake_case`(lowercase_with_underscores)
* Variables, constants, parameters, and named parameters should be in `lowerCamelCase`

## Import package
_Use relative imports for files in lib. When use relative and absolute imports together then It is possible to create confusion when the same class gets imported from two different ways. To avoid this case we should use a relative path in the lib/ folder._

```dart
   // Don't
    import 'package:demo/src/utils/dialog_utils.dart';

    // Do
    import '../../../utils/dialog_utils.dart';
```
## Specify types for class member
 * Always specify the type of member when it’s value type is known. Avoid using var when possible.

```dart
    //Don't
    var item = 10;
    final car = Car();
    const timeOut = 2000;

    //Do
    int item = 10;
    final Car bar = Car();
    String name = 'john';
    const int timeOut = 20;
```

* Avoid using as instead, use is operator
    Usually, The as cast operator throws an exception if the cast is not possible. To avoid an exception being thrown, one can use is.

```dart
    //Don't
    (item as Animal).name = 'Lion';

    //Do
    if (item is Animal)
    item.name = 'Lion';
```

* Use if condition instead of conditional expression
    Many times we need to render a widget based on some condition in Row and Column. If conditional expression return null in any case then we should use if condition only.
```dart
    //Don't
    Widget getText(BuildContext context) {
    return Row(
        children: [
        Text("Hello"),
        Platform.isAndroid ? Text("Android") : null,
        Platform.isAndroid ? Text("Android") : SizeBox(),
        Platform.isAndroid ? Text("Android") : Container(),
        ]
    );
    }

    //Do
    Widget getText(BuildContext context) {
    return Row(
        children: 
        [
            Text("Hello"), 
            if (Platform.isAndroid) Text("Android")
        ]
    );
    }
```
* Use spread collections

 _When existing items are already stored in another collection, spread collection syntax leads to simpler code._
```dart
    //Don't
    var y = [4,5,6];
    var x = [1,2];
    x.addAll(y);


    //Do
    var y = [4,5,6];
    var x = [1,2,...y];
```
* Use Cascades Operator
_If we wont to perform a sequence of operation on the same object then we should use the Cascades(..) operator._

```dart
      // Don't
      var path = Path();
      path.lineTo(0, size.height);
      path.lineTo(size.width, size.height);
      path.lineTo(size.width, 0);
      path.close();  

      // Do
      var path = Path()
      ..lineTo(0, size.height)
      ..lineTo(size.width, size.height)
      ..lineTo(size.width, 0)
      ..close(); 
```

* Use ListView.builder for a long list

_When working with infinite lists or very large lists, it’s usually advisable to use a ListView builder in order to improve performance.
Default ListView constructor builds the whole list at once. ListView.builder creates a lazy list and when the user is scrolling down the list, Flutter builds widgets on-demand._


* Use Const in Widgets
_The widget which will not change when setState call we should define it as constant. It will prevent widget to rebuild so it improves performance._

```dart
    Container(
      padding: const EdgeInsets.only(top: 10),
      color: Colors.black,
      child: const Center(
        child: const Text(
          "No Data found",
          style: const TextStyle(fontSize: 30, fontWeight: FontWeight.w800),
        ),
      ),
    );
```

* Begin global constant names with prefix "k"
```dart
const String kSaveButtonTitle = 'Save';
const Color _kBarrierColor = Colors.black54;
```

### [Read More](https://github.com/flutter/flutter/wiki/Style-guide-for-Flutter-repo)