import 'package:dccast/screens/profile.dart';
import 'package:dccast/screens/video_detail.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import '../../constants/assets.dart';
import '../../constants/styles.dart';
import '../../model/user_media.dart';
import '../../utils/device.dart';
import '../../utils/media.dart';
import '../video/video_grid_item.dart';

class HomeGridComponent extends StatefulWidget {
  final eContent content;
  final bool scrollable;

  const HomeGridComponent({Key key,
    this.content,
    this.scrollable}) : super(key: key);
  @override
  _HomeGridComponentState createState() => _HomeGridComponentState();
}

class _HomeGridComponentState extends State<HomeGridComponent> {
  bool isLoading = true;
  UserMedia selectedMedia;
  @override
  Widget build(BuildContext context) {
    return Consumer<MediaUtils>(builder: (context, media, child) {
      selectedMedia = eContent.live == widget.content
          ? media.liveListRecentModel
          : media.vodListRecentModel;
      return isLoading
          ? Center(
              child: Lottie.asset(Assets.animLoad,
                  alignment: Alignment.center,
                  width: DeviceUtils.getScaledSize(context, 0.3)),
            )
          : GridView.builder(
              padding: EdgeInsets.all(6.0),
              itemCount:
                  selectedMedia == null ? 0 : selectedMedia?.results?.length,
              shrinkWrap: true,
              physics: widget.scrollable != null && widget.scrollable
              ? AlwaysScrollableScrollPhysics()
              : NeverScrollableScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 16.0,
                mainAxisSpacing: 8.0,
              ),
              itemBuilder: (BuildContext context, int index) {
                return _buildGridItem(selectedMedia?.results[index], context);
              },
            );
    });
  }

  Widget _buildGridItem(Media item, BuildContext context) => Container(
        decoration: BoxDecoration(color: Colors.white),
        margin: const EdgeInsets.only(bottom: 8.0),
        child: item != null
            ? VideoGridItem(
                id: item?.id,
                title: item?.title,
                subtitle: item?.mediaCategory?.name,
                kinds: item?.kinds,
                views: item?.views,
                thumnail: item?.mediaThumbnail,
                media: item,
                onPressed: () => _detailScreen(context, item),
              )
            : SizedBox(),
      );

  void _detailScreen(BuildContext context, Media item) {
    Provider.of<MediaUtils>(context, listen: false)
        .mediaDetail(item, widget.content);
    Future.delayed(Duration(milliseconds: 250), () {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => VideoDetailScreen(content: widget.content),
          ));
    });
  }

  void _fetchVod() {
    Provider.of<MediaUtils>(context, listen: false).fetchRecentMedias(
        options: 
            "category=VOD&ordering=-created&limit=20&page=1&is_hit_active=0&is_popular=0",
        success: () {
          setState(() {
            isLoading = false;
          });
        },
        error: (String error) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              margin: EdgeInsets.only(bottom: 60.0, left: 10.0, right: 10.0),
              content: Text(error),
            ),
          );
        });
  }

  void _fetchLive() {
    Provider.of<MediaUtils>(context, listen: false).fetchMedias(
        options:
            "category=LIVE&limit=20&is_hit_active=0&is_popular=0&ordering=-alive",
        success: () {
          setState(() {
            isLoading = false;
          });
        },
        error: (String error) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              margin: EdgeInsets.only(bottom: 60.0, left: 10.0, right: 10.0),
              content: Text(error),
            ),
          );
        });
  }

  @override
  void initState() {
    eContent.vod == widget.content ? _fetchVod() : _fetchLive();
    super.initState();
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  void dispose() {
    super.dispose();
  }
}
