import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';

import '../../constants/assets.dart';
import '../../constants/styles.dart';
import '../../model/user_media.dart';
import '../../screens/video_detail.dart';
import '../../utils/device.dart';
import '../../utils/media.dart';

class HomeSwiperComponent extends StatefulWidget {
  final eContent content;

  HomeSwiperComponent(this.content);

  @override
  _HomeSwiperComponentState createState() => _HomeSwiperComponentState();
}

class _HomeSwiperComponentState extends State<HomeSwiperComponent> {
  UserMedia selectedMedia;
  bool isLoading = true;

  @override
  Widget build(BuildContext context) {
    return Consumer<MediaUtils>(builder: (context, media, child) {
      selectedMedia = eContent.vod == widget.content
          ? media.hitVodMedia
          : media.hitLiveMedia;

      return selectedMedia != null
          ? Swiper(
              itemBuilder: (BuildContext context, int position) {
                return _buildSwiperItem(
                    context, selectedMedia?.results[position]);
              },
              itemCount: selectedMedia?.results?.length,
              pagination: SwiperPagination())
          : Container();
    });
  }

  Widget _buildSwiperItem(BuildContext context, Media item) {
    return GestureDetector(
      onTap: () {
        Provider.of<MediaUtils>(context, listen: false)
            .mediaDetail(item, widget.content);
        Future.delayed(Duration(milliseconds: 250), () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    VideoDetailScreen(content: widget.content),
              ));
        });
        // );
        // Future.delayed(Duration(milliseconds: 250), () {

        // });
      },
      child: Stack(
        children: <Widget>[
          CachedNetworkImage(
              fit: BoxFit.cover,
              width: DeviceUtils.getScaledWidth(context, 1.0),
              imageUrl: item.mediaThumbnail,
              placeholder: (context, url) =>
                  Image.asset(Assets.videoThumbnail, fit: BoxFit.cover),
              errorWidget: (context, url, error) {
                return Image.asset(Assets.videoThumbnail, fit: BoxFit.cover);
              }),
          (item?.isPopular ?? false)
              ? Positioned(
                  top: 10,
                  left: 10,
                  child: Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 2.0, horizontal: 8.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: kRedColor,
                    ),
                    child: Text(
                        eContent.vod == widget.content
                            ? translate('top_vod')
                            : translate('top_live'),
                        style: kSwiperStyle),
                  ),
                )
              : Container(),
          Positioned(
              bottom: 25,
              left: 10,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  item.isHit
                      ? Text(
                          eContent.vod == widget.content
                              ? translate("hit_vod")
                              : translate("hit_live"),
                          style: kSwiperStyle)
                      : SizedBox(),
                  SizedBox(height: 4.0),
                  Row(
                    children: [
                      ClipOval(
                        child: CachedNetworkImage(
                            width: 20,
                            height: 20,
                            fit: BoxFit.cover,
                            imageUrl: item.user?.profileImage,
                            errorWidget: (context, url, error) {
                              return Container(
                                padding: EdgeInsets.only(top: 3.0),
                                color: Colors.white,
                                child: Image.asset(
                                  Assets.profileImage,
                                  color: kPrimaryColor,
                                ),
                              );
                            }),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 4.0),
                        child: Text(item.user?.nickName, style: kSwiperStyle),
                      )
                    ],
                  )
                ],
              )),
        ],
      ),
    );
  }
}
