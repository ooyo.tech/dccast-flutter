import 'package:dccast/utils/overlay_services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';

import '../../constants/styles.dart';
import '../../model/user_media.dart';
import '../../screens/video_detail.dart';
import '../../utils/media.dart';
import '../../utils/toast.dart';
import '../video/videolistitem.dart';

class HomeListComponent extends StatelessWidget {
  UserMedia selectedMedia;
  bool scrollable;
  final eContent contentType;

  HomeListComponent(
    this.contentType, this.scrollable);

  @override
  Widget build(BuildContext context) {
    return Consumer<MediaUtils>(builder: (context, media, child) {
      selectedMedia = eContent.live == contentType
          ? media.liveListModel
          : media.vodListModel;
      return ListView.builder(
        shrinkWrap: true,
        physics: scrollable
         ? AlwaysScrollableScrollPhysics()
         : NeverScrollableScrollPhysics(),
        padding: const EdgeInsets.all(10.0),
        itemCount: selectedMedia == null ? 0 : selectedMedia?.results?.length,
        itemBuilder: (context, index) {
          return _buildListItem(selectedMedia?.results[index], context, index);
        },
      );
    });
  }

  Widget _buildListItem(Media item, BuildContext context, int index) => Container(
        decoration: BoxDecoration(color: Colors.white),
        margin: const EdgeInsets.only(bottom: 8.0),
        child: item != null
            ? VideoListItem(
                id: item?.id,
                title: item?.title,
                subtitle: item?.explanation,
                kinds: item?.kinds,
                dates: item?.created,
                crown: item?.isHit,
                duration: secToMinConverter(item?.duration ?? 0),
                views: item?.views,
                thumnail: item?.mediaThumbnail,
                content: contentType,
                media: item,
                onPressed: () => _detailScreen(context, item),
                onDelete: () {
                  if (eContent.live == contentType) {
                    Provider.of<MediaUtils>(context, listen: false)
                      .liveListModel.results.removeAt(index);
                    Provider.of<MediaUtils>(context, listen: false)
                      .liveListModel.count--;
                  } else {
                    Provider.of<MediaUtils>(context, listen: false)
                      .vodListModel.results.removeAt(index);
                    Provider.of<MediaUtils>(context, listen: false)
                      .vodListModel.count--;
                  }
                },
              )
            : SizedBox(),
      );

  void _detailScreen(BuildContext context, Media item) {
    if (item.category == "LIVE" && !item.alive) {
      showToast(translate("live.ended"));
      return;
    }
    print("testorat");
    Provider.of<MediaUtils>(context, listen: false)
        .mediaDetail(item, contentType);
    Future.delayed(Duration(milliseconds: 250), () {
      // OverlayService().addVideoTitleOverlay(context,
      //   VideoDetailScreen(content: contentType)
      // );

      Navigator.push(
          context, //VideoDetailScreen.id
          MaterialPageRoute(
            builder: (context) => VideoDetailScreen(content: contentType),
          ));
    });
  }
}
