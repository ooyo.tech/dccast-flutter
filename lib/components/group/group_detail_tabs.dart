import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../constants/styles.dart';
import 'group_live.dart';
import 'group_members.dart';
import 'group_vod.dart';

class GroupDetailTabsComponent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Column(
        children: <Widget>[
          Divider(),
          Container(
            color: Colors.white,
            constraints: BoxConstraints.expand(height: 50),
            child: TabBar(
                indicator: UnderlineTabIndicator(
                  borderSide: BorderSide(width: 3.0, color: kPrimaryColor),
                ),
                tabs: [
                  Tab(
                      child: Text(translate("castlist.group.members"),
                          style: kTextStyle)),
                  Tab(child: Text(translate("live"), style: kTextStyle)),
                  Tab(child: Text(translate("vod"), style: kTextStyle)),
                ]),
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: TabBarView(children: [
                GroupMembersComponent(),
                GroupLiveComponent(),
                GroupVodComponent()
              ]),
            ),
          )
        ],
      ),
    );
  }
}
