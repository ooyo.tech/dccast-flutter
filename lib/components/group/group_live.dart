import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/group_list.dart';
import '../../model/group_media_list.dart';
import '../../model/user_media.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../../utils/group.dart';
import '../video/videolistitem.dart';

class GroupLiveComponent extends StatefulWidget {
  @override
  _GroupLiveComponentState createState() => _GroupLiveComponentState();
}

class _GroupLiveComponentState extends State<GroupLiveComponent> {
  bool isLoading = false;
  GroupMediaList _groupMediaList;
  int lastPageId = 1;
  final storage = FlutterSecureStorage();
  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
        progressIndicator: Lottie.asset(Assets.animLoad,
            width: DeviceUtils.getScaledSize(context, 0.4),
            height: DeviceUtils.getScaledSize(context, 0.4)),
        inAsyncCall: isLoading,
        child: Column(
          children: [
            Expanded(
              child: NotificationListener<ScrollNotification>(
                  onNotification: (ScrollNotification scrollInfo) {
                    if (!isLoading &&
                        scrollInfo.metrics.pixels ==
                            scrollInfo.metrics.maxScrollExtent) {
                      this.fetchGroupLive();
                      setState(() {
                        isLoading = true;
                      });
                    }
                  },
                  child: _groupMediaList?.results == null ||
                          _groupMediaList?.results.isEmpty
                      ? Center(
                          child: Lottie.asset(Assets.animNoData,
                              alignment: Alignment.center,
                              width: DeviceUtils.getScaledSize(context, 0.5)),
                        )
                      : ListView.builder(
                          padding: const EdgeInsets.all(0.0),
                          itemCount: _groupMediaList == null
                              ? 0
                              : _groupMediaList?.results?.length,
                          itemBuilder: (context, index) {
                            return _buildListItem(
                                _groupMediaList.results[index].media, index);
                          },
                        )),
            )
          ],
        ));
  }

  Widget _buildListItem(Media item, int index) => Container(
        decoration: BoxDecoration(color: Colors.white),
        padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
        margin: const EdgeInsets.only(bottom: 8.0),
        child: item != null
            ? VideoListItem(
                id: item?.id,
                title: item?.title,
                subtitle: item?.explanation,
                kinds: item?.kinds,
                dates: item?.created,
                crown: item?.isHit,
                duration: secToMinConverter(item?.duration ?? 0),
                views: item?.views,
                thumnail: item?.mediaThumbnail,
                media: item,
                onDelete: () {
                  setState(() {
                    _groupMediaList.results.removeAt(index);
                    _groupMediaList.count--;
                    // Provider.of<GroupUtil>(context, listen: true).liveList
                    //   .results.removeAt(index);
                    // Provider.of<GroupUtil>(context, listen: true).liveList
                    //   .count--;
                  });
                },
              )
            : SizedBox(),
      );

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    this.fetchGroupLive();
    super.initState();
    // call get json data function
    // selectedPage = Selec
  }

  Future<String> fetchGroupLive() async {
    setState(() {
      isLoading = true;
    });
    final dio = Dio();
    try {
      Map<String, dynamic> data = {
        // "user_id": userID,
        "page": lastPageId.toString()
      };
      await Provider.of<GroupUtil>(context, listen: false).fetchGroupLive(data);
      setState(() {
        GroupMediaList newItems =
         Provider.of<GroupUtil>(context, listen: false).liveList;
        if (_groupMediaList == null) {
          _groupMediaList = newItems;
        } else {
          _groupMediaList.results.addAll(newItems.results);
        }
        lastPageId = lastPageId + 1;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }
}
