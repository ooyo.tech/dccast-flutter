import 'package:cached_network_image/cached_network_image.dart';
import 'package:dccast/components/group/group_vod.dart';
import 'package:dccast/screens/live/add_live.dart';
import 'package:dccast/screens/vod/add_vod.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

import '../../constants/assets.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/group_list.dart';
import '../../utils/device.dart';
import '../../utils/group.dart';

class GroupDetailHeaderComponent extends StatelessWidget {
  Group _group;
  @override
  Widget build(BuildContext context) {
    _group = Provider.of<GroupUtil>(context, listen: false).chosenGroup;
    return Container(
      // height: DeviceUtils.getScaledHeight(context, 0.25),
      padding: EdgeInsets.only(top: Metrics.doubleBase),
      margin: EdgeInsets.symmetric(horizontal: Metrics.doubleBase),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Column(
                  children: [
                    GestureDetector(
                      onTap: () async {
                        await Navigator.push(
                          context,
                          MaterialPageRoute(builder: 
                            (context) => AddLiveScreen(groupId: _group.id),
                          ),
                        );
                        Provider.of<GroupUtil>(context, listen: false)
                          .fetchGroupLive({"page": 1});
                      },
                      child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 16.0),
                        padding: EdgeInsets.symmetric(
                            horizontal: 16.0, vertical: 12.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(32.0),
                          color: kGreyColor,
                        ),
                        child: Row(
                          children: [
                            Image.asset(Assets.sideMenuLive),
                            SizedBox(width: 12.0),
                            Text(translate("castlist.group.start-live")),
                          ],
                        ),
                      )
                    ),
                    SizedBox(height: 12.0),
                    GestureDetector(
                      onTap: () async {
                        await Navigator.push(
                          context,
                          MaterialPageRoute(builder: 
                            (context) => StartVodScreen(groupId: _group.id),
                          ),
                        );
                        Provider.of<GroupUtil>(context, listen: false)
                          .fetchGroupVOD({"page": 1});
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: 16.0, vertical: 12.0),
                        margin: EdgeInsets.symmetric(horizontal: 16.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(32.0),
                          color: kGreyColor,
                        ),
                        child: Row(
                          children: [
                            Image.asset(Assets.sideMenuVod),
                            SizedBox(width: 12.0),
                            Text(translate("castlist.group.upload-vod")),
                          ],
                        ),
                      )
                    )
                  ],
                ),
              ),
              Container(
                child: ClipOval(
                  child: _group.profileImg != null
                      ? CachedNetworkImage(
                          fit: BoxFit.cover,
                          width: DeviceUtils.getScaledWidth(context, 0.3),
                          height: DeviceUtils.getScaledWidth(context, 0.3),
                          imageUrl: _group.profileImg,
                          placeholder: (context, url) =>
                              Image.asset(Assets.appLogo, fit: BoxFit.cover),
                          errorWidget: (context, url, error) {
                            return Image.asset(
                              Assets.appLogo,
                              fit: BoxFit.contain,
                              width: 25.0,
                              height: 25.0,
                              color: kPrimaryColor,
                            );
                          })
                      : Container(
                          width: DeviceUtils.getScaledWidth(context, 0.3),
                          height: DeviceUtils.getScaledWidth(context, 0.3),
                          color: kGreyColor,
                          child: Icon(
                            FontAwesomeIcons.users,
                            color: kBlack50Color,
                            size: DeviceUtils.getScaledWidth(context, 0.15),
                          )),
                ),
              )
            ],
          ),
          Text(_group.name.toString(), style: kTextBoldStyle),
          Text(_group.message.toString()),
        ],
      ),
    );
  }
}
