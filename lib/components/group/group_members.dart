import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

import '../../constants/assets.dart';
import '../../constants/server_settings.dart';
import '../../model/group_list.dart';
import '../../model/group_members.dart';
import '../../model/user.dart';
import '../../screens/profile.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../../utils/group.dart';
import '../user_list_item.dart';

class GroupMembersComponent extends StatefulWidget {
  @override
  _GroupMembersComponentState createState() => _GroupMembersComponentState();
}

class _GroupMembersComponentState extends State<GroupMembersComponent> {
  bool isLoading = false;
  GroupMembers _groupMembers;
  int lastPageId = 1;
  final storage = FlutterSecureStorage();
  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
        progressIndicator: Lottie.asset(Assets.animLoad,
            width: DeviceUtils.getScaledSize(context, 0.3),
            height: DeviceUtils.getScaledSize(context, 0.3)),
        inAsyncCall: isLoading,
        child: Column(
          children: [
            Expanded(
                child: (_groupMembers?.results == null
                 || _groupMembers.results?.isEmpty)
                    ? Center(
                        child: Lottie.asset(Assets.animNoData,
                            alignment: Alignment.center,
                            width: DeviceUtils.getScaledSize(context, 0.3)),
                      )
                    : ListView.builder(
                        padding: const EdgeInsets.all(0.0),
                        itemCount: _groupMembers == null
                            ? 0
                            : _groupMembers?.results?.length,
                        itemBuilder: (context, index) {
                          return _buildMemberItem(_groupMembers.results[index]);
                        },
                      )),
          ],
        ));
  }

  Widget _buildMemberItem(Profile item) => Container(
        decoration: BoxDecoration(color: Colors.white),
        margin: const EdgeInsets.only(bottom: 8.0),
        padding: EdgeInsets.all(4.0),
        child: Column(
          children: <Widget>[
            UserListItem(
              title: item.user.username,
              subtitle: item.nickName,
              profileImage: item.profileImage,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ProfileScreen(userProfile: item),
                  ),
                );
              },
            ),
          ],
        ),
      );

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    super.initState();
    fetchGroupMembers();
    // call get json data function
    // selectedPage = Selec
  }

  Future<String> fetchGroupMembers() async {
    setState(() {
      isLoading = true;
    });
    final dio = Dio();
    Group _group = Provider.of<GroupUtil>(context, listen: false).chosenGroup;

    try {
      final response = await DioClient(dio).get(ServerSettings.getGroupsURL +
          "/" +
          _group.id.toString() +
          "/members");

      var jsonResponse = GroupMembers.fromJson(response);

      print(response.toString());
      setState(() {
        GroupMembers newItems = jsonResponse;
        if (_groupMembers == null) {
          _groupMembers = newItems;
        } else {
          _groupMembers.results.addAll(newItems.results);
        }
        lastPageId = lastPageId + 1;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }
}
