import 'dart:async';
import 'dart:convert';

import 'package:adhara_socket_io/adhara_socket_io.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';

import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/comments.dart';
import '../../utils/auth.dart';
import '../../utils/socket.dart';
import '../../utils/toast.dart';

class ChatListComponent extends StatefulWidget {
  static const String id = 'chat_list_screen';
  final int roomID;
  bool fullscreen;
  bool owner;

  ChatListComponent({
    Key key,
    @required this.roomID,
    this.fullscreen,
    this.owner
  }) : super(key: key);

  @override
  ChatListComponentState createState() => ChatListComponentState();
}

class ChatListComponentState extends State<ChatListComponent> {

  final storage = FlutterSecureStorage();
  String username;
  String nickname;
  String userID;
  int membersCount = 0;
  List<Chat> chats = [];
  FocusNode chatFocus = FocusNode();
  TextEditingController chatController = TextEditingController();
  final ScrollController _chatScroller = ScrollController();
  SocketChat socketChat;
  SocketIOManager manager;
  SocketIO socket;

  @override
  void initState() {
    /*var chat = Chat();
    chat.message = "message";
    chat.userId = "userId";
    chats.add(chat);
    chats.add(chat);
    chats.add(chat);
    chats.add(chat);
    chats.add(chat);
    chats.add(chat);
    chats.add(chat);
    chats.add(chat);
    chats.add(chat);
    chats.add(chat);
    chats.add(chat);
    chats.add(chat);
    chats.add(chat);
    chats.add(chat);
    chats.add(chat);
    chats.add(chat);
    chats.add(chat);
    chats.add(chat);
    var chat2 = Chat();
    chat2.userId = "spinaweb";
    chat2.message = "last message";
    chat2.type = "dccon";
    chat2.dcconUrl = "https://dcimg5.dcinside.com/dccon.php?no=62b5df2be09d3ca567b1c5bc12d46b394aa3b1058c6e4d0ca41648b65ae3266e2d11ab02b78e7055e414deec46f96b242872cfb63c49d411fad69afb6e8d39d0cc8f731d84695b";
    chats.add(chat2);*/
    super.initState();
    initSocket();
  }

  @override
  Widget build(BuildContext context) {
    return _liveChatList(context);
  }

  @override
  void dispose() {
    if (socket != null) {
      socket.emit("leave_chat", [widget.roomID]);
      manager?.clearInstance(socket);
    }
    super.dispose();
  }

  Widget _liveChatList(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10),
      child: ListView.separated(
        shrinkWrap: true,
        reverse: true,
        physics: BouncingScrollPhysics(),
        itemCount: chats.length,
        itemBuilder: (content, index) => 
        GestureDetector(
          onLongPress: () {
            if (widget.owner ?? false) {
              if (chats[chats.length - index - 1].userId != username) {
                _chatOptions(chats[chats.length - index - 1].userId);
              }
            }
          },
          child: chats[chats.length - index - 1].type == "dccon"
            ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("${chats[chats.length - index - 1].userId}: ",
                  style: TextStyle(
                    color: widget.fullscreen
                      ? Colors.white
                      : Colors.black,
                    fontWeight: FontWeight.bold
                  )
                ),
                CachedNetworkImage(
                  fit: BoxFit.cover,
                  width: 80.0,
                  height: 80.0,
                  imageUrl: chats[chats.length - index - 1].dcconUrl,
                  placeholder:(context, url) => 
                    Image.asset(Assets.loading),
                  errorWidget: (context, url, error) {
                    return Image.asset(Assets.loading);
                  }
                )
              ],
            )
            : RichText(
              text: TextSpan(
              style: TextStyle(color: widget.fullscreen
                ? Colors.white
                : Colors.black),
              children: <TextSpan>[
                TextSpan(text: "${chats[chats.length - index - 1].userId}: ",
                  style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: chats[chats.length - index - 1].message),
              ],
            ),
          )
        ),
        separatorBuilder: (context, index) => SizedBox(height: 20),
      )
    );
  }

  Future<void> initSocket() async {
    userID = await storage.read(key: Preferences.user_id);
    username = Provider.of<Auth>(context, listen: false).userProfile.user.username;
    nickname = Provider.of<Auth>(context, listen: false).userProfile.nickName;

    manager = SocketIOManager();
    socket = await manager.createInstance(SocketOptions(
      '${ServerSettings.baseUploadUrl}/socket.io',
      enableLogging: true,
      transports: [
        Transports.webSocket,
      ]
    ));
    print("socket");
    print(socket);
    socket.onConnect.listen((data) {
      var message = {
        "room": widget.roomID,
        "userId": username,
      };
      print(message);
      socket.emit("join", [message]);
      socket.emit("add user", [username]);
      socket.emit("members count", [widget.roomID]);
    });
    socket.onConnectError.listen(pPrint);
    socket.onConnectTimeout.listen(pPrint);
    socket.onError.listen(pPrint);
    socket.onDisconnect.listen(pPrint);

    socket.on('new message').listen((data) {
      if (data != null) {
        var chat = Chat();//.fromJson(data);
        data = data[0]["message"];
        String message = data["message"];
        chat.type = "msg";
        if (message.startsWith("dcconUrl:")) {
          chat.type = "dccon";
          chat.dcconUrl = message.replaceFirst("dcconUrl:", "");
        }
        chat.message = message;
        chat.userId = data["nickname"];
        setState(() {
          chats.add(chat);
        });
      }
    });
    socket.on('no message').listen((data) {
      if (data != null) {
        data = data[0];
        var msg = "";
        if (data["userId"] == username) {
          if (data["type"] == "mute") {
            msg = translate("live.comment.disabled");
          } else if (data["type"] == "3min") {
            msg = translate("live.comment.banned.3min");
          } else if (data["type"] == "kick") {
            msg = translate("live.comment.banned");
          }
          showToast(msg);
        }
      }
    });
    socket.on('all_members').listen((data) {
      if (data != null) {
        data = data[0]["members"];
        membersCount = data - 1;
      }
    });
    await socket.connect();
  }

  void toggleMute(bool value) {
    socket.emit("mute_all", [widget.roomID]);
  }

  void sendChat(String messageText) {
    var message = {
      "room": widget.roomID,
      "userId": username,
      "nickname": nickname,
      "message": messageText
    };
    socket.emit("new message", [message]);
  }

  void pPrint(Object data) {
    print("===SOCKET===");
    if (data is Map) {
      data = json.encode(data);
    }
    print(data);
  }

  Future<void> _chatOptions(userID) async {
    var option = "";
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, setModalState) {
          return Dialog(
            elevation: 0,
            backgroundColor: Colors.transparent,
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(Metrics.singleBase),
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(Metrics.halfBase),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, top: 8.0),
                      child: Text(
                        translate("live.chat.ban.title"),
                        style: kTextPrimaryStyle,
                      ),
                    ),
                    SizedBox(height: 8.0),
                    RadioListTile(
                      contentPadding: const EdgeInsets.all(0),
                      dense: true,
                      value: "mute",
                      groupValue: option,
                      title: Text(translate("live.chat.3min-ban")),
                      onChanged: (currentKind) {
                        setModalState(() {
                          option = "mute";
                        });
                      },
                      selected: option == "mute",
                      activeColor: kPrimaryColor,
                    ),
                    RadioListTile(
                      contentPadding: const EdgeInsets.all(0),
                      dense: true,
                      value: "kick",
                      groupValue: option,
                      title: Text(translate("live.chat.force")),
                      onChanged: (currentKind) {
                        setModalState(() {
                          option = "kick";
                        });
                      },
                      selected: option == "kick",
                      activeColor: kPrimaryColor,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          child: FlatButton(
                            child: Text(translate("cancel"),
                                style: TextStyle(color: Colors.white)),
                            color: Colors.grey,
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                        SizedBox(width: 8.0),
                        Expanded(
                          child: FlatButton(
                            color: kPrimaryColor,
                            child: Text(translate("confirm"),
                                style: TextStyle(color: Colors.white)),
                            onPressed: () {
                              var message = {
                                "room": widget.roomID,
                                "userId": userID
                              };
                              socket.emit(option, [message]);
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        });
      }
    );
  }
}
