import 'package:flutter/material.dart';

class AddLiveListTile extends StatelessWidget {
  AddLiveListTile(
      {@required this.title,
      this.leading,
      this.trailing,
      Function this.onPressed});

  final Widget title;
  final Widget leading;
  final Widget trailing;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed != null ? onPressed : () {},
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          children: [
            Container(
              height: 50.0,
              child: Row(
                children: [
                  leading,
                  SizedBox(width: 14.0),
                  title,
                  SizedBox(width: 16.0),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: trailing
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              height: 1.0,
            )
          ],
        ),
      ),
    );
  }
}
