import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/styles.dart';
import '../../model/comments.dart';
import '../../utils/comments.dart';
import '../../utils/toast.dart';
import '../report_dialog.dart';
import 'comment_reply_item.dart';
import 'comment_send.dart';

const _replyLength = 2;

class CommentItemComponent extends StatelessWidget {
  CommentItemComponent({this.comment, this.replyShow = true});

  final storage = FlutterSecureStorage();
  final Comment comment;
  final bool replyShow;

  @override
  Widget build(BuildContext context) {
    final now = DateTime.now();
    final today = DateTime(now.year, now.month, now.day);
    final thisyear = DateTime(now.year);
    var datetime = " ";
    if (comment.created != null) {
      DateTime created = comment.created;
      if (created.isAfter(today)) {
        datetime += DateFormat("HH:mm").format(created);
      } else if (created.isBefore(thisyear)) {
        datetime += DateFormat("yy.MM.dd").format(created);
      } else {
        datetime += DateFormat("MM.dd").format(created);
      }
    }
    return ListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 0),
      isThreeLine: true,
      leading: CachedNetworkImage(
          fit: BoxFit.cover,
          width: 40.0,
          height: 40.0,
          imageUrl: comment.user.profileImage,
          placeholder: (context, url) => Image.asset(Assets.profileImage),
          errorWidget: (context, url, error) {
            return Image.asset(Assets.videoThumbnail);
          }),
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(child: 
                      Text(comment.user.nickName.toString(),
                        style: kTextGreyStyle, maxLines: 1)
                    ),
                    Spacer(),
                    Row(children: [
                      Icon(Icons.watch_later_outlined,
                          color: kBlack50Color, size: 14.0),
                      Text(datetime, style: kTextGreyStyle),
                    ])
                  ]
                ),
              ),
              IconButton(
                padding: EdgeInsets.zero,
                alignment: Alignment.topCenter,
                iconSize: 20.0,
                icon: FaIcon(
                  FontAwesomeIcons.ellipsisV,
                  color: kBlack50Color,
                  size: 20.0,
                ),
                onPressed: () {
                  _moreInfo(context, comment);
                },
              ),
            ],
          ),
          SizedBox(height: 6.0),
          comment.dcconUrl != null
              ? CachedNetworkImage(
                fit: BoxFit.cover,
                width: 80.0,
                height: 80.0,
                imageUrl: comment.dcconUrl,
                placeholder:(context, url) => 
                  Image.asset(Assets.loading),
                errorWidget: (context, url, error) {
                  return Image.asset(Assets.loading);
                }
              )//Image.network(comment.dcconUrl)
              : Text(comment.text.toString())
        ],
      ),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              FlatButton(
                minWidth: 20.0,
                onPressed: () {
                  Provider.of<CommentsUtil>(context, listen: false)
                      .likeComment(comment);
                },
                child: Row(
                  children: [
                    Icon(
                      comment.liked
                          ? FontAwesomeIcons.solidThumbsUp
                          : FontAwesomeIcons.thumbsUp,
                      size: 15.0,
                      color: kPrimaryColor,
                    ),
                    SizedBox(
                      width: 6.0,
                    ),
                    Text(comment.like.toString(), style: kTextPrimaryStyle),
                  ],
                ),
              ),
              FlatButton(
                minWidth: 20.0,
                onPressed: () {
                  Provider.of<CommentsUtil>(context, listen: false)
                      .dislikeComment(comment);
                },
                child: Row(
                  children: [
                    Icon(
                      comment.disliked
                          ? FontAwesomeIcons.solidThumbsDown
                          : FontAwesomeIcons.thumbsDown,
                      size: 15.0,
                      color: kPrimaryColor,
                    ),
                    SizedBox(width: 6.0),
                    Text(comment.disLike.toString(), style: kTextPrimaryStyle),
                  ],
                ),
              )
            ],
          ),
          if (replyShow)
          Container(
            padding: EdgeInsets.symmetric(horizontal: 0.0),//50.0
            child: comment != null && comment.replies != null
              && comment.replies.isNotEmpty
              ? Column(children: 
              List.generate(
                  min(comment.replies.length, _replyLength), (index) {
                return CommentItemComponent(
                  replyShow: false,
                  comment: comment?.replies[index]);
                }
              )
            )
            : Container()
          ),
          replyShow
            ? Row(
              children: [
                comment.replies.length - _replyLength > 0
                  ? FlatButton(
                      padding: EdgeInsets.zero,
                      child: Text(
                        translate("view") +
                            " " +
                            (comment.replies.length - _replyLength)
                            .toString() +
                            " " +
                            translate("reply"),
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: kPrimaryColor),
                      ),
                      onPressed: () {
                        _showCommentReply(context, comment);
                      },
                    )
                  : Container(),
                /*FlatButton(
                  padding: EdgeInsets.zero,
                  child: Text(
                    translate("reply"),
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: kPrimaryColor),
                  ),
                  onPressed: () {
                  },
                )*/
              ],
            )
          : SizedBox(),
        ],
      ),
    );
  }

  void _showCommentReply(BuildContext context, Comment comment) async {
    await showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: true,
        backgroundColor: Colors.white,
        context: context,
        builder: (BuildContext bc) {
          return FractionallySizedBox(
            heightFactor: 0.64,
            child: CommentReplyItemComponent(comment: comment),
          );
        });
  }

  void _moreInfo(BuildContext context, Comment comment) async {
    String userID = await storage.read(key: Preferences.user_id);
    await showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return ListView(
            shrinkWrap: true,
            children: [
              if (comment.user.id.toString() == userID)
                ListTile(
                  leading: Icon(Icons.edit_outlined),
                  title: Text(translate("edit")),
                  onTap: () async {
                    Navigator.of(context).pop();
                    await Duration.millisecondsPerSecond / 100;
                    showModalBottomSheet(
                      isScrollControlled: true,
                      isDismissible: true,
                      backgroundColor: Colors.white,
                      context: context,
                      builder: (BuildContext bc) {
                        return Container(
                          padding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).viewInsets.bottom),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  print("pronting hahha");
                                  showModalBottomSheet(
                                    isScrollControlled: true,
                                    isDismissible: true,
                                    backgroundColor: Colors.white,
                                    context: context,
                                    builder: (BuildContext bc) {
                                      return CommentSendScreen();
                                    }
                                  );
                                },
                                child: CommentSendScreen(
                                  comment: comment,
                                  onSuccess: (text) {
                                    comment.text = text;
                                  }
                                )
                              )
                            ]
                          )
                        );
                      }
                    );
                  }
                ),
              ListTile(
                leading: Icon(Icons.reply),
                title: Text(translate("reply")),
                onTap: () {
                  _showCommentReply(context, comment);
                }
              ),
              if (comment.user.id.toString() != userID)
                ListTile(
                  leading: Icon(Icons.report_gmailerrorred_outlined),
                  title: Text(translate("report")),
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return ReportDialog(
                              title: translate("report"),
                              type: eReport.COMMENT, comment: comment);
                        });
                  },
                ),
              if (comment.user.id.toString() == userID)
                ListTile(
                  leading: Icon(Icons.delete_outline, color: Colors.red),
                  title: Text(translate("delete"),
                    style: TextStyle(color: Colors.red)),
                  onTap: () {
                    _showMyDialog(
                        context,
                        translate("comment_delete_desc"),
                        comment,
                        TextButton(
                          child: Text(translate("alert.button.ok")),
                          onPressed: () {
                            Provider.of<CommentsUtil>(context, listen: false)
                                .deleteComment(comment)
                                .then((value) {
                              Navigator.pop(context);
                              Navigator.pop(context);
                            });
                            showToast(" 댓글이 삭제 되었습니다");
                          },
                        ));
                  },
                )
            ],
          );
        });
  }

  Future<void> _showMyDialog(BuildContext context, String message,
      Comment comment, Widget extraBtn) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(translate("comment_delete_title")),
          content: SingleChildScrollView(
            child: Text(message),
          ),
          actions: <Widget>[
            TextButton(
              child: extraBtn != null
                  ? Text(translate("group.delete.cancel"))
                  : Text(translate("alert.button.ok")),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            extraBtn
          ],
        );
      },
    );
  }
}
