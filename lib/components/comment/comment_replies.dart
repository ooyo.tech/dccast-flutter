import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../constants/assets.dart';
import '../../constants/styles.dart';
import '../../model/comments.dart';

class CommentRepliesComponent extends StatelessWidget {
  CommentRepliesComponent({this.reply});

  final Reply reply;
  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding:
          EdgeInsets.zero, //.symmetric(horizontal: 2.0, vertical: 4.0),
      isThreeLine: true,
      leading: CachedNetworkImage(
          fit: BoxFit.cover,
          width: 40.0,
          height: 40.0,
          imageUrl: reply.user.profileImage,
          placeholder: (context, url) => Image.asset(Assets.profileImage),
          errorWidget: (context, url, error) {
            return Image.asset(Assets.videoThumbnail);
          }),
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(reply.user.nickName.toString(),
              style: kTextGreyStyle, maxLines: 1),
          Row(
            children: [
              Icon(Icons.watch_later_outlined,
                  color: kBlack50Color, size: 14.0),
              Text(timeago.format(reply.created, locale: 'ko'),
                  style: kTextGreyStyle)
            ],
          ),
          SizedBox(height: 6.0),
          reply.dcconUrl != null
              ? Image.network(reply.dcconUrl)
              : Text(reply.text.toString())
        ],
      ),
      subtitle: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Icon(
                  FontAwesomeIcons.solidThumbsUp,
                  size: 15.0,
                  color: kPrimaryColor,
                ),
                SizedBox(
                  width: 6.0,
                ),
                Text(reply.like.toString(), style: kTextPrimaryStyle),
                SizedBox(width: 6.0),
                Icon(
                  FontAwesomeIcons.thumbsDown,
                  size: 15.0,
                  color: kPrimaryColor,
                ),
                SizedBox(width: 6.0),
                Text(reply.disLike.toString(), style: kTextPrimaryStyle),
              ],
            ),
          ],
        ),
      ),
      trailing: IconButton(
        padding: EdgeInsets.zero,
        alignment: Alignment.topCenter,
        iconSize: 20.0,
        icon: FaIcon(
          FontAwesomeIcons.ellipsisV,
          color: kBlack50Color,
          size: 20.0,
        ),
        onPressed: () {},
      ),
    );
  }
}
