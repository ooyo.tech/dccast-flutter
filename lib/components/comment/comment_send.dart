import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import '../../constants/assets.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/comments.dart';
import '../../model/user.dart';
import '../../model/user_media.dart';
import '../../utils/auth.dart';
import '../../utils/comments.dart';
import '../../utils/dio_client.dart';
import '../../utils/media.dart';
import 'comment_dccon.dart';

class CommentSendScreen extends StatefulWidget {
  final int parentComment;
  final Function onSuccess;
  Function disabled = null;
  Comment comment;

  CommentSendScreen({Key key,
    this.comment,
    this.parentComment,
    this.disabled,
    this.onSuccess})
      : super(key: key);

  @override
  _CommentSendScreenState createState() => _CommentSendScreenState();
}

class _CommentSendScreenState extends State<CommentSendScreen> {
  final storage = FlutterSecureStorage();
  final commentController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  FocusNode focusNode = FocusNode();

  void initState() {
    super.initState();
    if (widget.comment != null) {
      commentController.text = widget.comment.text;
    }
    focusNode.requestFocus();
  }

  void dispose() {
    commentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration( //   
        border: Border(bottom: BorderSide(color: Colors.grey),
        ),
      ),
      child: ListTile(
      contentPadding: EdgeInsets.symmetric(horizontal: 20.0),
      title: (Form(
        key: _formKey,
        child: Row(
          children: [
            Image.asset(Assets.profileImage, color: kPrimaryColor),
            SizedBox(width: 16.0),
            Expanded(
              child: TextFormField(
                readOnly: widget.disabled != null,
                focusNode: focusNode,
                autofocus: widget.comment != null,
                validator: (value) {
                  if (value.isEmpty) {
                    // return translate("empty_fields");
                  }
                  return null;
                },
                onTap: widget.disabled,
                controller: commentController,
                style: TextStyle(color: kPrimaryColor, fontSize: 16),
                decoration: InputDecoration(
                  hintStyle: TextStyle(color: kGreyColor),
                  hintText: translate('comment_add'),
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                ),
              ),
            ),
            IconButton(
              icon: Icon(FontAwesomeIcons.smile),
              color: Colors.grey,
              onPressed: widget.disabled != null ? widget.disabled : showEmojis
            )
          ],
        ),
      )),
      trailing: IconButton(
        icon: Icon(FontAwesomeIcons.solidPaperPlane),
        color: kPrimaryColor,
        onPressed: () {
          if (widget.disabled != null) {
            widget.disabled();
          } else {
            _submit(context);
          }
        }
      ),
    )
    );
  }

  void showEmojis() {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: true,
      backgroundColor: Colors.white,
      context: context,
      builder: (BuildContext bc) {
        return CommentDcconScreen(sendEmoji: _sendEmoji);
      }
    );
  }

  void _sendEmoji(BuildContext context, Dccon dccon) async {
    print(dccon.img);
    var chosenMedia =
        Provider.of<MediaUtils>(context, listen: false).chosenMedia;
    var user = Provider.of<Auth>(context, listen: false).authenticatedUser;

    if (widget.comment == null) {
      Map data = {
        "media": chosenMedia.id,
        "user": user.profile.id,
        "text": "",
        "dccon_url": dccon.img
      };

      commentController.clear();
      if (widget.parentComment != null) {
        data['parent'] = widget.parentComment;
      }
      await _callSubmit(context, data);
      Navigator.of(context).pop();
    }
  }

  void _submit(BuildContext context) async {
    if (_formKey.currentState.validate() && commentController.text != "") {
      Media chosenMedia =
          Provider.of<MediaUtils>(context, listen: false).chosenMedia;
      User user = Provider.of<Auth>(context, listen: false).authenticatedUser;

      Map data = {
        "media": chosenMedia.id,
        "user": user.profile.id,
        "text": commentController.text
      };

      commentController.clear();
      if (widget.parentComment != null) {
        data['parent'] = widget.parentComment;
      }
      _callSubmit(context, data);
    }
  }
        
  void _callSubmit(BuildContext context, Map data) async {
    Media chosenMedia =
        Provider.of<MediaUtils>(context, listen: false).chosenMedia;

    try {
      if (widget.comment == null) {
        final dio = Dio();
        commentController.clear();
        await DioClient(dio)
            .post(ServerSettings.getCreateCommentURL,
                data: data)
            .then((value) {
          Provider.of<CommentsUtil>(context, listen: false)
              .fetchComment(mediaID: chosenMedia.id);
        });
        if (widget.onSuccess != null) {
          widget.onSuccess();
        }
      } else {
        var authToken = Provider.of<Auth>(context, listen: false).token;
        var url = '${ServerSettings.getUpdateCommentURL}${widget.comment.id}/';

        await http.put(Uri.parse(url), body: jsonEncode(data), headers: {
            "Authorization" : "Token $authToken",
            "Content-Type": "application/json"
          }).then((result) {
            Navigator.of(context).pop();
        });
        widget.onSuccess(data["text"]);
      }

    } on Exception catch (e) {
      print(e);
    }
  }
}
