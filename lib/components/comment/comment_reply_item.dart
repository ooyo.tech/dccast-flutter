import 'dart:math';

import 'package:dccast/constants/styles.dart';
import 'package:dccast/screens/video_detail.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../model/comments.dart';
import 'comment_item.dart';
import 'comment_replies.dart';
import 'comment_send.dart';

const _replyLength = 5;

class CommentReplyItemComponent extends StatefulWidget {
  static const String id = 'comment_screen';
  final Comment comment;

  CommentReplyItemComponent({Key key, this.comment})
      : super(key: key);

  @override
  _CommentReplyItemState createState() => _CommentReplyItemState();
}


class _CommentReplyItemState extends State<CommentReplyItemComponent> {

  int _shownLength = _replyLength;

  @override
  Widget build(BuildContext context) {
    return ListView(children: [
      Container(
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey),
          ),
        ),
        child: ListTile(
          contentPadding: EdgeInsets.symmetric(
            horizontal: 0.0),
          title: Row(
            children: [
              IconButton(
                icon: Icon(Icons.arrow_back),
                color: Colors.grey,
                onPressed: () {
                  Navigator.of(context).pop();
                }
              ),
              Text(translate("reply")),
              SizedBox(width: 16.0),
              Text(widget.comment?.replies == null
                ? "0"
                : widget.comment?.replies.length.toString()),
            ],
          ),
          trailing: IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              Navigator.of(context).pop();/*Until(
                ModalRoute.withName(VideoDetailScreen.id));*/
            }
          )
        )
      ),
      CommentSendScreen(
        parentComment: widget.comment.id,
        onSuccess: () {
          Navigator.of(context).pop();
        },
      ),
      // CommentItemComponent(comment: comment, replyShow: false),
      Container(
        padding: EdgeInsets.only(left: 0.0),//50.0
        child: Column(
          children: List.generate(
            min(_shownLength, widget.comment?.replies?.length), (index) {
          return CommentItemComponent(comment: widget.comment?.replies[index]);
          })
        ),
      ),
      if (widget.comment.replies.length > _shownLength)
      Container(
        padding: EdgeInsets.only(left: 66),
        child:
          Row(children: [
          TextButton(
            child: Text(
              translate("view") +
                  " " +
                  (widget.comment.replies.length - _shownLength)
                  .toString() +
                  " " +
                  translate("reply"),
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryColor),
            ),
            onPressed: () {
              setState(() {
              _shownLength += _replyLength;
              });
              // _showCommentReply(context, comment);
            },
          )
        ])
      )
    ]);
  }
}
