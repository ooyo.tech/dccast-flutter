import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../model/comments.dart';
import '../../utils/auth.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';

class CommentDcconScreen extends StatefulWidget {
  final Function sendEmoji;

  CommentDcconScreen({
    Key key,
    this.sendEmoji
  }) : super(key: key);

  @override
  _CommentDcconScreenState createState() => _CommentDcconScreenState();
}

class _CommentDcconScreenState extends State<CommentDcconScreen> {
  final storage = FlutterSecureStorage();
  List<Dccon> dcconTabs;
  Map<String, DcconPage> dcconPages = {};
  String chosenPackageIdx;
  bool isLoading = false;

  void initState() {
    super.initState();
    _getEmojis(context);
  }

  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(
      builder: (BuildContext context, setState) {
        return FractionallySizedBox(
        heightFactor: 0.64,
        child: (isLoading) ? 
          Lottie.asset(Assets.animLoad,
            width: DeviceUtils.getScaledSize(context, 0.3),
            height: DeviceUtils.getScaledSize(context, 0.3))
          : (dcconPages == null || dcconPages.isEmpty) ?
          Center(
            child: Lottie.asset(Assets.animNoData,
            width: DeviceUtils.getScaledSize(context, 0.6),
            height: DeviceUtils.getScaledSize(context, 0.6))
          )
          : Column(
            children: [
              Expanded(
                child: GridView.builder(
                padding: EdgeInsets.all(5.0),
                // physics: NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4,
                    crossAxisSpacing: 5.0,
                    mainAxisSpacing: 5.0),
                itemCount: dcconPages[chosenPackageIdx].dccons.length,
                itemBuilder: (ctx, index) {
                  return GestureDetector(
                    onTap: () {
                      widget.sendEmoji(context,
                        dcconPages[chosenPackageIdx].dccons[index]);
                    },
                    child: GridTile(
                      child: CachedNetworkImage(
                        fit: BoxFit.cover,
                        width: 40.0,
                        height: 40.0,
                        imageUrl: 
                          dcconPages[chosenPackageIdx].dccons[index].img,
                        placeholder:(context, url) => 
                          Image.asset(Assets.loading),
                        errorWidget: (context, url, error) {
                          return Image.asset(Assets.loading);
                        }
                      )
                    )
                  );
                }
              )),
              Container(
                padding: const EdgeInsets.all(5),
                height: 70.0,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black)
                ),
                child: ListView.separated(
                  itemCount: dcconTabs == null ? 0 : dcconTabs.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        setState(() {
                          chosenPackageIdx = dcconTabs[index].package_idx;
                        });
                      },
                      child: CachedNetworkImage(
                        fit: BoxFit.cover,
                        width: 60.0,
                        height: 60.0,
                        imageUrl: dcconTabs[index].img,
                        placeholder:(context, url) => 
                          Image.asset(Assets.loading),
                        errorWidget: (context, url, error) {
                          return Image.asset(Assets.loading);
                        }
                      )
                    );
                  },
                  separatorBuilder: (context, index) => SizedBox(width: 5),
                )
              )
            ]
          )
        );
      }
    );
  }

  Future<void> _getEmojis(BuildContext context) async {
    final dio = Dio();
    var appID = await storage.read(key: Preferences.appID);
    var user = Provider.of<Auth>(context, listen: false)
      .authenticatedUser;
    setState(() {
      isLoading = true;
    });
    try {
      var data = {
        "kind": "dccon",
        "type": "list",
        "app_id": appID,
        "user_id": user.dcinside.userId
      };
      print(data);

      final response = await DioClient(dio)
      .post(ServerSettings.getApiUpdateURL, data: data);
      setState(() {
        isLoading = false;
        if (response["tab"] != null) {
          dcconTabs = List<Dccon>.from(
            response["tab"].map((x) => Dccon.fromJson(x)));
        }
        if (response["list"] != null) {
          dcconPages = {};
          chosenPackageIdx = "";
          for (var i=0; i<response["list"].length; i++) {
            var page = response["list"][i];
            var dcconPage = DcconPage.fromJson(page);
            if (dcconPage.dccons.isNotEmpty) {
              dcconPages[dcconPage.dccons[0].package_idx] = dcconPage;
              if (chosenPackageIdx == "") {
                chosenPackageIdx = dcconPage.dccons[0].package_idx;
              }
            }
          }
        }
      });
    } on Exception catch (error) {
    }
  }
}
