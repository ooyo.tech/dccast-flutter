import 'package:dccast/components/comment/comment_send.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';


class CommentEditScreen extends StatefulWidget {
  static const String id = 'comment_edit_screen';

  /*CommentEditScreen({Key key, this.parentId})
      : super(key: key);*/

  @override
  _CommentEditScreenState createState() => _CommentEditScreenState();
}

class _CommentEditScreenState extends State<CommentEditScreen> {
  int lastPageId = 1;
  bool isLoading = false;

  final storage = FlutterSecureStorage();
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: CommentSendScreen()
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
}
