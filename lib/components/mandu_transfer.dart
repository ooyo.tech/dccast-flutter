import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import '../constants/assets.dart';
import '../constants/preferences.dart';
import '../constants/server_settings.dart';
import '../constants/styles.dart';
import '../model/user_mandu.dart';
import '../screens/mandu_history.dart';
import '../utils/auth.dart';
import '../utils/device.dart';
import '../utils/dio_client.dart';
import '../utils/media.dart';
import '../utils/toast.dart';

class ManduTransferComponent extends StatefulWidget {
  @override
  _ManduTransferComponentState createState() => _ManduTransferComponentState();
}

class _ManduTransferComponentState extends State<ManduTransferComponent> {
  UserMandu _userMandu;
  final storage = FlutterSecureStorage();

  TextEditingController passwordController = TextEditingController();
  final textController = TextEditingController();
  int manduCount = 1;
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    _userMandu = Provider.of<Auth>(context, listen: false).userMandu;
    NumberFormat numberFormat = NumberFormat.decimalPattern();
    textController.text = manduCount.toString() + " 개";
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
      child: isLoading
        ? Center(
            child: Lottie.asset(Assets.animLoad,
                alignment: Alignment.center, width: 160.0, height: 160.0),
          )
        : ListView(
        shrinkWrap: true,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    translate("mandu.number.title"),
                    style: kTextStyle,
                  ),
                  Text(
                    numberFormat.format(double.parse(_userMandu.cause)),
                    style: TextStyle(
                      color: kPrimaryColor,
                      fontSize: 26.0,
                      fontFamily: 'Noto Sans',
                    ),
                  )
                ],
              ),
              Container(
                width: 30.0,
                height: 30.0,
                margin: EdgeInsets.only(bottom: 6.0),
                decoration: BoxDecoration(
                    border: Border.all(color: kGreyColor),
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: AssetImage(Assets.manduIcon),
                        fit: BoxFit.contain)),
              ),
              Expanded(child: Container()),
              FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4.0),
                      side: BorderSide(color: kPrimaryColor)),
                  color: Colors.white,
                  textColor: kPrimaryColor,
                  padding: EdgeInsets.all(8.0),
                  onPressed: () {
                    Navigator.pushNamed(context, ManduHistoryScreen.id);
                  },
                  child: Text(translate("mandu.purchase")))
            ],
          ),
          SizedBox(height: 8.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: DeviceUtils.getScaledWidth(context, 0.1),
                height: DeviceUtils.getScaledWidth(context, 0.1),
                decoration: BoxDecoration(
                    color: kGreyColor,
                    border: Border.all(color: kGreyColor),
                    shape: BoxShape.circle),
                child: IconButton(
                    icon: Icon(Icons.remove),
                    onPressed: () {
                      setState(() {
                        manduCount = manduCount > 1 ? manduCount - 1 : 1;
                        textController.text = manduCount.toString() + " 개";
                      });
                    }),
              ),
              SizedBox(width: DeviceUtils.getScaledWidth(context, 0.05)),
              Container(
                width: DeviceUtils.getScaledWidth(context, 0.15),
                height: DeviceUtils.getScaledWidth(context, 0.15),
                margin: EdgeInsets.only(bottom: 6.0),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(Assets.videoManduIcon),
                        fit: BoxFit.contain)),
              ),
              SizedBox(width: DeviceUtils.getScaledWidth(context, 0.05)),
              Container(
                  width: DeviceUtils.getScaledWidth(context, 0.1),
                  height: DeviceUtils.getScaledWidth(context, 0.1),
                  decoration: BoxDecoration(
                      color: kGreyColor,
                      border: Border.all(color: kGreyColor),
                      shape: BoxShape.circle),
                  child: IconButton(
                      icon: Icon(Icons.add),
                      onPressed: () {
                        setState(() {
                          manduCount = manduCount + 1;
                          textController.text = manduCount.toString() + " 개";
                        });
                      }))
            ],
          ),
          SizedBox(height: 8.0),
          Text(
            translate("mandu.number.title"),
            style: kTextPrimaryStyle,
          ),
          SizedBox(height: 6.0),
          TextFormField(
            enabled: false,
            controller: textController,
            style: TextStyle(color: kPrimaryColor, fontWeight: FontWeight.w500),
            decoration: InputDecoration(
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 8.0, vertical: 0.0),
              disabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: kPrimaryColor),
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
              ),
            ),
          ),
          SizedBox(height: 4.0),
          FlatButton(
            minWidth: DeviceUtils.getScaledWidth(context, 1),
            onPressed: () {
              passwordController.text = "";
              return showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text(translate("password")), 
                    content: Container(
                      child: //[
                        TextField( 
                          autofocus: true,
                          onEditingComplete: _submit,
                          controller: passwordController,
                          obscureText: true,
                          decoration: InputDecoration(
                            hintText: translate("password")), 
                        ),
                        /*FlatButton(
                          child: Text(translate("submit"))
                        )
                      ]*/
                    )
                  );
                }
              );
            },
            child: Text(
              translate("선물하기"),
              style: kSwiperStyle,
            ),
            color: kPrimaryColor,
          )
        ],
      ),
    );
  }

  Future<void> _submit() async {
    Navigator.of(context).pop();
    final dio = Dio();
    var appID = await storage.read(key: Preferences.appID);
    var user = Provider.of<Auth>(context, listen: false).authenticatedUser;
    setState(() {
      isLoading = true;
    });
    try {
      var media = Provider.of<MediaUtils>(context, listen: false).chosenMedia;
      var data = {
        "kind": "cash_gift",
        "present_cash": manduCount,
        "app_id": appID,
        "user_id": user.dcinside.userId,
        "password": passwordController.text,
        "r_no": media.user.userNo
      };

      final response = await DioClient(dio)
      .postWithStatus(ServerSettings.getApiUpdateURL, data: data);
      setState(() {
        isLoading = false;
      });
      var msg = translate("mandu.send.successfully"); 
      if (response.data.length > 0) {
        var respJson = jsonDecode(response.data[0]);
        if (respJson["result"] != "success") {
          msg = respJson["cause"];
        }
      }
      if (response.statusCode == 200) {
        var data = {
          "kind": "mandu_gift",
          "from_user": user.profile.id,
          "to_user": media.user.id,
          "media": media.id,
          "transaction_type": "선물하기",
          "quantity": manduCount
        };
        Provider.of<Auth>(context, listen: false).userMandu.cause = int.parse(Provider.of<Auth>(context, listen: false).userMandu.cause) - manduCount;

        DioClient(dio)
        .post(ServerSettings.getApiUpdateURL, data: data);
        Navigator.of(context).pop();
      }
      showToast(msg);
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }
}
