import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../constants/assets.dart';
import '../constants/styles.dart';

class UserListItem extends StatelessWidget {
  UserListItem(
      {@required this.title,
      this.subtitle,
      this.trailing,
      this.profileImage,
      this.onPressed});

  final String title;
  final String subtitle;
  final Widget trailing;
  final String profileImage;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8.0),
      padding: EdgeInsets.all(4.0),
      child: Row(
        children: [
          GestureDetector(
            onTap: onPressed,
            child: ClipOval(
              child: profileImage != null
                  ? CachedNetworkImage(
                      fit: BoxFit.cover,
                      width: 40.0,
                      height: 40.0,
                      imageUrl: profileImage,
                      placeholder: (context, url) =>
                          Image.asset(Assets.appLogo, fit: BoxFit.cover),
                      errorWidget: (context, url, error) {
                        return Image.asset(
                          Assets.appLogo,
                          fit: BoxFit.contain,
                          width: 40.0,
                          height: 40.0,
                          color: kPrimaryColor,
                        );
                      })
                  : Image.asset(
                      Assets.appLogo,
                      fit: BoxFit.contain,
                      width: 40.0,
                      height: 40.0,
                      color: kPrimaryColor,
                    ),
            ),
          ),
          Expanded(
              child: GestureDetector(
            onTap: onPressed,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: Metrics.singleBase),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    title,
                    style: kNotificationTitle,
                  ),
                  Text(subtitle != null ? subtitle : "",
                  style: kNotificationSubtitle)
                ],
              ),
            ),
          )),
          trailing != null ? trailing : SizedBox()
        ],
      ),
    );
  }
}
