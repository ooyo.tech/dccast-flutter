import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../constants/preferences.dart';
import '../constants/server_settings.dart';
import '../constants/styles.dart';
import '../model/comments.dart';
import '../utils/dio_client.dart';
import '../utils/toast.dart';

/// Dialog for reporting comment or media
class ReportDialog extends StatefulWidget {
  final Comment comment;
  final eReport type;
  final int mediaID;
  final String title;

  const ReportDialog({Key key, @required this.type, this.comment, this.mediaID, this.title})
      : super(key: key);
  @override
  _ReportDialogState createState() => _ReportDialogState();
}

class _ReportDialogState extends State<ReportDialog> {
  String selectedKind;
  List<String> kinds = [
    translate("report_an_ad"),
    translate("report_porno"),
    translate("report_abuse"),
    translate("report_other")
  ];
  final storage = FlutterSecureStorage();
  final textController = TextEditingController();
  @override
  void initState() {
    super.initState();
    selectedKind = kinds[0];
  }

  setSelectedKind(String kind) {
    setState(() {
      selectedKind = kind;
    });
  }

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(Metrics.singleBase),
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(Metrics.halfBase),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 8.0, top: 8.0),
                child: Text(
                  widget.type == eReport.COMMENT
                  ? translate("report")
                  : widget.type == eReport.VOD
                  ? translate("vod_report")
                  : translate("live_report"),
                  style: kTextPrimaryStyle,
                ),
              ),
              Column(
                  children: List.generate(kinds.length, (index) {
                return RadioListTile(
                  contentPadding: const EdgeInsets.all(0),
                  dense: true,
                  value: kinds[index],
                  groupValue: selectedKind,
                  title: Text(kinds[index].toString()),
                  onChanged: (currentKind) {
                    setSelectedKind(currentKind);
                  },
                  selected: selectedKind == kinds[index],
                  activeColor: kPrimaryColor,
                );
              })),
              TextFormField(
                controller: textController,
                keyboardType: TextInputType.multiline,
                maxLines: 4,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                ),
              ),
              SizedBox(height: 22.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: FlatButton(
                      child: Text(translate("cancel"),
                          style: TextStyle(color: Colors.white)),
                      color: Colors.grey,
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  SizedBox(width: 8.0),
                  Expanded(
                    child: FlatButton(
                      color: kPrimaryColor,
                      child: Text(translate("confirm"),
                          style: TextStyle(color: Colors.white)),
                      onPressed: () {
                        _submit();
                      },
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void _submit() async {
    if (textController.text == "") {
      showToast("설명을 입력하세요");
      return;
    }
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);

    try {
      var url = ServerSettings.getDeclarationURL + "?user=$userID&"
        + (widget.type == eReport.COMMENT ? "comment=${widget.comment.id}"
        : "media=${widget.mediaID}");

      var response = await DioClient(dio).get(url);
      if (response["count"] != "0" && response["count"] > 0) {
        showToast(widget.type == eReport.COMMENT
              ? translate("comment_report_already")
              :  widget.type == eReport.VOD
                ? translate("vod_report_already")
                : translate("live_report_already"));
        Navigator.of(context).pop();
        return;
      }

      url = ServerSettings.getReportURL;
      Map<String, dynamic> data = {
        "user": userID,
        "division": widget.type == eReport.COMMENT
          ? "COMMENT"
          : widget.type == eReport.LIVE
          ? "LIVE"
          : "VOD",
        "content": textController.text,
        "comment": widget.type == eReport.COMMENT ? widget.comment.id : "",
        "kinds": selectedKind,
        "media": widget.type == eReport.COMMENT
            ? widget.comment.media
            : widget.mediaID
      };

      Navigator.of(context).pop();
      await DioClient(dio).post(url, data: data).then((value) {
        showToast(widget.type == eReport.COMMENT
              ? translate("report_success")
              : widget.type == eReport.LIVE
              ? translate("live_report_success")
              : translate("vod_report_success"));
      });
    } on Exception catch (error) {
      print(error.toString());
    }
  }
}
