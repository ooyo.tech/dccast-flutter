
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

import '../constants/assets.dart';
import '../constants/preferences.dart';
import '../constants/server_settings.dart';
import '../constants/styles.dart';
import '../model/user_media.dart';
import '../screens/comment.dart';
import '../screens/login.dart';
import '../utils/auth.dart';
import '../utils/device.dart';
import '../utils/dio_client.dart';
import '../utils/media.dart';
import '../utils/toast.dart';
import 'home/home_list.dart';
import 'mandu_transfer.dart';
import 'video/video_info.dart';

enum contentType { LIST, COMMENTS, MANDU, SHARE }

class VideoDetailBottomComponent extends StatefulWidget {

  VideoDetailBottomComponent({
    Key key
  }) : super(key: key);

  @override
  _VideoDetailBottomComponentState createState() =>
      _VideoDetailBottomComponentState();
}

class _VideoDetailBottomComponentState
    extends State<VideoDetailBottomComponent> {
  contentType selectedType = contentType.LIST;
  Media media;
  String favoriteCurrent;
  bool isFavorite = false;
  String subscribeCurrent;
  bool isSubscribe = false;
  final double spacerWidth = 10;
  final storage = FlutterSecureStorage();
  
  @override
  Widget build(BuildContext context) {
    media = Provider.of<MediaUtils>(context, listen: false).chosenMedia;
    return Stack(
      children: [
        SingleChildScrollView(
          child: Column(
            children: [
              VideoInfoComponent(),
              HomeListComponent(eContent.vod, false),
              SizedBox(
                height: 60.0,
              )
            ],
          ),
        ),
        Positioned(
          bottom: 1.0,
          left: 2.0,
          child: _bottomMenu(context),
        )
      ],
    );
  }

  Widget _bottomMenu(BuildContext context) {
    return Container(
      height: 60.0,
      padding: EdgeInsets.symmetric(horizontal: 8.0),
      width: DeviceUtils.getScaledWidth(context, 1.0),
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          FlatButton(
            onPressed: () {
              if (Provider.of<Auth>(context, listen: false).authenticated) {
                _showComment();
              } else {
                Navigator.pushNamed(context, LoginScreen.id);
              }
            },
            color: kGreyColor,
            minWidth: Metrics.doubleBase,
            padding: EdgeInsets.all(12.0),
            shape: CircleBorder(),
            child: Image.asset(Assets.videoCommentIcon),
          ),
          SizedBox(width: spacerWidth),
          FlatButton(
            onPressed: () {
              if (Provider.of<Auth>(context, listen: false).authenticated) {
                _showMandu();
              } else {
                Navigator.pushNamed(context, LoginScreen.id);
              }
            },
            color: kGreyColor,
            minWidth: Metrics.doubleBase,
            padding: EdgeInsets.all(12.0),
            shape: CircleBorder(),
            child: Image.asset(Assets.videoManduIcon),
          ),
          SizedBox(width: spacerWidth),
          FlatButton(
            onPressed: () {
              if (Provider.of<Auth>(context, listen: false).authenticated) {
                _changeFavorite();
              } else {
                Navigator.pushNamed(context, LoginScreen.id);
              }
            },
            color: kGreyColor,
            minWidth: Metrics.doubleBase,
            padding: EdgeInsets.all(12.0),
            shape: CircleBorder(),
            child: Image.asset(
              Assets.heartIcon,
              color: isFavorite == true ? kPrimaryColor : kBlackColor,
            ),
          ),
          SizedBox(width: spacerWidth),
          FlatButton(
            onPressed: () =>
                {Share.share(ServerSettings.getShareURL + media.id.toString())},
            color: kGreyColor,
            minWidth: Metrics.doubleBase,
            padding: EdgeInsets.all(12.0),
            shape: CircleBorder(),
            child: Image.asset(Assets.shareIcon),
          ),
          Expanded(
            child: SizedBox(),
          ),
          FlatButton(
            onPressed: _changeSubscribe,
            color: isSubscribe ? Colors.grey : kRedColor,
            padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 4.0),
            shape: StadiumBorder(),
            child: Text(
                isSubscribe ? translate("subscribed") : translate("subscribe"),
                style: TextStyle(color: Colors.white)),
          ),
        ],
      ),
    );
  }

  void _showComment() async {
    await showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: true,
        context: context,
        builder: (BuildContext bc) {
          return FractionallySizedBox(
            heightFactor: 0.64 ,
            child: CommentScreen(),
          );
        });
  }

  void _showMandu() async {
    await showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: true,
        backgroundColor: Colors.white,
        context: context,
        builder: (BuildContext bc) {
          return FractionallySizedBox(
            heightFactor: 0.64,
            child: ListView(
              physics: NeverScrollableScrollPhysics(),
              children: [
                ListTile(
                  title: Text(translate("mandu.gift.title")),
                  trailing: IconButton(
                      icon: Icon(Icons.close_rounded),
                      onPressed: () {
                        Navigator.pop(context);
                      }),
                ),
                Divider(),
                ManduTransferComponent()
              ],
            ),
          );
        });
  }

  @override
  void initState() {
    _fetchFavoriteStatus();
    _fetchSubscribeStatus();
    super.initState();
  }

  void _fetchFavoriteStatus() async {
    final dio = Dio();
    media = Provider.of<MediaUtils>(context, listen: false).chosenMedia;
    String userID = await storage.read(key: Preferences.user_id);
    try {
      Map<String, dynamic> data = {
        "kind": "avail_favorite",
        "user": userID,
        "media": media.id
      };
      await DioClient(dio)
          .post(ServerSettings.getFavoriteStatusMediaURL, data: data)
          .then((value) => {
                setState(() {
                  isFavorite =
                      value["status"].toString() == "false" ? true : false;
                  favoriteCurrent = value["id"].toString();
                })
              });
    } on Exception catch (error) {
      print(error.toString());
    }
  }

  void _changeFavorite() async {
    final dio = Dio();
    media = Provider.of<MediaUtils>(context, listen: false).chosenMedia;
    String userID = await storage.read(key: Preferences.user_id);
    try {
      if (favoriteCurrent != null && isFavorite) {
        //remove
        await DioClient(dio)
            .delete(
                ServerSettings.getUserFavoriteRemoveURL + favoriteCurrent + "/")
            .then((value) {
          _fetchFavoriteStatus();
          showToast("즐겨찾기가 해제되었습니다");
        });
      } else {
        //create
        Map data = {"user": userID, "media": media.id};
        await DioClient(dio)
            .post(ServerSettings.getFavoriteMediaURL, data: data)
            .then((value) {
          _fetchFavoriteStatus();
          showToast("즐겨찾기에 추가했습니다");
        });
      }
    } on Exception catch (error) {
      print(error);
    }
  }

  void _fetchSubscribeStatus() async {
    final dio = Dio();
    media = Provider.of<MediaUtils>(context, listen: false).chosenMedia;
    String userID = await storage.read(key: Preferences.user_id);
    try {
      Map<String, dynamic> data = {
        "kind": "avail_subscribe",
        "from_user": userID,
        "to_user": media.user.id
      };
      await DioClient(dio)
          .post(ServerSettings.getSubscribeStatusURL, data: data)
          .then((value) => {
                print("status:" + value.toString()),
                setState(() {
                  isSubscribe =
                      value["status"].toString() == "false" ? true : false;
                  subscribeCurrent = value["id"].toString();
                })
              });
    } on Exception catch (error) {
      print(error.toString());
    }
  }

  void _changeSubscribe() async {
    final dio = Dio();
    media = Provider.of<MediaUtils>(context, listen: false).chosenMedia;
    String userID = await storage.read(key: Preferences.user_id);
    try {
      if (subscribeCurrent != null && isSubscribe) {
        //remove
        await DioClient(dio)
            .delete(ServerSettings.getDeleteSubURL + subscribeCurrent + "/")
            .then((value) {
          print("delete:" + value.toString());
          _fetchSubscribeStatus();
          showToast("구독이 취소되었습니다");
        });
      } else {
        //create
        Map data = {"from_user": userID, "to_user": media.user.id};
        print("from" + userID + "to:" + media.user.id.toString());
        await DioClient(dio)
            .post(ServerSettings.getCreateSubscribeMediaURL, data: data)
            .then((value) {
          setState(() {
            subscribeCurrent = value["id"].toString();
          });
          _fetchSubscribeStatus();
          showToast("구독이 완료되었습니다");
        });
      }
    } on Exception catch (error) {
      print("sub create:" + error.toString());
    }
  }
}
