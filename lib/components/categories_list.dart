import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import '../constants/assets.dart';
import '../constants/styles.dart';
import '../model/category.dart';
import '../screens/category.dart';
import '../utils/category.dart';

class CategoryListComponent extends StatefulWidget {
  final eContent content;

  const CategoryListComponent({Key key, this.content}) : super(key: key);
  @override
  _CategoryListComponentState createState() => _CategoryListComponentState();
}

class _CategoryListComponentState extends State<CategoryListComponent> {
  bool isLoading = false;
  Category categories;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40.0,
      color: Colors.white,
      child: Consumer<CategoryUtils>(builder: (context, category, child) {
        categories = category.availableCategories;
        return isLoading
            ? Center(
                child: Lottie.asset(Assets.animLoad,
                    alignment: Alignment.center, width: 40.0, height: 40.0),
              )
            : ListView.builder(
                itemCount: categories == null ? 0 : categories.results.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CategoryScreen(
                              content: widget.content, selectedTab: index),
                        ),
                      );
                    },
                    child: Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(horizontal: 16.0),
                      margin: EdgeInsets.symmetric(vertical: 4.0),
                      color: Colors.white,
                      child: Text(
                        categories.results[index].name,
                        style: kCategoryTitle,
                      ),
                    ),
                  );
                });
      }),
    );
  }

  void _fetchData() {
    if (Provider.of<CategoryUtils>(context, listen: false)
      .availableCategories != null) {
      return;
    }
    Provider.of<CategoryUtils>(context, listen: false).fetchCategories(
        success: () {
      setState(() {
        isLoading = false;
      });
    }, error: (String error) {
      Scaffold.of(context).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          margin: EdgeInsets.only(bottom: 60.0, left: 10.0, right: 10.0),
          content: Text(error),
        ),
      );
    });
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    _fetchData();
    super.initState();
  }

  void dispose() {
    super.dispose();
  }
}
