import 'package:flutter/material.dart';

import '../constants/assets.dart';
import '../constants/styles.dart';

class NotificationItem extends StatelessWidget {
  NotificationItem(
      {@required this.title,
      this.subtitle,
      this.date,
      Function this.onPressed});

  final String title;
  final String subtitle;
  final String date;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 8.0),
        child: Row(
          children: [
            Stack(children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 4.0),
                child: CircleAvatar(
                  child: Image.asset(
                    Assets.appLogo,
                    width: 20.0,
                  ),
                ),
              ),
              Positioned(
                bottom: 0.0,
                right: 0.0,
                child: Image.asset(
                  Assets.notifCheck,
                  width: 15.0,
                  height: 15.0,
                ),
              )
            ]),
            Expanded(
                child: Container(
              padding: EdgeInsets.symmetric(horizontal: Metrics.singleBase),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    title,
                    style: kNotificationTitle,
                  ),
                  Text(subtitle, style: kNotificationSubtitle),
                  Text(date, style: kNotificationSubtitle)
                ],
              ),
            )),
          ],
        ),
      ),
    );
  }
}
