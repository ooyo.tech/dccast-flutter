import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../constants/assets.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/user.dart';
import '../../model/user_stats.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';

class ProfileHeader extends StatefulWidget {
  final Profile userProfile;

  const ProfileHeader({Key key, this.userProfile}) : super(key: key);

  @override
  _ProfileHeaderState createState() => _ProfileHeaderState();
}

class _ProfileHeaderState extends State<ProfileHeader> {
  UserStats _stats;
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
        height: DeviceUtils.getScaledHeight(context, 0.15),
        child: Row(
          children: [
            Container(
                margin: EdgeInsets.symmetric(horizontal: 8.0),
                child: CachedNetworkImage(
                    fit: BoxFit.cover,
                    width: DeviceUtils.getScaledWidth(context, 0.2),
                    height: DeviceUtils.getScaledWidth(context, 0.2),
                    imageUrl: widget.userProfile.profileImage,
                    placeholder: (context, url) =>
                        Image.asset(Assets.videoThumbnail),
                    errorWidget: (context, url, error) {
                      return Image.asset(Assets.videoThumbnail);
                    })),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(widget.userProfile.nickName,
                        textAlign: TextAlign.left),
                    Text(
                      widget.userProfile.stateMessage ?? "",
                      style: kNotificationSubtitle,
                      maxLines: 2,
                    ),
                    SizedBox(height: 4.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _statusItem(
                            _stats != null
                                ? _stats.subscribers.toString()
                                : "0",
                            translate("subscribe")),
                        _statusItem(
                            _stats != null ? _stats.following.toString() : "0",
                            translate("following")),
                        _statusItem(
                            _stats != null ? _stats.followers.toString() : "0",
                            translate("followers")),
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      Divider(
        color: kBlack50Color,
      )
    ]);
  }

  Widget _statusItem(String no, String text) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 4.0),
      child: Column(
        children: [
          Text(no,
              textAlign: TextAlign.center,
              style: TextStyle(fontFamily: "Gothic A1")),
          Text(text, textAlign: TextAlign.center, style: kNotificationSubtitle),
        ],
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchUserStatus(widget.userProfile.id.toString());
  }

  Future<void> fetchUserStatus(String userID) async {
    final dio = Dio();

    try {
      var _statsURL = ServerSettings.getStatCountURL + userID;
      final _statsResponse = await DioClient(dio).get(_statsURL);

      print(_statsResponse.toString());
      setState(() {
        if (_stats == null) {
          _stats = UserStats.fromJson(_statsResponse);
        }
      });
    } on Exception catch (error) {}
  }
}
