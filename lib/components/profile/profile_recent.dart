import 'package:dccast/utils/pagewise.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pagewise/flutter_pagewise.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

import '../../constants/assets.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/user.dart';
import '../../model/user_recent.dart';
import '../../utils/auth.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../video/videolistitem.dart';

class ProfileRecent extends StatefulWidget {
  final String searchText;
  final Profile userProfile;
  final String category;

  const ProfileRecent({Key key, this.searchText, this.userProfile, this.category})
      : super(key: key);

  @override
  _ProfileRecentState createState() => _ProfileRecentState();
}

class _ProfileRecentState extends State<ProfileRecent> {
  bool isLoading = false;
  User user;
  UserRecent userRecent;
  int lastPageId = 1;
  final storage = FlutterSecureStorage();
  EditablePagewiseLoadController _pageLoadController;

  @override
  Widget build(BuildContext context) {
    return _buildRecentListView();
  }

  Widget _buildRecentListView() {
    // filterSearchResults();
    final int videoCount =
        userRecent?.results != null ? userRecent?.results?.length : 0;
    return ModalProgressHUD(
      progressIndicator: Lottie.asset(Assets.animLoad,
          width: DeviceUtils.getScaledSize(context, 0.3),
          height: DeviceUtils.getScaledSize(context, 0.3)),
      inAsyncCall: isLoading,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
            child: Text("Videos (" + userRecent?.count.toString() + ")",
                style: kTextGreyStyle),
          ),
          Expanded(
            child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: RefreshIndicator (
                onRefresh: () {
                  setState(() {
                    _pageLoadController.reset();
                  });
                  return Future.any(null);
                },
                child: PagewiseListView(
                  pageLoadController: _pageLoadController,
                  itemBuilder: (context, entry, index) {
                    return _buildImageColumn(entry, index);
                  },
                  noItemsFoundBuilder: (context) {
                    return Center(
                      child: Lottie.asset(Assets.animNoData,
                        alignment: Alignment.center,
                        width: DeviceUtils.getScaledSize(context, 0.5)),
                    );
                  },
                  loadingBuilder: (context) => Center(
                    child: Lottie.asset(Assets.animLoad,
                      width: DeviceUtils.getScaledSize(context, 0.3),
                      height: DeviceUtils.getScaledSize(context, 0.3)),
                  ),
                  showRetry: false,
                  errorBuilder: (context, error) {
                    return Text('Error: $error');
                  }
                )
              )
            )
          ),
        ],
      ),
    );
  }

  Widget _buildListView() {
    return ListView.builder(
      padding: const EdgeInsets.all(10),
      itemCount: userRecent == null ? 0 : userRecent.results.length,
      itemBuilder: (context, index) {
        return _buildImageColumn(userRecent.results[index], index);
      },
    );
    // return Consumer<UserRecent>(builder: (context, userRecent, child) {
    // });
  }

  Widget _buildImageColumn(Result item, int index) => Container(
        decoration: BoxDecoration(color: Colors.white),
        margin: const EdgeInsets.only(bottom: 8.0),
        child: Column(
          children: [
            item != null && item?.media != null
                ? VideoListItem(
                    id: item?.id,
                    title: item?.media?.title,
                    subtitle: item?.media?.explanation,
                    kinds: item?.media?.kinds,
                    dates: item?.media?.created,
                    crown: item?.media?.isHit,
                    duration: secToMinConverter(item?.media?.duration ?? 0),
                    views: item?.media?.views,
                    thumnail: item?.media?.mediaThumbnail,
                    menu: "recent",
                    content: widget.category == "LIVE" ?
                      eContent.live : eContent.vod,
                    onDelete: () {
                      setState(() {
                        _pageLoadController.removeAt(index);
                        userRecent.results.removeAt(index);
                        userRecent.count = userRecent.count - 1;
                      });
                    }
                  )
                : SizedBox()
          ],
        ),
      );

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    super.initState();
    _pageLoadController = EditablePagewiseLoadController(
      pageSize: 20,
      pageFuture: (pi) async {
        var result = await getJSONData(pi);
        setState(() { });
        return result;
      }
    );
  }

  void filterSearchResults() {
    List<Result> dummySearchList = List<Result>();
    dummySearchList.addAll(userRecent.results);
    print("filter search called not null:" + widget.searchText);

    List<Result> dummyListData = List<Result>();
    dummySearchList.forEach((item) {
      if (item.media != null &&
          item.media.title != null &&
          item.media.title.contains(widget.searchText)) {
        dummyListData.add(item);
      }
    });

    //
    setState(() {
      userRecent.results.clear();
      userRecent.results.addAll(dummyListData);
      isLoading = false;
    });

    return;
  }

  Future<List<Result>> getJSONData(int pageID) async {
    final dio = Dio();
    Profile loggedInUser =
        Provider.of<Auth>(context, listen: false).userProfile;
    try {
      String userID = widget.userProfile == null
          ? loggedInUser.id.toString()
          : widget.userProfile.id.toString();
      var url = ServerSettings.getUserRecentMediaURL +
          userID +
          "&ordering=-created&limit=20&page=${(pageID+1)}";
      if (widget.category != null) {
        url += "&media__category=" + widget.category;
      }

      final response = await DioClient(dio).get(url);
      var jsonResponse = UserRecent.fromJson(response);

      UserRecent newItems = jsonResponse;
      if (userRecent == null) {
        userRecent = newItems;
      } else {
        userRecent.results.addAll(newItems.results);
      }
      lastPageId = lastPageId + 1;
      return newItems.results;
    } on Exception catch (error) {
      print(error);
    }
    return [];
  }

  void dispose() {
    _pageLoadController?.dispose();
    super.dispose();
  }
}
