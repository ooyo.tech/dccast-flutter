import 'package:dccast/screens/vod/edit_vod.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

import '../../constants/assets.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/user.dart';
import '../../model/user_media.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../video/videolistitem.dart';

class ProfileMedia extends StatefulWidget {
  final String searchText;
  final Profile userProfile;
  String ordering;

  ProfileMedia({Key key, this.searchText,
    this.userProfile, this.ordering}) : super(key: key);

  @override
  ProfileMediaState createState() => ProfileMediaState();
}

class ProfileMediaState extends State<ProfileMedia> {
  bool isLoading = false;
  User user;
  UserMedia userMedias;
  int lastPageId = 1;
  int _totalPages = 2;
  String _ordering;
  String _searchText;
  final storage = FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    return _buildMediaListView();
  }

  Widget _buildMediaListView() {
    final int videoCount =
        userMedias?.results != null ? userMedias?.results?.length : 0;
    return ModalProgressHUD(
      progressIndicator: Lottie.asset(Assets.animLoad,
          width: DeviceUtils.getScaledSize(context, 0.3),
          height: DeviceUtils.getScaledSize(context, 0.3)),
      inAsyncCall: isLoading,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
            child: Text("Videos " + 
              (userMedias != null ? "(${userMedias?.count})" : ""),
                style: kTextGreyStyle),
          ),
          Expanded(
            child: NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (!isLoading &&
                    scrollInfo.metrics.pixels ==
                        scrollInfo.metrics.maxScrollExtent) {
                  this.getJSONData();
                }
              },
              child: isLoading ? Container() :
                  (userMedias == null || userMedias?.results == null
                  ? Center(
                      child: Lottie.asset(Assets.animNoData,
                          alignment: Alignment.center,
                          width: DeviceUtils.getScaledSize(context, 0.5)),
                    )
                  : _buildListView()),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListView() {
    return ListView.builder(
      padding: const EdgeInsets.all(10),
      itemCount: userMedias == null ? 0 : userMedias.results.length,
      itemBuilder: (context, index) {
        return _buildImageColumn(userMedias.results[index], index);
      },
    );
  }

  Widget _buildImageColumn(Media item, index) => Container(
        decoration: BoxDecoration(color: Colors.white),
        margin: const EdgeInsets.only(bottom: 8.0),
        child: Column(
          children: <Widget>[
            item?.id != null
                ? VideoListItem(
                    id: item?.id,
                    title: item?.title,
                    subtitle: item?.explanation,
                    kinds: item?.kinds,
                    dates: item?.created,
                    crown: item?.isHit,
                    media: item,
                    duration: secToMinConverter(item?.duration ?? 0),
                    views: item?.views,
                    thumnail: item?.mediaThumbnail,
                    favorite: item?.favorite,
                    menu: "myContent",
                    onEdit: () async {
                      print(item.id);
                      var result = await Navigator.push(context,
                      MaterialPageRoute(builder: (context) =>
                        EditVodScreen(media: item)));
                      Navigator.of(context).pop();
                      if (result == "refresh") {
                        userMedias = null;
                        lastPageId = 1;
                        _totalPages = 2;
                        getJSONData();
                      }
                    },
                    onDelete: () {
                      setState(() {
                        userMedias.results.removeAt(index);
                        userMedias.count = userMedias.count - 1;
                      });
                    },
                  )
                : SizedBox()
          ],
        ),
      );

  @override
  void initState() {
    // call get json data function
    // selectedPage = Selec
    _ordering = widget.ordering;
    lastPageId = 1;
    _totalPages = 2;
    this.getJSONData();
    super.initState();
  }

  void setOrdering(var ordering) {
    _ordering = ordering;
    userMedias = null;
    lastPageId = 1;
    _totalPages = 2;
    getJSONData();
  }

  void setSearchText(var searchText) {
    _searchText = searchText;
    userMedias = null;
    lastPageId = 1;
    _totalPages = 2;
    getJSONData();
  }

  Future<String> getJSONData() async {
    if (lastPageId > _totalPages) {
      return "success";
    }
    setState(() {
      isLoading = true;
    });
    final dio = Dio();
    try {
      var url = ServerSettings.getUserMediaURL +
          widget.userProfile.id.toString() +
          "&ordering=" + _ordering + 
          "&limit=20&page=" +
          lastPageId.toString();
      if (_searchText != null) {
        url += "&search=$_searchText";
      }
      print(url);

      final response = await DioClient(dio).get(url);
      var jsonResponse = UserMedia.fromJson(response);
      _totalPages = jsonResponse.totalPages;

      setState(() {
        UserMedia newItems = jsonResponse;

        if (userMedias == null) {
          userMedias = newItems;
        } else {
          userMedias.results.addAll(newItems.results);
        }
        lastPageId = lastPageId + 1;
        isLoading = false;
      });
      return "sucessful";
    } on DioError catch (error) {
      print("error: ${error.response}");
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
  }
}
