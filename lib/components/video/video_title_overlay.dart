import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../constants/styles.dart';
import '../../utils/overlay_handler.dart';

class VideoTitleOverlayWidget extends StatefulWidget {
  final Function onClear;
  final Widget widget;

  VideoTitleOverlayWidget({this.onClear, this.widget}): assert(widget != null);

  @override
  _VideoTitleOverlayWidgetState createState() => _VideoTitleOverlayWidgetState();
}

class _VideoTitleOverlayWidgetState extends State<VideoTitleOverlayWidget> {
  double width;
  double oldWidth;
  double oldHeight;
  double height;

  bool isInPipMode = false;


  Offset offset = Offset(0, 0);

  Widget player;

  _onExitPipMode() {
    Future.microtask(() {
      setState(() {
        isInPipMode = false;
        width = oldWidth;
        height = oldHeight;
        offset = Offset(0, 0);
      });
    });
    Future.delayed(Duration(milliseconds: 250), () {
      Provider.of<OverlayHandlerProvider>(context, listen: false).disablePip();
    });
  }

  _onPipMode() {
    double aspectRatio = Provider.of<OverlayHandlerProvider>(context, listen: false).aspectRatio;

    print("true   $aspectRatio");
//    Provider.of<OverlayHandlerProvider>(context, listen: false).enablePip();
    Future.delayed(Duration(milliseconds: 100), () {
      print("true   Future.microtask");

      setState(() {
        isInPipMode = true;
        width = oldWidth;
        height = 80;
        print(oldHeight - height - 16);
        offset = Offset(16, oldHeight - height - 0);
//        height = (Constants.VIDEO_HEIGHT_PIP/aspectRatio) + 33;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if(width == null || height == null) {
      oldWidth = width = MediaQuery.of(context).size.width;
      oldHeight = height = MediaQuery.of(context).size.height;
    }
    return Consumer<OverlayHandlerProvider>(
      builder: (context, overlayProvider, _) {
        if(overlayProvider.inPipMode != isInPipMode) {
          isInPipMode = overlayProvider.inPipMode;
          if (isInPipMode)
            _onPipMode();
          else
            _onExitPipMode();
        }
        return AnimatedPositioned(
          duration: Duration(milliseconds: 400),
          // left: offset.dx,
          top: offset.dy,
          child: Material(
            elevation: isInPipMode ? 5.0 : 0.0,
            child: AnimatedContainer(
              height: height,
              width: width,
              child: widget.widget,
              duration: Duration(milliseconds: 250),
            ),
          ),
        );
      }
    );
  }
}
