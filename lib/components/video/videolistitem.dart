import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/user.dart';
import '../../model/user_media.dart';
import '../../screens/profile.dart';
import '../../screens/video_detail.dart';
import '../../utils/auth.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../../utils/media.dart';
import '../../utils/toast.dart';

class VideoListItem extends StatefulWidget {
  VideoListItem(
      {@required this.id,
      @required this.title,
      @required this.subtitle,
      @required this.thumnail,
      this.views,
      this.duration,
      this.dates,
      this.crown,
      this.kinds,
      @required this.content = eContent.vod,
      @required this.media,
      this.favorite,
      this.onPressed,
      this.onEdit,
      this.onDelete,
      this.menu});

  final int id;
  final String title;
  final String subtitle;
  final DateTime dates;
  final String thumnail;
  final int views;
  final String duration;
  final bool crown;
  final String kinds;
  final Media media;
  final eContent content;
  final Function onPressed;
  final Function onDelete;
  final Function onEdit;
  final String menu;
  int favorite;

  @override
  VideoListItemState createState() => VideoListItemState();
}

class VideoListItemState extends State<VideoListItem> {
  final storage = FlutterSecureStorage();
  final TextEditingController _pinPutController = TextEditingController();
  final FocusNode _pinPutFocusNode = FocusNode();
  Profile profile;


  @override
  Widget build(BuildContext context) {
    final now = DateTime.now();
    final today = DateTime(now.year, now.month, now.day);
    final thisyear = DateTime(now.year);
    var datetime = "";
    if (widget.dates != null) {
      datetime = " | ";
      if (widget.dates.isAfter(today)) {
        datetime += DateFormat("HH:mm").format(widget.dates);
      } else if (widget.dates.isBefore(thisyear)) {
        datetime += DateFormat("yy.MM.dd").format(widget.dates);
      } else {
        datetime += DateFormat("MM.dd").format(widget.dates);
      }
    }
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: () {
            if (widget.media.category == "LIVE" && !widget.media.alive) {
              showToast(translate("live.ended"));
              return;
            }
            if (widget.media.category == "LIVE" &&
              widget.media.liveSetpass == true) {
              _askPinCode(widget.media.livePassword);
            } if (widget.media.kinds == translate("vod.upload.share.under19")
              && (profile == null || !profile.adultCertification)) {
              showToast(translate("alert.under19"));
            } else if (widget.onPressed == null) {
              Provider.of<MediaUtils>(context, listen: false)
                  .mediaDetail(widget.media, widget.content);
              Future.delayed(Duration(milliseconds: 250), () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                        VideoDetailScreen(content: widget.content),
                    ));
              });
            } else {
              widget.onPressed();
            }
          },
          child: Stack(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: 
                widget.thumnail != null
                  ? CachedNetworkImage(
                      fit: BoxFit.cover,
                      width: DeviceUtils.getScaledWidth(context, 0.45),
                      height: DeviceUtils.getScaledWidth(context, 0.25),
                      imageUrl: widget.thumnail,
                      placeholder: (context, url) =>
                          Image.asset(Assets.videoThumbnail),
                      errorWidget: (context, url, error) {
                        return Image.asset(Assets.videoThumbnail);
                      })
                  : Image.asset(
                      Assets.videoThumbnail,
                      fit: BoxFit.cover,
                      width: DeviceUtils.getScaledWidth(context, 0.45),
                      height: DeviceUtils.getScaledWidth(context, 0.25),
                    )
              ),
              widget.content == eContent.live
                  ? Positioned(
                      bottom: Metrics.singleBase,
                      right: Metrics.singleBase,
                      child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 8.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(3.0),
                            color: kBlack50Color,
                          ),
                          child: Row(
                            children: [
                              Image.asset(
                                Assets.profileImage,
                                width: 10.0,
                                height: 10.0,
                                color: kGreyColor,
                              ),
                              SizedBox(
                                width: 4.0,
                              ),
                              Text(
                                widget.views.toString(),
                                style: kDurationText,
                              )
                            ],
                          )),
                    )
                  : Positioned(
                      bottom: Metrics.singleBase,
                      right: Metrics.singleBase,
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 8.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(3.0),
                          color: kBlack50Color,
                        ),
                        child: Text(
                          widget.duration.isNotEmpty ? widget.duration : "00:00",
                          style: kDurationText,
                        ),
                      ),
                    ),
              Positioned(
                top: Metrics.singleBase,
                right: Metrics.singleBase,
                child: Row(
                  children: [
                    if (widget.media?.kinds ==
                      translate("vod.upload.share.under19"))
                    Container(
                      padding: EdgeInsets.all(4.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        color: kRedColor,
                      ),
                      child: Text("19+", style: TextStyle(
                        color: kWhiteColor,
                        fontSize: 10
                      ))
                    ),
                    if (widget.media?.liveSetpass ?? false)
                    SizedBox(width: 4.0),
                    if (widget.media?.liveSetpass ?? false)
                    Image.asset(
                      Assets.lockImage,
                      width: 24.0,
                      height: 24.0,
                    ),
                    if (widget.crown)
                    SizedBox(width: 4.0),
                    if (widget.media?.isHit ?? false && widget.media?.isHitActive ?? false)
                    Container(
                      padding: EdgeInsets.all(Metrics.halfBase),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(3.0),
                        color: kPrimaryColor,
                      ),
                      child: Image.asset(
                        Assets.crownImage,
                        width: 12.0,
                        height: 12.0,
                      ),
                    )
                  ]
                )
              ),
            ]
          )
        ),
        Expanded(
          child: GestureDetector(
            onTap: () {
              if (widget.media.category == "LIVE" &&
                widget.media.liveSetpass == true) {
                _askPinCode(widget.media.livePassword);
              } if (widget.media.kinds == translate("vod.upload.share.under19")
                && (profile == null || !profile.adultCertification)) {
                showToast(translate("alert.under19"));
              } else {
                widget.onPressed();
              }
            },
            child: Container(
              padding: EdgeInsets.symmetric(
                  horizontal: Metrics.doubleBase, vertical: Metrics.singleBase),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  widget.title != null
                      ? Text(
                          widget.title,
                          style: kTextStyle,
                          maxLines: 2,
                        )
                      : SizedBox(height: 0.0),
                  widget.content == eContent.vod && widget.subtitle != null
                      ? Text(
                          widget.subtitle.length >= 12
                          ? widget.subtitle.substring(0, 12)
                          : widget.subtitle,
                          style: kNotificationSubtitle,
                          maxLines: 2,
                        )
                      : Text(
                          widget.media?.mediaCategory?.name != null
                              ? widget.media?.mediaCategory?.name
                              : "",
                          style: kNotificationSubtitle,
                        ),
                  Text(translate("views") + 
                    (widget.media?.views != null
                    ? widget.media?.views?.toString()
                    : widget.views.toString())
                     + datetime,
                    style: TextStyle(
                      fontFamily: 'Noto Sans',
                      color: kBlack50Color,
                      fontWeight: FontWeight.bold,
                      fontSize: 12.0,
                    )
                  ),
                  /*widget.kinds != null
                      ? Text(widget.kinds, style: kNotificationSubtitle)
                      : SizedBox(),*/
                ],
              ),
            ),
          ),
        ),
        IconButton(
          padding: EdgeInsets.symmetric(vertical: Metrics.singleBase),
          alignment: Alignment.topCenter,
          iconSize: 20.0,
          icon: FaIcon(
            FontAwesomeIcons.ellipsisV,
            color: kBlack50Color,
            size: 20.0,
          ),
          onPressed: () {
            _settingModalBottomSheet(context);
          },
        )
      ],
    );
  }


  BoxDecoration get _pinPutDecoration {
    return BoxDecoration(
      border: Border.all(color: kBlackColor),
      borderRadius: BorderRadius.circular(10.0),
    );
  }

  String errorMsg = "";
  void _askPinCode(livePassword) {
    _pinPutController.text = "";
    errorMsg = "";
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      builder: (BuildContext bc) {
        return FractionallySizedBox(
          heightFactor: 0.5,
          child: Wrap(
            children: <Widget>[
              ListTile(
                trailing: Icon(
                  Icons.close_rounded,
                  color: kBlackColor,
                ),
                onTap: () => {Navigator.of(context).pop()},
              ),
              Divider(
                color: kBlack50Color,
                height: 1.0,
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3.0),
                  border: Border.all(color: Colors.white),
                ),
                padding: const EdgeInsets.all(20.0),
                child: PinPut(
                  fieldsCount: 4,
                  autofocus: true,
                  preFilledWidget: Text("*"),
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  textStyle:
                      const TextStyle(fontSize: 25.0, color: kPrimaryColor),
                  eachFieldWidth: 50.0,
                  eachFieldHeight: 50.0,
                  onSubmit: (var pin) async {
                    pin = _pinPutController.text;
                    if (pin == livePassword) {
                      Navigator.pop(context);
                      widget.onPressed();
                    } else {
                      setState(() {
                        _pinPutController.text = "";
                        _pinPutFocusNode.requestFocus();
                        // errorMsg = "패스 코드 시도 실패";
                      });
                      showToast(translate("패스 코드 시도 실패"));
                    }
                  },
                  focusNode: _pinPutFocusNode,
                  controller: _pinPutController,
                  submittedFieldDecoration: _pinPutDecoration,
                  selectedFieldDecoration: _pinPutDecoration.copyWith(
                    color: Colors.white,
                    border: Border.all(
                      width: 2,
                      color: kPrimaryColor,
                    ),
                  ),
                  followingFieldDecoration: _pinPutDecoration,
                ),
              ),
              Center(
                child: Text(errorMsg,
                  style: TextStyle(color: Colors.red))
              )
            ],
          ),
        );
      });
  }

  @override
  void initState() {
    profile = Provider.of<Auth>(context, listen: false).userProfile;
    super.initState();
  }

  Future<void> _createFavorite(setModalState) async {
    final dio = Dio();
    var userID = await storage.read(key: Preferences.user_id);
    try {
      Map data = {"user": userID, "media": widget.id};
      await DioClient(dio)
          .post(ServerSettings.getFavoriteMediaURL, data: data)
          .then((value) {
            print(value);
        setModalState(() {
          widget.favorite = value['id'];
          print(widget.favorite);
        });
      });
    } on Exception catch (error) {
      print(error.toString());
    }
  }

  Future<void> _removeFavorite(setModalState) async {
    final dio = Dio();
    try {
      var url = "${ServerSettings.getUserFavoriteRemoveURL}${widget.favorite}/";

      await DioClient(dio).delete(url);
      setModalState(() {
        widget.favorite = null;
        print(widget.favorite);
      });
    } on Exception catch (error) {
      print(error.toString());
    }
  }

  Future<void> _deleteMedia(BuildContext context) async {
    final dio = Dio();
    try {
      var url = ServerSettings.createVODMediaURL + widget.id.toString() + "/";

      await DioClient(dio).delete(url).then((value) {
        Provider.of<MediaUtils>(context, listen: false)
            .removeMedia(widget.media, widget.content);
      });
      if (widget.onDelete != null) {
        widget.onDelete();
      }
      showToast(translate("media.more.vod.deleted"));
      Navigator.of(context).pop();
      Navigator.of(context).pop();
              
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
    }
  }

  Future<void> _deleteRecent(BuildContext context) async {
    final dio = Dio();
    try {
      var url = ServerSettings.getDeleteRecentMediaURL + widget.id.toString() + "/";

      await DioClient(dio).delete(url).then((value) {
        Provider.of<MediaUtils>(context, listen: false)
            .removeMedia(widget.media, widget.content);
      });
      widget.onDelete();
      showToast(translate("media.more.delete.success"));
      Navigator.of(context).pop();
      Navigator.of(context).pop();
              
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
    }
  }

  void _settingModalBottomSheet(context) async {
    String userID = await storage.read(key: Preferences.user_id);
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return StatefulBuilder(
        builder: (BuildContext context, StateSetter setModalState) {
          return Container(
            child: Wrap(
              children: <Widget>[
                if (widget.menu != "myContent"
                  && widget.menu != "recent")
                ListTile(
                  leading: Icon(Icons.tv_outlined),
                  title: Text(translate("media.more.goto")),
                  onTap: () =>
                  Future.delayed(Duration(milliseconds: 250), () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                          ProfileScreen(userProfile: widget.media?.user),
                        ));
                  })
                ),
                if (widget.menu == "myContent" && widget.onEdit != null
                 && widget.media?.user != null &&
                        userID != null &&
                        widget.media?.user?.id == int.parse(userID))
                ListTile(
                    leading: Icon(Icons.edit),
                  title: Text(translate("edit")),
                  onTap: widget.onEdit),
                ListTile(
                  leading: FaIcon(FontAwesomeIcons.share),
                  title: Text(translate("media.more.share")),
                  onTap: () => {
                    Share.share(
                        ServerSettings.getShareURL + widget.media.id.toString())
                  },
                ),
                ListTile(
                  leading: FaIcon(FontAwesomeIcons.solidHeart,
                    color: widget.favorite != null ? Colors.red:null),
                  title: Text(translate('media.more.fav')),
                  onTap: widget.favorite != null 
                    ? () {_removeFavorite(setModalState);}
                    : () {_createFavorite(setModalState);},
                ),
                if (widget.menu == "recent")
                ListTile(
                  leading: FaIcon(FontAwesomeIcons.trash),
                  title: Text(translate("delete")),
                  onTap: () {
                    _askDelete(context);
                  },
                ),
                if (widget.media?.user != null &&
                        userID != null &&
                        widget.media?.user?.id == int.parse(userID))
                ListTile(
                  leading: FaIcon(FontAwesomeIcons.trash),
                  title: Text(translate("delete")),
                  onTap: () {
                    _askDelete(context);
                  },
                )
              ],
            ),
          );
        });
      });
  }

  Future<void> _askDelete(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(widget.menu == "recent" ?
            (widget.content == eContent.live
            ? translate("media.more.delete.message.live")
            : translate("media.more.delete.message.vod"))
            :
            translate("media.more.vod.delete")),
          /*content: SingleChildScrollView(
            child: Text(message),
          ),*/
          actions: <Widget>[
            TextButton(
              child: Text(translate("group.delete.cancel")),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(translate("alert.button.ok")),
              onPressed: () {
                if (widget.menu == "recent") {
                  _deleteRecent(context);
                } else {
                  _deleteMedia(context);
                }
              },
            ),
          ],
        );
      },
    );
  }
}
