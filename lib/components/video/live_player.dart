import 'package:better_player/better_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../constants/common.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/user_media.dart';
import '../../screens/video_detail.dart';
import '../report_dialog.dart';

class LivePlayer extends StatefulWidget {
  final eContent content;
  final Media media;
  VideoDetailScreenState detailState;

  LivePlayer({
    Key key,
    this.content,
    this.media,
    this.detailState
  }) : super(key: key);

  @override
  _LivePlayerState createState() =>
      _LivePlayerState();

  dispose() => _LivePlayerState().dispose();

  pause() => _LivePlayerState().pause();

}

class _LivePlayerState extends State<LivePlayer> {

  Media media;
  String mediaPrefix;
  double aspectRatio = 16 / 9;
  bool _hideControls = false;
  String resAuto;
  String res1080p;
  String res720p;
  String res480p;
  String res240p;
  int activeResolution = 0;


  BetterPlayer player;
  BetterPlayerController _controller;
  BetterPlayerDataSource _dataSource;
  BetterPlayerConfiguration _config;
  final GlobalKey _pipKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    media = widget.media;

  print("playing");
    print("media.mediaId");
    mediaPrefix = "${ServerSettings.wowZaUrl}/live/_definst_/";
    resAuto = "$mediaPrefix${videoFormatRemove(media.mediaId)}/playlist.m3u8";
    res1080p = "$mediaPrefix${videoFormatRemove(media.mediaId)}_source/playlist.m3u8";
    res720p = "$mediaPrefix${videoFormatRemove(media.mediaId)}_720/playlist.m3u8";
    res480p = "$mediaPrefix${videoFormatRemove(media.mediaId)}_360/playlist.m3u8";
    res240p = "$mediaPrefix${videoFormatRemove(media.mediaId)}_160/playlist.m3u8";
    _dataSource = 
      BetterPlayerDataSource(
        BetterPlayerDataSourceType.network,
        resAuto,
        liveStream: true,
        notificationConfiguration: BetterPlayerNotificationConfiguration(
          showNotification: false
        ),
        // Following is not used for now
        resolutions: {
          translate("live_auto"): resAuto,
          translate("live_240"): res240p,
          translate("live_480"): res480p,
          translate("live_720"): res720p,
          translate("live_1080"): res1080p,
        });

    var _portraitOrientation = [
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown
    ];
    _config = BetterPlayerConfiguration(
      autoPlay: true,
      looping: false,
      fit: BoxFit.contain,
      translations: [ko_translation],
      autoDetectFullscreenDeviceOrientation: false,
      deviceOrientationsAfterFullScreen: _portraitOrientation,
      controlsConfiguration: BetterPlayerControlsConfiguration(
        showControlsOnInitialize: false,
        showControls: false,
        iconsColor: Colors.white,
        progressBarPlayedColor: kPrimaryColor,
        progressBarHandleColor: Colors.white,
        progressBarBufferedColor: Colors.white,
        progressBarBackgroundColor: Colors.black,
        enableOverflowMenu: false,
        enableSkips: false,
        enableFullscreen: false,
        enablePlayPause: false,
        enablePip: false,
        enableSubtitles: false,
        enableAudioTracks: false,
        enablePlaybackSpeed: false,
        enableRetry: false,
        loadingColor: Colors.white,
        overflowModalColor: kPrimaryColor,
        overflowModalTextColor: Colors.white,
        overflowMenuIconsColor: Colors.white,
      ),
    );
    _controller = BetterPlayerController(
      _config,
      betterPlayerDataSource: _dataSource
    );
    _controller.setBetterPlayerGlobalKey(_pipKey);
    _controller.addEventsListener((event){
      if (event.betterPlayerEventType==BetterPlayerEventType.controlsVisible) {
        setState(() {
        _hideControls = false;
        });
      }
      if (event.betterPlayerEventType==BetterPlayerEventType.controlsHidden) {
        setState(() {
        _hideControls = true;
        });
      }
      if (event.betterPlayerEventType == BetterPlayerEventType.openFullscreen) {
        SystemChrome.setPreferredOrientations(_portraitOrientation);
      }
    });


    player = BetterPlayer(
      controller: _controller,
      key: _pipKey
    );
  }

  @override
  void dispose() {
    print("player releasing");
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var fullScreenHeight = MediaQuery.of(context).size.height;
    if (Scaffold.of(context).appBarMaxHeight != null) {
      fullScreenHeight -= Scaffold.of(context).appBarMaxHeight;
    }
    if (kToolbarHeight != null) {
      fullScreenHeight += kToolbarHeight;
    }
    return Stack(
      clipBehavior: Clip.none,
      children: <Widget>[
        AnimatedContainer(
          duration: Duration(milliseconds: 250),
          width: MediaQuery.of(context).size.width,
          height: fullScreenHeight,
          color: Colors.black,
          constraints: BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width,
            maxHeight: fullScreenHeight,
          ),
          child: player,
        ),
        AnimatedOpacity(
          opacity: 1,//_hideControls ? 0.0 : 1.0,
          duration: Duration(milliseconds: 180),
          child: Row(children: [
            Text("   [${media.mediaCategory.name}] ${media.title}",
              style: TextStyle(color: Colors.white)
            ),
            Spacer(),
            /*IconButton(
              icon: Icon(
                Icons.volume_mute
              ),
              color: Colors.white,
              onPressed: () {
                _controller.setVolume(0);
              }
            ),*/
            /*IconButton(
              icon: Icon(Icons.picture_in_picture_alt),
              color: Colors.white,
              onPressed: () {
                _controller.enablePictureInPicture(
                  _controller.betterPlayerGlobalKey);
              }
            ),*/
            IconButton(
              icon: Icon(widget.detailState.liveFullscreen
                ? Icons.fullscreen_exit
                : Icons.fullscreen
              ),
              color: Colors.white,
              onPressed: () {
                widget.detailState.setState(() {
                  widget.detailState.liveFullscreen = 
                    !widget.detailState.liveFullscreen;
                });
              }
            ),
            IconButton(
              icon: Icon(Icons.settings),
              color: Colors.white,
              onPressed: () {
                _showOptions();
              }
            ),
            IconButton(
              icon: Icon(Icons.close),
              color: Colors.white,
              onPressed: () {
                Navigator.of(context).pop();
              }
            ),
          ])
        ),
      ],
    );
  }
  
  Future<void> _showOptions() async {
    switch (await showDialog<dynamic>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Text(
            translate("select_resolution"),
            style: TextStyle(
              fontSize: 14.0,
              color: kPrimaryColor
            ),
          ),
          children: <Widget>[
            SimpleDialogOption(
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 0
              ),
              onPressed: () { Navigator.pop(context, "live_auto"); },
              child: Row(children: [
                Transform.scale(
                  scale: 0.8,
                  child: Radio(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    fillColor: 
                      MaterialStateColor.resolveWith((states) => kPrimaryColor),
                    value: 0,
                    groupValue: activeResolution,
                  )
                ),
                Text(translate("live_auto"))
              ]),
            ),
            SimpleDialogOption(
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 0
              ),
              onPressed: () { Navigator.pop(context, "live_240p"); },
              child: Row(children: [
                Transform.scale(
                  scale: 0.8,
                  child: Radio(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    fillColor: 
                      MaterialStateColor.resolveWith((states) => kPrimaryColor),
                    value: 1,
                    groupValue: activeResolution,
                  )
                ),
                Text(translate("live_240"))
              ]),
            ),
            SimpleDialogOption(
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 0
              ),
              onPressed: () { Navigator.pop(context, "live_480p"); },
              child: Row(children: [
                Transform.scale(
                  scale: 0.8,
                  child: Radio(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    fillColor: 
                      MaterialStateColor.resolveWith((states) => kPrimaryColor),
                    value: 2,
                    groupValue: activeResolution,
                  )
                ),
                Text(translate("live_480"))
              ]),
            ),
            SimpleDialogOption(
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 0
              ),
              onPressed: () { Navigator.pop(context, "live_720p"); },
              child: Row(children: [
                Transform.scale(
                  scale: 0.8,
                  child: Radio(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    fillColor: 
                      MaterialStateColor.resolveWith((states) => kPrimaryColor),
                    value: 3,
                    groupValue: activeResolution,
                  )
                ),
                Text(translate("live_720"))
              ]),
            ),
            SimpleDialogOption(
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 0
              ),
              onPressed: () { Navigator.pop(context, "live_1080p"); },
              child: Row(children: [
                Transform.scale(
                  scale: 0.8,
                  child: Radio(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    fillColor: 
                      MaterialStateColor.resolveWith((states) => kPrimaryColor),
                    value: 4,
                    groupValue: activeResolution,
                  )
                ),
                Text(translate("live_1080"))
              ]),
            ),
            SimpleDialogOption(
              child: Text(
                translate("basic_settings"),
                style: TextStyle(
                  fontSize: 14.0,
                  color: kPrimaryColor
                )
              ),
            ),
            SimpleDialogOption(
              padding: const EdgeInsets.symmetric(
                horizontal: 35,
                vertical: 5
              ),
              onPressed: () { Navigator.pop(context, "report"); },
              child: Text(translate("live_report")),
            ),
            SimpleDialogOption(
              child: Text(
                translate("live.options.view-mode"),
                style: TextStyle(
                  fontSize: 14.0,
                  color: kPrimaryColor
                )
              ),
            ),
            SimpleDialogOption(
              padding: const EdgeInsets.symmetric(
                horizontal: 35,
                vertical: 5
              ),
              onPressed: () { 
                _controller.enablePictureInPicture(
                  _controller.betterPlayerGlobalKey);
              },
              child: Text(translate("live.options.radio")),
            ),
            SimpleDialogOption(
              padding: const EdgeInsets.symmetric(
                horizontal: 35,
                vertical: 5
              ),
              onPressed: () { 
                _controller.enablePictureInPicture(
                  _controller.betterPlayerGlobalKey);
              },
              child: Text(translate("live.options.pip")),
            ),
            SimpleDialogOption(
              child: Text(
                translate("change.name.title"),
                style: TextStyle(
                  fontSize: 14.0,
                  color: kPrimaryColor
                )
              ),
            ),
            SimpleDialogOption(
              padding: const EdgeInsets.symmetric(
                horizontal: 35,
                vertical: 5
              ),
              onPressed: () { 
                _controller.enablePictureInPicture(
                  _controller.betterPlayerGlobalKey);
              },
              child: Text(translate("change.name.title")),
            ),
          ],
        );
      }
    )) {
      case "live_1080p":
        // _controller.setResolution(res1080p);
        activeResolution = 4;
        _controller.setTrack(
          _controller.betterPlayerAsmsTracks[activeResolution]);
        break;
      case "live_720p":
        // _controller.setResolution(res720p);
        activeResolution = 3;
        _controller.setTrack(
          _controller.betterPlayerAsmsTracks[activeResolution]);
        break;
      case "live_360p":
        // _controller.setResolution(res480p);
        activeResolution = 2;
        _controller.setTrack(
          _controller.betterPlayerAsmsTracks[activeResolution]);
        break;
      case "live_240p":
        // _controller.setResolution(res240p);
        activeResolution = 1;
        _controller.setTrack(
          _controller.betterPlayerAsmsTracks[activeResolution]);
        break;
      case "live_auto":
        // _controller.setResolution(resAuto);
        activeResolution = 0;
        _controller.setTrack(
          _controller.betterPlayerAsmsTracks[activeResolution]);
        break;
      case "report":
        // Navigator.of(context).pop();
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ReportDialog(
              title: translate("live_report"),
              type: eReport.LIVE, mediaID: media.id);
        });
        break;
      default:
        break;
    }
  }

  void pause() {
    _controller.pause();
  }

  String videoFormatRemove(String mediaID) {
    return mediaID.replaceAll(".mp4", "");
  }
}
