import 'package:better_player/better_player.dart';
import 'package:dccast/components/video/video_title_overlay.dart';
import 'package:dccast/utils/overlay_handler.dart';
import 'package:dccast/utils/overlay_services.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';

import '../../constants/common.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/user_media.dart';
import '../../screens/vod/edit_vod.dart';
import '../../utils/dio_client.dart';
import '../../utils/toast.dart';
import '../report_dialog.dart';

class VodPlayer extends StatefulWidget {
  final eContent content;
  final Media media;

  const VodPlayer({
    Key key,
    this.content,
    this.media
  }) : super(key: key);

  @override
  VodPlayerState createState() =>
      VodPlayerState();

  dispose() => VodPlayerState().dispose();

  pause() => VodPlayerState().pause();

}

class VodPlayerState extends State<VodPlayer> {

  Media media;
  String mediaPrefix;
  double aspectRatio = 16 / 9;
  bool _hideControls = false;
  String resAuto;
  String res1080p;
  String res720p;
  String res480p;
  String res360p;
  String res240p;
  int activeResolution = 0;
  String userID;
  final storage = FlutterSecureStorage();

  BetterPlayer player;
  BetterPlayerController _controller;
  BetterPlayerDataSource _dataSource;
  BetterPlayerConfiguration _config;
  final GlobalKey _pipKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    init();
    media = widget.media;

    mediaPrefix = "${ServerSettings.wowZaUrl}/vod/_definst_/";
    resAuto = "${"$mediaPrefix/smil:"}${videoFormatRemove(media.mediaId)}.smil/playlist.m3u8";
    res1080p = "$mediaPrefix${videoFormatRemove(media.mediaId)}_1080p/playlist.m3u8";
    res720p = "$mediaPrefix${videoFormatRemove(media.mediaId)}_720p/playlist.m3u8";
    res480p = "$mediaPrefix${videoFormatRemove(media.mediaId)}_480p/playlist.m3u8";
    res360p = "$mediaPrefix${videoFormatRemove(media.mediaId)}_360p/playlist.m3u8";
    res240p = "$mediaPrefix${videoFormatRemove(media.mediaId)}_240p/playlist.m3u8";
    _dataSource = 
      BetterPlayerDataSource(
        BetterPlayerDataSourceType.network,
        resAuto,
        notificationConfiguration: BetterPlayerNotificationConfiguration(
          showNotification: false
        ),
        // Following is not used for now. It uses HLS tracks instead
        resolutions: {
          translate("video_auto"): resAuto,
          translate("video_240"): res240p,
          translate("video_360"): res360p,
          translate("video_720"): res720p,
          translate("video_1080"): res1080p,
        });

    _config = BetterPlayerConfiguration(
      autoPlay: true,
      looping: false,
      fullScreenByDefault: false,
      fit: BoxFit.contain,
      aspectRatio: aspectRatio,
      translations: [ko_translation],
      controlsConfiguration: BetterPlayerControlsConfiguration(
        iconsColor: Colors.white,
        progressBarPlayedColor: kPrimaryColor,
        progressBarHandleColor: Colors.white,
        progressBarBufferedColor: Colors.white,
        progressBarBackgroundColor: Colors.black,
        enableOverflowMenu: false,
        enableSkips: false,
        enableFullscreen: true,
        enablePlayPause: true,
        enablePip: true,
        enableSubtitles: false,
        enableAudioTracks: false,
        enablePlaybackSpeed: false,
        enableRetry: true,
        loadingColor: Colors.white,
        overflowModalColor: kPrimaryColor,
        overflowModalTextColor: Colors.white,
        overflowMenuIconsColor: Colors.white,
                            // playerTheme: BetterPlayerTheme.custom,
        customControlsBuilder: (controller) {
          return Positioned.fill(
           child: IconButton(
            icon: Icon(Icons.settings),
            color: Colors.white,
            onPressed: () {
              _showOptions();
            }
            )
          );
        }
      ),
    );
    _controller = BetterPlayerController(
      _config,
      betterPlayerDataSource: _dataSource
    );
    _controller.setBetterPlayerGlobalKey(_pipKey);
    _controller.addEventsListener((event){
      if (event.betterPlayerEventType==BetterPlayerEventType.controlsVisible) {
        setState(() {
        _hideControls = false;
        });
      }
      if (event.betterPlayerEventType==BetterPlayerEventType.controlsHidden) {
        setState(() {
        _hideControls = true;
        });
      }
    });
    // _controller.setOverriddenAspectRatio(9/16);

    player = BetterPlayer(
      controller: _controller,
      key: _pipKey
    );
  }

  init() async {
    userID = await storage.read(key: Preferences.user_id);
  }

  @override
  void dispose() {
    print("player releasing");
    // _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: <Widget>[
        AnimatedContainer(
          duration: Duration(milliseconds: 250),
          width: MediaQuery.of(context).size.width,
          color: Colors.black,
          constraints: BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width,
          ),
          child: AspectRatio(
            aspectRatio: aspectRatio,
            child: player,
          ),
        ),
        AnimatedOpacity(
          opacity: _hideControls ? 0.0 : 1.0,
          duration: Duration(milliseconds: 180),
          child: IconButton(
            icon: Icon(Icons.settings),
            color: Colors.white,
            onPressed: () {
              _showOptions();
            }
          )
        ),
      ],
    );
  }
  
  Future<void> _showOptions() async {
    switch (await showDialog<dynamic>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Text(
            translate("select_resolution"),
            style: TextStyle(
              fontSize: 14.0,
              color: kPrimaryColor
            ),
          ),
          
          children: <Widget>[
            SimpleDialogOption(
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 0
              ),
              onPressed: () { Navigator.pop(context, "video_auto"); },
              child: Row(children: [
                Transform.scale(
                  scale: 0.8,
                  child: Radio(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    fillColor: 
                      MaterialStateColor.resolveWith((states) => kPrimaryColor),
                    value: 0,
                    groupValue: activeResolution,
                  )
                ),
                Text(translate("video_auto"))
              ]),
            ),
            SimpleDialogOption(
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 0
              ),
              onPressed: () { Navigator.pop(context, "video_240p"); },
              child: Row(children: [
                Transform.scale(
                  scale: 0.8,
                  child: Radio(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    fillColor: 
                      MaterialStateColor.resolveWith((states) => kPrimaryColor),
                    value: 1,
                    groupValue: activeResolution,
                  )
                ),
                Text(translate("video_240"))
              ]),
            ),
            SimpleDialogOption(
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 0
              ),
              onPressed: () { Navigator.pop(context, "video_360p"); },
              child: Row(children: [
                Transform.scale(
                  scale: 0.8,
                  child: Radio(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    fillColor: 
                      MaterialStateColor.resolveWith((states) => kPrimaryColor),
                    value: 2,
                    groupValue: activeResolution,
                  )
                ),
                Text(translate("video_360"))
              ]),
            ),
            SimpleDialogOption(
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 0
              ),
              onPressed: () { Navigator.pop(context, "video_720p"); },
              child: Row(children: [
                Transform.scale(
                  scale: 0.8,
                  child: Radio(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    fillColor: 
                      MaterialStateColor.resolveWith((states) => kPrimaryColor),
                    value: 3,
                    groupValue: activeResolution,
                  )
                ),
                Text(translate("video_720"))
              ]),
            ),
            SimpleDialogOption(
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 0
              ),
              onPressed: () { Navigator.pop(context, "video_1080p"); },
              child: Row(children: [
                Transform.scale(
                  scale: 0.8,
                  child: Radio(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    fillColor: 
                      MaterialStateColor.resolveWith((states) => kPrimaryColor),
                    activeColor: kPrimaryColor,
                    value: 4,
                    groupValue: activeResolution,
                  )
                ),
                Text(translate("video_1080"))
              ]),
            ),
            if (widget.media.user.id.toString() == userID)
            SimpleDialogOption(
              child: Text(
                translate("mymedia_settings"),
                style: TextStyle(
                  fontSize: 14.0,
                  color: kPrimaryColor)
              ),
            ),
            if (widget.media.user.id.toString() == userID)
            SimpleDialogOption(
              padding: const EdgeInsets.symmetric(
                horizontal: 35,
                vertical: 5
              ),
              onPressed: () { Navigator.pop(context, "edit"); },
              child: Text(translate("edit")),
            ),
            if (widget.media.user.id.toString() == userID)
            SimpleDialogOption(
              padding: const EdgeInsets.symmetric(
                horizontal: 35,
                vertical: 5
              ),
              onPressed: () { Navigator.pop(context, "delete"); },
              child: Text(translate("delete")),
            ),
            SimpleDialogOption(
              child: Text(
                translate("basic_settings"),
                style: TextStyle(
                  fontSize: 14.0,
                  color: kPrimaryColor)
              ),
            ),
            SimpleDialogOption(
              padding: const EdgeInsets.symmetric(
                horizontal: 35,
                vertical: 5
              ),
              onPressed: () { Navigator.pop(context, "report"); },
              child: Text(translate("vod_report")),
            ),
          ],
        );
      }
    )) {
      case "video_1080p":
        // _controller.setResolution(res1080p);
        activeResolution = 4;
        _controller.setTrack(
          _controller.betterPlayerAsmsTracks[activeResolution]);
        break;
      case "video_720p":
        // _controller.setResolution(res720p);
        activeResolution = 3;
        _controller.setTrack(
          _controller.betterPlayerAsmsTracks[activeResolution]);
        break;
      case "video_360p":
        // var date = DateTime.utc(2021, 5, 4);
        // if (widget.media.created.isBefore(date)) {
        //   _controller.setResolution(res480p);
        // } else {
        //   _controller.setResolution(res360p);
        // }
        activeResolution = 2;
        _controller.setTrack(
          _controller.betterPlayerAsmsTracks[activeResolution]);
        break;
      case "video_240p":
        // _controller.setResolution(res240p);
        activeResolution = 1;
        _controller.setTrack(
          _controller.betterPlayerAsmsTracks[activeResolution]);
        break;
      case "video_auto":
        // _controller.setResolution(resAuto);
        activeResolution = 0;
        _controller.setTrack(
          _controller.betterPlayerAsmsTracks[activeResolution]);
        break;
      case "report":
        // Navigator.of(context).pop();
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ReportDialog(
              title: translate("vod_report"),
              type: eReport.VOD, mediaID: media.id);
        });
        break;
      case "edit":
        var result = await Navigator.push(context,
        MaterialPageRoute(builder: (context) =>
          EditVodScreen(media: widget.media)));
        Navigator.of(context).pop();
        break;
      case "delete":
        _askDelete(context);
        break;
      default:
        break;
    }
  }

  void pause() {
    _controller?.pause();
  }

  String videoFormatRemove(String mediaID) {
    return mediaID.replaceAll(".mp4", "");
  }

  Future<void> _askDelete(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(translate("media.more.vod.delete")),
          /*content: SingleChildScrollView(
            child: Text(message),
          ),*/
          actions: <Widget>[
            TextButton(
              child: Text(translate("group.delete.cancel")),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(translate("alert.button.ok")),
              onPressed: () async {
                final dio = Dio();
                try {
                  var url = ServerSettings.createVODMediaURL + widget.media.id.toString() + "/";

                  await DioClient(dio).delete(url);
                  showToast(translate("media.more.vod.deleted"));
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();
                          
                  return "sucessful";
                } on Exception catch (error) {
                  print(error.toString());
                }
              },
            ),
          ],
        );
      },
    );
  }
}
