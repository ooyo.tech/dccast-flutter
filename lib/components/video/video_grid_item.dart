import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:share/share.dart';

import '../../constants/assets.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/user_media.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';

class VideoGridItem extends StatelessWidget {
  VideoGridItem(
      {@required this.id,
      @required this.title,
      @required this.subtitle,
      @required this.thumnail,
      this.views,
      this.kinds,
      this.media,
      this.content,
      this.onPressed});

  final int id;
  final String title;
  final String subtitle;
  final String thumnail;
  final int views;
  final String kinds;
  final Media media;
  final eContent content;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              thumnail != null
                  ? CachedNetworkImage(
                      fit: BoxFit.cover,
                      width: DeviceUtils.getScaledWidth(context, 1.0),
                      height: DeviceUtils.getScaledWidth(context, 0.25),
                      imageUrl: thumnail,
                      placeholder: (context, url) =>
                          Image.asset(Assets.videoThumbnail),
                      errorWidget: (context, url, error) {
                        return Image.asset(Assets.videoThumbnail);
                      })
                  : Image.asset(
                      Assets.videoThumbnail,
                      fit: BoxFit.cover,
                      width: DeviceUtils.getScaledWidth(context, 1.0),
                      height: DeviceUtils.getScaledWidth(context, 0.25),
                    ),
              Positioned(
                top: Metrics.singleBase,
                right: Metrics.singleBase,
                child: Row(
                  children: [
                    if (media?.kinds ==
                      translate("vod.upload.share.under19"))
                    Container(
                      padding: EdgeInsets.all(4.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        color: kRedColor,
                      ),
                      child: Text("19+", style: TextStyle(
                        color: kWhiteColor,
                        fontSize: 10
                      ))
                    ),
                    if (media?.liveSetpass)
                    SizedBox(width: 4.0),
                    if (media?.liveSetpass)
                    Image.asset(
                      Assets.lockImage,
                      width: 24.0,
                      height: 24.0,
                    ),
                    if (media?.isHit && media?.isHitActive)
                    SizedBox(width: 4.0),
                    if (media?.isHit && media?.isHitActive)
                    Container(
                      padding: EdgeInsets.all(Metrics.halfBase),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(3.0),
                        color: kPrimaryColor,
                      ),
                      child: Image.asset(
                        Assets.crownImage,
                        width: 12.0,
                        height: 12.0,
                      ),
                    )
                  ]
                )
              ),
            ],
          ),
          Expanded(
            child: ListTile(
              isThreeLine: true,
              contentPadding: EdgeInsets.all(4.0),
              title: title != null
                  ? Text(title, style: kSmallTextStyle, maxLines: 2)
                  : SizedBox(),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  subtitle != null
                      ? Text(subtitle, style: kNotificationSubtitle)
                      : SizedBox(),
                  kinds != null
                      ? Text(kinds, style: kNotificationSubtitle)
                      : SizedBox(),
                ],
              ),
              trailing: IconButton(
                padding: EdgeInsets.zero,
                alignment: Alignment.topCenter,
                iconSize: 20.0,
                icon: FaIcon(
                  FontAwesomeIcons.ellipsisV,
                  color: kBlack50Color,
                  size: 20.0,
                ),
                onPressed: () {
                  _settingModalBottomSheet(context);
                },
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<String> _removeFavorite() async {
    final dio = Dio();
    try {
      var url = ServerSettings.getUserFavoriteRemoveURL + id.toString() + "/";

      await DioClient(dio).delete(url);
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
    }
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                    leading: Icon(Icons.tv_outlined),
                    title: Text(translate("view_video")),
                    onTap: onPressed),
                ListTile(
                  leading: FaIcon(FontAwesomeIcons.share),
                  title: Text(translate("share")),
                  onTap: () => {
                    Share.share(
                        ServerSettings.getShareURL + media?.id.toString())
                  },
                ),
                ListTile(
                  leading: FaIcon(FontAwesomeIcons.solidHeart),
                  title: Text(translate('favorite')),
                  onTap: _removeFavorite,
                ),
              ],
            ),
          );
        });
  }
}
