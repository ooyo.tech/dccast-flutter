import 'package:cached_network_image/cached_network_image.dart';
import 'package:dccast/constants/preferences.dart';
import 'package:dccast/constants/server_settings.dart';
import 'package:dccast/model/friends.dart';
import 'package:dccast/screens/login.dart';
import 'package:dccast/screens/profile.dart';
import 'package:dccast/utils/auth.dart';
import 'package:dccast/utils/dio_client.dart';
import 'package:dccast/utils/toast.dart';
import 'package:dio/dio.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:provider/provider.dart';

import '../../constants/assets.dart';
import '../../constants/styles.dart';
import '../../model/user_media.dart';
import '../../utils/media.dart';

class LiveInfoComponent extends StatefulWidget {
  @override
  _LiveInfoComponentState createState() =>
      _LiveInfoComponentState();
}

class _LiveInfoComponentState extends State<LiveInfoComponent> {
  final storage = FlutterSecureStorage();
  int friendStatus = -1;
  int friendId = 0;
  Media media;

  @override
  Widget build(BuildContext context) {
    media = Provider.of<MediaUtils>(context, listen: true).chosenMedia;
    if (friendStatus == -1) {
      isFriend(media?.user?.id);
    }
    return ListTile(
      title: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  ProfileScreen(userProfile: media?.user),
            ),
          );
        },
        child: Text(
          media?.user?.nickName,
          style: kTextPrimaryStyle,
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          softWrap: true,
        ),
      ),
      leading: ClipOval(
        child: media?.user?.profileImage != null
          ? CachedNetworkImage(
              fit: BoxFit.cover,
              // width: 25.0,
              // height: 25.0,
              imageUrl: media?.user?.profileImage,
              placeholder: (context, url) =>
                  Image.asset(Assets.appLogo, fit: BoxFit.cover),
              errorWidget: (context, url, error) {
                return Image.asset(
                  Assets.profileIcon,
                  fit: BoxFit.contain,
                  color: kPrimaryColor,
                );
              })
          : Image.asset(
              Assets.profileIcon,
              fit: BoxFit.contain,
              width: 22.0,
              height: 22.0,
              color: kPrimaryColor,
            ),
      ),
      subtitle: Row(
        children: [
          Icon(FontAwesomeIcons.eye, color: kBlack50Color, size: 15.0),
          SizedBox(width: 5.0),
          Text(translate("views"), style: kNotificationSubtitle),
          SizedBox(width: 5.0),
          Text(media?.views?.toString(), style: kNotificationSubtitle)
        ],
      ),
      trailing: FlatButton(
        onPressed: /*friendStatus == 1
          ? null : */
          () {
          if (Provider.of<Auth>(context, listen: false).authenticated) {
            _friendUser(media?.user?.id);
          } else {
            Navigator.pushNamed(context, LoginScreen.id);
          }
        },
        disabledColor: Colors.grey[300],
        color: friendStatus == 2 ? Colors.red : kWhiteColor,
        // padding: EdgeInsets.zero,
        height: 24.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
          side: BorderSide(color: friendStatus == 2
            ? Colors.red
            : kPrimaryColor
            )
          ),
        child: Text(
          friendStatus == 2
          ? translate("unfriend")
          : friendStatus == 1
          ? translate("friend_req_sent")
          : translate("add_friend"),
          style: friendStatus == 2
            ? TextStyle(color: Colors.white)
            : kTextPrimaryStyle,
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  Future<String> isFriend(int toUser) async {
    final dio = Dio();
    String fromuser = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getListFriendsURL + 
        "?from_user=$fromuser&to_user=$toUser";
      final response = await DioClient(dio).get(url);
      var result = Friends.fromJson(response);
      if (result.results.isNotEmpty) {
        if (result.results[0].accepted) {
          setState(() {
            friendStatus = 2;
          });
          friendId = result.results[0].id;
        } else {
          setState(() {
            friendStatus = 1;
          });
        }
      } else {
        setState(() {
          friendStatus = 0;
        });
      }

      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
    }
  }

  Future<String> _friendUser(int toUser) async {
    final dio = Dio();
    String fromuser = await storage.read(key: Preferences.user_id);
    try {
      if (friendStatus >= 1) {
        var url = ServerSettings.getSendFriendRequestURL + "delete/?" + 
          "from_user=$fromuser&to_user=$toUser";
        final response = await DioClient(dio).delete(url);
        setState(() {
          friendStatus = 0;
        });
        showToast(translate("unfriend_success"));
      } else {
        var url = ServerSettings.getSendFriendRequestURL;
        Map<String, dynamic> data = {"from_user": fromuser, "to_user": toUser};
        final response = await DioClient(dio).post(url, data: data);
        setState(() {
          friendStatus = 1;
        });
        showToast(translate("friend.request-sent"));
      }

      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
    }
  }
}
