
import 'package:dccast/components/video/live_info.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

import '../constants/assets.dart';
import '../constants/preferences.dart';
import '../constants/server_settings.dart';
import '../constants/styles.dart';
import '../model/comments.dart';
import '../model/user_media.dart';
import '../screens/login.dart';
import '../utils/auth.dart';
import '../utils/device.dart';
import '../utils/dio_client.dart';
import '../utils/media.dart';
import '../utils/toast.dart';
import 'comment/comment_dccon.dart';
import 'live/chat_list.dart';
import 'mandu_transfer.dart';
import 'video/video_info.dart';

enum contentType { LIST, COMMENTS, MANDU, SHARE }

class LiveStackComponent extends StatefulWidget {
  bool fullScreen = false;
  ChatListComponent chatListComponent;
  final GlobalKey<ChatListComponentState> chatListComponentKey;

  LiveStackComponent({
    Key key,
    this.fullScreen,
    this.chatListComponent,
    this.chatListComponentKey
  }) : super(key: key);

  @override
  _LiveStackComponentState createState() =>
      _LiveStackComponentState();
}

class _LiveStackComponentState
    extends State<LiveStackComponent> {
  contentType selectedType = contentType.LIST;
  Media media;
  String favoriteCurrent;
  bool isFavorite = false;
  String subscribeCurrent;
  bool isSubscribe = false;
  String userID;
  final double spacerWidth = 10;
  final storage = FlutterSecureStorage();

  FocusNode chatFocus = FocusNode();
  TextEditingController chatController = TextEditingController();
  final ScrollController _chatScroller = ScrollController();
  
  @override
  Widget build(BuildContext context) {
    media = Provider.of<MediaUtils>(context, listen: false).chosenMedia;
    return widget.fullScreen ? _buildStack(context)
      : _buildBottom(context);
  }

  Widget _buildBottom(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: SingleChildScrollView(
            controller: _chatScroller,
            child: Column(
              children: [
                LiveInfoComponent(),
                widget.chatListComponent
              ],
            ),
          )
        ),
        _liveBottomMenu(context),
        SingleChildScrollView(
  physics: ClampingScrollPhysics(),
  child: 
        Container(
          height: 60.0,
          padding: EdgeInsets.symmetric(horizontal: 8.0),
          width: DeviceUtils.getScaledWidth(context, 1.0),
          color: widget.fullScreen 
            ? Colors.transparent
            : Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: _liveChatComponent(context, true)
              )
            ]
          )
        )
)
      ],
    );
  }

  Widget _buildStack(BuildContext context) {
    return Positioned(
      bottom: 2.0,
      left: 2.0,
      right: 2.0,
      top: 64.0,
      child: Column(
      children: [
        Expanded(child: widget.chatListComponent),
        SizedBox(
          height: 20,
        ),
        _liveBottomMenu(context),
      ])
    );
  }

  Widget _liveChatComponent(BuildContext context, bool disabled) {
    return TextFormField(
      readOnly: disabled,
      focusNode: chatFocus,
      autofocus: !disabled && !widget.fullScreen,
      style: TextStyle(color: kBlackColor, fontSize: 16),
      controller: chatController,
      onFieldSubmitted: (val) => 
        _sendChat(chatController.text),
      onTap: () {
        if (disabled) {
          bottomCommentTextField();
        }
      },
      decoration: InputDecoration(
        filled: true,
        fillColor: kGreyColor,
        hintText: translate("live_comment_add"),
        contentPadding:
            const EdgeInsets.only(left: 14.0, bottom: 0.0, top: 0.0),
        enabledBorder: OutlineInputBorder(
          borderSide: const
            BorderSide(color: kGreyColor, width: 1.0),
          borderRadius: BorderRadius.circular(25.7)
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const 
            BorderSide(color: kGreyColor, width: 1.0),
          borderRadius: BorderRadius.circular(25.7)
        ),
        suffixIcon: Container(
          width: 100,
          child: Row(
            children: [
              IconButton(
                icon: Icon(FontAwesomeIcons.smile),
                color: Colors.grey,
                onPressed: () {
                  if (disabled) {
                    bottomCommentTextField();
                  } else {
                    Future.delayed(Duration.zero, chatFocus.unfocus);
                    showEmojis();
                  }
                }
              ),
              IconButton(
                icon: Icon(FontAwesomeIcons.solidPaperPlane),
                color: kPrimaryColor,
                onPressed: () {
                  if (disabled) {
                    bottomCommentTextField();
                  } else {
                    Future.delayed(Duration.zero, chatFocus.unfocus);
                    if (chatController.text != "")  {
                      _sendChat(chatController.text);
                    }
                  }
                }
              ),
            ])
        )
      ),
    );
  }

  void bottomCommentTextField() {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: true,
      backgroundColor: kGreyColor,
      context: context,
      builder: (BuildContext bc) {
        return Padding(
          // padding: const EdgeInsets.all(0),
          padding: MediaQuery.of(bc).viewInsets,
          child: Container(
            child: _liveChatComponent(bc, false)
          )
        );
      }
    );
  }

  Widget _liveBottomMenu(BuildContext context) {
    return Container(
      height: 60.0,
      padding: EdgeInsets.symmetric(horizontal: 8.0),
      width: DeviceUtils.getScaledWidth(context, 1.0),
      color: widget.fullScreen 
        ? Colors.transparent
        : Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          if (widget.fullScreen)
          Expanded(
            child: _liveChatComponent(context, false)
          ),
          if (widget.fullScreen)
          SizedBox(width: spacerWidth),
          FlatButton(
            onPressed: () {
              if (Provider.of<Auth>(context, listen: false).authenticated) {
                _showMandu();
              } else {
                Navigator.pushNamed(context, LoginScreen.id);
              }
            },
            color: widget.fullScreen 
              ? Colors.transparent
              : kGreyColor,
            minWidth: Metrics.doubleBase,
            padding: EdgeInsets.all(12.0),
            shape: CircleBorder(),
            child: Image.asset(Assets.videoManduIcon),
          ),
          SizedBox(width: spacerWidth),
          FlatButton(
            onPressed: () {
              if (Provider.of<Auth>(context, listen: false).authenticated) {
                _changeFavorite();
              } else {
                Navigator.pushNamed(context, LoginScreen.id);
              }
            },
            color: widget.fullScreen 
              ? Colors.transparent
              : kGreyColor,
            minWidth: Metrics.doubleBase,
            padding: EdgeInsets.all(12.0),
            shape: CircleBorder(),
            child: Image.asset(
              Assets.heartIcon,
              color: isFavorite == true ? kPrimaryColor : kBlackColor,
            ),
          ),
          if (!widget.fullScreen)
          SizedBox(width: spacerWidth),
          if (!widget.fullScreen)
          FlatButton(
            onPressed: () =>
                {Share.share(ServerSettings.getShareURL + media.id.toString())},
            color: kGreyColor,
            minWidth: Metrics.doubleBase,
            padding: EdgeInsets.all(12.0),
            shape: CircleBorder(),
            child: Image.asset(Assets.shareIcon),
          ),
          if (!widget.fullScreen)
          Expanded(
            child: SizedBox(),
          ),
          if (!widget.fullScreen)
          FlatButton(
            onPressed: _changeSubscribe,
            color: isSubscribe ? Colors.grey : kRedColor,
            padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 4.0),
            shape: StadiumBorder(),
            child: Text(
                isSubscribe ? translate("subscribed") : translate("subscribe"),
                style: TextStyle(color: Colors.white)),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    _fetchFavoriteStatus(false);
    _fetchSubscribeStatus(false);
    super.initState();
  }

  @override
  void dispose() {
    print("Disposed");
    _chatScroller.dispose();
    chatController.dispose();
    super.dispose();
  }

  void _showMandu() async {
    await showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: true,
        backgroundColor: Colors.white,
        context: context,
        builder: (BuildContext bc) {
          return FractionallySizedBox(
            heightFactor: 0.64,
            child: ListView(
              physics: NeverScrollableScrollPhysics(),
              children: [
                ListTile(
                  title: Text(translate("mandu.gift.title")),
                  trailing: IconButton(
                      icon: Icon(Icons.close_rounded),
                      onPressed: () {
                        Navigator.pop(context);
                      }),
                ),
                Divider(),
                ManduTransferComponent()
              ],
            ),
          );
        });
  }

  void _fetchFavoriteStatus(bool toast) async {
    final dio = Dio();
    media = Provider.of<MediaUtils>(context, listen: false).chosenMedia;
    String userID = await storage.read(key: Preferences.user_id);
    try {
      Map<String, dynamic> data = {
        "kind": "avail_favorite",
        "user": userID,
        "media": media.id
      };
      await DioClient(dio)
          .post(ServerSettings.getFavoriteStatusMediaURL, data: data)
          .then((value) {
                setState(() {
                  isFavorite =
                      value["status"].toString() == "false" ? true : false;
                  favoriteCurrent = value["id"].toString();
                });
                if (toast) {
                  showToast(value["status"].toString() == "false"
                        ? "즐겨찾기에 추가했습니다"
                        : "즐겨찾기가 해제되었습니다");
                }
              });
    } on Exception catch (error) {
      print(error.toString());
    }
  }

  void _changeFavorite() async {
    final dio = Dio();
    media = Provider.of<MediaUtils>(context, listen: false).chosenMedia;
    String userID = await storage.read(key: Preferences.user_id);
    try {
      if (favoriteCurrent != null && isFavorite) {
        //remove
        await DioClient(dio)
            .delete(
                ServerSettings.getUserFavoriteRemoveURL + favoriteCurrent + "/")
            .then((value) {
          _fetchFavoriteStatus(true);
        });
      } else {
        //create
        Map data = {"user": userID, "media": media.id};
        await DioClient(dio)
            .post(ServerSettings.getFavoriteMediaURL, data: data)
            .then((value) {
          _fetchFavoriteStatus(true);
        });
      }
    } on Exception catch (error) {
      print(error);
    }
  }

  void _fetchSubscribeStatus(bool toast) async {
    final dio = Dio();
    media = Provider.of<MediaUtils>(context, listen: false).chosenMedia;
    String userID = await storage.read(key: Preferences.user_id);
    try {
      Map<String, dynamic> data = {
        "kind": "avail_subscribe",
        "from_user": userID,
        "to_user": media.user.id
      };
      await DioClient(dio)
          .post(ServerSettings.getSubscribeStatusURL, data: data)
          .then((value) {
                setState(() {
                  isSubscribe =
                      value["status"].toString() == "false" ? true : false;
                  subscribeCurrent = value["id"].toString();
                });
                if (toast) {
                  showToast(value["status"].toString() == "false"
                        ? "구독이 완료되었습니다"
                        : "구독이 취소되었습니다");
                }
              });
    } on Exception catch (error) {
      print(error.toString());
    }
  }

  void _changeSubscribe() async {
    final dio = Dio();
    media = Provider.of<MediaUtils>(context, listen: false).chosenMedia;
    String userID = await storage.read(key: Preferences.user_id);
    try {
      if (subscribeCurrent != null && isSubscribe) {
        //remove
        await DioClient(dio)
            .delete(ServerSettings.getDeleteSubURL + subscribeCurrent + "/")
            .then((value) {
          print("delete:" + value.toString());
          _fetchSubscribeStatus(true);
        });
      } else {
        //create
        Map data = {"from_user": userID, "to_user": media.user.id};
        print("from" + userID + "to:" + media.user.id.toString());
        await DioClient(dio)
            .post(ServerSettings.getCreateSubscribeMediaURL, data: data)
            .then((value) {
          setState(() {
            subscribeCurrent = value["id"].toString();
          });
          _fetchSubscribeStatus(true);
        });
      }
    } on Exception catch (error) {
      print("sub create:" + error.toString());
    }
  }

  void showEmojis() {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: true,
      backgroundColor: Colors.white,
      context: context,
      builder: (BuildContext bc) {
        return CommentDcconScreen(sendEmoji: (context, Dccon dccon) {
          _sendChat("dcconUrl:${dccon.img}");
          Navigator.of(context).pop();
        });
      }
    );
  }

  void _sendChat(String messageText) {
    if (media.liveChatDisable) {
      showToast(translate("live.comment.chat_lock"));
    } else {
      widget.chatListComponentKey.currentState.sendChat(messageText);
      chatController.text = "";
    }
    if (messageText.startsWith("dcconUrl")) {
      Navigator.of(context).pop();
    }
    Navigator.of(context).pop();
  }

}
