import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

import '../constants/assets.dart';
import '../constants/styles.dart';
import '../model/arguments/selectedtabarguments.dart';
import '../model/user.dart';
import '../model/user_mandu.dart';
import '../model/user_point.dart';
import '../screens/login.dart';
import '../screens/mandu_history.dart';
import '../screens/notification/notifications.dart';
import '../screens/point_mandu/points_index.dart';
import '../screens/profile.dart';
import '../screens/settings.dart';
import '../utils/auth.dart';

class SideMenuHeader extends StatefulWidget {
  @override
  _SideMenuHeaderState createState() => _SideMenuHeaderState();
}

class _SideMenuHeaderState extends State<SideMenuHeader> {
  double statusBarHeight;
  UserPoint userPoint;
  UserMandu userMandu;
  final storage = FlutterSecureStorage();
  @override
  Widget build(BuildContext context) {
    statusBarHeight = MediaQuery.of(context).padding.top;
    return Consumer<Auth>(builder: (context, auth, child) {
      if (auth.loggedIn) {
        return loggedIn(context, auth.authenticatedUser);
      } else {
        return notLoggedIn(context);
      }
    });
  }

  void dispose() {
    super.dispose();
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  Widget loggedIn(BuildContext context, User user) {
    userPoint = Provider.of<Auth>(context, listen: false).userPoint;
    userMandu = Provider.of<Auth>(context, listen: false).userMandu;
    return Container(
      color: kPrimaryColor,
      padding: EdgeInsets.fromLTRB(Metrics.singleBase, statusBarHeight,
          Metrics.doubleBase, Metrics.doubleBase),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: FlatButton(
                  onPressed: () {},
                  child: Row(
                    children: [
                      Text(
                        translate('app_name'),
                        style: kSideTitleStyle,
                      ),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, NotificationsScreen.id,
                      arguments: SelectedTabArguments(0));
                },
                child: FaIcon(
                  FontAwesomeIcons.solidBell,
                  color: Colors.white,
                  size: 20.0,
                ),
              ),
              SizedBox(width: 5.0),
              FlatButton(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                minWidth: 25.0,
                onPressed: () {
                  Navigator.pushNamed(context, SettingsScreen.id);
                },
                child: Image.asset(
                  Assets.sideMenuSettings,
                  width: 25.0,
                  height: 25.0,
                ),
              )
            ],
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: Metrics.singleBase),
            child: Row(
              children: [
                GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, ProfileScreen.id);
                    },
                    child: CachedNetworkImage(
                        fit: BoxFit.cover,
                        width: 50.0,
                        height: 50.0,
                        imageUrl: user.profile.profileImage,
                        errorWidget: (context, url, error) {
                          return Container(
                            width: 50.0,
                            height: 50.0,
                            color: Colors.white,
                            child: Image.asset(Assets.profileImage,
                                color: kPrimaryColor),
                          );
                        })),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: 8.0,
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, ProfileScreen.id);
                          },
                          child: Text(
                            user.user.username,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 8.0,
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, ManduHistoryScreen.id);
                          },
                          child: Row(
                            children: [
                              Image.asset(
                                Assets.manduIcon,
                                width: 30.0,
                                height: 30.0,
                              ),
                              SizedBox(
                                width: 5.0,
                              ),
                              Text(
                                userMandu == null ? '0' : userMandu.cause.toString(),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.normal),
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, PointsScreen.id);
                          },
                          child: Row(
                            children: [
                              Image.asset(
                                Assets.pointIcon,
                                width: 30.0,
                                height: 30.0,
                              ),
                              SizedBox(
                                width: 5.0,
                              ),
                              userPoint == null
                                  ? Container(
                                      width: 20.0,
                                      height: 20.0,
                                      child: CircularProgressIndicator(
                                        valueColor:
                                            AlwaysStoppedAnimation<Color>(
                                                Colors.white),
                                        strokeWidth: 2.0,
                                      ))
                                  : Text(
                                      userPoint.point,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.normal),
                                    )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget notLoggedIn(BuildContext context) {
    return Container(
      color: kPrimaryColor,
      padding: EdgeInsets.fromLTRB(Metrics.singleBase, statusBarHeight,
          Metrics.doubleBase, Metrics.doubleBase),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: FlatButton(
                  onPressed: () {},
                  child: Row(
                    children: [
                      Text(
                        translate("app_name"),
                        style: kSideTitleStyle,
                      ),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.popAndPushNamed(context, LoginScreen.id);
                },
                child: Image.asset(
                  Assets.sideMenuProfile,
                  color: Colors.white,
                  height: 25.0,
                  width: 25.0,
                ),
              ),
              SizedBox(width: 5.0),
              FlatButton(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                minWidth: 25.0,
                onPressed: () {
                  Navigator.pushNamed(context, SettingsScreen.id);
                },
                child: Image.asset(
                  Assets.sideMenuSettings,
                  width: 25.0,
                  height: 25.0,
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
