import 'package:dccast/utils/socket.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';

import './utils/auth.dart';
import 'utils/advertisement.dart';
import 'utils/category.dart';
import 'utils/comments.dart';
import 'utils/group.dart';
import 'utils/media.dart';
import 'utils/overlay_handler.dart';
import 'utils/routes.dart';

void main() async {
  var delegate = await LocalizationDelegate.create(
      fallbackLocale: 'ko', supportedLocales: ['en', 'ko']);
  WidgetsFlutterBinding.ensureInitialized();

  // runApp(LocalizedApp(delegate, MyApp()));
  runApp(
    LocalizedApp(
      delegate,
      MultiProvider(providers: [
        ChangeNotifierProvider(create: (context) => Auth()),
        ChangeNotifierProvider(create: (context) => MediaUtils()),
        ChangeNotifierProvider(create: (context) => CategoryUtils()),
        ChangeNotifierProvider(create: (context) => OverlayHandlerProvider()),
        ChangeNotifierProvider(create: (context) => AdvertisementUtil()),
        ChangeNotifierProvider(create: (context) => CommentsUtil()),
        ChangeNotifierProvider(create: (context) => GroupUtil()),
        ChangeNotifierProvider(create: (context) => SocketChat()),
      ], child: Routes()),
    ),
  );
}

// TODO: all HTTP port acceptable, change this settings on Info.plist and AndroidManifest
// TODO: TOKEN shalgaj bgaa API-aar hereglegchee secure hiih, Auth file attempt dr
// TODO: SettingsScreen fix apis, configs
// TODO: video play check with settings LTE or wifi

// MultiProvider(providers: [
// // ChangeNotifierProvider(create: (context) => UserRecent()),
// ChangeNotifierProvider(create: (context) => Auth()),
// ], child: Routes()),
