// To parse this JSON data, do
//
//     final userFavorites = userFavoritesFromJson(jsonString);

import 'dart:convert';
import 'user.dart';
import 'user_media.dart';

UserFavorites userFavoritesFromJson(String str) =>
    UserFavorites.fromJson(json.decode(str));

String userFavoritesToJson(UserFavorites data) => json.encode(data.toJson());

class UserFavorites {
  UserFavorites({
    this.results,
    this.next,
    this.previous,
    this.count,
    this.totalPages,
  });

  List<Result> results;
  String next;
  dynamic previous;
  int count;
  int totalPages;

  factory UserFavorites.fromJson(Map<String, dynamic> json) => UserFavorites(
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
        next: json["next"],
        previous: json["previous"],
        count: json["count"],
        totalPages: json["total_pages"],
      );

  Map<String, dynamic> toJson() => {
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "next": next,
        "previous": previous,
        "count": count,
        "total_pages": totalPages,
      };
}

class Result {
  Result({
    this.id,
    this.user,
    this.media,
    this.created,
  });

  int id;
  Profile user;
  Media media;
  DateTime created;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        user: json["user"] == null ? null : Profile.fromJson(json["user"]),
        media: json["media"] == null ? null : Media.fromJson(json["media"]),
        created: DateTime.parse(json["created"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user": user == null ? null : user.toJson(),
        "media": media == null ? null : media.toJson(),
        "created": created.toIso8601String(),
      };
}
