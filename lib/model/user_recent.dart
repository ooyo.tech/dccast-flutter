// To parse this JSON data, do
//
//     final userRecent = userRecentFromJson(jsonString);

import 'dart:convert';
import 'media.dart';
import 'user.dart';

UserRecent userRecentFromJson(String str) =>
    UserRecent.fromJson(json.decode(str));

String userRecentToJson(UserRecent data) => json.encode(data.toJson());

class UserRecent {
  UserRecent({
    this.results,
    this.next,
    this.previous,
    this.count,
    this.totalPages,
  });

  List<Result> results;
  dynamic next;
  dynamic previous;
  int count;
  int totalPages;

  factory UserRecent.fromJson(Map<String, dynamic> json) => UserRecent(
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
        next: json["next"],
        previous: json["previous"],
        count: json["count"],
        totalPages: json["total_pages"],
      );

  Map<String, dynamic> toJson() => {
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "next": next,
        "previous": previous,
        "count": count,
        "total_pages": totalPages,
      };
}

class Result {
  Result({
    this.id,
    this.user,
    this.media,
    this.created,
  });

  int id;
  Profile user;
  Media media;
  DateTime created;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        user: Profile.fromJson(json["user"]),
        media: json["media"] == null ? null : Media.fromJson(json["media"]),
        created: DateTime.parse(json["created"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user": user.toJson(),
        "media": media == null ? null : media.toJson(),
        "created": created.toIso8601String(),
      };
}
