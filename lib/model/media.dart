import 'dart:convert';
import 'user.dart';

class Media {
  Media({
    this.id,
    this.category,
    this.created,
    this.title,
    this.explanation,
    this.mediaType,
    this.duration,
    this.kinds,
    this.liveDeploy,
    this.liveMember,
    this.liveChatDisable,
    this.liveSetpass,
    this.isHit,
    this.hitCreated,
    this.isHitActive,
    this.hitCompleted,
    this.isComplete,
    this.completeCreated,
    this.isPopular,
    this.popularCreated,
    this.user,
    this.mediaCategory,
    this.views,
    this.mediaId,
    this.mediaThumbnail,
    this.livePassword,
    this.alive,
    this.forcedOffs,
    this.liveLastUpdate,
    this.liveVodId,
    this.liveStartDatetime,
    this.like,
    this.disLike,
    this.liveResolution,
    this.isConverted,
    this.liked,
    this.disliked,
  });

  int id;
  Category category;
  DateTime created;
  String title;
  String explanation;
  String mediaType;
  int duration;
  String kinds;
  String liveDeploy;
  String liveMember;
  bool liveChatDisable;
  bool liveSetpass;
  bool isHit;
  DateTime hitCreated;
  bool isHitActive;
  DateTime hitCompleted;
  bool isComplete;
  dynamic completeCreated;
  bool isPopular;
  dynamic popularCreated;
  Profile user;
  MediaCategory mediaCategory;
  int views;
  String mediaId;
  String mediaThumbnail;
  String livePassword;
  bool alive;
  List<dynamic> forcedOffs;
  DateTime liveLastUpdate;
  String liveVodId;
  DateTime liveStartDatetime;
  int like;
  int disLike;
  String liveResolution;
  bool isConverted;
  bool liked;
  bool disliked;

  factory Media.fromJson(Map<String, dynamic> json) => Media(
        id: json["id"],
        category: categoryValues.map[json["category"]],
        created: DateTime.parse(json["created"]),
        title: json["title"] == null ? "" : json["title"],
        explanation: json["explanation"] == null ? null : json["explanation"],
        mediaType: json["media_type"] == null ? null : json["media_type"],
        duration: json["duration"],
        kinds: json["kinds"] == null ? null : json["kinds"],
        liveDeploy: json["live_deploy"] == null ? null : json["live_deploy"],
        liveMember: json["live_member"] == null ? null : json["live_member"],
        liveChatDisable: json["live_chat_disable"],
        liveSetpass: json["live_setpass"],
        isHit: json["is_hit"],
        hitCreated: json["hit_created"] == null
            ? null
            : DateTime.parse(json["hit_created"]),
        isHitActive: json["is_hit_active"],
        hitCompleted: json["hit_completed"] == null
            ? null
            : DateTime.parse(json["hit_completed"]),
        isComplete: json["is_complete"],
        completeCreated: json["complete_created"],
        isPopular: json["is_popular"],
        popularCreated: json["popular_created"],
        user: Profile.fromJson(json["user"]),
        mediaCategory: json["media_category"] == null ?
          null : MediaCategory.fromJson(json["media_category"]),
        views: json["views"],
        mediaId: json["media_id"],
        mediaThumbnail: json["media_thumbnail"],
        livePassword:
            json["live_password"] == null ? null : json["live_password"],
        alive: json["alive"],
        forcedOffs: List<dynamic>.from(json["forced_offs"].map((x) => x)),
        liveLastUpdate: json["live_last_update"] == null
            ? null
            : DateTime.parse(json["live_last_update"]),
        liveVodId: json["live_vod_id"] == null ? null : json["live_vod_id"],
        liveStartDatetime: json["live_start_datetime"] == null
            ? null
            : DateTime.parse(json["live_start_datetime"]),
        like: json["like"],
        disLike: json["dis_like"],
        liveResolution:
            json["live_resolution"] == null ? null : json["live_resolution"],
        isConverted: json["is_converted"],
        liked: json["liked"] == null ? null : json["liked"],
        disliked: json["disliked"] == null ? null : json["disliked"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "category": categoryValues.reverse[category],
        "created": created.toIso8601String(),
        "title": title == null ? "" : title,
        "explanation": explanation == null ? null : explanation,
        "media_type": mediaType == null ? null : mediaType,
        "duration": duration,
        "kinds": kinds == null ? null : kinds,
        "live_deploy": liveDeploy == null ? null : liveDeploy,
        "live_member": liveMember == null ? null : liveMember,
        "live_chat_disable": liveChatDisable,
        "live_setpass": liveSetpass,
        "is_hit": isHit,
        "hit_created": hitCreated == null ? null : hitCreated.toIso8601String(),
        "is_hit_active": isHitActive,
        "hit_completed":
            hitCompleted == null ? null : hitCompleted.toIso8601String(),
        "is_complete": isComplete,
        "complete_created": completeCreated,
        "is_popular": isPopular,
        "popular_created": popularCreated,
        "user": user.toJson(),
        "media_category": mediaCategory.toJson(),
        "views": views,
        "media_id": mediaId,
        "media_thumbnail": mediaThumbnail,
        "live_password": livePassword == null ? null : livePassword,
        "alive": alive,
        "forced_offs": List<dynamic>.from(forcedOffs.map((x) => x)),
        "live_last_update":
            liveLastUpdate == null ? null : liveLastUpdate.toIso8601String(),
        "live_vod_id": liveVodId == null ? null : liveVodId,
        "live_start_datetime": liveStartDatetime == null
            ? null
            : liveStartDatetime.toIso8601String(),
        "like": like,
        "dis_like": disLike,
        "live_resolution": liveResolution == null ? null : liveResolution,
        "is_converted": isConverted,
        "liked": liked == null ? null : liked,
        "disliked": disliked == null ? null : disliked,
      };
}

enum Category { VOD, LIVE }

final categoryValues = EnumValues({"LIVE": Category.LIVE, "VOD": Category.VOD});

class MediaCategory {
  MediaCategory({
    this.id,
    this.name,
    this.createDate,
  });

  int id;
  String name;
  DateTime createDate;

  factory MediaCategory.fromJson(Map<String, dynamic> json) => MediaCategory(
        id: json["id"],
        name: json["name"],
        createDate: DateTime.parse(json["create_date"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "create_date": createDate.toIso8601String(),
      };
}

enum Certification { EMPTY }

final certificationValues = EnumValues({"미인증": Certification.EMPTY});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
