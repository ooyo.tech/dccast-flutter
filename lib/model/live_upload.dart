// To parse this JSON data, do
//
//     final liveUpload = liveUploadFromJson(jsonString);

import 'dart:convert';

LiveUpload liveUploadFromJson(String str) =>
    LiveUpload.fromJson(json.decode(str));

String liveUploadToJson(LiveUpload data) => json.encode(data.toJson());

class LiveUpload {
  LiveUpload({
    this.id,
    this.category,
    this.division,
    this.created,
    this.title,
    this.explanation,
    this.mediaType,
    this.duration,
    this.kinds,
    this.liveDeploy,
    this.liveMember,
    this.liveChatDisable,
    this.liveSetpass,
    this.isHit,
    this.hitCreated,
    this.isHitActive,
    this.hitCompleted,
    this.isComplete,
    this.completeCreated,
    this.isPopular,
    this.popularCreated,
    this.user,
    this.mediaCategory,
    this.views,
    this.mediaId,
    this.mediaThumbnail,
    this.livePassword,
    this.alive,
    this.forcedOffs,
    this.liveLastUpdate,
    this.liveVodId,
    this.liveStartDatetime,
    this.like,
    this.disLike,
    this.liveResolution,
    this.isConverted,
    this.dcGallery,
    this.dcGalleryName,
    this.orientation,
  });

  int id;
  String category;
  String division;
  DateTime created;
  String title;
  dynamic explanation;
  String mediaType;
  int duration;
  dynamic kinds;
  String liveDeploy;
  String liveMember;
  bool liveChatDisable;
  bool liveSetpass;
  bool isHit;
  dynamic hitCreated;
  bool isHitActive;
  dynamic hitCompleted;
  bool isComplete;
  dynamic completeCreated;
  bool isPopular;
  dynamic popularCreated;
  int user;
  int mediaCategory;
  int views;
  String mediaId;
  String mediaThumbnail;
  dynamic livePassword;
  bool alive;
  List<dynamic> forcedOffs;
  dynamic liveLastUpdate;
  dynamic liveVodId;
  dynamic liveStartDatetime;
  int like;
  int disLike;
  String liveResolution;
  bool isConverted;
  dynamic dcGallery;
  dynamic dcGalleryName;
  String orientation;

  factory LiveUpload.fromJson(Map<String, dynamic> json) => LiveUpload(
        id: json["id"] == null ? null : json["id"],
        category: json["category"] == null ? null : json["category"],
        division: json["division"] == null ? null : json["division"],
        created:
            json["created"] == null ? null : DateTime.parse(json["created"]),
        title: json["title"] == null ? null : json["title"],
        explanation: json["explanation"],
        mediaType: json["media_type"] == null ? null : json["media_type"],
        duration: json["duration"] == null ? null : json["duration"],
        kinds: json["kinds"],
        liveDeploy: json["live_deploy"] == null ? null : json["live_deploy"],
        liveMember: json["live_member"] == null ? null : json["live_member"],
        liveChatDisable: json["live_chat_disable"] == null
            ? null
            : json["live_chat_disable"],
        liveSetpass: json["live_setpass"] == null ? null : json["live_setpass"],
        isHit: json["is_hit"] == null ? null : json["is_hit"],
        hitCreated: json["hit_created"],
        isHitActive:
            json["is_hit_active"] == null ? null : json["is_hit_active"],
        hitCompleted: json["hit_completed"],
        isComplete: json["is_complete"] == null ? null : json["is_complete"],
        completeCreated: json["complete_created"],
        isPopular: json["is_popular"] == null ? null : json["is_popular"],
        popularCreated: json["popular_created"],
        user: json["user"] == null ? null : json["user"],
        mediaCategory:
            json["media_category"] == null ? null : json["media_category"],
        views: json["views"] == null ? null : json["views"],
        mediaId: json["media_id"] == null ? null : json["media_id"],
        mediaThumbnail:
            json["media_thumbnail"] == null ? null : json["media_thumbnail"],
        livePassword: json["live_password"],
        alive: json["alive"] == null ? null : json["alive"],
        forcedOffs: json["forced_offs"] == null
            ? null
            : List<dynamic>.from(json["forced_offs"].map((x) => x)),
        liveLastUpdate: json["live_last_update"],
        liveVodId: json["live_vod_id"],
        liveStartDatetime: json["live_start_datetime"],
        like: json["like"] == null ? null : json["like"],
        disLike: json["dis_like"] == null ? null : json["dis_like"],
        liveResolution:
            json["live_resolution"] == null ? null : json["live_resolution"],
        isConverted: json["is_converted"] == null ? null : json["is_converted"],
        dcGallery: json["dc_gallery"],
        dcGalleryName: json["dc_gallery_name"],
        orientation: json["orientation"] == null ? null : json["orientation"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "category": category == null ? null : category,
        "division": division == null ? null : division,
        "created": created == null ? null : created.toIso8601String(),
        "title": title == null ? null : title,
        "explanation": explanation,
        "media_type": mediaType == null ? null : mediaType,
        "duration": duration == null ? null : duration,
        "kinds": kinds,
        "live_deploy": liveDeploy == null ? null : liveDeploy,
        "live_member": liveMember == null ? null : liveMember,
        "live_chat_disable": liveChatDisable == null ? null : liveChatDisable,
        "live_setpass": liveSetpass == null ? null : liveSetpass,
        "is_hit": isHit == null ? null : isHit,
        "hit_created": hitCreated,
        "is_hit_active": isHitActive == null ? null : isHitActive,
        "hit_completed": hitCompleted,
        "is_complete": isComplete == null ? null : isComplete,
        "complete_created": completeCreated,
        "is_popular": isPopular == null ? null : isPopular,
        "popular_created": popularCreated,
        "user": user == null ? null : user,
        "media_category": mediaCategory == null ? null : mediaCategory,
        "views": views == null ? null : views,
        "media_id": mediaId == null ? null : mediaId,
        "media_thumbnail": mediaThumbnail == null ? null : mediaThumbnail,
        "live_password": livePassword,
        "alive": alive == null ? null : alive,
        "forced_offs": forcedOffs == null
            ? null
            : List<dynamic>.from(forcedOffs.map((x) => x)),
        "live_last_update": liveLastUpdate,
        "live_vod_id": liveVodId,
        "live_start_datetime": liveStartDatetime,
        "like": like == null ? null : like,
        "dis_like": disLike == null ? null : disLike,
        "live_resolution": liveResolution == null ? null : liveResolution,
        "is_converted": isConverted == null ? null : isConverted,
        "dc_gallery": dcGallery,
        "dc_gallery_name": dcGalleryName,
        "orientation": orientation == null ? null : orientation,
      };
}
