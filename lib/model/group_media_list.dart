// To parse this JSON data, do
//
//     final groupMediaList = groupMediaListFromJson(jsonString);

import 'dart:convert';
import 'group_list.dart';
import 'user_media.dart';

GroupMediaList groupMediaListFromJson(String str) =>
    GroupMediaList.fromJson(json.decode(str));

String groupMediaListToJson(GroupMediaList data) => json.encode(data.toJson());

class GroupMediaList {
  GroupMediaList({
    this.count,
    this.totalPages,
    this.previous,
    this.next,
    this.results,
  });

  int count;
  int totalPages;
  dynamic previous;
  dynamic next;
  List<Result> results;

  factory GroupMediaList.fromJson(Map<String, dynamic> json) => GroupMediaList(
        count: json["count"] == null ? null : json["count"],
        totalPages: json["total_pages"] == null ? null : json["total_pages"],
        previous: json["previous"],
        next: json["next"],
        results: json["results"] == null
            ? null
            : List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count == null ? null : count,
        "total_pages": totalPages == null ? null : totalPages,
        "previous": previous,
        "next": next,
        "results": results == null
            ? null
            : List<dynamic>.from(results.map((x) => x.toJson())),
      };
}

class Result {
  Result({
    this.id,
    this.group,
    this.media,
    this.timestamp,
    this.category,
    this.liveShare,
    this.liveType,
    this.liveLock,
    this.chatLock,
    this.vodRating,
  });

  int id;
  Group group;
  Media media;
  DateTime timestamp;
  String category;
  dynamic liveShare;
  dynamic liveType;
  bool liveLock;
  int chatLock;
  int vodRating;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"] == null ? null : json["id"],
        group: json["group"] == null ? null : Group.fromJson(json["group"]),
        media: json["media"] == null ? null : Media.fromJson(json["media"]),
        timestamp: json["timestamp"] == null
            ? null
            : DateTime.parse(json["timestamp"]),
        category: json["category"] == null ? null : json["category"],
        liveShare: json["live_share"],
        liveType: json["live_type"],
        liveLock: json["live_lock"] == null ? null : json["live_lock"],
        chatLock: json["chat_lock"] == null ? null : json["chat_lock"],
        vodRating: json["vod_rating"] == null ? null : json["vod_rating"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "group": group == null ? null : group.toJson(),
        "media": media == null ? null : media.toJson(),
        "timestamp": timestamp == null ? null : timestamp.toIso8601String(),
        "category": category == null ? null : category,
        "live_share": liveShare,
        "live_type": liveType,
        "live_lock": liveLock == null ? null : liveLock,
        "chat_lock": chatLock == null ? null : chatLock,
        "vod_rating": vodRating == null ? null : vodRating,
      };
}
