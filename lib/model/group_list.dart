// To parse this JSON data, do
//
//     final groupList = groupListFromJson(jsonString);

import 'dart:convert';

import 'package:dccast/constants/server_settings.dart';

GroupList groupListFromJson(String str) => GroupList.fromJson(json.decode(str));

String groupListToJson(GroupList data) => json.encode(data.toJson());

class GroupList {
  GroupList({
    this.count,
    this.totalPages,
    this.previous,
    this.next,
    this.results,
  });

  int count;
  int totalPages;
  dynamic previous;
  dynamic next;
  List<Group> results;

  factory GroupList.fromJson(Map<String, dynamic> json) => GroupList(
        count: json["count"] == null ? null : json["count"],
        totalPages: json["total_pages"] == null ? null : json["total_pages"],
        previous: json["previous"],
        next: json["next"],
        results: json["results"] == null
            ? null
            : List<Group>.from(json["results"].map((x) => Group.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count == null ? null : count,
        "total_pages": totalPages == null ? null : totalPages,
        "previous": previous,
        "next": next,
        "results": results == null
            ? null
            : List<dynamic>.from(results.map((x) => x.toJson())),
      };
}

class Group {
  Group({
    this.id,
    this.name,
    this.message,
    this.contactPerson,
    this.members,
    this.mediaList,
    this.profileImg,
  });

  int id;
  String name;
  String message;
  int contactPerson;
  List<int> members;
  List<int> mediaList;
  String profileImg;

  factory Group.fromJson(Map<String, dynamic> json) => Group(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? "" : json["name"],
        message: json["message"] == null ? "" : json["message"],
        contactPerson:
            json["contact_person"] == null ? null : json["contact_person"],
        members: json["members"] == null
            ? null
            : List<int>.from(json["members"].map((x) => x)),
        mediaList: json["media_list"] == null
            ? null
            : List<int>.from(json["media_list"].map((x) => x)),
        profileImg: json["profile_img"] == null ? null : 
            (json["profile_img"].startsWith('http') 
              ? json["profile_img"]
              : ServerSettings.baseUrl + "/" + json["profile_img"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "message": message == null ? null : message,
        "contact_person": contactPerson == null ? null : contactPerson,
        "members":
            members == null ? null : List<dynamic>.from(members.map((x) => x)),
        "media_list": mediaList == null
            ? null
            : List<dynamic>.from(mediaList.map((x) => x)),
        "profile_img": profileImg == null ? null : profileImg,
      };
}
