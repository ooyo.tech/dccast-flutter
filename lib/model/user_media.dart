// To parse this JSON data, do
//
//     final userMedia = userMediaFromJson(jsonString);

import 'dart:convert';
import 'user.dart';

UserMedia userMediaFromJson(String str) => UserMedia.fromJson(json.decode(str));

String userMediaToJson(UserMedia data) => json.encode(data.toJson());

class UserMedia {
  UserMedia({
    this.results,
    this.next,
    this.previous,
    this.count,
    this.totalPages,
  });

  List<Media> results;
  String next;
  dynamic previous;
  int count;
  int totalPages;

  factory UserMedia.fromJson(Map<String, dynamic> json) => UserMedia(
        results:
            List<Media>.from(json["results"].map((x) => Media.fromJson(x))),
        next: json["next"],
        previous: json["previous"],
        count: json["count"],
        totalPages: json["total_pages"],
      );

  Map<String, dynamic> toJson() => {
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "next": next,
        "previous": previous,
        "count": count,
        "total_pages": totalPages,
      };
}

class Media {
  Media({
    this.id,
    this.category,
    this.division,
    this.created,
    this.title,
    this.explanation,
    this.mediaType,
    this.duration,
    this.kinds,
    this.liveDeploy,
    this.liveMember,
    this.liveChatDisable,
    this.liveSetpass,
    this.isHit,
    this.hitCreated,
    this.isHitActive,
    this.hitCompleted,
    this.isComplete,
    this.completeCreated,
    this.isPopular,
    this.popularCreated,
    this.user,
    this.mediaCategory,
    this.views,
    this.mediaId,
    this.mediaThumbnail,
    this.livePassword,
    this.alive,
    this.forcedOffs,
    this.liveLastUpdate,
    this.liveVodId,
    this.liveStartDatetime,
    this.like,
    this.disLike,
    this.liveResolution,
    this.isConverted,
    this.dcGallery,
    this.dcGalleryName,
    this.orientation,
    this.favorite,
    this.liked,
    this.disliked,
  });

  int id;
  String category;
  String division;
  DateTime created;
  String title;
  String explanation;
  String mediaType;
  int duration;
  String kinds;
  String liveDeploy;
  String liveMember;
  bool liveChatDisable;
  bool liveSetpass;
  bool isHit;
  dynamic hitCreated;
  bool isHitActive;
  dynamic hitCompleted;
  bool isComplete;
  dynamic completeCreated;
  bool isPopular;
  dynamic popularCreated;
  Profile user;
  MediaCategory mediaCategory;
  int views;
  String mediaId;
  String mediaThumbnail;
  String livePassword;
  bool alive;
  List<dynamic> forcedOffs;
  DateTime liveLastUpdate;
  dynamic liveVodId;
  DateTime liveStartDatetime;
  int like;
  int disLike;
  String liveResolution;
  bool isConverted;
  String dcGallery;
  String dcGalleryName;
  Orientation orientation;
  int favorite;
  bool liked;
  bool disliked;

  factory Media.fromJson(Map<String, dynamic> json) => Media(
        id: json["id"],
        category: json["category"],
        division: json["division"],
        created: DateTime.parse(json["created"]),
        title: json["title"] == null ? "" : json["title"],
        explanation: json["explanation"] == null ? "" : json["explanation"],
        mediaType: json["media_type"] == null ? "" : json["media_type"],
        duration: json["duration"],
        kinds: json["kinds"] == null ? "" : json["kinds"],
        liveDeploy: json["live_deploy"] == null ? "" : json["live_deploy"],
        liveMember: json["live_member"] == null ? "" : json["live_member"],
        liveChatDisable: json["live_chat_disable"],
        liveSetpass: json["live_setpass"],
        isHit: json["is_hit"],
        hitCreated: json["hit_created"],
        isHitActive: json["is_hit_active"],
        hitCompleted: json["hit_completed"],
        isComplete: json["is_complete"],
        completeCreated: json["complete_created"],
        isPopular: json["is_popular"],
        popularCreated: json["popular_created"],
        user: Profile.fromJson(json["user"]),
        mediaCategory: json["media_category"] == null ? null
                 : MediaCategory.fromJson(json["media_category"]),
        views: json["views"],
        mediaId: json["media_id"],
        mediaThumbnail: json["media_thumbnail"],
        livePassword:
            json["live_password"] == null ? "" : json["live_password"],
        alive: json["alive"],
        forcedOffs: List<dynamic>.from(json["forced_offs"].map((x) => x)),
        liveLastUpdate: json["live_last_update"] == null
            ? null
            : DateTime.parse(json["live_last_update"]),
        liveVodId: json["live_vod_id"],
        liveStartDatetime: json["live_start_datetime"] == null
            ? null
            : DateTime.parse(json["live_start_datetime"]),
        like: json["like"],
        disLike: json["dis_like"],
        liveResolution:
            json["live_resolution"] == null ? "" : json["live_resolution"],
        isConverted: json["is_converted"],
        dcGallery: json["dc_gallery"] == null ? "" : json["dc_gallery"],
        dcGalleryName:
            json["dc_gallery_name"] == null ? "" : json["dc_gallery_name"],
        orientation: orientationValues.map[json["orientation"]],
        favorite: json["favorite"],
        liked: json["liked"] == null ? false : json["liked"],
        disliked: json["disliked"] == null ? false : json["disliked"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "category": category,
        "division": division,
        "created": created.toIso8601String(),
        "title": title,
        "explanation": explanation == null ? "" : explanation,
        "media_type": mediaType == null ? "" : mediaType,
        "duration": duration,
        "kinds": kinds == null ? null : kinds,
        "live_deploy": liveDeploy == null ? null : liveDeploy,
        "live_member": liveMember == null ? null : liveMember,
        "live_chat_disable": liveChatDisable,
        "live_setpass": liveSetpass,
        "is_hit": isHit,
        "hit_created": hitCreated,
        "is_hit_active": isHitActive,
        "hit_completed": hitCompleted,
        "is_complete": isComplete,
        "complete_created": completeCreated,
        "is_popular": isPopular,
        "popular_created": popularCreated,
        "user": user.toJson(),
        "media_category": mediaCategory.toJson(),
        "views": views,
        "media_id": mediaId,
        "media_thumbnail": mediaThumbnail,
        "live_password": livePassword == null ? null : livePassword,
        "alive": alive,
        "forced_offs": List<dynamic>.from(forcedOffs.map((x) => x)),
        "live_last_update":
            liveLastUpdate == null ? null : liveLastUpdate.toIso8601String(),
        "live_vod_id": liveVodId,
        "live_start_datetime": liveStartDatetime == null
            ? null
            : liveStartDatetime.toIso8601String(),
        "like": like,
        "dis_like": disLike,
        "live_resolution": liveResolution == null ? null : liveResolution,
        "is_converted": isConverted,
        "dc_gallery": dcGallery == null ? null : dcGallery,
        "dc_gallery_name": dcGalleryName == null ? null : dcGalleryName,
        "orientation": orientationValues.reverse[orientation],
        "favorite": favorite == null ? null : favorite,
        "liked": liked == null ? null : liked,
        "disliked": disliked == null ? null : disliked,
      };
}

class MediaCategory {
  MediaCategory({
    this.id,
    this.name,
    this.createDate,
  });

  int id;
  String name;
  DateTime createDate;

  factory MediaCategory.fromJson(Map<String, dynamic> json) => MediaCategory(
        id: json["id"],
        name: json["name"],
        createDate: DateTime.parse(json["create_date"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "create_date": createDate.toIso8601String(),
      };
}

enum Orientation { LANDSCAPE, PORTRAIT }

final orientationValues = EnumValues(
    {"landscape": Orientation.LANDSCAPE, "portrait": Orientation.PORTRAIT});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}

class SearchHistory {
  SearchHistory({
    this.id,
    this.text,
    this.user,
    this.created,
  });

  int id;
  String text;
  DateTime created;
  Profile user;

  factory SearchHistory.fromJson(Map<String, dynamic> json) => SearchHistory(
      id: json["id"],
      user: Profile.fromJson(json["user"]),
      text: json["text"],
      created: DateTime.parse(json["created"]),
    );

  Map<String, dynamic> toJson() => {
      "id": id,
      "user": user.toJson(),
      "text": text,
      "created": created,
    };
}

class SearchHistoryModel {
  SearchHistoryModel({
    this.results,
    this.next,
    this.previous,
    this.count,
    this.totalPages,
  });

  List<SearchHistory> results;
  String next;
  dynamic previous;
  int count;
  int totalPages;

  factory SearchHistoryModel.fromJson(Map<String, dynamic> json) => SearchHistoryModel(
        results:
            List<SearchHistory>.from(json["results"].map((x) => SearchHistory.fromJson(x))),
        next: json["next"],
        previous: json["previous"],
        count: json["count"],
        totalPages: json["total_pages"],
      );

  Map<String, dynamic> toJson() => {
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "next": next,
        "previous": previous,
        "count": count,
        "total_pages": totalPages,
      };
}