// To parse this JSON data, do
//
//     final vod = vodFromJson(jsonString);

import 'dart:convert';

Vod vodFromJson(String str) => Vod.fromJson(json.decode(str));

String vodToJson(Vod data) => json.encode(data.toJson());

class Vod {
  Vod({
    this.results,
    this.next,
    this.previous,
    this.count,
    this.totalPages,
  });

  List<Result> results;
  String next;
  dynamic previous;
  int count;
  int totalPages;

  factory Vod.fromJson(Map<String, dynamic> json) => Vod(
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
        next: json["next"],
        previous: json["previous"],
        count: json["count"],
        totalPages: json["total_pages"],
      );

  Map<String, dynamic> toJson() => {
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "next": next,
        "previous": previous,
        "count": count,
        "total_pages": totalPages,
      };
}

class Result {
  Result({
    this.id,
    this.title,
    this.text,
    this.category,
    this.thumbnail,
    this.sendDatetime,
    this.isRead,
    this.toUser,
    this.contentId,
  });

  int id;
  String title;
  String text;
  String category;
  String thumbnail;
  DateTime sendDatetime;
  bool isRead;
  int toUser;
  String contentId;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        title: json["title"],
        text: json["text"],
        category: json["category"],
        thumbnail: json["thumbnail"],
        sendDatetime: DateTime.parse(json["send_datetime"]),
        isRead: json["is_read"],
        toUser: json["to_user"],
        contentId: json["content_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "text": text,
        "category": category,
        "thumbnail": thumbnail,
        "send_datetime": sendDatetime.toIso8601String(),
        "is_read": isRead,
        "to_user": toUser,
        "content_id": contentId,
      };
}
