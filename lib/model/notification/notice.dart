// To parse this JSON data, do
//
//     final notice = noticeFromJson(jsonString);

import 'dart:convert';

Notice noticeFromJson(String str) => Notice.fromJson(json.decode(str));

String noticeToJson(Notice data) => json.encode(data.toJson());

class Notice {
  Notice({
    this.results,
    this.next,
    this.previous,
    this.count,
    this.totalPages,
  });

  List<Result> results;
  String next;
  dynamic previous;
  int count;
  int totalPages;

  factory Notice.fromJson(Map<String, dynamic> json) => Notice(
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
        next: json["next"],
        previous: json["previous"],
        count: json["count"],
        totalPages: json["total_pages"],
      );

  Map<String, dynamic> toJson() => {
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "next": next,
        "previous": previous,
        "count": count,
        "total_pages": totalPages,
      };
}

class Result {
  Result({
    this.id,
    this.title,
    this.text,
    this.category,
    this.thumbnail,
    this.sendDatetime,
    this.isRead,
    this.toUser,
    this.contentId,
  });

  int id;
  String title;
  String text;
  String category;
  dynamic thumbnail;
  DateTime sendDatetime;
  bool isRead;
  int toUser;
  String contentId;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        title: json["title"],
        text: json["text"],
        category: json["category"],
        thumbnail: json["thumbnail"],
        sendDatetime: DateTime.parse(json["send_datetime"]),
        isRead: json["is_read"],
        toUser: json["to_user"],
        contentId: json["content_id"] == null ? null : json["content_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "text": text,
        "category": category,
        "thumbnail": thumbnail,
        "send_datetime": sendDatetime.toIso8601String(),
        "is_read": isRead,
        "to_user": toUser,
        "content_id": contentId == null ? null : contentId,
      };
}

class PushNotification {
  PushNotification({
    this.title,
    this.body,
    this.type,
    this.from_user_id,
    this.content_id,
    this.timestamp,
    this.thumbnail
  });

  String title;
  String body;
  String type;
  String from_user_id;
  String content_id;
  String timestamp;
  String thumbnail;

  factory PushNotification.fromJson(Map<String, dynamic> json) {
    return PushNotification(
      title: json["notification"]["title"],
      body: json["notification"]["body"],
      type: json["data"]["type"],
      from_user_id: json["data"]["from_user_id"],
      content_id: json["data"]["content_id"],
      timestamp: json["data"]["timestamp"],
      thumbnail: json["data"]["thumbnail"]
    );
  }
}