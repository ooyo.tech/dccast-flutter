// To parse this JSON data, do
//
//     final live = liveFromJson(jsonString);

import 'dart:convert';

Live liveFromJson(String str) => Live.fromJson(json.decode(str));

String liveToJson(Live data) => json.encode(data.toJson());

class Live {
  Live({
    this.results,
    this.next,
    this.previous,
    this.count,
    this.totalPages,
  });

  List<Result> results;
  String next;
  dynamic previous;
  int count;
  int totalPages;

  factory Live.fromJson(Map<String, dynamic> json) => Live(
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
        next: json["next"],
        previous: json["previous"],
        count: json["count"],
        totalPages: json["total_pages"],
      );

  Map<String, dynamic> toJson() => {
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "next": next,
        "previous": previous,
        "count": count,
        "total_pages": totalPages,
      };
}

class Result {
  Result({
    this.id,
    this.title,
    this.text,
    this.category,
    this.thumbnail,
    this.sendDatetime,
    this.isRead,
    this.toUser,
    this.contentId,
  });

  int id;
  Title title;
  String text;
  Category category;
  String thumbnail;
  DateTime sendDatetime;
  bool isRead;
  int toUser;
  String contentId;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        title: titleValues.map[json["title"]],
        text: json["text"],
        category: categoryValues.map[json["category"]],
        thumbnail: json["thumbnail"],
        sendDatetime: DateTime.parse(json["send_datetime"]),
        isRead: json["is_read"],
        toUser: json["to_user"],
        contentId: json["content_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": titleValues.reverse[title],
        "text": text,
        "category": categoryValues.reverse[category],
        "thumbnail": thumbnail,
        "send_datetime": sendDatetime.toIso8601String(),
        "is_read": isRead,
        "to_user": toUser,
        "content_id": contentId,
      };
}

enum Category { NEW_LIVE }

final categoryValues = EnumValues({"NEW_LIVE": Category.NEW_LIVE});

enum Title { LIVE, A3234_LIVE, TC_TO_GO_HVUBIBIN_LIVE }

final titleValues = EnumValues({
  "a3234 님의 LIVE 가 시작 되었습니다.": Title.A3234_LIVE,
  "하하하할 님의 LIVE 가 시작 되었습니다.": Title.LIVE,
  "모어양tc to go hvubibin 님의 LIVE 가 시작 되었습니다.": Title.TC_TO_GO_HVUBIBIN_LIVE
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
