// To parse this JSON data, do
//
//     final userStats = userStatsFromJson(jsonString);

import 'dart:convert';

UserStats userStatsFromJson(String str) => UserStats.fromJson(json.decode(str));

String userStatsToJson(UserStats data) => json.encode(data.toJson());

class UserStats {
  UserStats({
    this.following,
    this.followers,
    this.subscribers,
    this.friends,
  });

  int following;
  int followers;
  int subscribers;
  int friends;

  factory UserStats.fromJson(Map<String, dynamic> json) => UserStats(
        following: json["following"] == null ? null : json["following"],
        followers: json["followers"] == null ? null : json["followers"],
        subscribers: json["subscribers"] == null ? null : json["subscribers"],
        friends: json["friends"] == null ? null : json["friends"],
      );

  Map<String, dynamic> toJson() => {
        "following": following == null ? null : following,
        "followers": followers == null ? null : followers,
        "subscribers": subscribers == null ? null : subscribers,
        "friends": friends == null ? null : friends,
      };
}
