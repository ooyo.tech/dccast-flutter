// To parse this JSON data, do
//
//     final pointTransaction = pointTransactionFromJson(jsonString);

import 'dart:convert';
import 'package:dccast/model/user_media.dart';

import 'user.dart';

PointTransaction pointTransactionFromJson(String str) =>
    PointTransaction.fromJson(json.decode(str));

String pointTransactionToJson(PointTransaction data) =>
    json.encode(data.toJson());

class PointTransaction {
  PointTransaction({
    this.results,
    this.count,
    this.totalPages,
    this.next,
    this.previous,
  });

  List<Transactions> results;
  int count;
  int totalPages;
  dynamic next;
  dynamic previous;

  factory PointTransaction.fromJson(Map<String, dynamic> json) =>
      PointTransaction(
        results: json["results"] == null
            ? null
            : List<Transactions>.from(
                json["results"].map((x) => Transactions.fromJson(x))),
        count: json["count"] == null ? null : json["count"],
        totalPages: json["total_pages"] == null ? null : json["total_pages"],
        next: json["next"],
        previous: json["previous"],
      );

  Map<String, dynamic> toJson() => {
        "results": results == null
            ? null
            : List<dynamic>.from(results.map((x) => x.toJson())),
        "count": count == null ? null : count,
        "total_pages": totalPages == null ? null : totalPages,
        "next": next,
        "previous": previous,
      };
}

class Transactions {
  Transactions({
    this.id,
    this.fromUser,
    this.toUser,
    this.transactionType,
    this.quantity,
    this.media,
    this.created,
  });

  int id;
  Profile fromUser;
  Profile toUser;
  String transactionType;
  int quantity;
  Media media;
  DateTime created;

  factory Transactions.fromJson(Map<String, dynamic> json) => Transactions(
        id: json["id"] == null ? null : json["id"],
        fromUser: json["from_user"] == null
            ? null
            : Profile.fromJson(json["from_user"]),
        toUser:
            json["to_user"] == null ? null : Profile.fromJson(json["to_user"]),
        transactionType:
            json["transaction_type"] == null ? null : json["transaction_type"],
        quantity: json["quantity"] == null ? null : json["quantity"],
        media: json["media"] == null ? null : Media.fromJson(json["media"]),
        created:
            json["created"] == null ? null : DateTime.parse(json["created"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "from_user": fromUser == null ? null : fromUser.toJson(),
        "to_user": toUser == null ? null : toUser.toJson(),
        "transaction_type": transactionType == null ? null : transactionType,
        "quantity": quantity == null ? null : quantity,
        "media": media == null ? null : media.toJson(),
        "created": created == null ? null : created.toIso8601String(),
      };
}
