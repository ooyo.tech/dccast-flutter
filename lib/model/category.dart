// To parse this JSON data, do
//
//     final category = categoryFromJson(jsonString);

import 'dart:convert';

Category categoryFromJson(String str) => Category.fromJson(json.decode(str));

String categoryToJson(Category data) => json.encode(data.toJson());

class Category {
  Category({
    this.results,
    this.count,
    this.totalPages,
    this.next,
    this.previous,
  });

  List<Result> results;
  int count;
  int totalPages;
  dynamic next;
  dynamic previous;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
        count: json["count"],
        totalPages: json["total_pages"],
        next: json["next"],
        previous: json["previous"],
      );

  Map<String, dynamic> toJson() => {
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "count": count,
        "total_pages": totalPages,
        "next": next,
        "previous": previous,
      };
}

class Result {
  Result({
    this.id,
    this.name,
    this.img,
    this.createDate,
  });

  int id;
  String name;
  String img;
  DateTime createDate;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        name: json["name"],
        img: json["img"],
        createDate: DateTime.parse(json["create_date"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "img": img,
        "create_date": createDate.toIso8601String(),
      };
}

class DcGallery {
  DcGallery({
    this.minorGall,
    this.mainGall,
    this.miniGall,
    this.mainRecommGall,
    this.minorRecomGall
  });

  List<DcGalleryItem> minorGall;
  List<DcGalleryItem> mainGall;
  List<DcGalleryItem> miniGall;
  List<DcGalleryItem> mainRecommGall;
  List<DcGalleryItem> minorRecomGall;
  int count;

  factory DcGallery.fromJson(Map<String, dynamic> json) => DcGallery(
    minorGall:List<DcGalleryItem>.from(
      json["minor_gall"].map((x) => DcGalleryItem.fromJson(x))),
    mainGall:List<DcGalleryItem>.from(
      json["main_gall"].map((x) => DcGalleryItem.fromJson(x))),
    miniGall:List<DcGalleryItem>.from(
      json["mini_gall"].map((x) => DcGalleryItem.fromJson(x))),
    mainRecommGall:List<DcGalleryItem>.from(
      json["main_recomm_gall"].map((x) => DcGalleryItem.fromJson(x))),
    minorRecomGall:List<DcGalleryItem>.from(
      json["minor_recomm_gall"].map((x) => DcGalleryItem.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "minor_gall": List<DcGalleryItem>.from(minorGall.map((x) => x.toJson())),
    "main_gall": List<DcGalleryItem>.from(mainGall.map((x) => x.toJson())),
    "mini_gall": List<DcGalleryItem>.from(miniGall.map((x) => x.toJson())),
    "main_recomm_gall": List<DcGalleryItem>.from(mainRecommGall.map((x) => x.toJson())),
    "minor_recomm_gall": List<DcGalleryItem>.from(minorRecomGall.map((x) => x.toJson())),
  };
}

class DcGalleryItem {
  DcGalleryItem({
    this.id,
    this.gallState,
    this.title
  });

  String id;
  String gallState;
  String title;

  factory DcGalleryItem.fromJson(Map<String, dynamic> json) => DcGalleryItem(
    id: json["id"],
    gallState: json["gallState"],
    title: json["title"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "gallState": gallState,
    "title": title,
  };
}
