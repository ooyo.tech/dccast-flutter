// To parse this JSON data, do
//
//     final groupMembers = groupMembersFromJson(jsonString);

import 'dart:convert';
import 'user.dart';

GroupMembers groupMembersFromJson(String str) =>
    GroupMembers.fromJson(json.decode(str));

GroupMembers fromJson(Map<String, dynamic> json) =>
    GroupMembers.fromJson(json);

String groupMembersToJson(GroupMembers data) => json.encode(data.toJson());

class GroupMembers {
  GroupMembers({
    this.count,
    this.totalPages,
    this.previous,
    this.next,
    this.results,
  });

  int count;
  int totalPages;
  dynamic previous;
  dynamic next;
  List<Profile> results;

  factory GroupMembers.fromJson(Map<String, dynamic> json) => GroupMembers(
        count: json["count"] == null ? null : json["count"],
        totalPages: json["total_pages"] == null ? null : json["total_pages"],
        previous: json["previous"],
        next: json["next"],
        results: json["results"] == null
            ? null
            : List<Profile>.from(
                json["results"].map((x) => Profile.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count == null ? null : count,
        "total_pages": totalPages == null ? null : totalPages,
        "previous": previous,
        "next": next,
        "results": results == null
            ? null
            : List<dynamic>.from(results.map((x) => x.toJson())),
      };
}
