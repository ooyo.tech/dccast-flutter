import 'dart:convert';

List<UserMandu> userManduFromJson(String str) =>
    List<UserMandu>.from(json.decode(str).map((x) => UserMandu.fromJson(x)));

String userManduToJson(List<UserMandu> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class UserManduList {
  final List<UserMandu> mandus;

  UserManduList({
    this.mandus,
  });

  factory UserManduList.fromJson(List<dynamic> parsedJson) {
    List<UserMandu> mandus = List<UserMandu>();
    mandus = parsedJson.map((i) => UserMandu.fromJson(i)).toList();

    return UserManduList(mandus: mandus);
  }
}

class UserMandu {
  UserMandu({
    this.result,
    this.cause,
  });

  String result;
  dynamic cause;

  factory UserMandu.fromJson(Map<String, dynamic> json) => UserMandu(
        result: json["result"] == null ? null : json["result"],
        cause: json["cause"] == null ? null : json["cause"],
      );

  Map<String, dynamic> toJson() => {
        "result": result == null ? null : result,
        "cause": cause == null ? null : cause,
      };
}
