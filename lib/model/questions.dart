// To parse this JSON data, do
//
//     final questions = questionsFromJson(jsonString);

import 'dart:convert';
import 'package:dccast/constants/server_settings.dart';

import 'user.dart';

Questions questionsFromJson(String str) => Questions.fromJson(json.decode(str));

String questionsToJson(Questions data) => json.encode(data.toJson());

class Questions {
  Questions({
    this.previous,
    this.totalPages,
    this.next,
    this.results,
    this.count,
  });

  dynamic previous;
  int totalPages;
  dynamic next;
  List<Question> results;
  int count;

  factory Questions.fromJson(Map<String, dynamic> json) => Questions(
        previous: json["previous"],
        totalPages: json["total_pages"],
        next: json["next"],
        results: List<Question>.from(
            json["results"].map((x) => Question.fromJson(x))),
        count: json["count"],
      );

  Map<String, dynamic> toJson() => {
        "previous": previous,
        "total_pages": totalPages,
        "next": next,
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "count": count,
      };
}

class Question {
  Question({
    this.id,
    this.title,
    this.question,
    this.answer,
    this.answerDate,
    this.created,
    this.user,
    this.state,
    this.kinds,
    this.imgPath,
    this.fileName,
  });

  int id;
  String title;
  String question;
  String answer;
  DateTime answerDate;
  DateTime created;
  Profile user;
  String state;
  String kinds;
  String imgPath;
  String fileName;

  factory Question.fromJson(Map<String, dynamic> json) => Question(
        id: json["id"],
        title: json["title"],
        question: json["question"],
        answer: json["answer"],
        answerDate: json["answer_date"] == null
            ? null
            : DateTime.parse(json["answer_date"]),
        created: DateTime.parse(json["created"]),
        user: Profile.fromJson(json["user"]),
        state: json["state"],
        kinds: json["kinds"] == null ? null : json["kinds"],
        imgPath: json["img_path"] == null ? null : 
          (json["img_path"].startsWith('http') 
            ? json["img_path"]
            : ServerSettings.baseUrl + json["img_path"]),
        fileName: json["file_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "question": question,
        "answer": answer,
        "answer_date": answerDate == null ? null : answerDate.toIso8601String(),
        "created": created.toIso8601String(),
        "user": user.toJson(),
        "state": state,
        "kinds": kinds == null ? null : kinds,
        "img_path": imgPath == null ? null : imgPath,
        "file_name": fileName,
      };
}
