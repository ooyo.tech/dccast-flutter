import 'dart:convert';

UserBlock userBlockFromJson(String str) => UserBlock.fromJson(json.decode(str));

String userBlockToJson(UserBlock data) => json.encode(data.toJson());

class UserBlock {
  UserBlock({
    this.blockEndDate,
    this.blockMsg,
    this.division,
    this.isBlock,
  });

  DateTime blockEndDate;
  String blockMsg;
  String division;
  bool isBlock;

  factory UserBlock.fromJson(Map<String, dynamic> json) => UserBlock(
        blockEndDate: json["block_end_date"] == null
            ? null
            : DateTime.parse(json["block_end_date"]),
        blockMsg: json["block_msg"] == null ? null : json["block_msg"],
        division: json["division"] == null ? null : json["division"],
        isBlock: json["is_block"] == null ? null : json["is_block"],
      );

  Map<String, dynamic> toJson() => {
        "block_end_date":
            blockEndDate == null ? null : blockEndDate.toIso8601String(),
        "block_msg": blockMsg == null ? null : blockMsg,
        "division": division == null ? null : division,
        "is_block": isBlock == null ? null : isBlock,
      };
}
