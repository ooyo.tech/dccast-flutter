// To parse this JSON data, do
//
//     final live = liveFromJson(jsonString);

import 'dart:convert';

Live liveFromJson(String str) => Live.fromJson(json.decode(str));

String liveToJson(Live data) => json.encode(data.toJson());

class Live {
  Live({
    this.next,
    this.results,
    this.previous,
    this.totalPages,
    this.count,
  });

  dynamic next;
  List<Result> results;
  dynamic previous;
  int totalPages;
  int count;

  factory Live.fromJson(Map<String, dynamic> json) => Live(
        next: json["next"],
        results: json["results"] == null
            ? null
            : List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
        previous: json["previous"],
        totalPages: json["total_pages"] == null ? null : json["total_pages"],
        count: json["count"] == null ? null : json["count"],
      );

  Map<String, dynamic> toJson() => {
        "next": next,
        "results": results == null
            ? null
            : List<dynamic>.from(results.map((x) => x.toJson())),
        "previous": previous,
        "total_pages": totalPages == null ? null : totalPages,
        "count": count == null ? null : count,
      };
}

class Result {
  Result({
    this.id,
    this.category,
    this.division,
    this.created,
    this.title,
    this.explanation,
    this.mediaType,
    this.duration,
    this.kinds,
    this.liveDeploy,
    this.liveMember,
    this.liveChatDisable,
    this.liveSetpass,
    this.isHit,
    this.hitCreated,
    this.isHitActive,
    this.hitCompleted,
    this.isComplete,
    this.completeCreated,
    this.isPopular,
    this.popularCreated,
    this.user,
    this.mediaCategory,
    this.views,
    this.mediaId,
    this.mediaThumbnail,
    this.livePassword,
    this.alive,
    this.forcedOffs,
    this.liveLastUpdate,
    this.liveVodId,
    this.liveStartDatetime,
    this.like,
    this.disLike,
    this.liveResolution,
    this.isConverted,
    this.dcGallery,
    this.dcGalleryName,
    this.orientation,
  });

  int id;
  String category;
  String division;
  DateTime created;
  String title;
  dynamic explanation;
  String mediaType;
  int duration;
  dynamic kinds;
  String liveDeploy;
  String liveMember;
  bool liveChatDisable;
  bool liveSetpass;
  bool isHit;
  dynamic hitCreated;
  bool isHitActive;
  dynamic hitCompleted;
  bool isComplete;
  dynamic completeCreated;
  bool isPopular;
  dynamic popularCreated;
  ResultUser user;
  MediaCategory mediaCategory;
  int views;
  String mediaId;
  String mediaThumbnail;
  dynamic livePassword;
  bool alive;
  List<dynamic> forcedOffs;
  dynamic liveLastUpdate;
  dynamic liveVodId;
  dynamic liveStartDatetime;
  int like;
  int disLike;
  String liveResolution;
  bool isConverted;
  dynamic dcGallery;
  dynamic dcGalleryName;
  String orientation;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"] == null ? null : json["id"],
        category: json["category"] == null ? null : json["category"],
        division: json["division"] == null ? null : json["division"],
        created:
            json["created"] == null ? null : DateTime.parse(json["created"]),
        title: json["title"] == null ? null : json["title"],
        explanation: json["explanation"],
        mediaType: json["media_type"] == null ? null : json["media_type"],
        duration: json["duration"] == null ? null : json["duration"],
        kinds: json["kinds"],
        liveDeploy: json["live_deploy"] == null ? null : json["live_deploy"],
        liveMember: json["live_member"] == null ? null : json["live_member"],
        liveChatDisable: json["live_chat_disable"] == null
            ? null
            : json["live_chat_disable"],
        liveSetpass: json["live_setpass"] == null ? null : json["live_setpass"],
        isHit: json["is_hit"] == null ? null : json["is_hit"],
        hitCreated: json["hit_created"],
        isHitActive:
            json["is_hit_active"] == null ? null : json["is_hit_active"],
        hitCompleted: json["hit_completed"],
        isComplete: json["is_complete"] == null ? null : json["is_complete"],
        completeCreated: json["complete_created"],
        isPopular: json["is_popular"] == null ? null : json["is_popular"],
        popularCreated: json["popular_created"],
        user: json["user"] == null ? null : ResultUser.fromJson(json["user"]),
        mediaCategory: json["media_category"] == null
            ? null
            : MediaCategory.fromJson(json["media_category"]),
        views: json["views"] == null ? null : json["views"],
        mediaId: json["media_id"] == null ? null : json["media_id"],
        mediaThumbnail:
            json["media_thumbnail"] == null ? null : json["media_thumbnail"],
        livePassword: json["live_password"],
        alive: json["alive"] == null ? null : json["alive"],
        forcedOffs: json["forced_offs"] == null
            ? null
            : List<dynamic>.from(json["forced_offs"].map((x) => x)),
        liveLastUpdate: json["live_last_update"],
        liveVodId: json["live_vod_id"],
        liveStartDatetime: json["live_start_datetime"],
        like: json["like"] == null ? null : json["like"],
        disLike: json["dis_like"] == null ? null : json["dis_like"],
        liveResolution:
            json["live_resolution"] == null ? null : json["live_resolution"],
        isConverted: json["is_converted"] == null ? null : json["is_converted"],
        dcGallery: json["dc_gallery"],
        dcGalleryName: json["dc_gallery_name"],
        orientation: json["orientation"] == null ? null : json["orientation"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "category": category == null ? null : category,
        "division": division == null ? null : division,
        "created": created == null ? null : created.toIso8601String(),
        "title": title == null ? null : title,
        "explanation": explanation,
        "media_type": mediaType == null ? null : mediaType,
        "duration": duration == null ? null : duration,
        "kinds": kinds,
        "live_deploy": liveDeploy == null ? null : liveDeploy,
        "live_member": liveMember == null ? null : liveMember,
        "live_chat_disable": liveChatDisable == null ? null : liveChatDisable,
        "live_setpass": liveSetpass == null ? null : liveSetpass,
        "is_hit": isHit == null ? null : isHit,
        "hit_created": hitCreated,
        "is_hit_active": isHitActive == null ? null : isHitActive,
        "hit_completed": hitCompleted,
        "is_complete": isComplete == null ? null : isComplete,
        "complete_created": completeCreated,
        "is_popular": isPopular == null ? null : isPopular,
        "popular_created": popularCreated,
        "user": user == null ? null : user.toJson(),
        "media_category": mediaCategory == null ? null : mediaCategory.toJson(),
        "views": views == null ? null : views,
        "media_id": mediaId == null ? null : mediaId,
        "media_thumbnail": mediaThumbnail == null ? null : mediaThumbnail,
        "live_password": livePassword,
        "alive": alive == null ? null : alive,
        "forced_offs": forcedOffs == null
            ? null
            : List<dynamic>.from(forcedOffs.map((x) => x)),
        "live_last_update": liveLastUpdate,
        "live_vod_id": liveVodId,
        "live_start_datetime": liveStartDatetime,
        "like": like == null ? null : like,
        "dis_like": disLike == null ? null : disLike,
        "live_resolution": liveResolution == null ? null : liveResolution,
        "is_converted": isConverted == null ? null : isConverted,
        "dc_gallery": dcGallery,
        "dc_gallery_name": dcGalleryName,
        "orientation": orientation == null ? null : orientation,
      };
}

class MediaCategory {
  MediaCategory({
    this.id,
    this.name,
    this.createDate,
  });

  int id;
  String name;
  DateTime createDate;

  factory MediaCategory.fromJson(Map<String, dynamic> json) => MediaCategory(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        createDate: json["create_date"] == null
            ? null
            : DateTime.parse(json["create_date"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "create_date": createDate == null ? null : createDate.toIso8601String(),
      };
}

class ResultUser {
  ResultUser({
    this.id,
    this.user,
    this.userNo,
    this.nameCertification,
    this.autoLogin,
    this.nickName,
    this.stateMessage,
    this.noticeLive,
    this.noticeVod,
    this.noticeChat,
    this.noticeNotice,
    this.noticeSound,
    this.noticeVibration,
    this.phone,
    this.phoneCertification,
    this.limitMobileData,
    this.stopRecentView,
    this.stopRecentSearch,
    this.profileImage,
    this.phoneCertificationDate,
    this.adultCertification,
    this.setPass,
    this.passString,
    this.adultCertificationDate,
    this.lastNameCertification,
    this.emailCertification,
    this.isVip,
    this.isVipActive,
    this.vipCreateDate,
    this.profileOriginalImage,
    this.clientToken,
    this.onAir,
    this.onAirMedia,
    this.isBlock,
    this.blockEndDate,
  });

  int id;
  UserUser user;
  String userNo;
  bool nameCertification;
  bool autoLogin;
  String nickName;
  String stateMessage;
  bool noticeLive;
  bool noticeVod;
  bool noticeChat;
  bool noticeNotice;
  bool noticeSound;
  bool noticeVibration;
  String phone;
  bool phoneCertification;
  bool limitMobileData;
  bool stopRecentView;
  bool stopRecentSearch;
  String profileImage;
  dynamic phoneCertificationDate;
  bool adultCertification;
  bool setPass;
  dynamic passString;
  DateTime adultCertificationDate;
  String lastNameCertification;
  String emailCertification;
  bool isVip;
  bool isVipActive;
  DateTime vipCreateDate;
  String profileOriginalImage;
  String clientToken;
  bool onAir;
  dynamic onAirMedia;
  bool isBlock;
  dynamic blockEndDate;

  factory ResultUser.fromJson(Map<String, dynamic> json) => ResultUser(
        id: json["id"] == null ? null : json["id"],
        user: json["user"] == null ? null : UserUser.fromJson(json["user"]),
        userNo: json["user_no"] == null ? null : json["user_no"],
        nameCertification: json["name_certification"] == null
            ? null
            : json["name_certification"],
        autoLogin: json["auto_login"] == null ? null : json["auto_login"],
        nickName: json["nick_name"] == null ? null : json["nick_name"],
        stateMessage:
            json["state_message"] == null ? null : json["state_message"],
        noticeLive: json["notice_live"] == null ? null : json["notice_live"],
        noticeVod: json["notice_vod"] == null ? null : json["notice_vod"],
        noticeChat: json["notice_chat"] == null ? null : json["notice_chat"],
        noticeNotice:
            json["notice_notice"] == null ? null : json["notice_notice"],
        noticeSound: json["notice_sound"] == null ? null : json["notice_sound"],
        noticeVibration:
            json["notice_vibration"] == null ? null : json["notice_vibration"],
        phone: json["phone"] == null ? null : json["phone"],
        phoneCertification: json["phone_certification"] == null
            ? null
            : json["phone_certification"],
        limitMobileData: json["limit_mobile_data"] == null
            ? null
            : json["limit_mobile_data"],
        stopRecentView:
            json["stop_recent_view"] == null ? null : json["stop_recent_view"],
        stopRecentSearch: json["stop_recent_search"] == null
            ? null
            : json["stop_recent_search"],
        profileImage:
            json["profile_image"] == null ? null : json["profile_image"],
        phoneCertificationDate: json["phone_certification_date"],
        adultCertification: json["adult_certification"] == null
            ? null
            : json["adult_certification"],
        setPass: json["set_pass"] == null ? null : json["set_pass"],
        passString: json["pass_string"],
        adultCertificationDate: json["adult_certification_date"] == null
            ? null
            : DateTime.parse(json["adult_certification_date"]),
        lastNameCertification: json["last_name_certification"] == null
            ? null
            : json["last_name_certification"],
        emailCertification: json["email_certification"] == null
            ? null
            : json["email_certification"],
        isVip: json["is_vip"] == null ? null : json["is_vip"],
        isVipActive:
            json["is_vip_active"] == null ? null : json["is_vip_active"],
        vipCreateDate: json["vip_create_date"] == null
            ? null
            : DateTime.parse(json["vip_create_date"]),
        profileOriginalImage: json["profile_original_image"] == null
            ? null
            : json["profile_original_image"],
        clientToken: json["client_token"] == null ? null : json["client_token"],
        onAir: json["on_air"] == null ? null : json["on_air"],
        onAirMedia: json["on_air_media"],
        isBlock: json["is_block"] == null ? null : json["is_block"],
        blockEndDate: json["block_end_date"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "user": user == null ? null : user.toJson(),
        "user_no": userNo == null ? null : userNo,
        "name_certification":
            nameCertification == null ? null : nameCertification,
        "auto_login": autoLogin == null ? null : autoLogin,
        "nick_name": nickName == null ? null : nickName,
        "state_message": stateMessage == null ? null : stateMessage,
        "notice_live": noticeLive == null ? null : noticeLive,
        "notice_vod": noticeVod == null ? null : noticeVod,
        "notice_chat": noticeChat == null ? null : noticeChat,
        "notice_notice": noticeNotice == null ? null : noticeNotice,
        "notice_sound": noticeSound == null ? null : noticeSound,
        "notice_vibration": noticeVibration == null ? null : noticeVibration,
        "phone": phone == null ? null : phone,
        "phone_certification":
            phoneCertification == null ? null : phoneCertification,
        "limit_mobile_data": limitMobileData == null ? null : limitMobileData,
        "stop_recent_view": stopRecentView == null ? null : stopRecentView,
        "stop_recent_search":
            stopRecentSearch == null ? null : stopRecentSearch,
        "profile_image": profileImage == null ? null : profileImage,
        "phone_certification_date": phoneCertificationDate,
        "adult_certification":
            adultCertification == null ? null : adultCertification,
        "set_pass": setPass == null ? null : setPass,
        "pass_string": passString,
        "adult_certification_date": adultCertificationDate == null
            ? null
            : "${adultCertificationDate.year.toString().padLeft(4, '0')}-${adultCertificationDate.month.toString().padLeft(2, '0')}-${adultCertificationDate.day.toString().padLeft(2, '0')}",
        "last_name_certification":
            lastNameCertification == null ? null : lastNameCertification,
        "email_certification":
            emailCertification == null ? null : emailCertification,
        "is_vip": isVip == null ? null : isVip,
        "is_vip_active": isVipActive == null ? null : isVipActive,
        "vip_create_date": vipCreateDate == null
            ? null
            : "${vipCreateDate.year.toString().padLeft(4, '0')}-${vipCreateDate.month.toString().padLeft(2, '0')}-${vipCreateDate.day.toString().padLeft(2, '0')}",
        "profile_original_image":
            profileOriginalImage == null ? null : profileOriginalImage,
        "client_token": clientToken == null ? null : clientToken,
        "on_air": onAir == null ? null : onAir,
        "on_air_media": onAirMedia,
        "is_block": isBlock == null ? null : isBlock,
        "block_end_date": blockEndDate,
      };
}

class UserUser {
  UserUser({
    this.id,
    this.email,
    this.username,
    this.isSuperuser,
    this.isStaff,
    this.lastLogin,
    this.dateJoined,
    this.firstName,
    this.lastName,
  });

  int id;
  String email;
  String username;
  bool isSuperuser;
  bool isStaff;
  DateTime lastLogin;
  DateTime dateJoined;
  String firstName;
  String lastName;

  factory UserUser.fromJson(Map<String, dynamic> json) => UserUser(
        id: json["id"] == null ? null : json["id"],
        email: json["email"] == null ? null : json["email"],
        username: json["username"] == null ? null : json["username"],
        isSuperuser: json["is_superuser"] == null ? null : json["is_superuser"],
        isStaff: json["is_staff"] == null ? null : json["is_staff"],
        lastLogin: json["last_login"] == null
            ? null
            : DateTime.parse(json["last_login"]),
        dateJoined: json["date_joined"] == null
            ? null
            : DateTime.parse(json["date_joined"]),
        firstName: json["first_name"] == null ? null : json["first_name"],
        lastName: json["last_name"] == null ? null : json["last_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "email": email == null ? null : email,
        "username": username == null ? null : username,
        "is_superuser": isSuperuser == null ? null : isSuperuser,
        "is_staff": isStaff == null ? null : isStaff,
        "last_login": lastLogin == null ? null : lastLogin.toIso8601String(),
        "date_joined": dateJoined == null ? null : dateJoined.toIso8601String(),
        "first_name": firstName == null ? null : firstName,
        "last_name": lastName == null ? null : lastName,
      };
}
