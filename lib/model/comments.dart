// To parse this JSON data, do
//
//     final comments = commentsFromJson(jsonString);

import 'dart:convert';

import 'package:dccast/model/user.dart';

Comments commentsFromJson(String str) => Comments.fromJson(json.decode(str));

String commentsToJson(Comments data) => json.encode(data.toJson());

class Comments {
  Comments({
    this.next,
    this.results,
    this.previous,
    this.count,
    this.totalPages,
  });

  dynamic next;
  List<Comment> results;
  dynamic previous;
  int count;
  int totalPages;

  factory Comments.fromJson(Map<String, dynamic> json) => Comments(
        next: json["next"],
        results: json["results"] == null
            ? null
            : List<Comment>.from(
                json["results"].map((x) => Comment.fromJson(x))),
        previous: json["previous"],
        count: json["count"] == null ? null : json["count"],
        totalPages: json["total_pages"] == null ? null : json["total_pages"],
      );

  Map<String, dynamic> toJson() => {
        "next": next,
        "results": results == null
            ? null
            : List<dynamic>.from(results.map((x) => x.toJson())),
        "previous": previous,
        "count": count == null ? null : count,
        "total_pages": totalPages == null ? null : totalPages,
      };
}

class Comment {
  Comment({
    this.id,
    this.media,
    this.user,
    this.text,
    this.created,
    this.parent,
    this.replies,
    this.like,
    this.disLike,
    this.dcconUrl,
    this.liked,
    this.disliked,
  });

  int id;
  int media;
  Profile user;
  dynamic text;
  DateTime created;
  dynamic parent;
  List<Comment> replies;
  int like;
  int disLike;
  String dcconUrl;
  bool liked;
  bool disliked;

  factory Comment.fromJson(Map<String, dynamic> json) => Comment(
        id: json["id"] == null ? null : json["id"],
        media: json["media"] == null ? null : json["media"],
        user: json["user"] == null ? null : Profile.fromJson(json["user"]),
        text: json["text"],
        created: json["created"] == null ? null 
          : DateTime.parse(json["created"]).toLocal(),
        parent: json["parent"],
        replies: json["replies"] == null
            ? null
          : List<Comment>.from(json["replies"].map((x) => Comment.fromJson(x))),
        like: json["like"] == null ? null : json["like"],
        disLike: json["dis_like"] == null ? null : json["dis_like"],
        dcconUrl: json["dccon_url"] == null ? null : json["dccon_url"],
        liked: json["liked"] == null ? null : json["liked"],
        disliked: json["disliked"] == null ? null : json["disliked"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "media": media == null ? null : media,
        "user": user == null ? null : user.toJson(),
        "text": text,
        "created": created == null ? null : created.toIso8601String(),
        "parent": parent,
        "replies": replies == null
            ? null
            : List<dynamic>.from(replies.map((x) => x.toJson())),
        "like": like == null ? null : like,
        "dis_like": disLike == null ? null : disLike,
        "dccon_url": dcconUrl == null ? null : dcconUrl,
        "liked": liked == null ? null : liked,
        "disliked": disliked == null ? null : disliked,
      };
}

class Reply {
  Reply({
    this.id,
    this.media,
    this.user,
    this.text,
    this.created,
    this.parent,
    this.replies,
    this.like,
    this.disLike,
    this.dcconUrl,
  });

  int id;
  int media;
  Profile user;
  String text;
  DateTime created;
  int parent;
  List<dynamic> replies;
  int like;
  int disLike;
  String dcconUrl;

  factory Reply.fromJson(Map<String, dynamic> json) => Reply(
        id: json["id"] == null ? null : json["id"],
        media: json["media"] == null ? null : json["media"],
        user: json["user"] == null ? null : Profile.fromJson(json["user"]),
        text: json["text"] == null ? null : json["text"],
        created:
            json["created"] == null ? null : DateTime.parse(json["created"]),
        parent: json["parent"] == null ? null : json["parent"],
        replies: json["replies"] == null
            ? null
            : List<dynamic>.from(json["replies"].map((x) => x)),
        like: json["like"] == null ? null : json["like"],
        disLike: json["dis_like"] == null ? null : json["dis_like"],
        dcconUrl: json["dccon_url"] == null ? null : json["dccon_url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "media": media == null ? null : media,
        "user": user == null ? null : user.toJson(),
        "text": text == null ? null : text,
        "created": created == null ? null : created.toIso8601String(),
        "parent": parent == null ? null : parent,
        "replies":
            replies == null ? null : List<dynamic>.from(replies.map((x) => x)),
        "like": like == null ? null : like,
        "dis_like": disLike == null ? null : disLike,
        "dccon_url": dcconUrl == null ? null : dcconUrl,
      };
}


class DcconPage {
  DcconPage({
    this.dccons
  });

  List<Dccon> dccons;

  factory DcconPage.fromJson(dynamic json) => DcconPage(
    dccons: json == null
            ? null
            : List<Dccon>.from(json.map((x) => Dccon.fromJson(x))),
  );

  dynamic toJson() => {
    "dccons": dccons == null ? null : dccons,
  };
}

class Dccon {
  Dccon({
    this.package_idx,
    this.img,
    this.detail_idx,
    this.title
  });

  String package_idx;
  String detail_idx;
  String img;
  String title;

  factory Dccon.fromJson(dynamic json) => Dccon(
        package_idx: json["package_idx"] == null ? null : json["package_idx"],
        detail_idx: json["detail_idx"] == null ? null : json["detail_idx"],
        img: json["img"] == null ? null : json["img"],
        title: json["title"] == null ? null : json["title"],
      );

  dynamic toJson() => {
        "package_idx": package_idx == null ? null : package_idx,
        "detail_idx": detail_idx == null ? null : detail_idx,
        "img": img == null ? null : img,
        "title": title == null ? null : title,
      };
}

class Chat {
  Chat({
    this.userId,
    this.message,
    this.dcconUrl,
    this.type
  });

  dynamic userId;
  dynamic message;
  String dcconUrl;
  String type;

  factory Chat.fromJson(Map<String, dynamic> json) => Chat(
    userId: json["userId"] == null ? null : json["userId"],
    message: json["message"] == null ? null : json["message"],
    dcconUrl: json["dccon_url"] == null ? null : json["dccon_url"],
  );

  Map<String, dynamic> toJson() => {
    "userId": userId == null ? null : userId,
    "message": message == null ? null : message,
    "dccon_url": dcconUrl == null ? null : dcconUrl,
  };
}