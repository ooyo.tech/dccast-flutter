// To parse this JSON data, do
//
//     final Subscribe = SubscribeFromJson(jsonString);

import 'dart:convert';
import 'user.dart';

Subscribe subscribeFromJson(String str) => Subscribe.fromJson(json.decode(str));

String subscribeToJson(Subscribe data) => json.encode(data.toJson());

class Subscribe {
  Subscribe({
    this.results,
    this.count,
    this.totalPages,
    this.next,
    this.previous,
  });

  List<Result> results;
  int count;
  int totalPages;
  dynamic next;
  dynamic previous;

  factory Subscribe.fromJson(Map<String, dynamic> json) => Subscribe(
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
        count: json["count"],
        totalPages: json["total_pages"],
        next: json["next"],
        previous: json["previous"],
      );

  Map<String, dynamic> toJson() => {
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "count": count,
        "total_pages": totalPages,
        "next": next,
        "previous": previous,
      };
}

class Result {
  Result({
    this.id,
    this.fromUser,
    this.toUser,
    this.datetime,
    this.accepted,
  });

  int id;
  Profile fromUser;
  Profile toUser;
  DateTime datetime;
  bool accepted;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        fromUser: Profile.fromJson(json["from_user"]),
        toUser: Profile.fromJson(json["to_user"]),
        datetime: json["datetime"] != null 
          ? DateTime.parse(json["datetime"])
          : DateTime.parse(json["created"]),
        accepted: json["accepted"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "from_user": fromUser.toJson(),
        "to_user": toUser.toJson(),
        "datetime": datetime.toIso8601String(),
        "accepted": accepted,
      };
}
