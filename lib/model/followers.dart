// To parse this JSON data, do
//
//     final followers = followersFromJson(jsonString);

import 'dart:convert';
import 'user.dart';

Followers followersFromJson(String str) => Followers.fromJson(json.decode(str));

String followersToJson(Followers data) => json.encode(data.toJson());

class Followers {
  Followers({
    this.results,
    this.count,
    this.totalPages,
    this.next,
    this.previous,
  });

  List<Result> results;
  int count;
  int totalPages;
  dynamic next;
  dynamic previous;

  factory Followers.fromJson(Map<String, dynamic> json) => Followers(
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
        count: json["count"],
        totalPages: json["total_pages"],
        next: json["next"],
        previous: json["previous"],
      );

  Map<String, dynamic> toJson() => {
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "count": count,
        "total_pages": totalPages,
        "next": next,
        "previous": previous,
      };
}

class Result {
  Result({
    this.id,
    this.userId,
    this.followerId,
    this.followDate,
    this.deleted,
    this.following,
    this.follower,
    this.user,
  });

  int id;
  int userId;
  int followerId;
  String followDate;
  bool deleted;
  bool following;
  Profile follower;
  Profile user;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        userId: json["user_id"],
        followerId: json["follower_id"],
        followDate: json["follow_date"],
        deleted: json["deleted"],
        following: json["following"],
        follower: Profile.fromJson(json["follower"]),
        user: Profile.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "follower_id": followerId,
        "follow_date": followDate,
        "deleted": deleted,
        "following": following,
        "follower": follower.toJson(),
        "user": user.toJson(),
      };
}

class FollowStatus {
  FollowStatus({
    this.id,
    this.userId,
    this.followerId,
    this.followDate,
    this.following,
    this.deleted,
  });

  int id;
  int userId;
  int followerId;
  String followDate;
  bool following;
  bool deleted;

  factory FollowStatus.fromJson(Map<String, dynamic> json) => FollowStatus(
        id: json["id"],
        userId: json["user_id"],
        followerId: json["follower_id"],
        followDate: json["follow_date"],
        following: json["following"],
        deleted: json["deleted"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "follower_id": followerId,
        "follow_date": followDate,
        "following": following,
        "deleted": deleted,
      };
}
