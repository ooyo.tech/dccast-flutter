// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

import 'package:dccast/constants/server_settings.dart';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  User({
    this.token,
    this.profile,
    this.dcinside,
    this.user,
    this.ip,
    this.sessionId,
  });

  String token;
  Profile profile;
  Dcinside dcinside;
  UserClass user;
  String ip;
  String sessionId;

  factory User.fromJson(Map<String, dynamic> json) => User(
        token: json["token"] == null ? null : json["token"],
        profile:
            json["profile"] == null ? null : Profile.fromJson(json["profile"]),
        dcinside: json["dcinside"] == null
            ? null
            : Dcinside.fromJson(json["dcinside"]),
        user: json["user"] == null ? null : UserClass.fromJson(json["user"]),
        ip: json["ip"] == null ? null : json["ip"],
        sessionId: json["session_id"] == null ? null : json["session_id"],
      );

  Map<String, dynamic> toJson() => {
        "token": token == null ? null : token,
        "profile": profile == null ? null : profile.toJson(),
        "dcinside": dcinside == null ? null : dcinside.toJson(),
        "user": user == null ? null : user.toJson(),
        "ip": ip == null ? null : ip,
        "session_id": sessionId == null ? null : sessionId,
      };
}

class Dcinside {
  Dcinside({
    this.mailSend,
    this.pwCampaign,
    this.isDormancy,
    this.name,
    this.userNo,
    this.otpToken,
    this.result,
    this.userId,
    this.isAdult,
    this.stype,
    this.isOtp,
  });

  String mailSend;
  String pwCampaign;
  String isDormancy;
  String name;
  String userNo;
  String otpToken;
  bool result;
  String userId;
  String isAdult;
  String stype;
  String isOtp;

  factory Dcinside.fromJson(Map<String, dynamic> json) => Dcinside(
        mailSend: json["mail_send"] == null ? null : json["mail_send"],
        pwCampaign: json["pw_campaign"] == null ? null : json["pw_campaign"],
        isDormancy: json["is_dormancy"] == null ? null : json["is_dormancy"],
        name: json["name"] == null ? null : json["name"],
        userNo: json["user_no"] == null ? null : json["user_no"],
        otpToken: json["otp_token"] == null ? null : json["otp_token"],
        result: json["result"] == null ? null : json["result"],
        userId: json["user_id"] == null ? null : json["user_id"],
        isAdult: json["is_adult"] == null ? null : json["is_adult"],
        stype: json["stype"] == null ? null : json["stype"],
        isOtp: json["is_otp"] == null ? null : json["is_otp"],
      );

  Map<String, dynamic> toJson() => {
        "mail_send": mailSend == null ? null : mailSend,
        "pw_campaign": pwCampaign == null ? null : pwCampaign,
        "is_dormancy": isDormancy == null ? null : isDormancy,
        "name": name == null ? null : name,
        "user_no": userNo == null ? null : userNo,
        "otp_token": otpToken == null ? null : otpToken,
        "result": result == null ? null : result,
        "user_id": userId == null ? null : userId,
        "is_adult": isAdult == null ? null : isAdult,
        "stype": stype == null ? null : stype,
        "is_otp": isOtp == null ? null : isOtp,
      };
}

class Profile {
  Profile({
    this.id,
    this.user,
    this.userNo,
    this.nameCertification,
    this.autoLogin,
    this.nickName,
    this.stateMessage,
    this.noticeLive,
    this.noticeVod,
    this.noticeChat,
    this.noticeNotice,
    this.noticeSound,
    this.noticeVibration,
    this.phone,
    this.phoneCertification,
    this.limitMobileData,
    this.stopRecentView,
    this.stopRecentSearch,
    this.profileImage,
    this.phoneCertificationDate,
    this.adultCertification,
    this.setPass,
    this.passString,
    this.adultCertificationDate,
    this.lastNameCertification,
    this.emailCertification,
    this.isVip,
    this.isVipActive,
    this.vipCreateDate,
    this.profileOriginalImage,
    this.clientToken,
    this.onAir,
    this.onAirMedia,
    this.isBlock,
    this.blockEndDate,
  });

  int id;
  UserClass user;
  String userNo;
  bool nameCertification;
  bool autoLogin;
  String nickName;
  String stateMessage;
  bool noticeLive;
  bool noticeVod;
  bool noticeChat;
  bool noticeNotice;
  bool noticeSound;
  bool noticeVibration;
  String phone;
  bool phoneCertification;
  bool limitMobileData;
  bool stopRecentView;
  bool stopRecentSearch;
  String profileImage;
  dynamic phoneCertificationDate;
  bool adultCertification;
  bool setPass;
  dynamic passString;
  DateTime adultCertificationDate;
  String lastNameCertification;
  String emailCertification;
  bool isVip;
  bool isVipActive;
  DateTime vipCreateDate;
  String profileOriginalImage;
  String clientToken;
  bool onAir;
  dynamic onAirMedia;
  bool isBlock;
  dynamic blockEndDate;

  factory Profile.fromJson(Map<String, dynamic> json) => Profile(
        id: json["id"] == null ? null : json["id"],
        user: json["user"] == null ? null : UserClass.fromJson(json["user"]),
        userNo: json["user_no"] == null ? null : json["user_no"],
        nameCertification: json["name_certification"] == null
            ? null
            : json["name_certification"],
        autoLogin: json["auto_login"] == null ? null : json["auto_login"],
        nickName: json["nick_name"] == null ? null : json["nick_name"],
        stateMessage:
            json["state_message"] == null ? null : json["state_message"],
        noticeLive: json["notice_live"] == null ? null : json["notice_live"],
        noticeVod: json["notice_vod"] == null ? null : json["notice_vod"],
        noticeChat: json["notice_chat"] == null ? null : json["notice_chat"],
        noticeNotice:
            json["notice_notice"] == null ? null : json["notice_notice"],
        noticeSound: json["notice_sound"] == null ? null : json["notice_sound"],
        noticeVibration:
            json["notice_vibration"] == null ? null : json["notice_vibration"],
        phone: json["phone"] == null ? null : json["phone"],
        phoneCertification: json["phone_certification"] == null
            ? null
            : json["phone_certification"],
        limitMobileData: json["limit_mobile_data"] == null
            ? null
            : json["limit_mobile_data"],
        stopRecentView:
            json["stop_recent_view"] == null ? null : json["stop_recent_view"],
        stopRecentSearch: json["stop_recent_search"] == null
            ? null
            : json["stop_recent_search"],
        profileImage:
            json["profile_image"] == null ? null : 
            (json["profile_image"].startsWith('http') 
              ? json["profile_image"]
              : ServerSettings.baseUrl + json["profile_image"]),
        phoneCertificationDate: json["phone_certification_date"],
        adultCertification: json["adult_certification"] == null
            ? null
            : json["adult_certification"],
        setPass: json["set_pass"] == null ? null : json["set_pass"],
        passString: json["pass_string"],
        adultCertificationDate: json["adult_certification_date"] == null
            ? null
            : DateTime.parse(json["adult_certification_date"]),
        lastNameCertification: json["last_name_certification"] == null
            ? null
            : json["last_name_certification"],
        emailCertification: json["email_certification"] == null
            ? null
            : json["email_certification"],
        isVip: json["is_vip"] == null ? null : json["is_vip"],
        isVipActive:
            json["is_vip_active"] == null ? null : json["is_vip_active"],
        vipCreateDate: json["vip_create_date"] == null
            ? null
            : DateTime.parse(json["vip_create_date"]),
        profileOriginalImage: json["profile_original_image"] == null
            ? null
            : (json["profile_original_image"].startsWith('http') 
              ? json["profile_original_image"]
              : ServerSettings.baseUrl + json["profile_original_image"]),
        clientToken: json["client_token"] == null ? null : json["client_token"],
        onAir: json["on_air"] == null ? null : json["on_air"],
        onAirMedia: json["on_air_media"],
        isBlock: json["is_block"] == null ? null : json["is_block"],
        blockEndDate: json["block_end_date"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "user": user == null ? null : user.toJson(),
        "user_no": userNo == null ? null : userNo,
        "name_certification":
            nameCertification == null ? null : nameCertification,
        "auto_login": autoLogin == null ? null : autoLogin,
        "nick_name": nickName == null ? null : nickName,
        "state_message": stateMessage == null ? null : stateMessage,
        "notice_live": noticeLive == null ? null : noticeLive,
        "notice_vod": noticeVod == null ? null : noticeVod,
        "notice_chat": noticeChat == null ? null : noticeChat,
        "notice_notice": noticeNotice == null ? null : noticeNotice,
        "notice_sound": noticeSound == null ? null : noticeSound,
        "notice_vibration": noticeVibration == null ? null : noticeVibration,
        "phone": phone == null ? null : phone,
        "phone_certification":
            phoneCertification == null ? null : phoneCertification,
        "limit_mobile_data": limitMobileData == null ? null : limitMobileData,
        "stop_recent_view": stopRecentView == null ? null : stopRecentView,
        "stop_recent_search":
            stopRecentSearch == null ? null : stopRecentSearch,
        "profile_image": profileImage == null ? null : profileImage,
        "phone_certification_date": phoneCertificationDate,
        "adult_certification":
            adultCertification == null ? null : adultCertification,
        "set_pass": setPass == null ? null : setPass,
        "pass_string": passString,
        "adult_certification_date": adultCertificationDate == null
            ? null
            : "${adultCertificationDate.year.toString().padLeft(4, '0')}-${adultCertificationDate.month.toString().padLeft(2, '0')}-${adultCertificationDate.day.toString().padLeft(2, '0')}",
        "last_name_certification":
            lastNameCertification == null ? null : lastNameCertification,
        "email_certification":
            emailCertification == null ? null : emailCertification,
        "is_vip": isVip == null ? null : isVip,
        "is_vip_active": isVipActive == null ? null : isVipActive,
        "vip_create_date": vipCreateDate == null
            ? null
            : "${vipCreateDate.year.toString().padLeft(4, '0')}-${vipCreateDate.month.toString().padLeft(2, '0')}-${vipCreateDate.day.toString().padLeft(2, '0')}",
        "profile_original_image":
            profileOriginalImage == null ? null : profileOriginalImage,
        "client_token": clientToken == null ? null : clientToken,
        "on_air": onAir == null ? null : onAir,
        "on_air_media": onAirMedia,
        "is_block": isBlock == null ? null : isBlock,
        "block_end_date": blockEndDate,
      };
}

class UserClass {
  UserClass({
    this.id,
    this.email,
    this.username,
    this.isSuperuser,
    this.isStaff,
    this.lastLogin,
    this.dateJoined,
    this.firstName,
    this.lastName,
  });

  int id;
  String email;
  String username;
  bool isSuperuser;
  bool isStaff;
  DateTime lastLogin;
  DateTime dateJoined;
  String firstName;
  String lastName;

  factory UserClass.fromJson(Map<String, dynamic> json) => UserClass(
        id: json["id"] == null ? null : json["id"],
        email: json["email"] == null ? null : json["email"],
        username: json["username"] == null ? null : json["username"],
        isSuperuser: json["is_superuser"] == null ? null : json["is_superuser"],
        isStaff: json["is_staff"] == null ? null : json["is_staff"],
        lastLogin: json["last_login"] == null
            ? null
            : DateTime.parse(json["last_login"]),
        dateJoined: json["date_joined"] == null
            ? null
            : DateTime.parse(json["date_joined"]),
        firstName: json["first_name"] == null ? null : json["first_name"],
        lastName: json["last_name"] == null ? null : json["last_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "email": email == null ? null : email,
        "username": username == null ? null : username,
        "is_superuser": isSuperuser == null ? null : isSuperuser,
        "is_staff": isStaff == null ? null : isStaff,
        "last_login": lastLogin == null ? null : lastLogin.toIso8601String(),
        "date_joined": dateJoined == null ? null : dateJoined.toIso8601String(),
        "first_name": firstName == null ? null : firstName,
        "last_name": lastName == null ? null : lastName,
      };
}
