// To parse this JSON data, do
//
//     final friends = friendsFromJson(jsonString);

import 'dart:convert';
import 'user.dart';

Friends friendsFromJson(String str) => Friends.fromJson(json.decode(str));

String friendsToJson(Friends data) => json.encode(data.toJson());

class Friends {
  Friends({
    this.results,
    this.count,
    this.totalPages,
    this.next,
    this.previous,
  });

  List<Result> results;
  int count;
  int totalPages;
  dynamic next;
  dynamic previous;

  factory Friends.fromJson(Map<String, dynamic> json) => Friends(
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
        count: json["count"],
        totalPages: json["total_pages"],
        next: json["next"],
        previous: json["previous"],
      );

  Map<String, dynamic> toJson() => {
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "count": count,
        "total_pages": totalPages,
        "next": next,
        "previous": previous,
      };
}

class Result {
  Result({
    this.id,
    this.fromUser,
    this.toUser,
    this.datetime,
    this.accepted,
  });

  int id;
  Profile fromUser;
  Profile toUser;
  DateTime datetime;
  bool accepted;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        fromUser: Profile.fromJson(json["from_user"]),
        toUser: Profile.fromJson(json["to_user"]),
        datetime: DateTime.parse(json["datetime"]),
        accepted: json["accepted"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "from_user": fromUser.toJson(),
        "to_user": toUser.toJson(),
        "datetime": datetime.toIso8601String(),
        "accepted": accepted,
      };
}

class FriendProfile extends Profile {

  int id;
  UserClass user;
  String userNo;
  bool nameCertification;
  bool autoLogin;
  String nickName;
  String stateMessage;
  bool noticeLive;
  bool noticeVod;
  bool noticeChat;
  bool noticeNotice;
  bool noticeSound;
  bool noticeVibration;
  String phone;
  bool phoneCertification;
  bool limitMobileData;
  bool stopRecentView;
  bool stopRecentSearch;
  String profileImage;
  dynamic phoneCertificationDate;
  bool adultCertification;
  bool setPass;
  dynamic passString;
  DateTime adultCertificationDate;
  String lastNameCertification;
  String emailCertification;
  bool isVip;
  bool isVipActive;
  DateTime vipCreateDate;
  String profileOriginalImage;
  String clientToken;
  bool onAir;
  dynamic onAirMedia;
  bool isBlock;
  dynamic blockEndDate;
  String status;

  FriendProfile({
    this.id,
    this.user,
    this.userNo,
    this.nameCertification,
    this.autoLogin,
    this.nickName,
    this.stateMessage,
    this.noticeLive,
    this.noticeVod,
    this.noticeChat,
    this.noticeNotice,
    this.noticeSound,
    this.noticeVibration,
    this.phone,
    this.phoneCertification,
    this.limitMobileData,
    this.stopRecentView,
    this.stopRecentSearch,
    this.profileImage,
    this.phoneCertificationDate,
    this.adultCertification,
    this.setPass,
    this.passString,
    this.adultCertificationDate,
    this.lastNameCertification,
    this.emailCertification,
    this.isVip,
    this.isVipActive,
    this.vipCreateDate,
    this.profileOriginalImage,
    this.clientToken,
    this.onAir,
    this.onAirMedia,
    this.isBlock,
    this.blockEndDate,
    this.status
    });

  factory FriendProfile.fromJson(Map<String, dynamic> json) => FriendProfile(
        id: json["id"] == null ? null : json["id"],
        user: json["user"] == null ? null : UserClass.fromJson(json["user"]),
        userNo: json["user_no"] == null ? null : json["user_no"],
        nameCertification: json["name_certification"] == null
            ? null
            : json["name_certification"],
        autoLogin: json["auto_login"] == null ? null : json["auto_login"],
        nickName: json["nick_name"] == null ? null : json["nick_name"],
        stateMessage:
            json["state_message"] == null ? null : json["state_message"],
        noticeLive: json["notice_live"] == null ? null : json["notice_live"],
        noticeVod: json["notice_vod"] == null ? null : json["notice_vod"],
        noticeChat: json["notice_chat"] == null ? null : json["notice_chat"],
        noticeNotice:
            json["notice_notice"] == null ? null : json["notice_notice"],
        noticeSound: json["notice_sound"] == null ? null : json["notice_sound"],
        noticeVibration:
            json["notice_vibration"] == null ? null : json["notice_vibration"],
        phone: json["phone"] == null ? null : json["phone"],
        phoneCertification: json["phone_certification"] == null
            ? null
            : json["phone_certification"],
        limitMobileData: json["limit_mobile_data"] == null
            ? null
            : json["limit_mobile_data"],
        stopRecentView:
            json["stop_recent_view"] == null ? null : json["stop_recent_view"],
        stopRecentSearch: json["stop_recent_search"] == null
            ? null
            : json["stop_recent_search"],
        profileImage:
            json["profile_image"] == null ? null : json["profile_image"],
        phoneCertificationDate: json["phone_certification_date"],
        adultCertification: json["adult_certification"] == null
            ? null
            : json["adult_certification"],
        setPass: json["set_pass"] == null ? null : json["set_pass"],
        passString: json["pass_string"],
        adultCertificationDate: json["adult_certification_date"] == null
            ? null
            : DateTime.parse(json["adult_certification_date"]),
        lastNameCertification: json["last_name_certification"] == null
            ? null
            : json["last_name_certification"],
        emailCertification: json["email_certification"] == null
            ? null
            : json["email_certification"],
        isVip: json["is_vip"] == null ? null : json["is_vip"],
        isVipActive:
            json["is_vip_active"] == null ? null : json["is_vip_active"],
        vipCreateDate: json["vip_create_date"] == null
            ? null
            : DateTime.parse(json["vip_create_date"]),
        profileOriginalImage: json["profile_original_image"] == null
            ? null
            : json["profile_original_image"],
        clientToken: json["client_token"] == null ? null : json["client_token"],
        onAir: json["on_air"] == null ? null : json["on_air"],
        onAirMedia: json["on_air_media"],
        isBlock: json["is_block"] == null ? null : json["is_block"],
        blockEndDate: json["block_end_date"],
    status: json["status"] == null ? null : json["status"],
  );
}