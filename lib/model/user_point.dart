// To parse this JSON data, do
//
//     final userPoint = userPointFromJson(jsonString);

import 'dart:convert';
import 'user.dart';

UserPoint userPointFromJson(String str) => UserPoint.fromJson(json.decode(str));

String userPointToJson(UserPoint data) => json.encode(data.toJson());

class UserPoint {
  UserPoint({
    this.results,
    this.next,
    this.previous,
    this.count,
    this.totalPages,
  });

  List<Result> results;
  dynamic next;
  dynamic previous;
  int count;
  int totalPages;

  factory UserPoint.fromJson(Map<String, dynamic> json) => UserPoint(
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
        next: json["next"],
        previous: json["previous"],
        count: json["count"],
        totalPages: json["total_pages"],
      );

  Map<String, dynamic> toJson() => {
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "next": next,
        "previous": previous,
        "count": count,
        "total_pages": totalPages,
      };

  get point {
    return results[0].value.toString();
  }
  set point (double pp) {
    results[0].value = pp;
  }
}

class Result {
  Result({
    this.id,
    this.user,
    this.value,
    this.lastUpdate,
  });

  int id;
  Profile user;
  double value;
  DateTime lastUpdate;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        user: Profile.fromJson(json["user"]),
        value: json["value"],
        lastUpdate: DateTime.parse(json["last_update"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user": user.toJson(),
        "value": value,
        "last_update": lastUpdate.toIso8601String(),
      };
}
