// To parse this JSON data, do
//
//     final vodThumbUpload = vodThumbUploadFromJson(jsonString);

import 'dart:convert';

VodThumbUpload vodThumbUploadFromJson(String str) =>
    VodThumbUpload.fromJson(json.decode(str));

String vodThumbUploadToJson(VodThumbUpload data) => json.encode(data.toJson());

class VodThumbUpload {
  VodThumbUpload({
    this.fieldname,
    this.originalname,
    this.encoding,
    this.mimetype,
    this.destination,
    this.filename,
    this.path,
    this.size,
  });

  String fieldname;
  String originalname;
  String encoding;
  String mimetype;
  String destination;
  String filename;
  String path;
  int size;

  factory VodThumbUpload.fromJson(Map<String, dynamic> json) => VodThumbUpload(
        fieldname: json["fieldname"] == null ? null : json["fieldname"],
        originalname:
            json["originalname"] == null ? null : json["originalname"],
        encoding: json["encoding"] == null ? null : json["encoding"],
        mimetype: json["mimetype"] == null ? null : json["mimetype"],
        destination: json["destination"] == null ? null : json["destination"],
        filename: json["filename"] == null ? null : json["filename"],
        path: json["path"] == null ? null : json["path"],
        size: json["size"] == null ? null : json["size"],
      );

  Map<String, dynamic> toJson() => {
        "fieldname": fieldname == null ? null : fieldname,
        "originalname": originalname == null ? null : originalname,
        "encoding": encoding == null ? null : encoding,
        "mimetype": mimetype == null ? null : mimetype,
        "destination": destination == null ? null : destination,
        "filename": filename == null ? null : filename,
        "path": path == null ? null : path,
        "size": size == null ? null : size,
      };
}
