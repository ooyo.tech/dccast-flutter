// To parse this JSON data, do
//
//     final vodVideoUpload = vodVideoUploadFromJson(jsonString);

import 'dart:convert';

VodVideoUpload vodVideoUploadFromJson(String str) =>
    VodVideoUpload.fromJson(json.decode(str));

String vodVideoUploadToJson(VodVideoUpload data) => json.encode(data.toJson());

class VodVideoUpload {
  VodVideoUpload({
    this.fieldname,
    this.originalname,
    this.encoding,
    this.mimetype,
    this.destination,
    this.filename,
    this.path,
    this.size,
    this.duration,
    this.orientation,
  });

  String fieldname;
  String originalname;
  String encoding;
  String mimetype;
  String destination;
  String filename;
  String path;
  int size;
  int duration;
  String orientation;

  factory VodVideoUpload.fromJson(Map<String, dynamic> json) => VodVideoUpload(
        fieldname: json["fieldname"] == null ? null : json["fieldname"],
        originalname:
            json["originalname"] == null ? null : json["originalname"],
        encoding: json["encoding"] == null ? null : json["encoding"],
        mimetype: json["mimetype"] == null ? null : json["mimetype"],
        destination: json["destination"] == null ? null : json["destination"],
        filename: json["filename"] == null ? null : json["filename"],
        path: json["path"] == null ? null : json["path"],
        size: json["size"] == null ? null : json["size"],
        duration: json["duration"] == null ? null : json["duration"],
        orientation: json["orientation"] == null ? null : json["orientation"],
      );

  Map<String, dynamic> toJson() => {
        "fieldname": fieldname == null ? null : fieldname,
        "originalname": originalname == null ? null : originalname,
        "encoding": encoding == null ? null : encoding,
        "mimetype": mimetype == null ? null : mimetype,
        "destination": destination == null ? null : destination,
        "filename": filename == null ? null : filename,
        "path": path == null ? null : path,
        "size": size == null ? null : size,
        "duration": duration == null ? null : duration,
        "orientation": orientation == null ? null : orientation,
      };
}
