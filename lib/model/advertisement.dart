// To parse this JSON data, do
//
//     final advertisement = advertisementFromJson(jsonString);

import 'dart:convert';
import 'user.dart';

Advertisement advertisementFromJson(String str) =>
    Advertisement.fromJson(json.decode(str));

String advertisementToJson(Advertisement data) => json.encode(data.toJson());

class Advertisement {
  Advertisement({
    this.result,
  });

  Result result;

  factory Advertisement.fromJson(Map<String, dynamic> json) => Advertisement(
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
      );

  Map<String, dynamic> toJson() => {
        "result": result == null ? null : result.toJson(),
      };
}

class Result {
  Result({
    this.id,
    this.title,
    this.adverType,
    this.content,
    this.fullShow,
    this.locate,
    this.affil,
    this.file,
    this.userRate,
    this.numberOfClick,
    this.showCategory,
    this.state,
    this.deployDate,
    this.user,
    this.createDate,
    this.skipDuration
  });

  int id;
  String title;
  String adverType;
  String content;
  bool fullShow;
  Locate locate;
  Affil affil;
  String file;
  String userRate;
  int numberOfClick;
  List<ShowCategory> showCategory;
  String state;
  dynamic deployDate;
  Profile user;
  DateTime createDate;
  int skipDuration;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"] == null ? null : json["id"],
        title: json["title"] == null ? null : json["title"],
        adverType: json["adver_type"] == null ? null : json["adver_type"],
        content: json["content"] == null ? null : json["content"],
        fullShow: json["full_show"] == null ? null : json["full_show"],
        locate: json["locate"] == null ? null : Locate.fromJson(json["locate"]),
        affil: json["affil"] == null ? null : Affil.fromJson(json["affil"]),
        file: json["file"] == null ? null : json["file"],
        userRate: json["user_rate"] == null ? null : json["user_rate"],
        numberOfClick:
            json["number_of_click"] == null ? null : json["number_of_click"],
        showCategory: json["show_category"] == null
            ? null
            : List<ShowCategory>.from(
                json["show_category"].map((x) => ShowCategory.fromJson(x))),
        state: json["state"] == null ? null : json["state"],
        deployDate: json["deploy_date"],
        user: json["user"] == null ? null : Profile.fromJson(json["user"]),
        createDate: json["create_date"] == null
            ? null
            : DateTime.parse(json["create_date"]),
        skipDuration: json["skip_duration"] == null ? null : json["skip_duration"]
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "title": title == null ? null : title,
        "adver_type": adverType == null ? null : adverType,
        "content": content == null ? null : content,
        "full_show": fullShow == null ? null : fullShow,
        "locate": locate == null ? null : locate.toJson(),
        "affil": affil == null ? null : affil.toJson(),
        "file": file == null ? null : file,
        "user_rate": userRate == null ? null : userRate,
        "number_of_click": numberOfClick == null ? null : numberOfClick,
        "show_category": showCategory == null
            ? null
            : List<dynamic>.from(showCategory.map((x) => x.toJson())),
        "state": state == null ? null : state,
        "deploy_date": deployDate,
        "user": user == null ? null : user.toJson(),
        "create_date": createDate == null ? null : createDate.toIso8601String(),
        "skip_duration": skipDuration == null ? null : skipDuration,
      };
}

class Affil {
  Affil({
    this.id,
    this.companyName,
    this.adverName,
    this.user,
    this.managerName,
    this.contact,
    this.phone,
    this.email,
    this.site,
    this.remarks,
    this.createDate,
  });

  int id;
  String companyName;
  String adverName;
  int user;
  String managerName;
  String contact;
  String phone;
  String email;
  String site;
  String remarks;
  DateTime createDate;

  factory Affil.fromJson(Map<String, dynamic> json) => Affil(
        id: json["id"] == null ? null : json["id"],
        companyName: json["company_name"] == null ? null : json["company_name"],
        adverName: json["adver_name"] == null ? null : json["adver_name"],
        user: json["user"] == null ? null : json["user"],
        managerName: json["manager_name"] == null ? null : json["manager_name"],
        contact: json["contact"] == null ? null : json["contact"],
        phone: json["phone"] == null ? null : json["phone"],
        email: json["email"] == null ? null : json["email"],
        site: json["site"] == null ? null : json["site"],
        remarks: json["remarks"] == null ? null : json["remarks"],
        createDate: json["create_date"] == null
            ? null
            : DateTime.parse(json["create_date"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "company_name": companyName == null ? null : companyName,
        "adver_name": adverName == null ? null : adverName,
        "user": user == null ? null : user,
        "manager_name": managerName == null ? null : managerName,
        "contact": contact == null ? null : contact,
        "phone": phone == null ? null : phone,
        "email": email == null ? null : email,
        "site": site == null ? null : site,
        "remarks": remarks == null ? null : remarks,
        "create_date": createDate == null ? null : createDate.toIso8601String(),
      };
}

class Locate {
  Locate({
    this.id,
    this.title,
    this.mediaType,
    this.sizeWidth,
    this.sizeHeight,
    this.locateId,
    this.createDate,
    this.user,
  });

  int id;
  String title;
  String mediaType;
  String sizeWidth;
  String sizeHeight;
  String locateId;
  DateTime createDate;
  int user;

  factory Locate.fromJson(Map<String, dynamic> json) => Locate(
        id: json["id"] == null ? null : json["id"],
        title: json["title"] == null ? null : json["title"],
        mediaType: json["media_type"] == null ? null : json["media_type"],
        sizeWidth: json["size_width"] == null ? null : json["size_width"],
        sizeHeight: json["size_height"] == null ? null : json["size_height"],
        locateId: json["locate_id"] == null ? null : json["locate_id"],
        createDate: json["create_date"] == null
            ? null
            : DateTime.parse(json["create_date"]),
        user: json["user"] == null ? null : json["user"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "title": title == null ? null : title,
        "media_type": mediaType == null ? null : mediaType,
        "size_width": sizeWidth == null ? null : sizeWidth,
        "size_height": sizeHeight == null ? null : sizeHeight,
        "locate_id": locateId == null ? null : locateId,
        "create_date": createDate == null ? null : createDate.toIso8601String(),
        "user": user == null ? null : user,
      };
}

class ShowCategory {
  ShowCategory({
    this.id,
    this.name,
    this.createDate,
  });

  int id;
  String name;
  DateTime createDate;

  factory ShowCategory.fromJson(Map<String, dynamic> json) => ShowCategory(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        createDate: json["create_date"] == null
            ? null
            : DateTime.parse(json["create_date"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "create_date": createDate == null ? null : createDate.toIso8601String(),
      };
}
