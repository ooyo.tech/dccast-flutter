// To parse this JSON data, do
//
//     final pointHistory = pointHistoryFromJson(jsonString);

import 'dart:convert';
import 'user.dart';

PointHistory pointHistoryFromJson(String str) =>
    PointHistory.fromJson(json.decode(str));

String pointHistoryToJson(PointHistory data) => json.encode(data.toJson());

class PointHistory {
  PointHistory({
    this.results,
    this.count,
    this.totalPages,
    this.next,
    this.previous,
  });

  List<Histories> results;
  int count;
  int totalPages;
  dynamic next;
  dynamic previous;

  factory PointHistory.fromJson(Map<String, dynamic> json) => PointHistory(
        results: json["results"] == null
            ? null
            : List<Histories>.from(
                json["results"].map((x) => Histories.fromJson(x))),
        count: json["count"] == null ? null : json["count"],
        totalPages: json["total_pages"] == null ? null : json["total_pages"],
        next: json["next"],
        previous: json["previous"],
      );

  Map<String, dynamic> toJson() => {
        "results": results == null
            ? null
            : List<dynamic>.from(results.map((x) => x.toJson())),
        "count": count == null ? null : count,
        "total_pages": totalPages == null ? null : totalPages,
        "next": next,
        "previous": previous,
      };
}

class Histories {
  Histories({
    this.id,
    this.user,
    this.rate,
    this.points,
    this.mandus,
    this.created,
  });

  int id;
  Profile user;
  int rate;
  String points;
  int mandus;
  DateTime created;

  factory Histories.fromJson(Map<String, dynamic> json) => Histories(
        id: json["id"] == null ? null : json["id"],
        user: json["user"] == null ? null : Profile.fromJson(json["user"]),
        rate: json["rate"] == null ? null : json["rate"],
        points: json["points"].toString() == null
            ? null
            : json["points"].toString(),
        mandus: json["mandus"] == null ? null : json["mandus"],
        created:
            json["created"] == null ? null : DateTime.parse(json["created"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "user": user == null ? null : user.toJson(),
        "rate": rate == null ? null : rate,
        "points": points == null ? null : points,
        "mandus": mandus == null ? null : mandus,
        "created": created == null ? null : created.toIso8601String(),
      };
}
