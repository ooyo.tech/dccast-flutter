import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:timezone/timezone.dart';

import '../constants/preferences.dart';
import '../constants/server_settings.dart';
import '../model/user.dart';
import '../model/user_mandu.dart';
import '../model/user_point.dart';
import 'dio_client.dart';
import 'dio_error.dart';

class Auth extends ChangeNotifier {
  String token = '';
  final storage = FlutterSecureStorage();

  bool authenticated = false;
  bool autoLoginStatus = true;
  bool lteStatus = true;
  User authenticatedUser;
  UserPoint _userPoint;
  UserMandu _userMandu;

  get loggedIn {
    return authenticated;
  }

  get currentUser {
    // print(authenticatedUser.user.userna);
    return authenticatedUser;
  }

  UserPoint get userPoint {
    return _userPoint;
  }

  UserMandu get userMandu {
    return _userMandu;
  }

  Profile get userProfile {
    if (authenticatedUser == null) {
      return null;
    }
    return authenticatedUser.profile;
  }

  // Future<void> dcInsideLogin() async {
  //   String url = ServerSettings.dcInsideLoginURL;
  //   // user_id -> username, user_pw
  // }
  //
  // Future<void> checkAdultCertificate() async {
  //   @Headers({
  //     "Content-Type: application/x-www-form-urlencoded",
  //     "User-Agent: dcinside.castapp"
  //   })
  //   Map<String, dynamic> data = {"company_code": "", "userCode": ""};
  // }
  //
  Future<void> fetchPublicKey() async {
    DateFormat publicKeyFormat = DateFormat("yyyyMMddHH");
    final currentSeoulTime =
        new TZDateTime.from(DateTime.now(), getLocation('Asia/Seoul'));

    String formattedDate = publicKeyFormat.format(currentSeoulTime);

    var valueTokenStringBytes = utf8.encode("dcCastchk_" + formattedDate);
    var valueToken = sha256.convert(valueTokenStringBytes).toString();

    Map<String, dynamic> data = {
      "value_token": valueToken,
      "signature": Preferences.SIGNATURE,
      "pkg": Preferences.APPLICATION_ID,
      "vName": Preferences.VERSION_NAME
    };

    final dio = Dio();
    dio.options.headers['content-type'] = 'application/x-www-form-urlencoded';
    dio.options.headers['user-agent'] = 'dcinside.castapp';

    String appID = await storage.read(key: Preferences.appID);
    String appIDTime = await storage.read(key: Preferences.appIDTime);

    if (appIDTime != null &&
            currentSeoulTime.difference(DateTime.parse(appIDTime)).inHours >
                9 ||
        appID == null) {
      try {
        final response = await DioClient(dio)
            .post(ServerSettings.dcInsideGenerateKey, data: data);

        await storage.write(
            key: Preferences.appID, value: json.decode(response)[0]["app_id"]);
        await storage.write(
            key: Preferences.appIDTime, value: currentSeoulTime.toString());
      } on Exception catch (e) {}
    }
  }

  Future<dynamic> signin({Map data, Function success, Function error}) async {
    final dio = Dio();

    try {
      var url = ServerSettings.loginURL;
      final response = await DioClient(dio).post(url, data: data);
      this.authenticatedUser = User.fromJson(response);

      this.authenticated = true;
      await storage.write(key: Preferences.username, value: data["username"]);
      await storage.write(key: Preferences.password, value: data["password"]);

      _setStoredToken(authenticatedUser.token);
      _setUserID(authenticatedUser.user.id);
      this.autLogin(autoLoginStatus);
      fetchPointData(authenticatedUser.user.id.toString());
      fetchPublicKey();
      success();
      notifyListeners();
    } on Exception catch (e) {
      error(DioErrorUtil.handleError(e));
    }
  }

  void attempt({token = ''}) async {
    if (token.toString().isNotEmpty) {
      this.token = token;
    }
    if (this.token.toString().isEmpty) {
      return;
    }

    fetchPublicKey();
    String autoLogin = await storage.read(key: Preferences.auth_login);
    String username = await storage.read(key: Preferences.username);
    String password = await storage.read(key: Preferences.password);
    if (autoLogin != null && username != null && password != null) {
      signin(data: {
        'username': username,
        'password': password,
      }, success: () => {}, error: () => {});
      fetchPointData(await storage.read(key: Preferences.user_id));
    }

    if (authenticatedUser == null) {
      notifyListeners();
      _setUnauthorized();
    }
  }

  Future<void> fetchPointData(String userID) async {
    final dio = Dio();
    try {
      var pointURL = ServerSettings.getUserPointsURL + userID;
      final pointResponse = await DioClient(dio).get(pointURL);
      print("userID:" + userID);

      if (_userPoint == null) {
        _userPoint = UserPoint.fromJson(pointResponse);
      }

      print("mandu start here");
      Map data = {
        "user_id": authenticatedUser?.dcinside?.userId,
        "app_id": await storage.read(key: Preferences.appID)
      };

      dio.options.headers['content-type'] = 'application/x-www-form-urlencoded';
      dio.options.headers['user-agent'] = 'dcinside.castapp';

      var manduURL = ServerSettings.mobileDcIdUrl + "/api/dccast/cash_total";

      final manduResponse = await DioClient(dio).post(manduURL, data: data);

      print("manduResponse:" + manduResponse.toString());

      UserManduList _userManduList = UserManduList.fromJson(manduResponse);
      _userMandu = _userManduList.mandus[0];

      notifyListeners();
    } on DioError catch (error) {
      print(error.response.toString());
    }
  }

  void signout({Function success}) async {
    final dio = Dio();

    try {
      await DioClient(dio).post(ServerSettings.logoutURL);
      _setUnauthorized();
      notifyListeners();
      success();
    } catch (e) {
      //
    }
  }

  void _setUnauthorized() async {
    this.authenticated = false;
    this.authenticatedUser = null;
    await storage.delete(key: Preferences.auth_token);
    await storage.delete(key: Preferences.user_id);
  }

  void _setStoredToken(String token) async {
    this.token = token;
    await storage.write(key: Preferences.auth_token, value: token);
  }

  void _setUserID(int userID) async {
    await storage.write(key: Preferences.user_id, value: userID.toString());
  }

  void autLogin(bool status) async {
    if (status) {
      autoLoginStatus = true;
      await storage.write(key: Preferences.auth_login, value: "saved");
    } else {
      autoLoginStatus = false;
      await storage.delete(key: Preferences.auth_login);
    }
    notifyListeners();
  }

  void connectivityStatus(bool status) async {
    //
    if (status) {
      lteStatus = true;
      await storage.write(key: Preferences.connectivity, value: "enabled");
    } else {
      lteStatus = false;
      await storage.delete(key: Preferences.connectivity);
    }
    notifyListeners();
  }

  Future<dynamic> refreshDetail() async {
    final dio = Dio();

    try {
      var url = ServerSettings.meURL;
      final response = await DioClient(dio).get(url);
      authenticatedUser = User.fromJson(response);

      notifyListeners();
    } on Exception catch (e) {
      print(e);
    }
  }

}
