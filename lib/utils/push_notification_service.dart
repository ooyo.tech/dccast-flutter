import 'dart:convert';
import 'dart:io';

import 'package:dccast/model/user.dart';
import 'package:dccast/utils/auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:overlay_support/overlay_support.dart';
import 'package:provider/provider.dart';

import '../constants/preferences.dart';
import '../constants/server_settings.dart';
import '../model/notification/notice.dart';
import '../screens/castlist/follower_search.dart';
import 'popup_notification.dart';

Future<dynamic> _firebaseMessagingBackgroundHandler(
  RemoteMessage message) async {
  // Initialize the Firebase app
  await Firebase.initializeApp();
  // var notif = PushNotification.fromJson(message.data);
  print('onBackgroundMessage received: $message.data');
  // print(notif.content_id);
}

class PushNotificationService {
  // final FirebaseMessaging _fcm;

  PushNotificationService();

  Future initialise(BuildContext context) async {
    if (Platform.isIOS) {
      // _fcm.requestNotificationPermissions(IosNotificationSettings());
    }
    
    await Firebase.initializeApp();
    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        print("on message");
        _showNotice(context, message);
    });
    getToken(context);
  }

  void _showNotice(context, message) {
    var notif = PushNotification.fromJson(message);
    if ((notif.type == "NOTICE" && 
      Provider.of<Auth>(context, listen: false).userProfile.noticeNotice)
      || (notif.type == "LIVE" && 
      Provider.of<Auth>(context, listen: false).userProfile.noticeLive)
      || (notif.type == "VOD" && 
      Provider.of<Auth>(context, listen: false).userProfile.noticeVod)
      ) {
    showOverlayNotification((context) {
      return PopupNotification(
        notif: notif
      );
    });
    }
  }

  Future<void> getToken(BuildContext context) async {
    var token = await FirebaseMessaging.instance.getAPNSToken();
    var user = Provider.of<Auth>(context, listen: false).userProfile;
    if (user != null && user.clientToken != token) {
      final storage = FlutterSecureStorage();
      var userID = await storage.read(key: Preferences.user_id);
      var authToken = await storage.read(key: Preferences.auth_token);

      try {
        var url = ServerSettings.updateProfileURL + userID;
        var jsonStr = jsonEncode({
          "client_token": token,
          "user_no": userID,
          "user": userID
        });
        print(jsonStr);

        http.put(Uri.parse("$url/"), body: jsonStr , headers: {
            "Authorization" : "Token $authToken",
            "Content-Type": "application/json"
          }).then((result) {
          print(result.statusCode);
          print(result.body);
        });

      } on Exception {
      }
    }
  }
}