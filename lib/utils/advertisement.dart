import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

import '../constants/server_settings.dart';
import '../model/advertisement.dart';
import 'dio_client.dart';
import 'dio_error.dart';

/// Advertisement related provider utilities
class AdvertisementUtil extends ChangeNotifier {
  Advertisement advertisement;

  /// Fetch ads based on give data
  Future<dynamic> fetchAd({Map data, Function success, Function error}) async {
    final dio = Dio();

    try {
      final response =
          await DioClient(dio).post(ServerSettings.getAdsURL, data: data); 
      advertisement = Advertisement.fromJson(response);

      success();
      notifyListeners();
      return "";
    } on Exception catch (e) {
      error(DioErrorUtil.handleError(e));
    }
  }
}
