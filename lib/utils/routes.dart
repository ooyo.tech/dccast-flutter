import 'package:dccast/screens/castlist/castline_history.dart';
import 'package:dccast/screens/castlist/find_friends.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:provider/provider.dart';

import '../components/comment/comment_edit.dart';
import '../constants/preferences.dart';
import '../constants/styles.dart';
import '../screens/castlist/cast_list.dart';
import '../screens/castlist/follower_search.dart';
import '../screens/castlist/following_search.dart';
import '../screens/castlist/following_user_search_screen.dart';
import '../screens/castlist/friends.dart';
import '../screens/castlist/friends_request.dart';
import '../screens/castlist/friends_search.dart';
import '../screens/castlist/group_add.dart';
import '../screens/castlist/group_detail.dart';
import '../screens/castlist/group_list.dart';
import '../screens/castlist/group_members.dart';
import '../screens/castlist/group_search.dart';
import '../screens/category.dart';
import '../screens/comment.dart';
import '../screens/favorites.dart';
import '../screens/home.dart';
import '../screens/live/add_live.dart';
import '../screens/live/start_live.dart';
import '../screens/login.dart';
import '../screens/mandu_history.dart';
import '../screens/my_content/friends.dart';
import '../screens/my_content/my_content.dart';
import '../screens/my_content/recent.dart';
import '../screens/my_content/subscriptions.dart';
import '../screens/notification/notifications.dart';
import '../screens/point_mandu/points_index.dart';
import '../screens/profile.dart';
import '../screens/profile_edit.dart';
import '../screens/search_screen.dart';
import '../screens/settings.dart';
import '../screens/settings/history_privacy.dart';
import '../screens/settings/notice_settings.dart';
import '../screens/settings/privacy_policy.dart';
import '../screens/settings/question.dart';
import '../screens/settings/terms_of_use.dart';
import 'socket.dart';
import '../screens/splash.dart';
import '../screens/video_detail.dart';
import '../screens/video_list.dart';
import '../screens/vod/add_vod.dart';
import '../screens/vod/add_vod_camera.dart';
import '../screens/vod/add_vod_record.dart';
import '../screens/vod/edit_vod.dart';
import '../screens/vod/search_dcgallery.dart';
import 'auth.dart';
import 'time_helper_service.dart';

class Routes extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RoutesState();
  }
}

class RoutesState extends State<Routes> {
  final storage = FlutterSecureStorage();
  void _tryToAuthenticate() async {
    String token = await storage.read(key: Preferences.auth_token);
    if (token != null) {
      print("try auth");
      Provider.of<Auth>(context, listen: false).attempt(token: token);
    }
  }

  @override
  void initState() {
    _tryToAuthenticate();
    TimeHelperService().setup();
    super.initState();
  }

  // static final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
    
  Widget build(BuildContext context) {
    var localizationDelegate = LocalizedApp.of(context).delegate;
    // final pushNotificationService = 
    //   PushNotificationService(_firebaseMessaging);
    // pushNotificationService.initialise(context);

    return LocalizationProvider(
      state: LocalizationProvider.of(context).state,
      child: OverlaySupport(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'DCCast',
        theme: ThemeData.light().copyWith(primaryColor: kPrimaryColor, appBarTheme: AppBarTheme(
          color: kPrimaryColor
        )),
        initialRoute: SplashScreen.id,
        locale: const Locale('ko', ''),
        supportedLocales: localizationDelegate.supportedLocales,
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          localizationDelegate
        ],
        routes: {
          SplashScreen.id: (context) => SplashScreen(),
          LoginScreen.id: (context) => LoginScreen(),
          CommentScreen.id: (context) => CommentScreen(),
          CommentEditScreen.id: (context) => CommentEditScreen(),
          HomeScreen.id: (context) => HomeScreen(),
          SearchScreen.id: (context) => SearchScreen(),
          CategoryScreen.id: (context) => CategoryScreen(),
          NotificationsScreen.id: (context) => NotificationsScreen(),
          SettingsScreen.id: (context) => SettingsScreen(),
          FavoritesScreen.id: (context) => FavoritesScreen(),
          ProfileScreen.id: (context) => ProfileScreen(),
          ProfileEditScreen.id: (context) => ProfileEditScreen(),
          RecentScreen.id: (context) => RecentScreen(),
          SubscriptionScreen.id: (context) => SubscriptionScreen(),
          QuestionScreen.id: (context) => QuestionScreen(),
          MyContentScreen.id: (context) => MyContentScreen(),
          VideoDetailScreen.id: (context) => VideoDetailScreen(),
          AddLiveScreen.id: (context) => AddLiveScreen(),
          StartLiveScreen.id: (context) => StartLiveScreen(),
          StartVodScreen.id: (context) => StartVodScreen(),
          EditVodScreen.id: (context) => EditVodScreen(),
          PointsScreen.id: (context) => PointsScreen(),
          CameraApp.id: (context) => CameraApp(),
          AddVodCameraScreen.id: (context) => AddVodCameraScreen(),
          SearchDcGalleryScreen.id: (context) => SearchDcGalleryScreen(),
          // PipScreen.id: (context) => PipScreen(),,
          CastListScreen.id: (context) => CastListScreen(),
          CastListFriends.id: (context) => CastListFriends(),
          FollowerSearch.id: (context) => FollowerSearch(),
          FollowingUserSearchScreen.id: (context) =>
              FollowingUserSearchScreen(),
          FollowingSearch.id: (context) => FollowingSearch(),
          FriendsScreen.id: (context) => FriendsScreen(),
          FriendsSearchScreen.id: (context) => FriendsSearchScreen(),
          FindFriendsScreen.id: (context) => FindFriendsScreen(),
          FriendRequestScreen.id: (context) => FriendRequestScreen(),
          GroupMembers.id: (context) => GroupMembers(),
          GroupSearchScreen.id: (context) => GroupSearchScreen(),
          GroupListScreen.id: (context) => GroupListScreen(),
          GroupDetailScreen.id: (context) => GroupDetailScreen(),
          GroupAddScreen.id: (context) => GroupAddScreen(),
          ManduHistoryScreen.id: (context) => ManduHistoryScreen(),
          // VideoLiveTestScreen.id: (context) => VideoLiveTestScreen(),
          VideoListScreen.id: (context) => VideoListScreen(),
          NoticeSettingScreen.id: (context) => NoticeSettingScreen(),
          HistoryPrivacyScreen.id: (context) => HistoryPrivacyScreen(),
          TermsOfUseScreen.id: (context) => TermsOfUseScreen(),
          PrivacyPolicyScreen.id: (context) => PrivacyPolicyScreen(),
          CastlineHistory.id: (context) => CastlineHistory()
        },
      ),
    )
    );
  }
}
