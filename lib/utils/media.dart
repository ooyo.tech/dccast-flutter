import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../constants/preferences.dart';
import '../constants/server_settings.dart';
import '../constants/styles.dart';
import '../model/user_media.dart';
import 'dio_client.dart';
import 'dio_error.dart';
import 'toast.dart';

class MediaUtils extends ChangeNotifier {
  String token = '';
  final storage = FlutterSecureStorage();
  bool inPipMode = false;
  bool isInSmallMode = false;
  UserMedia hitVodMedia;
  UserMedia hitLiveMedia;
  UserMedia vodListModel;
  UserMedia vodListRecentModel;
  UserMedia liveListModel;
  UserMedia liveListRecentModel;
  UserMedia searchLiveModel;
  UserMedia searchVodModel;
  String searchText;
  SearchHistoryModel searchHistoryModel;

  Media _selectedMedia;
  get getHitVodResults {
    return hitVodMedia.results;
  }

  get getHitLiveResults {
    print("hit live");
    return hitLiveMedia.results;
  }

  Media get chosenMedia {
    return _selectedMedia;
  }

  get clearSearch {
    searchLiveModel?.results?.clear();
    searchVodModel?.results?.clear();
    notifyListeners();
  }

  mediaDetail(Media selected, eContent content) {
    Future.delayed(Duration(milliseconds: 250), () {
      _selectedMedia = selected;
      if (!inPipMode) enablePip();
    });

    notifyListeners();
  }

  removeMedia(Media selected, eContent content) {
    if (content == eContent.vod) {
      vodListModel.results.remove(selected);
    } else {
      liveListModel.results.remove(selected);
    }

    notifyListeners();
  }

  enablePip() {
    inPipMode = true;

    print("$inPipMode enablePip");
    notifyListeners();
  }

  disablePip() {
    inPipMode = false;
    print("$inPipMode disablePip");
    notifyListeners();
  }

  enableSmallMode() {
    isInSmallMode = true;
    notifyListeners();
  }

  disableSmallMode() {
    isInSmallMode = false;
    notifyListeners();
  }

  Future<dynamic> fetchHitVodMedia({Function success, Function error}) async {
    final dio = Dio();
    // String userID = await storage.read(key: Preferences.user_id);
    try {
      final response = await DioClient(dio).get(ServerSettings.getHitVodURL);
      hitVodMedia = UserMedia.fromJson(response);
      success();
      notifyListeners();
      return "sucessful";
    } on Exception catch (e) {
      error(DioErrorUtil.handleError(e));
    }
  }

  Future<dynamic> fetchHitLiveMedia({Function success, Function error}) async {
    final dio = Dio();
    // String userID = await storage.read(key: Preferences.user_id);
    try {
      final response = await DioClient(dio).get(ServerSettings.getHitLiveURL);
      hitLiveMedia = UserMedia.fromJson(response);
      success();
      notifyListeners();
      return "sucessful";
    } on Exception catch (e) {
      error(DioErrorUtil.handleError(e));
    }
  }

  Future<dynamic> fetchMedias(
      {String options, eContent type, Function success, Function error}) async {
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      /// Options
      /// 1. category=VOD|LIVE for recent list
      /// 2. ordering=-view for top list
      String optionUser = userID != null ? "&user_id=" + userID : "";
      String url = ServerSettings.getMediaURL + "?" + options + optionUser;

      final response = await DioClient(dio).get(url);
      if (eContent.live == type) {
        print("live fetch");
        liveListModel = UserMedia.fromJson(response);
      } else {
        print("vod fetch");
        vodListModel = UserMedia.fromJson(response);
      }
      success();
      notifyListeners();
      return "sucessful";
    } on Exception catch (e) {
      error(DioErrorUtil.handleError(e));
    }
  }

  Future<dynamic> fetchRecentMedias(
      {String options, eContent type, Function success, Function error}) async {
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      /// Options
      /// 1. category=VOD|LIVE for recent list
      /// 2. ordering=-view for top list
      String optionUser = userID != null ? "&user_id=" + userID : "";
      String url = ServerSettings.getMediaURL + "?" + options + optionUser;

      final response = await DioClient(dio).get(url);
      if (eContent.live == type) {
        print("live fetch");
        liveListRecentModel = UserMedia.fromJson(response);
      } else {
        print("vod fetch");
        vodListRecentModel = UserMedia.fromJson(response);
      }
      success();
      notifyListeners();
      return "sucessful";
    } on Exception catch (e) {
      error(DioErrorUtil.handleError(e));
    }
  }

  Future<dynamic> fetchSearchMedias(
      {String text, eContent type, Function success, Function error}) async {

    try {
      if (searchHistoryModel == null) {
        searchHistoryModel = SearchHistoryModel(
          results: []
        );
      }
      searchHistoryModel.results.insert(0, SearchHistory(
        text: text
      ));

      success();
      notifyListeners();
      return "sucessful";
    } on Exception catch (e) {
      error(DioErrorUtil.handleError(e));
    }
  }

  Future<bool> fetchMedia({dynamic mediaID}) async {
    final dio = Dio();

    String userID = await storage.read(key: Preferences.user_id);

    try {
      var url = ServerSettings.getMediaURL + "/$mediaID";
      // Map<String, dynamic> data = {"id": mediaID};
      print(url);
      final response = await DioClient(dio).get(url);
      // UserMedia newMedia = UserMedia.fromJson(response);
      // _selectedMedia = newMedia.results[0];
      _selectedMedia = Media.fromJson(response);
      notifyListeners();
      return true;
    } on Exception catch (error) {
      print(error.toString());
      return false;
    }
  }

  Future<void> likeMedia(Media media) async {
    final dio = Dio();

    String userID = await storage.read(key: Preferences.user_id);

    try {
      var url = ServerSettings.getMediaLikeURL;
      Map<String, dynamic> data = {
        "media": media.id,
        "user": userID,
        "is_like": !media.liked,
        "is_dislike": false
      };
      print("like:" + data.toString());
      await DioClient(dio).post(url, data: data).then((value) {
        fetchMedia(mediaID: media.id);
        var msg = "좋아요를 취소하셨습니다";
        if (_selectedMedia.liked) {
          msg = "좋아요를하셨습니다";
        }
        showToast(msg);
      });

      notifyListeners();
    } on Exception catch (error) {
      showToast(error.toString());
      print(error.toString());
    }
  }

  Future<void> dislikeMedia(Media media) async {
    final dio = Dio();

    String userID = await storage.read(key: Preferences.user_id);

    try {
      var url = ServerSettings.getMediaLikeURL;
      Map<String, dynamic> data = {
        "media": media.id,
        "user": userID,
        "is_dislike": !media.disliked,
        "is_like": false
      };
      await DioClient(dio).post(url, data: data).then((value) {
        fetchMedia(mediaID: media.id);
        var msg = "싫어요를 하셨습니다";
        if (_selectedMedia.disliked) {
          msg = "싫어요를 취소하셨습니다";
        }

        showToast(msg);
      });

      notifyListeners();
    } on Exception catch (error) {
      showToast(error.toString());

      print(error.toString());
    }
  }

  // Handling this on server side
  // Future<dynamic> createSeachHistory(int userID, String text) async {
  //   final dio = Dio();
  //   try {
  //     final response = await DioClient(dio).post(
  //       ServerSettings.getUserCreateRecentURL,
  //       data: {
  //       "user": userID,
  //       "text": text,
  //       }
  //     );
  //     final history = SearchHistory.fromJson(response);
  //     if (searchHistoryModel == null) {
  //       searchHistoryModel = SearchHistoryModel();
  //     }
  //     searchHistoryModel.results.insert(0, history);
  //     return true;
  //   } on Exception catch (e) {
  //     return false;
  //   }
  // }

  Future<void> fetchSearchHistory() async {
    final dio = Dio();

    var userID = await storage.read(key: Preferences.user_id);

    try {
      var url = "${ServerSettings.getUserRecentSearchURL}?user=$userID&ordering=-created";
      final response = await DioClient(dio).get(url);
      searchHistoryModel = SearchHistoryModel.fromJson(response);
      notifyListeners();
      return true;
    } on Exception catch (error) {
      print(error.toString());
      return false;
    }
  }

  Future<void> deleteSearchHistory(int index) async {
    final dio = Dio();
    try {
      searchHistoryModel.results.removeAt(index);
      notifyListeners();
      // ignore: lines_longer_than_80_chars
      await DioClient(dio).delete("${ServerSettings.deleteUserRecentSearchURL}${searchHistoryModel.results[index].id}/")
        .then((value) {
        });
    } on Exception catch (error) {
    }
  }
}
