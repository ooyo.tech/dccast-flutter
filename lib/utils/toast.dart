import 'package:fluttertoast/fluttertoast.dart';
import '../constants/styles.dart';

void showToast(String message) {
  Fluttertoast.showToast(
    msg: message,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
    timeInSecForIosWeb: 1,
    backgroundColor: kWhiteColor,
    textColor: kBlackColor,
    fontSize: 16.0);
}