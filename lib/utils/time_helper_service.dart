import 'package:timezone/timezone.dart';
import 'package:flutter/services.dart';

/// Fetch datetime global UTC
class TimeHelperService {
  TimeHelperService() {
    setup();
  }
  void setup() async {
    var byteData = await rootBundle.load('assets/timezone/2020d.tzf');
    initializeDatabase(byteData.buffer.asUint8List());
  }
}
