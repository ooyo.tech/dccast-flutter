import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../constants/preferences.dart';
import '../constants/server_settings.dart';
import '../constants/styles.dart';
import '../model/category.dart';
import '../model/user_media.dart';
import 'dio_client.dart';
import 'dio_error.dart';

class CategoryUtils extends ChangeNotifier {
  String token = '';
  final storage = FlutterSecureStorage();

  Category categories;
  UserMedia categoredMedias;

  get availableCategories {
    return categories;
  }

  get categoryResults {
    if (categories == null) {
      return [];
    }
    return categories.results;
  }

  Future<dynamic> fetchCategories({Function success, Function error}) async {
    final dio = Dio();
    // String userID = await storage.read(key: Preferences.user_id);
    try {
      final response = await DioClient(dio).get(ServerSettings.getCategoryURL);
      categories = Category.fromJson(response);
      success();
      notifyListeners();
      return "sucessful";
    } on Exception catch (e) {
      error(DioErrorUtil.handleError(e));
    }
  }

  Future<dynamic> fetchMediaByCategory(
      {eContent type, int categoryID, Function success, Function error}) async {
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      String url = ServerSettings.getMediaURL +
          "?category=" +
          type.toString() +
          "&media_category=" +
          categoryID.toString() +
          "&is_hit_active=0&is_popular=0";
      if (userID.isNotEmpty) {
        url = url + "&user_id=" + userID;
      }

      final response = await DioClient(dio).get(ServerSettings.getHitLiveURL);
      categoredMedias = UserMedia.fromJson(response);
      success();
      notifyListeners();
      return "sucessful";
    } on Exception catch (e) {
      error(DioErrorUtil.handleError(e));
    }
  }
}
