import 'package:flutter_pagewise/flutter_pagewise.dart';

class EditablePagewiseLoadController<T> extends PagewiseLoadController<T> {

  EditablePagewiseLoadController({PageFuture<T> pageFuture, int pageSize})
    : super(pageFuture: pageFuture, pageSize: pageSize);


  void removeAt(int index) {
    this.loadedItems.removeAt(index);
    this.notifyListeners();
  }

  void removeItem(bool Function(T item) test) {
    this.loadedItems.removeWhere(test);
    this.notifyListeners();
  }

  void addItem(T item) {
    this.loadedItems.add(item);
    this.notifyListeners();
  }

  void updateItem(bool Function(T item) test, T newItem) {
    int index = this.loadedItems.indexWhere(test);
    if (index >= 0) {
      this.loadedItems.replaceRange(index, index + 1, [newItem]);
      this.notifyListeners();
    }
  }
}
