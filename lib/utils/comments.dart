
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../constants/preferences.dart';
import '../constants/server_settings.dart';
import '../model/comments.dart';
import 'dio_client.dart';

class CommentsUtil extends ChangeNotifier {
  Comments _comments;
  final storage = FlutterSecureStorage();

  Comments get comments {
    return _comments;
  }

  Future<void> fetchComment({int mediaID}) async {
    return fetchCommentWithParent(mediaID: mediaID, parentID: null);
  }

  Future<void> fetchCommentWithParent({int mediaID, int parentID}) async {
    final dio = Dio();

    String userID = await storage.read(key: Preferences.user_id);

    try {
      var url = ServerSettings.getCommentListURL;
      Map<String, dynamic> data = {
        "media": mediaID,
      };
      if (parentID != null) {
        data['parent_id'] = parentID;
      }
      final response = await DioClient(dio).get(url, queryParameters: data);
      _comments = Comments.fromJson(response);
      notifyListeners();
    } on Exception catch (error) {
      print(error.toString());
    }
  }

  Future<void> likeComment(Comment comment) async {
    final dio = Dio();

    String userID = await storage.read(key: Preferences.user_id);

    try {
      var url = ServerSettings.getCommentLikeURL;
      Map<String, dynamic> data = {
        "comment": comment.id,
        "user": userID,
        "is_like": true
      };
      await DioClient(dio).post(url, data: data).then((value) {
        fetchComment(mediaID: comment.media);
      });

      notifyListeners();
    } on Exception catch (error) {
      print(error.toString());
    }
  }

  Future<void> dislikeComment(Comment comment) async {
    final dio = Dio();

    String userID = await storage.read(key: Preferences.user_id);

    try {
      var url = ServerSettings.getCommentLikeURL;
      Map<String, dynamic> data = {
        "comment": comment.id,
        "user": userID,
        "is_dislike": true
      };
      final response = await DioClient(dio).post(url, data: data).then((value) {
        fetchComment(mediaID: comment.media);
      });
      print("respone:" + response.toString());
      notifyListeners();
    } on Exception catch (error) {
      print(error.toString());
    }
  }

  Future<void> deleteComment(Comment comment) async {
    final dio = Dio();
    print(comment.id);
    try {
      var url =
          ServerSettings.getDeleteCommentURL + comment.id.toString() + "/";

      await DioClient(dio).delete(url).then((value) {
        fetchComment(mediaID: comment.media);
      });
      notifyListeners();
    } on Exception catch (error) {
      print(error.toString());
    }
  }
}
