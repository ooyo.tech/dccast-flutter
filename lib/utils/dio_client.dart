import 'package:dccast/constants/preferences.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class DioClient {
  // dio instance
  final Dio _dio;
  final storage = FlutterSecureStorage();

  /// injecting dio instance
  DioClient(this._dio);

  Future<void> intercept() async {
    var authToken = await storage.read(key: Preferences.auth_token);
    if (authToken != null && authToken != "authToken") {
      _dio.interceptors.add(
        InterceptorsWrapper(onRequest: (options,
        requestInterceptorHandler) async {
          assert(options != null);
        var customHeaders = {
          'Authorization': 'Token $authToken'
        };
        options.headers.addAll(customHeaders);
        return requestInterceptorHandler.next(options);
      }));
    }
  }

  // Get:-----------------------------------------------------------------------
  Future<dynamic> get(
    String uri, {
    Map<String, dynamic> queryParameters,
    options,
    CancelToken cancelToken,
    ProgressCallback onReceiveProgress,
  }) async {
    await intercept();
    try {
      final Response response = await _dio.get(
        uri,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onReceiveProgress: onReceiveProgress,
      );

      return response.data;
    } catch (e) {
      throw e;
    }
  }

  /// Delete:------------------------------------------------------------------
  Future<dynamic> delete(
    String uri, {
    data,
    Map<String, dynamic> queryParameters,
    options,
    CancelToken cancelToken,
    ProgressCallback onSendProgress,
    ProgressCallback onReceiveProgress,
  }) async {
    await intercept();
    try {
      final Response response = await _dio.delete(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
      );
      return response.data;
    } catch (e) {
      print("error:" + e.toString());
      throw e;
    }
  }

  // Post:---------------------------------------------------------------------
  Future<dynamic> post(
    String uri, {
    data,
    Map<String, dynamic> queryParameters,
    options,
    CancelToken cancelToken,
    ProgressCallback onSendProgress,
    ProgressCallback onReceiveProgress,
  }) async {
    await intercept();
    try {
      final Response response = await _dio.post(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );
      return response.data;
    } on DioError catch (e) {
      print("error:" + e.response.toString());
      throw e;
    }
  }

  Future<dynamic> postWithStatus(
    String uri, {
    data,
    Map<String, dynamic> queryParameters,
    options,
    CancelToken cancelToken,
    ProgressCallback onSendProgress,
    ProgressCallback onReceiveProgress,
  }) async {
    await intercept();
    try {
      final Response response = await _dio.post(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );
      return response;
    } on DioError catch (e) {
      print("error:" + e.response.toString());
      throw e;
    }
  }
}
