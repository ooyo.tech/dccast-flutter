import 'package:dccast/constants/styles.dart';
import 'package:dccast/model/notification/notice.dart';
import 'package:dccast/screens/video_detail.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:provider/provider.dart';

import 'media.dart';

class PopupNotification extends StatelessWidget {
  final PushNotification notif;

  // ignore: public_member_api_docs
  const PopupNotification({
    Key key,
    this.notif
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: kPrimaryColor,
      margin: const EdgeInsets.symmetric(horizontal: 0),
      child: SafeArea(
        child: GestureDetector(
          onTap: () {
            OverlaySupportEntry.of(context).dismiss();
            if (notif.type == "VOD" || notif.type == "LIVE") {
              Provider.of<MediaUtils>(context, listen: false)
                .fetchMedia(mediaID: int.parse(notif.content_id));
              Navigator.push(context,
                MaterialPageRoute(builder: (context) => 
                  VideoDetailScreen(content: 
                    notif.type == "VOD" ? eContent.vod : eContent.live
                  ),
                )
              );
            }
          },
          child: ListTile(
            leading: SizedBox.fromSize(
                size: const Size(40, 40),
                child: 
                  notif.thumbnail != null ?
                  ClipOval(child: Image.network(notif.thumbnail))
                  : Container()
            ),
            title: Text(notif.title, style: TextStyle(color: Colors.white)),
            subtitle: Text(notif.body, style: TextStyle(color: Colors.white))
          ),
        ),
      ),
    );
  }
}