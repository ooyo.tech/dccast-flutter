import 'package:dccast/constants/preferences.dart';
import 'package:dccast/model/friends.dart' as Friends;
import 'package:dccast/model/group_media_list.dart';
import 'package:dccast/model/user.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'dio_client.dart';
import 'dio_error.dart';

import '../constants/server_settings.dart';
import '../model/group_list.dart';

class GroupUtil extends ChangeNotifier {
  GroupMediaList vodList;
  GroupMediaList liveList;
  GroupList groupList;
  GroupList userGroupList;
  final storage = FlutterSecureStorage();
  Group _selectedGroup;
  List<Profile> _selectedMembers;
  List<Profile> searchFollowers;
  List<Friends.FriendProfile> searchFriends;
  List<Friends.Result> friendRequests;

  GroupList get groups {
    return groupList;
  }

  get clearChosenGroup {
    _selectedGroup = new Group();
    notifyListeners();
  }

  Group get chosenGroup {
    return _selectedGroup;
  }

  selectGroup(Group selected) {
    _selectedGroup = selected;
  }

  List<Profile> get selectedMembers {
    return _selectedMembers;
  }

  get clearMembers {
    if (_selectedMembers != null) {
      _selectedMembers.clear();
    }
    notifyListeners();
  }

  selectGroupMembers(List<Profile> friends) {
    _selectedMembers = List<Profile>();
    _selectedMembers.addAll(friends);
    notifyListeners();
  }

  Future<void> searchFromGroups() {
    String url = ServerSettings.getGroupsURL;
    //user -> userID, keyword -> text, page -> page id
  }

  Future<void> createGroup() {
    // name -> group name, message -> group desc, contact_person -> userID ,
    //members -> selected userIDs [], file -> picture
  }

  Future<void> updateGroup() {
    String url = ServerSettings.getGroupsURL + "{group_id}" + "/";
    // group_id,  name -> group name, message -> group desc,
    //contact_person -> userID ,
    //members -> selected userIDs [], file -> picture
  }

  Future<void> updateGroupMembers() {
    String url = ServerSettings.getGroupsURL + "{group_id}" + "/";
    // group_id, members
  }

  Future<void> fetchGroupVideos() {
    String url =
        ServerSettings.getGroupsURL + "{group_id}" + "/" + "{type}" + "/";
    // group_id, type, user_id, page
  }

  Future<void> fetchGroupMemberID() {
    String url = ServerSettings.getGroupsURL + "{group_id}" + "/members";
    // group_id, user_id, page
  }

  Future<void> leaveGroup() {
    String url = ServerSettings.getGroupsURL + "{group_id}" + "/" + "members/";
    //  group_id, member_id
  }

  get clearSearch {
    searchFollowers?.clear();
    notifyListeners();
  }

  String _latestFriendSearchText = "";
  Future<dynamic> fetchSearchFriend(
      {String text, Function success, Function error}) async {
    _latestFriendSearchText = text;
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      String url = ServerSettings.getFriendSearchURL;

      Map<String, dynamic> data = {"user_id": userID, "keyword": text};
      final response = await DioClient(dio).post(url, data: data);
      if (_latestFriendSearchText != text) {
        return;
      }

      searchFriends = List<Friends.FriendProfile>.from(
        response["results"].map((x) => Friends.FriendProfile.fromJson(x)));

      success(response["totalPages"]);
      notifyListeners();
      return "sucessful";
    } on Exception catch (e) {
      error(DioErrorUtil.handleError(e));
    }
  }

  Future<dynamic> fetchFriendRequests(
      {Function success, Function error}) async {
    if (friendRequests != null) {
      friendRequests.clear();
    }
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      String url = ServerSettings.getListFriendsURL + 
        "?to_user=" + userID +
        "&accepted=false";

      final response = await DioClient(dio).get(url);

      friendRequests = List<Friends.Result>.from(
        response["results"].map((x) => Friends.Result.fromJson(x)));

      success(friendRequests.length);
      notifyListeners();
      return "sucessful";
    } on Exception catch (e) {
      error(DioErrorUtil.handleError(e));
    }
  }

  Future<dynamic> fetchSearchFollowingUser(
      {String text, Function success, Function error}) async {
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      String url = ServerSettings.getFollowSearchURL;
      Map<String, dynamic> data = {"user_id": userID, "keyword": text};
      final response = await DioClient(dio).post(url, data: data);

      searchFollowers = List<Profile>.from(
        response["results"].map((x) => Profile.fromJson(x)));

      success(response["totalPages"]);
      notifyListeners();
      return "sucessful";
    } on Exception catch (e) {
      error(DioErrorUtil.handleError(e));
    }
  }

  Future<dynamic> fetchGroupVOD(Map<String, dynamic> data) async {
    final dio = Dio();
    // var userID = await storage.read(key: Preferences.user_id);
    var _group = chosenGroup;
    try {
      final response = await DioClient(dio).get(
          ServerSettings.getGroupsURL +
              "/" +
              _group.id.toString() +
              "/vod_list",
          queryParameters: data);
      vodList = GroupMediaList.fromJson(response);
      return "sucessful";
    } on Exception catch (error) {
    }
  }

  Future<String> fetchGroupLive(Map<String, dynamic> data) async {
    final dio = Dio();
    var _group = chosenGroup;
    try {
      final response = await DioClient(dio).get(
          ServerSettings.getGroupsURL +
              "/" +
              _group.id.toString() +
              "/live_list",
          queryParameters: data);
      liveList = GroupMediaList.fromJson(response);
      return "sucessful";
    } on Exception catch (error) {
    }
  }
}
