import 'package:dccast/utils/auth.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:adhara_socket_io/adhara_socket_io.dart';

const String URI = "http://121.125.77.40:5050/";

class SocketChat extends ChangeNotifier {

  int roomID;
  List<String> toPrint = ['trying to connect'];
  SocketIOManager manager;
  SocketIO socket;

  void joinRoom(roomID) {
    manager = SocketIOManager();
    this.roomID = roomID;
    initSocket();
  }

  Future<void> initSocket() async {
    final socket = await manager.createInstance(SocketOptions(
      '$URI',
      enableLogging: true,
      transports: [
        Transports.webSocket,
      ]
    ));
    socket.onConnect.listen((data) {
      var message = [];
      message.add(jsonEncode({"room": roomID}));
      socket.emit("join", message);
    });
    socket.onConnectError.listen(pPrint);
    socket.onConnectTimeout.listen(pPrint);
    socket.onError.listen(pPrint);
    socket.onDisconnect.listen(pPrint);
    socket.on('new message').listen(pPrint);
    await socket.connect();
  }

  Future<void> disconnect() async {
    await manager.clearInstance(socket);
  }

  void pPrint(Object data) {
    print(data);
    if (data is Map) {
      data = json.encode(data);
    }
    print(data);
  }

}
