import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:dccast/model/vod/upload_thumb.dart';
import 'package:dccast/utils/dio_client.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

import '../constants/assets.dart';
import '../constants/server_settings.dart';
import '../constants/styles.dart';
import '../utils/device.dart';
import '../model/user.dart';
import '../utils/auth.dart';

class ProfileEditScreen extends StatefulWidget {
  static const String id = 'profile_edit_screen';

  @override
  _ProfileEditScreenState createState() => _ProfileEditScreenState();
}

enum ImageMode { CAMERA, GALLERY }

class _ProfileEditScreenState extends State<ProfileEditScreen> {
  final storage = FlutterSecureStorage();
  final nameController = TextEditingController();
  final stateController = TextEditingController();
  final idController = TextEditingController();
  final emailController = TextEditingController();
  final phoneController = TextEditingController();
  bool isLoading = false;
  Profile profile;
  File imageFile;

  final _formKey = GlobalKey<FormState>();

  /// Get from gallery
  _getFromGallery() async {
    PickedFile pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      setState(() {
        imageFile = File(pickedFile.path);
      });
    }
  }

  /// Get from camera
  _getFromCamera() async {
    PickedFile pickedFile = await ImagePicker().getImage(
      source: ImageSource.camera,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      setState(() {
        imageFile = File(pickedFile.path);
      });
    }
  }

  @override
  void initState() {
    super.initState();
    // call get json data function
    // selectedPage = Selec
    profile = Provider.of<Auth>(context, listen: false).userProfile;
    print(profile.profileImage);
    nameController.text = profile.nickName;
    stateController.text = profile.stateMessage;
    idController.text = profile.user.username;
    emailController.text = profile.user.email;
    phoneController.text = profile.phone;
  }

  void dispose() {
    nameController.dispose();
    stateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(translate("profile.edit.title"))
      ),
      body: Container(
        color: Colors.white,
        child: ModalProgressHUD(
          progressIndicator: Lottie.asset(Assets.animLoad,
              width: DeviceUtils.getScaledSize(context, 0.3),
              height: DeviceUtils.getScaledSize(context, 0.3)),
          inAsyncCall: isLoading,
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                GestureDetector(
                  child: Container(
                      padding: EdgeInsets.all(16.0),
                      child: imageFile != null
                        ? Stack( 
                          clipBehavior: Clip.none,
                          alignment: Alignment.center,
                          children: [
                            Container(
                              width: DeviceUtils.getScaledWidth(context, 0.3),
                              height: DeviceUtils.getScaledWidth(context, 0.3),
                              decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  image: DecorationImage(
                                      image: FileImage(imageFile))),
                              child: null
                              ),
                              Positioned(
                                bottom: -32,
                                child:  ClipOval(
                                  child: Container(
                                    width: 64,
                                    height: 64,
                                    color: kGreyColor,
                                    child: Icon(
                                      FontAwesomeIcons.camera,
                                      color: kBlack50Color,
                                      size: 32,
                                    )
                                  )
                                )
                              )
                            ]
                          )
                          : Container(
                              width: DeviceUtils.getScaledWidth(context, 0.3),
                              height: DeviceUtils.getScaledWidth(context, 0.3),
                              decoration: profile != null &&
                                      profile.profileImage != null
                                  ? BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: kGreyColor,
                                    image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: NetworkImage(profile.profileImage))
                                    )
                                  : null,
                              child: ClipOval(
                                      child: Container(
                                          width: DeviceUtils.getScaledWidth(
                                              context, 0.3),
                                          height: DeviceUtils.getScaledWidth(
                                              context, 0.3),
                                          color: kGreyColor,
                                          child: Icon(
                                            FontAwesomeIcons.users,
                                            color: kBlack50Color,
                                            size: DeviceUtils.getScaledWidth(
                                                context, 0.15),
                                          )))
                                )),
                  onTap: () {
                    _askUser();
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return translate("profile.edit.name");
                      }
                      return null;
                    },
                    controller: nameController,
                    style: TextStyle(color: kBlackColor, fontSize: 16),
                    decoration: InputDecoration(
                      labelText: translate("profile.edit.name"),
                      focusColor: kBlackColor,
                      enabledBorder: const UnderlineInputBorder(
                        borderSide:
                            const BorderSide(color: kGreyColor, width: 1.0),
                      ),
                      focusedBorder: const UnderlineInputBorder(
                        borderSide:
                            const BorderSide(color: kGreyColor, width: 1.0),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return translate("profile.edit.state");
                      }
                      return null;
                    },
                    controller: stateController,
                    style: TextStyle(color: kBlackColor, fontSize: 16),
                    decoration: InputDecoration(
                      labelText: translate("profile.edit.state"),
                      enabledBorder: const UnderlineInputBorder(
                        borderSide:
                            const BorderSide(color: kGreyColor, width: 1.0),
                      ),
                      focusedBorder: const UnderlineInputBorder(
                        borderSide:
                            const BorderSide(color: kGreyColor, width: 1.0),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return translate("profile.edit.id");
                      }
                      return null;
                    },
                    controller: idController,
                    readOnly: true,
                    enabled: false,
                    style: TextStyle(color: kBlackColor, fontSize: 16),
                    decoration: InputDecoration(
                      labelText: translate("profile.edit.id"),
                      enabledBorder: const UnderlineInputBorder(
                        borderSide:
                            const BorderSide(color: kGreyColor, width: 1.0),
                      ),
                      focusedBorder: const UnderlineInputBorder(
                        borderSide:
                            const BorderSide(color: kGreyColor, width: 1.0),
                      ),
                    ),
                  ),
                ),
                /*Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return translate("profile.edit.email");
                      }
                      return null;
                    },
                    controller: emailController,
                    readOnly: true,
                    enabled: false,
                    style: TextStyle(color: kBlackColor, fontSize: 16),
                    decoration: InputDecoration(
                      labelText: translate("profile.edit.email"),
                      enabledBorder: const UnderlineInputBorder(
                        borderSide:
                            const BorderSide(color: kGreyColor, width: 1.0),
                      ),
                      focusedBorder: const UnderlineInputBorder(
                        borderSide:
                            const BorderSide(color: kGreyColor, width: 1.0),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return translate("profile.edit.phone");
                      }
                      return null;
                    },
                    controller: phoneController,
                    style: TextStyle(color: kBlackColor, fontSize: 16),
                    decoration: InputDecoration(
                      labelText: translate("profile.edit.phone"),
                      enabledBorder: const UnderlineInputBorder(
                        borderSide:
                            const BorderSide(color: kGreyColor, width: 1.0),
                      ),
                      focusedBorder: const UnderlineInputBorder(
                        borderSide:
                            const BorderSide(color: kGreyColor, width: 1.0),
                      ),
                    ),
                  ),
                ),*/
                SizedBox(height: 24.0),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: SizedBox(
                    width: double.infinity, // <-- match_parent
                    child: ElevatedButton(
                      onPressed: _submit,
                      style: ButtonStyle(backgroundColor:
                        MaterialStateProperty.all(kPrimaryColor)
                      ),
                      child: Text(
                        translate("submit"),
                        style: kSideTitleStyle,
                      )
                    )
                  )
                ),
                SizedBox(height: 16.0),
              ],
            ),
          ),
        ),
      ),
      resizeToAvoidBottomInset: false,
    );
  }

  void _submit() async {
    var userID = Provider.of<Auth>(context, listen: false).userProfile.id;
    var authToken = Provider.of<Auth>(context, listen: false).token;
    setState(() {
      isLoading = true;
    });
    try {
      var url = ServerSettings.updateProfileURL + userID.toString();
      var data = {
        "user_no": userID,
        "user": userID,
        "nick_name": nameController.text,
        //"phone": phoneController.text,
        "state_message": stateController.text
      };
      if (imageFile != null) {
        final dio = Dio();
        FormData thumData = FormData.fromMap({
          "thumbnail": await MultipartFile.fromFile(imageFile.path),
        });
        final thumbResponse = await DioClient(dio).post(
            ServerSettings.uploadVODThumbURL,
            data: thumData,
            cancelToken: CancelToken());

        data["profile_image"] = VodThumbUpload.fromJson(thumbResponse)
            .path
            .replaceAll("public", ServerSettings.videoThumbUrlChangeURL);
      }

      await http.put(Uri.parse("$url/"), body: jsonEncode(data), headers: {
        "Authorization": "Token $authToken",
        "Content-Type": "application/json"
      }).then((result) {
        if (result.statusCode == 200) {
          Provider.of<Auth>(context, listen: false).refreshDetail();
          Navigator.of(context).pop("refresh");
        }
      });
      setState(() {
        isLoading = false;
      });
    } on Exception {
      setState(() {
        isLoading = false;
      });
    }
  }

  Future _askUser() async {
    switch (await showDialog(
        context: context,
        builder: (context) => SimpleDialog(
              title: Text(translate("vod.upload.select-from")),
              elevation: 4,
              children: <Widget>[
                SimpleDialogOption(
                  child: Text(translate("vod.upload.select-gallery")),
                  onPressed: () {
                    Navigator.pop(context, ImageMode.GALLERY);
                  },
                ),
                SimpleDialogOption(
                  child: Text(translate("vod.upload.select-camera")),
                  onPressed: () {
                    Navigator.pop(context, ImageMode.CAMERA);
                  },
                ),
              ],
            ))) {
      case ImageMode.GALLERY:
        _getFromGallery();
        break;
      case ImageMode.CAMERA:
        _getFromCamera();
        break;
    }
  }
}
