import 'package:dccast/constants/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import '../components/comment/comment_item.dart';
import '../components/comment/comment_send.dart';
import '../constants/assets.dart';
import '../model/user_media.dart';
import '../utils/comments.dart';
import '../utils/device.dart';
import '../utils/media.dart';

class CommentScreen extends StatefulWidget {
  static const String id = 'comment_screen';
  int parentId;

  CommentScreen({Key key, this.parentId})
      : super(key: key);

  @override
  _CommentScreenState createState() => _CommentScreenState();
}

class _CommentScreenState extends State<CommentScreen> {
  int lastPageId = 1;
  bool isLoading = false;

  final storage = FlutterSecureStorage();
  @override
  Widget build(BuildContext context) {
    return Consumer<CommentsUtil>(builder: (context, comments, child) {
      return Container(
        color: Colors.white,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                border: Border(bottom: BorderSide(color: Colors.grey),
                ),
              ),
              child: ListTile(
                contentPadding: EdgeInsets.symmetric(
                  horizontal: 20.0),
                title: Row(
                  children: [
                    Text(translate("comments")),
                    SizedBox(width: 16.0),
                    Text(comments.comments == null
                      ? "0"
                      : comments.comments.count.toString()),
                    IconButton(
                      icon: Icon(Icons.refresh),
                      color: Colors.grey,
                      onPressed: _fetchComment
                    ),
                  ],
                ),
                trailing: IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    Navigator.of(context).pop();
                  }
                )
              )
            ),
            isLoading
            ? Lottie.asset(Assets.animLoad,
                width: DeviceUtils.getScaledSize(context, 0.3),
                height: DeviceUtils.getScaledSize(context, 0.3))
            : Expanded(
              child: ListView(
                children: [
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: bottomCommentTextField,
                    child: CommentSendScreen(disabled: bottomCommentTextField)
                  ),
                  comments.comments == null
                  ? Lottie.asset(Assets.animNoData,
                    width: DeviceUtils.getScaledSize(context, 0.3),
                    height: DeviceUtils.getScaledSize(context, 0.3))
                  : Column(
                    // padding: const EdgeInsets.all(0),
                    // itemCount: _comments == null ? 0 : _comments?.results?.length,
                    // itemBuilder: (context, index) {
                    //   return CommentItemComponent(comment: _comments.results[index]);
                    // },
                    children: List.generate(
                        comments.comments == null
                            ? 0
                            : comments.comments.results.length, (index) {
                      return CommentItemComponent(
                          comment: comments.comments.results[index]);
                    }),
                  ),
                ],
              )
            )
          ]
        )
      );
    });
  }

  void bottomCommentTextField() {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: true,
      backgroundColor: Colors.white,
      context: context,
      builder: (BuildContext bc) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: CommentSendScreen()
        );
      }
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    _fetchComment();
    super.initState();
  }

  void _fetchComment() {
    setState(() {
      isLoading = true;
    });
    Media chosenMedia =
        Provider.of<MediaUtils>(context, listen: false).chosenMedia;
    Provider.of<CommentsUtil>(context, listen: false)
        .fetchCommentWithParent(mediaID: chosenMedia.id,
        parentID: widget.parentId).then((value) => {
      setState(() {
        isLoading = false;
      })
    });
  }
}
