import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../constants/assets.dart';
import '../constants/preferences.dart';
import '../constants/server_settings.dart';
import '../model/user.dart';
import '../utils/auth.dart';
import '../utils/device.dart';
import '../utils/dio_client.dart';

class ManduHistoryScreen extends StatefulWidget {
  static const String id = 'mandu_history_screen';
  @override
  _ManduHistoryScreenState createState() => _ManduHistoryScreenState();
}

class _ManduHistoryScreenState extends State<ManduHistoryScreen> {
  final storage = FlutterSecureStorage();
  String htmlResponse;
  bool isLoading = true;
  WebViewController _controller;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(translate("mandu.title")),
      ),
      body: isLoading
          ? Center(
              child: Lottie.asset(Assets.animManduPreload,
                  alignment: Alignment.center,
                  width: DeviceUtils.getScaledSize(context, 0.5)),
            )
          : WebView(
              initialUrl: 'about:blank',
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (WebViewController webViewController) {
                _controller = webViewController;
                _loadHtmlRespone();
              },
            ),
    );
  }

  @override
  void initState() {
    super.initState();
    // call get json data function
    // selectedPage = Selec
    this.getJSONData();
  }

  Future<String> getJSONData() async {
    final dio = Dio();
    dio.options.headers['content-type'] = 'application/x-www-form-urlencoded';
    dio.options.headers['user-agent'] = 'dcinside.castapp';

    String appID = await storage.read(key: Preferences.appID);
    User user = Provider.of<Auth>(context, listen: false).authenticatedUser;
    try {
      print(appID);
      print(user.dcinside.userId);
      Map<String, dynamic> data = {
        "app_id": appID,
        "user_id": user.dcinside.userId
      };

      final response =
          await DioClient(dio).post(ServerSettings.manduHistoryURL, data: data);
      setState(() {
        htmlResponse = response;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = true;
      });
    }
  }

  _loadHtmlRespone() {
    _controller.loadUrl(Uri.dataFromString(htmlResponse,
            mimeType: 'text/html', encoding: Encoding.getByName('utf-8'))
        .toString());
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  void dispose() {
    super.dispose();
  }
}
