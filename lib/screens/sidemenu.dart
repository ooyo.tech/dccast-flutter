import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';

import '../components/sidemenu_header.dart';
import '../constants/assets.dart';
import '../constants/preferences.dart';
import '../constants/server_settings.dart';
import '../constants/styles.dart';
import '../model/user.dart';
import '../model/user_block.dart';
import '../utils/auth.dart';
import '../utils/category.dart';
import '../utils/dio_client.dart';
import 'castlist/cast_list.dart';
import 'category.dart';
import 'favorites.dart';
import 'live/add_live.dart';
import 'login.dart';
import 'my_content/my_content.dart';
import 'vod/add_vod.dart';

class SideMenu extends StatefulWidget {
  @override
  _SideMenuState createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {
  final storage = FlutterSecureStorage();
  bool isLiveBlocked = false;
  String liveBlockMessage = "";
  bool isVODBlocked = false;
  String vodBlockMessage = "";
  UserBlock userBlock;
  eContent selectedType = eContent.vod;
  @override
  Widget build(BuildContext context) {
    bool loggedIn = Provider.of<Auth>(context, listen: false).loggedIn;

    // ignore: omit_local_variable_types
    List<gridItem> items = [
      gridItem(Assets.sideMenuHome, translate('home'), false, () {
        Navigator.of(context).pop();
      }),
      gridItem(
          // Assets.sideMenuChannel,
          Assets.tvIcon,
          translate("my_content"),
          false, () {
        Navigator.popAndPushNamed(
            context, !loggedIn ? LoginScreen.id : MyContentScreen.id);
      }),
      gridItem(Assets.sideMenuFavorite, translate("favorite"), false, () {
        Navigator.popAndPushNamed(
            context, !loggedIn ? LoginScreen.id : FavoritesScreen.id);
      }),
      gridItem(Assets.sideMenuCast, translate("cast_list"), false, () {
        Navigator.popAndPushNamed(
            context, !loggedIn ? LoginScreen.id : CastListScreen.id);
      }),
    ];

    // ignore: omit_local_variable_types
    List<gridItem> doubleItems = [
      gridItem(Assets.sideMenuLive, translate("live.upload.live-share"), false,
          () {
        var profile = Provider.of<Auth>(context, listen: false).userProfile;
        if (profile == null) {
          Navigator.pushNamed(context, LoginScreen.id);
        } else if (isLiveBlocked) {
          _showMyDialog(context, liveBlockMessage);
        } else {
          Navigator.pushNamed(
              context, !loggedIn ? LoginScreen.id : AddLiveScreen.id);
        }
      }),
      gridItem(Assets.sideMenuVod, translate("vod.upload-vod-share"), false,
          () {
        var profile = Provider.of<Auth>(context, listen: false).userProfile;
        if (profile == null) {
          Navigator.pushNamed(context, LoginScreen.id);
        } else if (isVODBlocked) {
          _showMyDialog(context, vodBlockMessage);
        } else {
          Navigator.pushNamed(context, StartVodScreen.id);
        }
      }),
    ];

    return Drawer(
      child: Container(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            SideMenuHeader(),
            GridView.builder(
              padding: EdgeInsets.zero,
              itemCount: items.length,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 0.0,
                  mainAxisSpacing: 0.0,
                  childAspectRatio: 5 / 3),
              itemBuilder: (BuildContext context, int index) {
                return _gridItem(items[index]);
              },
            ),
            Container(
              color: kGreyColor,
              height: 20.0,
            ),
            GridView.builder(
              padding: EdgeInsets.zero,
              itemCount: doubleItems.length,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 0.0,
                  mainAxisSpacing: 0.0,
                  childAspectRatio: 2 / 1),
              itemBuilder: (BuildContext context, int index) {
                return _gridItem(doubleItems[index]);
              },
            ),
            Container(
              color: kGreyColor,
              height: 20.0,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: GestureDetector(
                    onTap: () => _changeType(eContent.live),
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 8.0),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: selectedType == eContent.live
                                      ? kPrimaryColor
                                      : Colors.transparent,
                                  width: 2.0))),
                      child: Text(
                        translate("live"),
                        style: TextStyle(
                            color: selectedType == eContent.live
                                ? kPrimaryColor
                                : kBlackColor,
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
                Expanded(
                    child: GestureDetector(
                  onTap: () => _changeType(eContent.vod),
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 8.0),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: selectedType == eContent.vod
                                    ? kPrimaryColor
                                    : Colors.transparent,
                                width: 2.0))),
                    child: Text(
                      translate('vod'),
                      style: TextStyle(
                          color: selectedType == eContent.vod
                              ? kPrimaryColor
                              : kBlackColor,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ))
              ],
            ),
            Consumer<CategoryUtils>(builder: (context, category, child) {
              return GridView.builder(
                padding: EdgeInsets.zero,
                itemCount: category.categoryResults.length,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    crossAxisSpacing: 0.0,
                    mainAxisSpacing: 0.0),
                itemBuilder: (BuildContext context, int index) {
                  return _gridItem(gridItem(
                      category.categoryResults[index].img,
                      category.categoryResults[index].name,
                      true,
                      () => {_selectedCategor(index)}));
                },
              );
            }),
          ],
        ),
      ),
    );
  }

  void _changeType(eContent type) {
    setState(() {
      selectedType = type;
    });
  }

  void _selectedCategor(int id) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) =>
            CategoryScreen(content: selectedType, selectedTab: id),
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    fetchSomet();
    _fetchBlock();
    super.initState();
  }

  void fetchSomet() async {
    String appId = await storage.read(key: Preferences.appID);
    User user = Provider.of<Auth>(context, listen: false).authenticatedUser;
    /*print("appId 1: " +
        appId +
        " user_id: " +
        user?.dcinside?.userId?.toString() +
        " user-No: " +
        user?.dcinside?.userNo);*/
  }

  void _fetchBlock() async {
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      Map<String, dynamic> liveData = {
        "kind": "check_block_user",
        "block_kind": "LIVE",
        "profile_id": userID
      };

      Map<String, dynamic> vodData = {
        "kind": "check_block_user",
        "block_kind": "VOD",
        "profile_id": userID
      };

      await DioClient(dio)
          .post(ServerSettings.getFavoriteStatusMediaURL, data: liveData)
          .then((value) => {
                userBlock = UserBlock.fromJson(value),
                if (userBlock.isBlock)
                  {
                    setState(() {
                      isLiveBlocked = true;
                      liveBlockMessage = userBlock.blockMsg;
                    })
                  }
              });

      await DioClient(dio)
          .post(ServerSettings.getFavoriteStatusMediaURL, data: vodData)
          .then((value) => {
                userBlock = UserBlock.fromJson(value),
                if (userBlock.isBlock)
                  {
                    setState(() {
                      isVODBlocked = true;
                      vodBlockMessage = userBlock.blockMsg;
                    })
                  }
              });
    } on Exception catch (error) {
      print(error.toString());
    }
  }
}

Widget _gridItem(gridItem item) {
  return GestureDetector(
    onTap: item?.onPress,
    child: Container(
      decoration:
          BoxDecoration(border: Border.all(color: Colors.grey.shade100)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          item.urlImage
              ? CachedNetworkImage(
                  fit: BoxFit.contain,
                  width: 30.0,
                  height: 30.0,
                  imageUrl: item.image,
                  placeholder: (context, url) =>
                      Image.asset(Assets.appLogo, fit: BoxFit.cover),
                  errorWidget: (context, url, error) {
                    return Image.asset(
                      Assets.sideMenuSport,
                      fit: BoxFit.contain,
                    );
                  })
              : Image.asset(item.image),
          SizedBox(height: 10.0),
          Text(
            item.title,
            style: kTextStyle,
          )
        ],
      ),
    ),
  );
}

Future<void> _showMyDialog(BuildContext context, String message) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        content: SingleChildScrollView(
          child: Text(message),
        ),
        actions: <Widget>[
          TextButton(
            child: Text(translate("alert.button.ok")),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

class gridItem {
  final String image;
  final String title;
  final bool urlImage;
  final Function onPress;
  gridItem(this.image, this.title, this.urlImage, this.onPress);
}
