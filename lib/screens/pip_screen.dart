// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
//
// import '../components/video/video_player_vlc.dart';
// import '../utils/easy_pip.dart';
// import '../utils/media.dart';
// import 'home_vod.dart';
//
// class PipScreen extends StatefulWidget {
//   static const String id = 'pip_screen';
//   @override
//   _PipScreenState createState() => _PipScreenState();
// }
//
// class _PipScreenState extends State<PipScreen> {
//   @override
//   Widget build(BuildContext context) {
//     var isEnabled = Provider.of<MediaUtils>(context, listen: true).inPipMode;
//     return Scaffold(
//       body: PIPStack(
//         backgroundWidget: HomeVodScreen(),
//         pipWidget: isEnabled
//             ? VideoPlayerComponentVlc()
//             : Container(
//                 child: Text("hello"),
//               ),
//         pipEnabled: isEnabled,
//         pipExpandedContent: Container(
//           color: Colors.white,
//           child: Text("hello"),
//         ),
//         onClosed: () {
//           Future.delayed(Duration(milliseconds: 250), () {
//             Provider.of<MediaUtils>(context, listen: false).disablePip();
//           });
//         },
//       ),
//     );
//   }
// }
