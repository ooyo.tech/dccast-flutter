import 'package:cached_network_image/cached_network_image.dart';
import 'package:dccast/components/user_list_item.dart';
import 'package:dccast/model/group_list.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import './friends_request.dart';
import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/friends.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../../utils/group.dart';
import '../profile.dart';
import 'group_detail.dart';

class GroupSearchScreen extends StatefulWidget {
  static const String id = 'group_search_screen';
  @override
  _GroupSearchState createState() =>
      _GroupSearchState();
}

class _GroupSearchState extends State<GroupSearchScreen> {
  final searchController = TextEditingController();
  String searchText;
  int lastPageId = 1;
  int _totalPages = 2;
  bool isLoading = false;
  final storage = FlutterSecureStorage();
  ScrollController _controller;
  GroupList _groupList;
  String _userID;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(translate("search"))
          ],
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop()
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        controller: _controller,
        children: <Widget>[
        TextFormField(
          controller: searchController,
          onChanged: (text) => {_fetchList(text)},
          style: TextStyle(fontSize: 16),
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
            hintText: translate("search"),
            floatingLabelBehavior: FloatingLabelBehavior.always,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
            prefixIcon: Padding(
              padding: EdgeInsets.only(top: 5),
              child: Icon(Icons.search),
            )
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
          color: Colors.white,
          child: isLoading
              ? Center(
                  child: Lottie.asset(Assets.animLoad,
                      alignment: Alignment.center,
                      width: DeviceUtils.getScaledSize(context, 0.5)),
                )
              : _buildListView(),
          ),
        ]
      )
    );
  }

  Widget _buildListView() {
    return _groupList?.results == null ||
          _groupList?.results.isEmpty
      ? Column(
          children: [
            Text(translate("enter.nickname.id"), style: kTextStyle),
            Center(
              child: Lottie.asset(Assets.animNoData,
                alignment: Alignment.center,
                width: DeviceUtils.getScaledSize(context, 0.5)),
            )
          ]
        )
      : ListView.builder(
        shrinkWrap: true,
        padding: const EdgeInsets.all(0.0),
        itemCount: _groupList == null
            ? 0
            : _groupList?.results?.length,
        itemBuilder: (context, index) {
          return ListTile(
            onTap: () {
              setState(() {
                isLoading = true;
              });
              Future.delayed(
                  Duration(milliseconds: 250),
                  () => {
                        Provider.of<GroupUtil>(context,
                                listen: false)
                            .selectGroup(
                                _groupList?.results[index])
                      }).then((value) => {
                    setState(() {
                      isLoading = false;
                    }),
                    Navigator.pushNamed(
                        context, GroupDetailScreen.id,
                        arguments: _userID)
                  });
            },
            contentPadding: EdgeInsets.symmetric(
                vertical: 6.0, horizontal: 12.0),
            leading: ClipOval(
              child: _groupList?.results[index].profileImg !=
                      null
                  ? CachedNetworkImage(
                      fit: BoxFit.cover,
                      width: 40.0,
                      height: 40.0,
                      imageUrl: _groupList?.results[index].profileImg,
                      placeholder: (context, url) =>
                          Image.asset(Assets.appLogo,
                              fit: BoxFit.cover),
                      errorWidget: (context, url, error) {
                        return Image.asset(
                          Assets.appLogo,
                          fit: BoxFit.contain,
                          width: 25.0,
                          height: 25.0,
                          color: kPrimaryColor,
                        );
                      })
                  : Container(
                      padding: EdgeInsets.all(8.0),
                      color: kPrimaryColor,
                      child: Image.asset(Assets.appLogo,
                          fit: BoxFit.contain,
                          width: 25.0,
                          height: 25.0),
                    ),
            ),
            title: Text(
              _groupList?.results[index].name.toString() +
                  " (" +
                  _groupList?.results[index].members.length
                      .toString() +
                  ")",
              style: kTextStyle,
            ),
            trailing: _groupList?.results[index].message != ""
                ? Container(
                    width: DeviceUtils.getScaledWidth(
                        context, 0.35),
                    padding: EdgeInsets.all(6.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      color: kGreyColor,
                    ),
                    child: Text(
                      _groupList?.results[index].message
                          .toString(),
                      maxLines: 2,
                      style: kTextGreyStyle,
                      textAlign: TextAlign.right,
                    ),
                  )
                : SizedBox(),
          );
        },
      );
  }

  Future<dynamic> _fetchList(String text) async {
    if (lastPageId > _totalPages) {
      return "successful";
    }
    setState(() {
      isLoading = true;
    });
    final dio = Dio();
    lastPageId = 1;
    _userID = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getGroupsSearchURL;
      var data = <String, dynamic>{
        "user": _userID,
        "keyword": text,
        "page": lastPageId
      };

      final response = await DioClient(dio).post(url, data: data);

      setState(() {
        _groupList = GroupList.fromJson(response);
        _totalPages = _groupList?.totalPages;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (e) {
    }
  }

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    super.initState();
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
      !_controller.position.outOfRange) {
    setState(() {//you can do anything here
    });
  }
  if (_controller.offset <= _controller.position.minScrollExtent &&
      !_controller.position.outOfRange) {
    setState(() {//you can do anything here
      });
    }
  }

  void dispose() {
    searchController.dispose();

    super.dispose();
  }
}
