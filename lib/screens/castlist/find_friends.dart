import 'package:dccast/components/user_list_item.dart';
import 'package:dccast/constants/preferences.dart';
import 'package:dccast/constants/server_settings.dart';
import 'package:dccast/screens/profile.dart';
import 'package:dccast/utils/dio_client.dart';
import 'package:dccast/utils/group.dart';
import 'package:dccast/model/friends.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import 'friends_request.dart';
import '../../constants/assets.dart';
import '../../constants/styles.dart';
import '../../utils/device.dart';

class FindFriendsScreen extends StatefulWidget {
  static const String id = 'find_friends_screen';
  @override
  _FindFriendsState createState() =>
      _FindFriendsState();
}

class _FindFriendsState extends State<FindFriendsScreen> {
  final searchController = TextEditingController();
  String searchText;
  int lastPageId = 1;
  int _totalPages = 2;
  bool isLoading = false;
  int _requestLength = 0;
  final storage = FlutterSecureStorage();
  ScrollController _controller;
  bool willRefresh = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(translate("friend.search.title")),
            TextButton(              
              child: Text(translate("count.friend.search.requests")
                + "${Provider.of<GroupUtil>(context, listen: false).friendRequests?.length ?? 0})",
                style: TextStyle(color: Colors.white)),
              onPressed: () async {
                var _result = await 
                Navigator.pushNamed(context,FriendRequestScreen.id);
                willRefresh = (_result == "refresh");
              }
            )
          ],
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => willRefresh ?
            Navigator.of(context).pop("refresh") :
            Navigator.of(context).pop()
        ),
      ),
      body: Column(
        // shrinkWrap: true,
        // controller: _controller,
        children: <Widget>[
        TextFormField(
          controller: searchController,
          onChanged: (text) {
            lastPageId = 1;
            _totalPages = 2;
            _fetchList(text);
          },
          textAlignVertical: TextAlignVertical.center,
          style: TextStyle(fontSize: 16),
          decoration: InputDecoration(
            hintText: translate("search"),
            floatingLabelBehavior: FloatingLabelBehavior.always,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
            prefixIcon: Padding(
              padding: EdgeInsets.only(top: 5),
              child: Icon(Icons.search),
            )
          ),
        ),
        Expanded(
          child: isLoading
              ? Center(
                  child: Lottie.asset(Assets.animLoad,
                      alignment: Alignment.center,
                      width: DeviceUtils.getScaledSize(context, 0.5)),
                )
              : searchController.text == ""
              ? Column(
                  children: [
                    Text(translate("enter.nickname.id"), style: kTextStyle),
                    Center(
                      child: Lottie.asset(Assets.animNoData,
                        alignment: Alignment.center,
                        width: DeviceUtils.getScaledSize(context, 0.5)),
                    )
                  ]
                )
              : _buildListView(),
          ),
        ]
      )
    );
  }

  Widget _buildListView() {
    return Consumer<GroupUtil>(builder: (context, groupUtil, child) {
      return groupUtil.searchFriends == null ||
              groupUtil.searchFriends?.length == 0
          ? Column(
              children: [
                Text(translate("media.search.no.data"), style: kTextStyle),
                Center(
                  child: Lottie.asset(Assets.animNoData,
                    alignment: Alignment.center,
                    width: DeviceUtils.getScaledSize(context, 0.5)),
                )
              ]
            )
          : ListView.builder(
              shrinkWrap: true,
              padding: const EdgeInsets.all(0),
              itemCount: groupUtil.searchFriends == null
                  ? 0
                  : groupUtil.searchFriends?.length,
              itemBuilder: (context, index) {
                return _buildListItem(
                    groupUtil?.searchFriends[index], index);
              },
            );
    });
  }

  Widget _buildListItem(FriendProfile item, int index) => Container(
      decoration: BoxDecoration(color: Colors.white),
      margin: const EdgeInsets.only(bottom: 0.0),
      child: item != null
          ? UserListItem(
              title: item.user.username,
              subtitle: item.nickName,
              profileImage: item.profileImage,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        ProfileScreen(userProfile: item),
                  ),
                );
              },
              trailing: Container(
                child: FlatButton(
                  height: 25.0,
                  color: item.status == "not_friends" ? 
                    Colors.white : kPrimaryColor,
                  onPressed: () {
                    _friendUser(item, index);
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: kPrimaryColor, width: 1.5)),
                  child: Text(
                    translate(item.status == "not_friends" ? 
                      "friend.request.friend" : (
                        item.status == "pending" ? "friend.request-sent"
                        : "friend.request.confirmed"
                      )),
                    style: TextStyle(color: item.status == "not_friends" ? 
                      kPrimaryColor : Colors.white),
                  ),
                ),
              ),
            )
          : SizedBox());

  void _fetchList(String searchText) {
    if (lastPageId > _totalPages) {
      return;
    }
    this.searchText = searchText;
    Provider.of<GroupUtil>(context, listen: false).fetchSearchFriend(
        text: searchText,
        success: (length) {
          _totalPages = length;
          setState(() {
            isLoading = false;
          });
        },
        error: (String error) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              margin: EdgeInsets.only(bottom: 60.0, left: 10.0, right: 10.0),
              content: Text(error),
            ),
          );
        });
  }

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    super.initState();
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
      !_controller.position.outOfRange) {
    setState(() {//you can do anything here
    });
  }
  if (_controller.offset <= _controller.position.minScrollExtent &&
      !_controller.position.outOfRange) {
    setState(() {//you can do anything here
      });
    }
  }

  void dispose() {
    searchController.dispose();

    super.dispose();
  }

  Future<String> _friendUser(FriendProfile item, int index) async {
    final dio = Dio();
    String fromuser = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getSendFriendRequestURL;
      Map<String, dynamic> data = {"from_user": fromuser, "to_user": item.id};
      final response = await DioClient(dio).post(url, data: data);

      setState(() {
        Provider.of<GroupUtil>(context, listen: false)
          .searchFriends.elementAt(index).status = "pending";
      });
      // _fetchList(searchText);
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
      setState(() {
        isLoading = false;
      });
    }
  }
}
