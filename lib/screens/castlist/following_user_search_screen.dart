import 'package:dccast/components/user_list_item.dart';
import 'package:dccast/constants/preferences.dart';
import 'package:dccast/constants/server_settings.dart';
import 'package:dccast/model/followers.dart';
import 'package:dccast/screens/profile.dart';
import 'package:dccast/utils/dio_client.dart';
import 'package:dccast/utils/group.dart';
import 'package:dccast/model/user.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import '../../constants/assets.dart';
import '../../constants/styles.dart';
import '../../utils/device.dart';
import '../../utils/media.dart';

class FollowingUserSearchScreen extends StatefulWidget {
  static const String id = 'following_user_search_screen';
  @override
  _FollowingUserSearchScreenState createState() =>
      _FollowingUserSearchScreenState();
}

class _FollowingUserSearchScreenState extends State<FollowingUserSearchScreen> {
  final searchController = TextEditingController();
  int lastPageId = 1;
  int _totalPages = 2;
  bool isLoading = false;
  FollowStatus _followStatus;
  final storage = FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            Expanded(
              child: TextFormField(
                controller: searchController,
                onChanged: (text) => {_fetchList(text)},
                style: TextStyle(color: Colors.white, fontSize: 16),
                decoration: InputDecoration(
                  hintText: translate("search"),
                  hintStyle: TextStyle(color: kGreyColor),
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                ),
              ),
            ),
            IconButton(
              icon: Icon(Icons.close_rounded),
              onPressed: () {
                searchController.clear();
                Provider.of<GroupUtil>(context, listen: false).clearSearch;
              },
            )
          ],
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
        color: Colors.white,
        child: isLoading
            ? Center(
                child: Lottie.asset(Assets.animLoad,
                    alignment: Alignment.center,
                    width: DeviceUtils.getScaledSize(context, 0.5)),
              )
            : _buildListView(),
      ),
    );
  }

  Widget _buildListView() {
    return Consumer<GroupUtil>(builder: (context, groupUtil, child) {
      print(groupUtil.searchFollowers);
      return groupUtil.searchFollowers == null ||
              groupUtil.searchFollowers?.length == 0
          ? (searchController.text == ""
            ? Column(
              children: [
                Text(translate("enter.nickname.id"), style: kTextStyle),
                Center(
                  child: Lottie.asset(Assets.animNoData,
                      alignment: Alignment.center,
                      width: DeviceUtils.getScaledSize(context, 0.5)),
                )
              ]
            )
            : Column(
              children: [
                Text(translate("media.search.no.data"), style: kTextStyle),
                Center(
                  child: Lottie.asset(Assets.animNoData,
                      alignment: Alignment.center,
                      width: DeviceUtils.getScaledSize(context, 0.5)),
                )
              ]
            )
          )
          : ListView.builder(
              padding: const EdgeInsets.all(0),
              itemCount: groupUtil.searchFollowers == null
                  ? 0
                  : groupUtil.searchFollowers?.length,
              itemBuilder: (context, index) {
                return _buildListItem(
                    groupUtil?.searchFollowers[index]);
              },
            );
    });
  }

  Widget _buildListItem(Profile item) => Container(
      decoration: BoxDecoration(color: Colors.white),
      margin: const EdgeInsets.only(bottom: 8.0),
      child: item != null
          ? UserListItem(
              title: item.user.username,
              subtitle: item.nickName,
              profileImage: item.profileImage,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        ProfileScreen(userProfile: item),
                  ),
                );
              },
              trailing: Container(
                child: FlatButton(
                  height: 25.0,
                  color: _followStatus != null &&
                          _followStatus.userId == item.id
                      ? kPrimaryColor
                      : Colors.white,
                  onPressed: () {
                    setState(() {
                      isLoading = true;
                    });
                    _followStatus != null &&
                            _followStatus.userId == item.id
                        ? _unfollowUser(item.id)
                        : _followUser(item.id);
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: kPrimaryColor, width: 1.5)),
                  child: Text(
                    translate(_followStatus != null &&
                            _followStatus.userId == item.id
                        ? "following"
                        : "follow"),
                    style: TextStyle(
                        color: _followStatus != null &&
                                _followStatus.userId == item.id
                            ? Colors.white
                            : kPrimaryColor),
                  ),
                ),
              ),
            )
          : SizedBox());

  void _fetchList(String searchText) {
    Provider.of<GroupUtil>(context, listen: false).fetchSearchFollowingUser(
        text: searchText,
        success: (totalPages) {
          _totalPages = totalPages;
          setState(() {
            isLoading = false;
          });
        },
        error: (String error) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              margin: EdgeInsets.only(bottom: 60.0, left: 10.0, right: 10.0),
              content: Text(error),
            ),
          );
        });
  }

  void dispose() {
    searchController.dispose();

    super.dispose();
  }

  Future<String> _unfollowUser(int userID) async {
    final dio = Dio();
    String followerId = await storage.read(key: Preferences.user_id);
    try {
      var url = "${ServerSettings.deleteFollowUserURL}$userID"
        +"&follower_id=$followerId";
      final response = await DioClient(dio).delete(url);

      setState(() {
        _followStatus = FollowStatus.fromJson(response);
        isLoading = false;
        Navigator.of(context).pop('refresh');
      });
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<String> _followUser(int userID) async {
    final dio = Dio();
    String followerId = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getFollowUserURL;
      Map data = {
        "user_id": userID,
        "follower_id": followerId,
        "following": false
      };

      final response = await DioClient(dio).post(url, data: data);

      setState(() {
        _followStatus = FollowStatus.fromJson(response);
        isLoading = false;
        Navigator.of(context).pop('refresh');
      });
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
      setState(() {
        isLoading = false;
      });
    }
  }
}
