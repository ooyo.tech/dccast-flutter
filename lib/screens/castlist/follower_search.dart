import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import '../../components/user_list_item.dart';
import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/followers.dart';
import '../../model/user.dart';
import '../../utils/auth.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../profile.dart';

class FollowerSearch extends StatefulWidget {
  static const String id = 'follower_search_screen';

  const FollowerSearch ({Key key}) : super(key: key);

  @override
  FollowerSearchState createState() => FollowerSearchState();
}

class FollowerSearchState extends State<FollowerSearch> {
  final searchController = TextEditingController();
  String searchText;
  int lastPageId = 1;
  int _totalPages = 2;
  bool isLoading = false;
  final storage = FlutterSecureStorage();
  ScrollController _controller;
  Followers _result;
  String _userID;
  String text;
  String _refresh = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(translate("search"))
          ],
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(_refresh)
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        controller: _controller,
        children: <Widget>[
        TextFormField(
          controller: searchController,
          onChanged: (text) => {_fetchList(text)},
          style: TextStyle(fontSize: 16),
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
            hintText: translate("search"),
            floatingLabelBehavior: FloatingLabelBehavior.always,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
            prefixIcon: Padding(
              padding: EdgeInsets.only(top: 5),
              child: Icon(Icons.search),
            )
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
          color: Colors.white,
          child: searchController.text == "" ?Column(
            children: [
              Text(translate("enter.nickname.id"), style: kTextStyle),
              Center(
                child: Lottie.asset(Assets.animNoData,
                    alignment: Alignment.center,
                    width: DeviceUtils.getScaledSize(context, 0.5))
              )
            ]
          )
          : isLoading
              ? Center(
                  child: Lottie.asset(Assets.animLoad,
                      alignment: Alignment.center,
                      width: DeviceUtils.getScaledSize(context, 0.5)),
                )
              : _result == null || _result.results.isEmpty
              ? Column(
                  children: [
                    Text(translate("media.search.no.data"), style: kTextStyle),
                    Center(
                      child: Lottie.asset(Assets.animNoData,
                          alignment: Alignment.center,
                          width: DeviceUtils.getScaledSize(context, 0.5))
                    )
                  ]
                )
              : ListView.builder(
                shrinkWrap: true,
              padding: const EdgeInsets.all(0),
              itemCount: _result == null ? 0 : _result.results.length,
              itemBuilder: (context, index) {
                return Slidable(
                  actionPane: SlidableDrawerActionPane(),
                  actionExtentRatio: 0.25,
                  child: _buildImageColumn(_result.results[index]),
                  secondaryActions: <Widget>[
                    IconSlideAction(
                      color: Colors.red,
                      icon: Icons.delete,
                      onTap: () {
                        _deleteUserUnfollow(_result.results[index].id);
                      },
                    ),
                  ],
                );
              },
            ),
          ),
        ]
      )
    );
  }

  Widget _buildImageColumn(Result item) => Container(
        decoration: BoxDecoration(color: Colors.white),
        margin: const EdgeInsets.only(bottom: 8.0),
        child: UserListItem(
          title: item.follower.nickName,
          subtitle: item.follower.stateMessage,
          profileImage: item.follower.profileImage,
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ProfileScreen(userProfile: item.follower),
              ),
            );
          },
          trailing: Container(
            child: FlatButton(
              height: 25.0,
              color: _userID == item.followerId
                  ? kPrimaryColor
                  : Colors.white,
              onPressed: () {
                setState(() {
                  isLoading = true;
                });
                item != null && item.following && !item.deleted
                    ? _getUserUnfollow(item.followerId)
                    : _getUserFollow(item.followerId);
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                  side: BorderSide(color: kPrimaryColor, width: 1.5)),
              child: Text(
                translate(_userID == item.followerId
                    ? "following"
                    : "follow"),
                style: TextStyle(
                    color: _userID == item.followerId
                        ? Colors.white
                        : kPrimaryColor),
              ),
            ),
          ),
        ),
      );

  Future<dynamic> _fetchList(String text) async {
    if (lastPageId > _totalPages) {
      return "successful";
    }
    this.text = text;
    setState(() {
      isLoading = true;
    });
    final dio = Dio();
    lastPageId = 1;
    _userID = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getFollowersSearchURL;
      var data = <String, dynamic>{
        "user_id": _userID,
        "keyword": text,
        "page": lastPageId
      };

      final response = await DioClient(dio).post(url, data: data);

      setState(() {
        _result = Followers.fromJson(response);
        _totalPages = _result.totalPages;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (e) {
    }
  }

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    _fetchList("");
    super.initState();
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
      !_controller.position.outOfRange) {
    setState(() {//you can do anything here
    });
  }
  if (_controller.offset <= _controller.position.minScrollExtent &&
      !_controller.position.outOfRange) {
    setState(() {//you can do anything here
      });
    }
  }

  void dispose() {
    searchController.dispose();

    super.dispose();
  }

  Future<String> _deleteUserUnfollow(int id) async {
    setState(() {
      isLoading = true;
    });
    final dio = Dio();
    try {
      var url = "${ServerSettings.getFollowUserURL}$id/";

      final response = await DioClient(dio).delete(url);
      _fetchList(text);

      setState(() {
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<String> _getUserUnfollow(int followerId) async {
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.deleteFollowUserURL +
          userID +
          "&follower_id=" +
          followerId.toString();

      final response = await DioClient(dio).get(url);
      setState(() {
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<String> _getUserFollow(int followerId) async {
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getFollowUserURL;
      Map data = {"user_id": userID, "follower_id": followerId};

      final response = await DioClient(dio).post(url, data: data);

      setState(() {
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
      setState(() {
        isLoading = false;
      });
    }
  }
}
