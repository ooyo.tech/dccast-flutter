import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../components/profile/profile_media.dart';
import '../../model/user.dart';
import '../../model/user_recent.dart';

class CastlineHistory extends StatefulWidget {
  static const String id = 'castline_history_screen';
  final Profile userProfile;

  const CastlineHistory({Key key, this.userProfile}) : super(key: key);

  @override
  _CastlineHistoryState createState() => _CastlineHistoryState();
}

class _CastlineHistoryState extends State<CastlineHistory> {
  bool isLoading = false;
  User user;
  UserRecent userRecent;
  int lastPageId = 1;
  final storage = FlutterSecureStorage();
  final GlobalKey<ProfileMediaState> _mediaKey = GlobalKey<ProfileMediaState>();

  @override
  Widget build(BuildContext context) {
    return _buildRecentListView();
  }

  Widget _buildRecentListView() {
    return Scaffold(
      appBar: AppBar(
        title: Text("내 Live/VOD 내역"),
      ),
      body: Container(
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: ProfileMedia(key: _mediaKey,
                  searchText: "",
                  userProfile: widget.userProfile,
                  ordering: "-created"),
              )
            ],
          )),
    );
  }

  void dispose() {
    super.dispose();
  }
}
