import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/group_list.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../../utils/group.dart';
import 'group_detail.dart';

class GroupListScreen extends StatefulWidget {
  static const String id = 'grouplist_screen';
  const GroupListScreen ({Key key}) : super(key: key);
  @override
  GroupListScreenState createState() => GroupListScreenState();
}

class GroupListScreenState extends State<GroupListScreen> {
  ScrollController scrollController = ScrollController();

  bool isLoading = false;
  GroupList _groupList;
  int lastPageId = 1;
  int _totalPages = 2;
  final storage = FlutterSecureStorage();
  String _userID;

  @override
  Widget build(BuildContext context) {
    _groupList = Provider.of<GroupUtil>(context, listen: false).userGroupList;
    return ModalProgressHUD(
      progressIndicator: Lottie.asset(Assets.animLoad,
          width: DeviceUtils.getScaledSize(context, 0.3),
          height: DeviceUtils.getScaledSize(context, 0.3)),
      inAsyncCall: isLoading,
      child: Column(
        children: [
          Expanded(
            child: RefreshIndicator(
                onRefresh: refresh,
                child: _groupList?.results == null
                    ? Center(
                        child: Lottie.asset(Assets.animLoad,
                            alignment: Alignment.center,
                            width: DeviceUtils.getScaledSize(context, 0.5)),
                      )
                    : _buildListView(),
            )
          ),
        ],
      ),
    );
  }

  Widget _buildListView() {
    return ListView.builder(
      padding: const EdgeInsets.all(0.0),
      controller: scrollController,
      itemCount: _groupList == null
          ? 0
          : _groupList?.results?.length,
      itemBuilder: (context, index) {
        return ListTile(
          onTap: () {
            setState(() {
              isLoading = true;
            });
            Future.delayed(
                Duration(milliseconds: 250),
                () => {
                      Provider.of<GroupUtil>(context,
                              listen: false)
                          .selectGroup(
                              _groupList.results[index])
                    }).then((value) async {
                  setState(() {
                    isLoading = false;
                  });
                  var result = await Navigator.pushNamed(
                      context, GroupDetailScreen.id,
                      arguments: _userID);
                  if (result == "refreshList") {
                    refresh();
                  }
                });
          },
          contentPadding: EdgeInsets.symmetric(
              vertical: 6.0, horizontal: 12.0),
          leading: ClipOval(
            child: _groupList.results[index].profileImg !=
                    null
                ? CachedNetworkImage(
                    fit: BoxFit.cover,
                    width: 40.0,
                    height: 40.0,
                    imageUrl: _groupList.results[index].profileImg,
                    placeholder: (context, url) =>
                        Image.asset(Assets.appLogo,
                            fit: BoxFit.cover),
                    errorWidget: (context, url, error) {
                      return Image.asset(
                        Assets.appLogo,
                        fit: BoxFit.contain,
                        width: 25.0,
                        height: 25.0,
                        color: kPrimaryColor,
                      );
                    })
                : Container(
                    padding: EdgeInsets.all(8.0),
                    color: kPrimaryColor,
                    child: Image.asset(Assets.appLogo,
                        fit: BoxFit.contain,
                        width: 25.0,
                        height: 25.0),
                  ),
          ),
          title: Text(
            _groupList.results[index].name.toString() +
                " (" +
                _groupList.results[index].members.length
                    .toString() +
                ")",
            style: kTextStyle,
          ),
          trailing: _groupList.results[index].message != ""
              ? Container(
                  width: DeviceUtils.getScaledWidth(
                      context, 0.35),
                  padding: EdgeInsets.all(6.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    color: kGreyColor,
                  ),
                  child: Text(
                    _groupList.results[index].message
                        .toString(),
                    maxLines: 2,
                    style: kTextGreyStyle,
                    textAlign: TextAlign.right,
                  ),
                )
              : SizedBox(),
        );
      }
    );
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    lastPageId = 1;
    refresh();
    super.initState();
    scrollController.addListener(() {
      if (!isLoading &&
          scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        this.fetchGroupList();
      }
    });
    // call get json data function
    // selectedPage = Selec
  }

  Future<String> refresh() {
    Provider.of<GroupUtil>(context, listen: false).userGroupList = null;
    lastPageId = 1;
    _totalPages = 2;
    return fetchGroupList();
  }

  Future<String> fetchGroupList() async {
    if (isLoading || lastPageId > _totalPages) {
      return "successful";
    }
    setState(() {
      isLoading = true;
    });
    final dio = Dio();
    _userID = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getGroupsURL + 
        "/?user_id=" + _userID + "&page=" + lastPageId.toString();

      final response = await DioClient(dio).get(url);
      var jsonResponse = GroupList.fromJson(response);
      _totalPages = jsonResponse.totalPages;
      print("total");
      print(_totalPages);
      setState(() {
        GroupList newItems = jsonResponse;
        if (_groupList == null) {
          _groupList = newItems;
        } else {
          _groupList.results.addAll(newItems.results);
        }
        Provider.of<GroupUtil>(context, listen: false)
        .userGroupList = _groupList;

        lastPageId = lastPageId + 1;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }
}
