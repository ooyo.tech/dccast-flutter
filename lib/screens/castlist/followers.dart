import 'package:dccast/screens/castlist/castline_history.dart';
import 'package:dccast/screens/video_detail.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

import '../../components/user_list_item.dart';
import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/followers.dart';
import '../../model/user.dart';
import '../../utils/auth.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../profile.dart';

class CastListFollowers extends StatefulWidget {
  @override
  _CastListFollowersState createState() => _CastListFollowersState();
}

class _CastListFollowersState extends State<CastListFollowers> {
  ScrollController scrollController = ScrollController();

  Followers _followers;
  FollowStatus _followStatus;
  int lastPageId = 1;
  int _totalPages = 2;
  bool isLoading = false;
  User user;
  final storage = FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    return Container(color: Colors.white, child: _buildPaginatedListView());
  }

  Widget _buildPaginatedListView() {
    return ModalProgressHUD(
      progressIndicator: Lottie.asset(Assets.animLoad,
          width: DeviceUtils.getScaledSize(context, 0.3),
          height: DeviceUtils.getScaledSize(context, 0.3)),
      inAsyncCall: isLoading,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            color: kGreyColor,
            width: DeviceUtils.getScaledWidth(context, 1.0),
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
            child: Text(translate("recent.view.title"),
                style: kNotificationSubtitle),
          ),
          Container(
            color: Colors.white,
            margin: EdgeInsets.symmetric(vertical: 5.0),
            padding: EdgeInsets.symmetric(vertical: 4.0),
            child: Consumer<Auth>(builder: (context, auth, child) {
              if (auth.loggedIn) {
                return UserListItem(
                    title: auth.authenticatedUser.user.username,
                    subtitle: auth.authenticatedUser.profile.nickName,
                    onPressed: () {
                      // ProfileMedia(key: _mediaKey,
                      // searchText: searchText,
                      // userProfile: user,
                      // ordering: selectedValue)

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                        builder: (context) {
                          return CastlineHistory(
                            userProfile: auth.authenticatedUser.profile,
                          );
                        },
                        )
                      );
                    });
              } else {
                return SizedBox();
              }
            }),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
            color: kGreyColor,
            width: DeviceUtils.getScaledWidth(context, 1.0),
            child: Text(
                translate("followers") +
                    " (" +
                    _followers?.results?.length.toString() +
                    ")",
                style: kNotificationSubtitle),
          ),
          Expanded(
            child: RefreshIndicator (
              onRefresh: refresh,
              child: _followers?.results == null
                  ? Container(color: Colors.white)
                  : _buildListView(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListView() {
    return ListView.builder(
      controller: scrollController,
      padding: const EdgeInsets.all(0),
      itemCount: _followers == null ? 0 : _followers.results.length,
      itemBuilder: (context, index) {
        return Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: _buildImageColumn(_followers.results[index]),
          secondaryActions: <Widget>[
            IconSlideAction(
              color: Colors.red,
              icon: Icons.delete,
              onTap: () {
                setState(() {
                  isLoading = true;
                });
                _getUserUnfollow(_followers.results[index].followerId, index);
                    // : _getUserFollow(_followers.results[index], index);
              },
            ),
          ],
        );
      },
    );
  }

  Widget _buildImageColumn(Result item) => Container(
        decoration: BoxDecoration(color: Colors.white),
        margin: const EdgeInsets.only(bottom: 8.0),
        child: Column(
          children: <Widget>[
            UserListItem(
              title: item.follower.nickName,
              subtitle: item.follower.stateMessage,
              profileImage: item.follower.profileImage,
              trailing: item.follower.onAir ? 
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 4,
                    vertical: 1
                  ),
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  constraints: BoxConstraints(
                    minWidth: 16,
                    minHeight: 12,
                  ),
                  child: Text("ON AIR",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                    ),
                    textAlign: TextAlign.center)
               ) : null,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                      // VideoDetailScreen(content: item.follower.),
                      ProfileScreen(userProfile: item.follower),
                  ),
                );
              },
            ),
          ],
        ),
      );

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    isLoading = true;
    getJSONData();
    super.initState();
    scrollController.addListener(() {
      if (!isLoading &&
          scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        getJSONData();
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  Future<String> refresh() {
    lastPageId = 1;
    _totalPages = 2;
    return getJSONData();
  }

  Future<String> getJSONData() async {
    if (lastPageId > _totalPages) {
      return "successful";
    }
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getFollowersURL +
          userID +
          "&page=" +
          lastPageId.toString();

      final response = await DioClient(dio).get(url);
      var jsonResponse = Followers.fromJson(response);
      _totalPages = jsonResponse.totalPages;
      setState(() {
        Followers newItems = jsonResponse;
        if (_followers == null) {
          _followers = newItems;
        } else {
          _followers.results.addAll(newItems.results);
        }
        lastPageId = lastPageId + 1;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<String> _getUserUnfollow(int followerId, int index) async {
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.deleteFollowUserURL +
          userID +
          "&follower_id=" +
          followerId.toString();

      final response = await DioClient(dio).get(url);
      setState(() {
        _followers.results.removeAt(index);
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<String> _getUserFollow(Result follower, int index) async {
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getFollowUserURL;
      Map data = {"user_id": userID, "follower_id": follower.followerId};

      final response = await DioClient(dio).post(url, data: data);

      setState(() {
        _followStatus = FollowStatus.fromJson(response);
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
      setState(() {
        isLoading = false;
      });
    }
  }
}

///unfollow ${id}&follower_id=${id}
