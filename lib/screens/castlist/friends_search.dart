import 'package:dccast/model/user.dart';
import 'package:dccast/utils/toast.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import '../../components/user_list_item.dart';
import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/friends.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../../utils/group.dart';
import '../profile.dart';

class FriendsSearchScreen extends StatefulWidget {
  static const String id = 'friends_search_screen';
  @override
  _FriendsSearchState createState() =>
      _FriendsSearchState();
}

class _FriendsSearchState extends State<FriendsSearchScreen> {
  final searchController = TextEditingController();
  String searchText;
  int lastPageId = 1;
  int _totalPages = 2;
  bool isLoading = false;
  int _requestLength = 0;
  final storage = FlutterSecureStorage();
  ScrollController _controller;
  bool willRefresh = false;
  Friends _friends;
  String _userID;
  ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(translate("my_friends")),
          ],
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => willRefresh ?
            Navigator.of(context).pop("refresh") :
            Navigator.of(context).pop()
        ),
      ),
      body: Column(
        children: <Widget>[
        TextFormField(
          controller: searchController,
          onChanged: (text) {
            lastPageId = 1;
            _totalPages = 2;
            _friends = null;
            _fetchList(text);
          },
          textAlignVertical: TextAlignVertical.center,
          style: TextStyle(fontSize: 16),
          decoration: InputDecoration(
            hintText: translate("search"),
            floatingLabelBehavior: FloatingLabelBehavior.always,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
            prefixIcon: Padding(
              padding: EdgeInsets.only(top: 5),
              child: Icon(Icons.search),
            )
          ),
        ),
        Expanded(
          child: isLoading
              ? Center(
                  child: Lottie.asset(Assets.animLoad,
                      alignment: Alignment.center,
                      width: DeviceUtils.getScaledSize(context, 0.5)),
                )
              : searchController.text == ""
              ? Column(
                  children: [
                    Text(translate("enter.nickname.id"), style: kTextStyle),
                    Center(
                      child: Lottie.asset(Assets.animNoData,
                        alignment: Alignment.center,
                        width: DeviceUtils.getScaledSize(context, 0.5)),
                    )
                  ]
                )
              : RefreshIndicator(
                onRefresh: refresh,
                child: _buildListView()
              ),
          ),
        ]
      )
    );
  }

  Widget _buildListView() {
    return _friends.results == null ||
            _friends.results?.length == 0
      ? Column(
          children: [
            Text(translate("media.search.no.data"), style: kTextStyle),
            Center(
              child: Lottie.asset(Assets.animNoData,
                alignment: Alignment.center,
                width: DeviceUtils.getScaledSize(context, 0.5)),
            )
          ]
        )
      : _buildListItem();
  }

  Widget _buildListItem() {
    return ListView.builder(
      controller: scrollController,
      padding: const EdgeInsets.all(0),
      itemCount: _friends == null ? 0 : _friends.results.length,
      itemBuilder: (context, index) {
        var _friend = _friends.results[index].fromUser;
        if (_friend.id == int.parse(_userID)) {
          _friend = _friends.results[index].toUser;
        }
        return Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: _buildImageColumn(_friend),
          secondaryActions: <Widget>[
            IconSlideAction(
              color: Colors.red,
              icon: Icons.delete,
              onTap: () {
                _showMyDialog(context, _friends.results[index]);
              },
            ),
          ],
        );
      },
    );
  }

  Widget _buildImageColumn(Profile item) => Container(
    decoration: BoxDecoration(color: Colors.white),
    child: Column(
      children: <Widget>[
        UserListItem(
          title: item.user.username,
          subtitle: item.nickName,
          profileImage: item.profileImage,
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    ProfileScreen(userProfile: item),
              ),
            );
          },
        ),
      ],
    ),
  );

  Future<void> _fetchList(String searchText) async {
    if (lastPageId > _totalPages) {
      return;
    }
    this.searchText = searchText;

    setState(() {
      isLoading = true;
    });
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    _userID = userID;
    try {
      var url = "${ServerSettings.getListFriendsURL}search/";
      var data = {
        "user_id": userID,
        "keyword": this.searchText,
        "page": lastPageId
      };

      final response = await DioClient(dio).post(url, data: data);
      var jsonResponse = Friends.fromJson(response);
      _totalPages = jsonResponse.totalPages;
      setState(() {
        Friends newItems = jsonResponse;
        if (_friends == null) {
          _friends = newItems;
        } else {
          _friends.results.addAll(newItems.results);
        }
        lastPageId = lastPageId + 1;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    super.initState();
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
      !_controller.position.outOfRange) {
    setState(() {//you can do anything here
    });
  }
  if (_controller.offset <= _controller.position.minScrollExtent &&
      !_controller.position.outOfRange) {
    setState(() {//you can do anything here
      });
    }
  }

  void dispose() {
    searchController.dispose();

    super.dispose();
  }

  Future<String> refresh() {
    lastPageId = 1;
    _totalPages = 2;
    _friends.results.clear();
    return _fetchList(searchController.text);
  }

  Future<String> _friendUser(FriendProfile item, int index) async {
    final dio = Dio();
    String fromuser = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getSendFriendRequestURL;
      Map<String, dynamic> data = {"from_user": fromuser, "to_user": item.id};
      final response = await DioClient(dio).post(url, data: data);

      setState(() {
        Provider.of<GroupUtil>(context, listen: false)
          .searchFriends.elementAt(index).status = "pending";
      });
      // _fetchList(searchText);
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<String> _getUnfriendUser(Result item) async {
    final dio = Dio();
    try {
      var url = ServerSettings.getUnfriendRequestURL +
          item.fromUser.id.toString() +
          "&to_user=" +
          item.toUser.id.toString();

      willRefresh = true;
      var response = await DioClient(dio).delete(url);
      _friends.results.remove(item);
      setState(() {
        isLoading = false;
        showToast(translate("unfriend_success"));
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<void> _showMyDialog(BuildContext context, Result item) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("친구에서 삭제하시겠습니까?"),
          /*content: SingleChildScrollView(
            child: Text(message),
          ),*/
          actions: <Widget>[
            TextButton(
              child: Text(translate("group.delete.cancel")),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(translate("alert.button.ok")),
              onPressed: () {
                _getUnfriendUser(item);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
