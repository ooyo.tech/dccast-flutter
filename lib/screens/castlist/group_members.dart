import 'package:dccast/utils/group.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

import '../../components/user_list_item.dart';
import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/friends.dart';
import '../../model/user.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../../utils/auth.dart';
import '../profile.dart';

class GroupMembers extends StatefulWidget {
  static const String id = 'group_members';
  @override
  _GroupMembersState createState() => _GroupMembersState();
}

class _GroupMembersState extends State<GroupMembers> {
  Friends _friends;
  int lastPageId = 1;
  bool isLoading = false;
  User user;
  final storage = FlutterSecureStorage();
  List<Profile> _selectedFriend = List<Profile>();
  String _userID;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(translate("castlist.group.members") +
            " " +
            translate("castlist.group.add.title")),
        actions: [
          FlatButton(
              onPressed: () {
                Future.microtask(() {
                  Provider.of<GroupUtil>(context, listen: false)
                      .selectGroupMembers(_selectedFriend);
                }).then((value) {
                  Navigator.pop(context);
                });
              },
              child: Text(translate("castlist.group.add.title"),
                  style: TextStyle(color: Colors.white)))
        ],
      ),
      body: Container(color: Colors.white, child: _buildPaginatedListView()),
    );
  }

  Widget _buildPaginatedListView() {
    return ModalProgressHUD(
      progressIndicator: Lottie.asset(Assets.animLoad,
          width: DeviceUtils.getScaledSize(context, 0.4),
          height: DeviceUtils.getScaledSize(context, 0.4)),
      inAsyncCall: isLoading,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
            color: kGreyColor,
            width: DeviceUtils.getScaledWidth(context, 1.0),
            child: Text(translate("friends"), style: kNotificationSubtitle),
          ),
          Expanded(
            child: _friends?.results == null
                ? Column(
                  children: [
                    Text(translate("enter.nickname.id"), style: kTextStyle),
                    Center(
                      child: Lottie.asset(Assets.animNoData,
                        alignment: Alignment.center,
                        width: DeviceUtils.getScaledSize(context, 0.5)),
                    )
                  ]
                )
                : _buildListView(),
          ),
        ],
      ),
    );
  }

  Widget _buildListView() {
    return ListView.builder(
        padding: const EdgeInsets.all(0),
        itemCount: _friends.results == null ? 0 : _friends.results.length,
        itemBuilder: (context, index) {
          var _friend = _friends.results[index].fromUser;
          if (_friend.id == int.parse(_userID)) {
            _friend = _friends.results[index].toUser;
          }
          return Container(
            decoration: BoxDecoration(color: Colors.white),
            margin: const EdgeInsets.only(bottom: 8.0),
            child: Column(
              children: <Widget>[
                UserListItem(
                  title: _friend.user.username,
                  subtitle: _friend.nickName,
                  profileImage: _friend.profileImage,
                  trailing: (_selectedFriend != null &&_selectedFriend.where(
                      (_item) => _item.id == _friend.id).isNotEmpty)
                      ? IconButton(
                          icon: Icon(Icons.radio_button_checked_outlined,
                            color: kPrimaryColor),
                          onPressed: () {
                            _toggleFriend(_friend);
                          })
                      : IconButton(
                          icon: Icon(Icons.radio_button_unchecked,
                            color: kPrimaryColor),
                          onPressed: () {
                            _toggleFriend(_friend);
                          }),
                  onPressed: () {
                    _toggleFriend(_friend);
                  },
                ),
              ],
            ),
          );
        });
  }

  void _toggleFriend(Profile _friend) {
    setState(() {
      if (_selectedFriend == null) {
        _selectedFriend = <Profile>[];
      }
      if (_selectedFriend.where(
        (_item) => _item.id == _friend.id).isNotEmpty) {
        _selectedFriend.removeWhere((element) =>
            element.id == _friend.id);
      } else {
        _selectedFriend.add(_friend);
      }
    });
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    _selectedFriend = Provider.of<GroupUtil>(context, listen: false)
    .selectedMembers;
    getJSONData();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  Future<String> getJSONData() async {
    setState(() {
        isLoading = true;
    });
    final dio = Dio();
    _userID = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getListFriendsURL +
          "?id=" + _userID +
          "&page=" +
          lastPageId.toString();

      final response = await DioClient(dio).get(url);
      var jsonResponse = Friends.fromJson(response);
      setState(() {
        Friends newItems = jsonResponse;
        if (_friends == null) {
          _friends = newItems;
        } else {
          _friends.results.addAll(newItems.results);
        }
        lastPageId = lastPageId + 1;
        setState(() {
          isLoading = false;
        });
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }
}
