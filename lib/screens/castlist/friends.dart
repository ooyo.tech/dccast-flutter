import 'package:dccast/utils/group.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

import '../../components/user_list_item.dart';
import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/friends.dart';
import '../../model/user.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../profile.dart';

class CastListFriends extends StatefulWidget {
  const CastListFriends ({Key key}) : super(key: key);

  static const String id = 'cast_list_friends';
  @override
  CastListFriendsState createState() => CastListFriendsState();

  void refresh() {
    CastListFriendsState().refresh();
  }
}

class CastListFriendsState extends State<CastListFriends> {
  ScrollController scrollController = ScrollController();

  Friends _friends;
  int lastPageId = 1;
  int _totalPages = 2;
  bool isLoading = false;
  User user;
  final storage = FlutterSecureStorage();
  String _userID;

  @override
  Widget build(BuildContext context) {
    return Container(color: Colors.white, child: _buildPaginatedListView());
  }

  Widget _buildPaginatedListView() {
    return ModalProgressHUD(
      progressIndicator: Lottie.asset(Assets.animLoad,
          width: DeviceUtils.getScaledSize(context, 0.3),
          height: DeviceUtils.getScaledSize(context, 0.3)),
      inAsyncCall: isLoading,
      child: (_friends?.results != null &&
        _friends.results.isEmpty && !isLoading) ?
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Center(
              child: Text(translate("friends.no.data"))
            ),
            Center(
              child: Lottie.asset(Assets.animNoData,
                  alignment: Alignment.center,
                  width: DeviceUtils.getScaledSize(context, 0.5)),
            )
          ]
        ) :
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
              color: kGreyColor,
              width: DeviceUtils.getScaledWidth(context, 1.0),
              child: Text(translate("friends"), style: kNotificationSubtitle),
            ),
            Expanded(
              child: RefreshIndicator(
                onRefresh: refresh,
                child: _friends?.results == null
                    ? Container(color: Colors.white)
                    : _buildListView(),
              ),
            ),
          ],
        ),
    );
  }

  Widget _buildListView() {
    return ListView.builder(
      controller: scrollController,
      padding: const EdgeInsets.all(0),
      itemCount: _friends == null ? 0 : _friends.results.length,
      itemBuilder: (context, index) {
        var _friend = _friends.results[index].fromUser;
        if (_friend.id == int.parse(_userID)) {
          _friend = _friends.results[index].toUser;
        }
        return Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: _buildImageColumn(_friend),
          secondaryActions: <Widget>[
            IconSlideAction(
              color: Colors.red,
              icon: Icons.delete,
              onTap: () {
                _showMyDialog(context, _friends.results[index]);
              },
            ),
          ],
        );
      },
    );
  }

  Widget _buildImageColumn(Profile item) => Container(
        decoration: BoxDecoration(color: Colors.white),
        margin: const EdgeInsets.only(bottom: 8.0),
        child: Column(
          children: <Widget>[
            UserListItem(
              title: item.user.username,
              subtitle: item.nickName,
              profileImage: item.profileImage,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        ProfileScreen(userProfile: item),
                  ),
                );
              },
            ),
          ],
        ),
      );

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    lastPageId = 1;
    getJSONData();
    super.initState();
    scrollController.addListener(() {
      if (!isLoading &&
          scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        getJSONData();
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  Future<String> refresh() {
    lastPageId = 1;
    _totalPages = 2;
    _friends.results.clear();
    return getJSONData();
  }

  Future<String> getJSONData() async {
    if (lastPageId > _totalPages) {
      return "successful";
    }
    setState(() {
      isLoading = true;
    });
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    _userID = userID;
    try {
      var url = ServerSettings.getListFriendsURL +
          "?id=" + userID +
          "&page=" +
          lastPageId.toString();
      print(url);

      final response = await DioClient(dio).get(url);
      var jsonResponse = Friends.fromJson(response);
      _totalPages = jsonResponse.totalPages;
      setState(() {
        Friends newItems = jsonResponse;
        if (_friends == null) {
          _friends = newItems;
        } else {
          _friends.results.addAll(newItems.results);
        }
        lastPageId = lastPageId + 1;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<String> _getUnfriendUser(Result item) async {
    final dio = Dio();
    try {
      var url = ServerSettings.getUnfriendRequestURL +
          item.fromUser.id.toString() +
          "&to_user=" +
          item.toUser.id.toString();

      var response = await DioClient(dio).delete(url);
      _friends.results.remove(item);
      setState(() {
        isLoading = false;
        Scaffold.of(context).showSnackBar(
          SnackBar(
            behavior: SnackBarBehavior.floating,
            margin: EdgeInsets.only(bottom: 60.0, left: 10.0, right: 10.0),
            content: Text(translate("unfriend_success")),
          ),
        );
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }


  Future<void> _showMyDialog(BuildContext context, Result item) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("친구에서 삭제하시겠습니까?"),
          /*content: SingleChildScrollView(
            child: Text(message),
          ),*/
          actions: <Widget>[
            TextButton(
              child: Text(translate("group.delete.cancel")),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(translate("alert.button.ok")),
              onPressed: () {
                _getUnfriendUser(item);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
