import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import '../../components/user_list_item.dart';
import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/followers.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../profile.dart';

class FollowingSearch extends StatefulWidget {
  static const String id = 'following_search_screen';

  const FollowingSearch ({Key key}) : super(key: key);

  @override
  FollowingSearchState createState() => FollowingSearchState();
}

class FollowingSearchState extends State<FollowingSearch> {
  final searchController = TextEditingController();
  String searchText;
  int lastPageId = 1;
  int _totalPages = 2;
  bool isLoading = false;
  final storage = FlutterSecureStorage();
  ScrollController _controller;
  Followers _result;
  String _userID;
  String text;
  String _refresh = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(translate("search"))
          ],
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(_refresh)
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        controller: _controller,
        children: <Widget>[
        TextFormField(
          controller: searchController,
          onChanged: (text) => {_fetchList(text)},
          style: TextStyle(fontSize: 16),
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
            hintText: translate("search"),
            floatingLabelBehavior: FloatingLabelBehavior.always,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
            prefixIcon: Padding(
              padding: EdgeInsets.only(top: 5),
              child: Icon(Icons.search),
            )
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
          color: Colors.white,
          child: isLoading
              ? Center(
                  child: Lottie.asset(Assets.animLoad,
                      alignment: Alignment.center,
                      width: DeviceUtils.getScaledSize(context, 0.5)),
                )
              : _result == null || _result.results.isEmpty
              ? (searchController.text == ""
                ? Column(
                  children: [
                    Text(translate("enter.nickname.id"), style: kTextStyle),
                    Center(
                      child: Lottie.asset(Assets.animNoData,
                          alignment: Alignment.center,
                          width: DeviceUtils.getScaledSize(context, 0.5)),
                    )
                  ]
                )
                : Column(
                  children: [
                    Text(translate("media.search.no.data"), style: kTextStyle),
                    Center(
                      child: Lottie.asset(Assets.animNoData,
                          alignment: Alignment.center,
                          width: DeviceUtils.getScaledSize(context, 0.5)),
                    )
                  ]
                )
              )
              : ListView.builder(
                shrinkWrap: true,
              padding: const EdgeInsets.all(0),
              itemCount: _result == null ? 0 : _result.results.length,
              itemBuilder: (context, index) {
                return Slidable(
                  actionPane: SlidableDrawerActionPane(),
                  actionExtentRatio: 0.25,
                  child: _buildImageColumn(_result.results[index]),
                  secondaryActions: <Widget>[
                    IconSlideAction(
                      color: Colors.red,
                      icon: Icons.delete,
                      onTap: () {
                        _deleteUserUnfollow(_result.results[index].id);
                      },
                    ),
                  ],
                );
              },
            ),
          ),
        ]
      )
    );
  }

  Widget _buildImageColumn(Result item) => Container(
        decoration: BoxDecoration(color: Colors.white),
        margin: const EdgeInsets.only(bottom: 8.0),
        child: UserListItem(
          title: item.user.nickName,
          subtitle: item.user.stateMessage,
          profileImage: item.user.profileImage,
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ProfileScreen(userProfile: item.user),
              ),
            );
          },
          trailing: Container(
            child: FlatButton(
              height: 25.0,
              color: _userID == item.userId
                  ? kPrimaryColor
                  : Colors.white,
              onPressed: () {
                setState(() {
                  isLoading = true;
                });
                _followUser(item.userId);
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                  side: BorderSide(color: kPrimaryColor, width: 1.5)),
              child: Text(
                translate(_userID == item.userId
                    ? "following"
                    : "follow"),
                style: TextStyle(
                    color: _userID == item.userId
                        ? Colors.white
                        : kPrimaryColor),
              ),
            ),
          ),
        ),
      );

  Future<dynamic> _fetchList(String text) async {
    if (lastPageId > _totalPages) {
      return "successful";
    }
    this.text = text;
    setState(() {
      isLoading = true;
    });
    final dio = Dio();
    lastPageId = 1;
    _userID = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getFollowingSearchURL;
      var data = <String, dynamic>{
        "user_id": _userID,
        "keyword": text,
        "page": lastPageId
      };

      final response = await DioClient(dio).post(url, data: data);
      print(response);

      setState(() {
        _result = Followers.fromJson(response);
        _totalPages = _result.totalPages;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (e) {
    }
  }

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    _fetchList("");
    super.initState();
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
      !_controller.position.outOfRange) {
    setState(() {//you can do anything here
    });
  }
  if (_controller.offset <= _controller.position.minScrollExtent &&
      !_controller.position.outOfRange) {
    setState(() {//you can do anything here
      });
    }
  }

  void dispose() {
    searchController.dispose();

    super.dispose();
  }

  Future<String> _deleteUserUnfollow(int id) async {
    setState(() {
      isLoading = true;
    });
    final dio = Dio();
    try {
      var url = "${ServerSettings.getFollowUserURL}$id/";

      final response = await DioClient(dio).delete(url);
      _fetchList(text);

      setState(() {
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<String> _followUser(int userID) async {
    _refresh = "refresh";
    final dio = Dio();
    String followerId = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getFollowUserURL;
      Map data = {"user_id": userID, "follower_id": followerId};

      final response = await DioClient(dio).post(url, data: data);
      _fetchList(text);

      setState(() {
        // _followStatus = FollowStatus.fromJson(response);
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
      setState(() {
        isLoading = false;
      });
    }
  }
}
