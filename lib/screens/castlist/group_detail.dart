import 'package:dccast/screens/castlist/group_add.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../components/group/group_detail_header.dart';
import '../../components/group/group_detail_tabs.dart';
import '../../model/group_list.dart';
import '../../utils/group.dart';

class GroupDetailScreen extends StatefulWidget {

  static const String id = 'group_detail_screen';
  const GroupDetailScreen ({Key key}) : super(key: key);

  @override
  GroupDetailScreenState createState() => GroupDetailScreenState();
}

class GroupDetailScreenState extends State<GroupDetailScreen> {

  Group _group;

  @override
  Widget build(BuildContext context) {
    final _userID = ModalRoute.of(context).settings.arguments;
    _group = Provider.of<GroupUtil>(context, listen: false).chosenGroup;
    return Scaffold(
      appBar: AppBar(
        title: Text(_group != null ? _group?.name?.toString() : ""),
        actions: [
          (_userID != null && _group?.contactPerson == int.parse(_userID)) ?
          IconButton(
              icon: Icon(Icons.edit),
              onPressed: () async {
                final result = await 
                Navigator.pushNamed(context, GroupAddScreen.id);
                if (result == 'refreshDetail') {
                  setState(() {});
                } else if (result == 'refreshList') {
                  Navigator.of(context).pop("refreshList");                  
                }
              }) : Container()
        ],
      ),
      body: _group != null
          ? Column(
              children: [
                GroupDetailHeaderComponent(),
                Expanded(
                  child: GroupDetailTabsComponent(),
                )
              ],
            )
          : Container(),
    );
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    throw UnimplementedError();
  }
}
