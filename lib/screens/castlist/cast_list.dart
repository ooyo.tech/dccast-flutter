import 'package:dccast/screens/castlist/find_friends.dart';
import 'package:dccast/screens/castlist/group_search.dart';
import 'package:dccast/screens/castlist/following_search.dart';
import 'package:dccast/screens/my_content/friends.dart';
import 'package:dccast/utils/group.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';

import '../../constants/styles.dart';
import '../search_screen.dart';
import 'follower_search.dart';
import 'followers.dart';
import 'following.dart';
import 'following_user_search_screen.dart';
import 'friends.dart';
import 'friends_search.dart';
import 'group_add.dart';
import 'group_list.dart';

class CastListScreen extends StatefulWidget {
  static const String id = 'castlist_screen';

  @override
  _CastListScreenState createState() => _CastListScreenState();
}

class _CastListScreenState extends State<CastListScreen>
 with SingleTickerProviderStateMixin {
  GlobalKey<CastListFollowingState> _castListFollowingKey = GlobalKey();
  GlobalKey<CastListFriendsState> _friendsScreenKey = GlobalKey();
  GlobalKey<GroupListScreenState> _groupListScreenKey = GlobalKey();

  TabController _tabController;
  int indexTab = 0;
  int _requestLength = 0;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
          appBar: AppBar(
            title: Text(translate("cast_list")),
            bottom: TabBar(
              controller: _tabController,
              tabs: <Widget>[
                Tab(
                  text: translate("followers"),
                ),
                Tab(
                  text: translate("following"),
                ),
                Tab(
                  child: Stack(
                    clipBehavior: Clip.none,
                    children: <Widget>[
                      Text(translate("friends")),
                      // ignore: lines_longer_than_80_chars
                      if (Provider.of<GroupUtil>(context, listen: false).friendRequests != null && Provider.of<GroupUtil>(context, listen: false).friendRequests.isNotEmpty) Positioned(
                        right: -18,
                        child: Container(
                          padding: EdgeInsets.symmetric(
                            horizontal: 4,
                            vertical: 1
                          ),
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(6),
                          ),
                          constraints: BoxConstraints(
                            minWidth: 16,
                            minHeight: 12,
                          ),
                          child: Text(
                            '${Provider.of<GroupUtil>(context, listen: false)
                            .friendRequests.length}',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 10,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )
                    ],
                  )
                ),
                Tab(
                  text: translate("groups"),
                ),
              ]),
            actions: <Widget>[
              GestureDetector(
                onTap: () async {
                  if (indexTab == 3) {
                    Navigator.pushNamed(context, GroupSearchScreen.id);
                  } else if (indexTab == 2) {
                    Provider.of<GroupUtil>(context, listen: false)
                      .clearSearch;
                    final result = await Navigator.pushNamed(
                        context, FriendsSearchScreen.id);
                    if (result == 'refresh') {
                      _friendsScreenKey.currentState.refresh();
                    }
                  } else if (indexTab == 1) {
                    final result = await Navigator.pushNamed(
                        context, FollowingSearch.id);
                    print(result);
                    if (result == 'refresh') {
                      _castListFollowingKey.currentState.refreshFollowing();
                    }
                  } else if (indexTab == 0) {
                    final result = await Navigator.pushNamed(
                        context, FollowerSearch.id);
                    print(result);
                    if (result == 'refresh') {
                      _castListFollowingKey.currentState.refreshFollowing();
                    }
                  }
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12.0),
                  child: Icon(
                    Icons.search,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
          body: TabBarView(
            controller: _tabController,
            children: [
              CastListFollowers(),
              CastListFollowing(key : _castListFollowingKey),
              CastListFriends(key: _friendsScreenKey),
              GroupListScreen(key : _groupListScreenKey)
            ],
          ),
          floatingActionButton: indexTab == 3
              ? FloatingActionButton(
                  onPressed: () async {
                    Provider.of<GroupUtil>(context, listen: false)
                        .clearChosenGroup;
                    final result = await
                      Navigator.pushNamed(context, GroupAddScreen.id);
                    if (result == 'refreshList') {
                      _groupListScreenKey.currentState.refresh();
                    }
                  },
                  child: Icon(Icons.add),
                  backgroundColor: kPrimaryColor,
                )
              : indexTab == 2 ? FloatingActionButton(
                onPressed: () async {
                  Provider.of<GroupUtil>(context, listen: false)
                      .clearSearch;
                  final result = await Navigator.pushNamed(
                      context, FindFriendsScreen.id);
                  if (result == 'refresh') {
                    _friendsScreenKey.currentState.refresh();
                  }
                },
                child: Icon(Icons.add),
                backgroundColor: kPrimaryColor,
              )
              : indexTab == 1
                  ? FloatingActionButton(
                      onPressed: () async {
                        Provider.of<GroupUtil>(context, listen: false)
                            .clearSearch;
                        final result = await Navigator.pushNamed(
                            context, FollowingUserSearchScreen.id);
                        if (result == 'refresh') {
                          _castListFollowingKey.currentState.refreshFollowing();
                        }
                      },
                      child: Icon(Icons.add),
                      backgroundColor: kPrimaryColor,
                    )
                  : SizedBox()),
    );
  }

  @override
  void initState() {
    _tabController = TabController(length: 4, vsync: this);
    _tabController.addListener(() {
      setState(() {
        indexTab = _tabController.index;
      });
    });
    Provider.of<GroupUtil>(context, listen: false).fetchFriendRequests(
        success: (int _length) {
          _requestLength = _length;
          setState(() {
          });
        },
        error: (String error) {
        });
    super.initState();
  }
}
