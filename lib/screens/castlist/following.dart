import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import '../../components/user_list_item.dart';
import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/followers.dart';
import '../../model/user.dart';
import '../../utils/auth.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../profile.dart';

class CastListFollowing extends StatefulWidget {

  const CastListFollowing ({Key key}) : super(key: key);

  @override
  CastListFollowingState createState() => CastListFollowingState();

  void refresh() {
    CastListFollowingState().refreshFollowing();
  }
}

class CastListFollowingState extends State<CastListFollowing> {
  ScrollController scrollController = ScrollController();

  Followers _followings;
  Followers _unfollowings;
  FollowStatus _followStatus;
  int lastPageId = 1;
  int _totalPages = 2;
  bool isLoading = false;
  User user;
  final storage = FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    return Container(color: Colors.white, child: _buildPaginatedListView());
  }

  Widget _buildPaginatedListView() {
    return ModalProgressHUD(
      progressIndicator: Lottie.asset(Assets.animLoad,
          width: DeviceUtils.getScaledSize(context, 0.3),
          height: DeviceUtils.getScaledSize(context, 0.3)),
      inAsyncCall: isLoading,
      child: ((_followings?.results != null && _followings.results.isEmpty)
        && (_unfollowings?.results != null && _unfollowings.results.isEmpty))?
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Center(
              child: Text(translate("following.no.data"))
            ),
            Center(
              child: Lottie.asset(Assets.animNoData,
                  alignment: Alignment.center,
                  width: DeviceUtils.getScaledSize(context, 0.5)),
            )
          ]
        ) :
      Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          if (_followings?.results != null && _followings.results.isNotEmpty)
            Container(
              color: kGreyColor,
              width: DeviceUtils.getScaledWidth(context, 1.0),
              padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
              child: Text(translate("following.followings") + 
              "(${(_followings != null)? _followings?.results?.length : 0})",
              style: kNotificationSubtitle),
            ),
          if (_followings?.results != null && _followings.results.isNotEmpty)
            Container(
              color: Colors.white,
              margin: EdgeInsets.symmetric(vertical: 5.0),
              padding: EdgeInsets.symmetric(vertical: 4.0),
              child:_buildFollowingView()
            ),
          if (_unfollowings?.results != null
            && _unfollowings.results.isNotEmpty)
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
              color: kGreyColor,
              width: DeviceUtils.getScaledWidth(context, 1.0),
              child: Text(
                  translate("following.unfollowings") +
                      " (" +
                      _unfollowings?.results?.length.toString() +
                      ")",
                  style: kNotificationSubtitle),
            ),
          if (_unfollowings?.results != null
            && _unfollowings.results.isNotEmpty)
            Expanded(
              child: RefreshIndicator(
                onRefresh: refreshFollowing,
                child: _buildUnfollowingListView(),
              ),
            ),
        ],
      ),
    );
  }

  Widget _buildFollowingView() {
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.25,
      child: _buildImageColumn(_followings.results[0])
    );
  }

  Widget _buildUnfollowingListView() {
    return ListView.builder(
      controller: scrollController,
      padding: const EdgeInsets.all(0),
      itemCount: _unfollowings == null ? 0 : _unfollowings.results.length,
      itemBuilder: (context, index) {
        return Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: _buildImageColumn(_unfollowings.results[index]),
          secondaryActions: <Widget>[
            IconSlideAction(
              color: Colors.red,
              icon: Icons.delete,
              onTap: () {
                _deleteUserUnfollow(_unfollowings.results[index].id);
              },
            ),
          ],
        );
      },
    );
  }

  Widget _buildImageColumn(Result item) => Container(
        decoration: BoxDecoration(color: Colors.white),
        margin: const EdgeInsets.only(bottom: 8.0),
        child: UserListItem(
          title: item.user.nickName,
          subtitle: item.user.stateMessage,
          profileImage: item.user.profileImage,
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ProfileScreen(userProfile: item.user),
              ),
            );
          },
          trailing: Container(
            child: FlatButton(
              height: 25.0,
              color: _followStatus != null &&
                      _followStatus.userId == item.userId
                  ? kPrimaryColor
                  : Colors.white,
              onPressed: () {
                setState(() {
                  isLoading = true;
                });
                if (_followStatus != null && 
                    _followStatus.userId == item.userId) {
                  _unfollowUser(item);
                } else {
                  _followUser(item.userId);
                }
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                  side: BorderSide(color: kPrimaryColor, width: 1.5)),
              child: Text(
                translate(_followStatus != null &&
                        _followStatus.userId == item.userId
                    ? "following"
                    : "follow"),
                style: TextStyle(
                    color: _followStatus != null &&
                            _followStatus.userId == item.userId
                        ? Colors.white
                        : kPrimaryColor),
              ),
            ),
          ),
        ),
      );

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    getFollowingJSONData();
    getUnfollowingJSONData();
    super.initState();
    scrollController.addListener(() {
      if (!isLoading &&
          scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        getUnfollowingJSONData();
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  Future<String> refreshFollowing() {
    setState(() {
      isLoading = true;
    });
    lastPageId = 1;
    _totalPages = 2;
    getFollowingJSONData();
    return getUnfollowingJSONData();
  }

  Future<String> getUnfollowingJSONData() async {
    if (lastPageId > _totalPages) {
      return "successful";
    }
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getUnfollowUserURL +
          userID +
          "&page=" +
          lastPageId.toString();

      final response = await DioClient(dio).get(url);
      var jsonResponse = Followers.fromJson(response);
      _totalPages = jsonResponse.totalPages;
      setState(() {
        Followers newItems = jsonResponse;
        if (_unfollowings == null || lastPageId == 1) {
          _unfollowings = newItems;
        } else {
          _unfollowings.results.addAll(newItems.results);
        }
        lastPageId = lastPageId + 1;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<String> getFollowingJSONData() async {

    final dio = Dio();
    var userID = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getFollowingURL +
          userID +
          "&page=1";
          // + lastPageId.toString();

      final response = await DioClient(dio).get(url);
      var jsonResponse = Followers.fromJson(response);
      setState(() {
        Followers newItems = jsonResponse;
        _followings = newItems;
        if (newItems != null && newItems.results != null
         && newItems.results.isNotEmpty) {
          _followStatus = FollowStatus.fromJson(newItems.results[0].toJson());
        }
        // _followings?.results?.addAll(newItems.results);
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<String> _deleteUserUnfollow(int id) async {
    setState(() {
      isLoading = true;
    });
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      var url = "${ServerSettings.getFollowUserURL}$id/";

      final response = await DioClient(dio).delete(url);
      _unfollowings.results.removeWhere((element) => element.id == id);
      //getUnfollowingJSONData();

      setState(() {
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<String> _unfollowUser(Result item) async {
    final dio = Dio();
    String followerId = await storage.read(key: Preferences.user_id);
    try {
      var url = "${ServerSettings.deleteFollowUserURL}${item.userId}"
        +"&follower_id=$followerId";
      final response = await DioClient(dio).delete(url);
      _followings = null;

      lastPageId = 1;
      // getUnfollowingJSONData();
      if (_unfollowings?.results?.isEmpty ?? true) {
        _unfollowings = Followers(results: [], count: 0);
      }
      setState(() {
        _unfollowings.results.insert(0, item);
        _unfollowings.count++;
        _followStatus = FollowStatus.fromJson(response);
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<String> _followUser(int userID) async {
    final dio = Dio();
    String followerId = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getFollowUserURL;
      Map data = {"user_id": userID, "follower_id": followerId};

      final response = await DioClient(dio).post(url, data: data);
      Result currentFollowing;
      if (_followings?.results?.isNotEmpty ?? false) {
        currentFollowing = _followings?.results[0];
      }
      await getFollowingJSONData();
      _unfollowings.results.removeWhere((element) => element.userId == userID);
      if (currentFollowing != null) {
        _unfollowings.results.add(currentFollowing);
      }
      // refreshFollowing();

      setState(() {
        // _followStatus = FollowStatus.fromJson(response);
        // isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
      setState(() {
        isLoading = false;
      });
    }
  }
}
