import 'package:dccast/components/user_list_item.dart';
import 'package:dccast/constants/preferences.dart';
import 'package:dccast/constants/server_settings.dart';
import 'package:dccast/screens/profile.dart';
import 'package:dccast/utils/dio_client.dart';
import 'package:dccast/utils/group.dart';
import 'package:dccast/model/friends.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

import '../../constants/assets.dart';
import '../../constants/styles.dart';
import '../../utils/device.dart';
import '../../utils/media.dart';

class FriendRequestScreen extends StatefulWidget {
  static const String id = 'friend_requests_screen';
  @override
  FriendRequestState createState() =>
      FriendRequestState();
}

class FriendRequestState extends State<FriendRequestScreen> {
  final searchController = TextEditingController();
  String searchText;
  int lastPageId = 1;
  int _totalPages = 2;
  bool isLoading = false;
  bool _willRefresh = false;

  final storage = FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(translate("friend.search.requests.title"))
          ],
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            if (_willRefresh) {
              Navigator.of(context).pop("refresh");
            } else {
              Navigator.of(context).pop();
            }
            if (lastPageId < _totalPages) {
              Provider.of<GroupUtil>(context, listen: false).fetchFriendRequests(
                  success: (int _length) {
                    _totalPages = _length;
                  },
                  error: (String error) {
                  });
            }
          },
        ),
      ),
      body: ModalProgressHUD(
      progressIndicator: Lottie.asset(Assets.animLoad,
          width: DeviceUtils.getScaledSize(context, 0.3),
          height: DeviceUtils.getScaledSize(context, 0.3)),
      inAsyncCall: isLoading,
      child:
        ListView(
          shrinkWrap: true,
          children: <Widget>[
            Container(
              color: kGreyColor,
              width: DeviceUtils.getScaledWidth(context, 1.0),
              padding: const 
              EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
              child: Text(translate("friend.search.requests.title"),
              style: kNotificationSubtitle),
            ), Container(
              color: Colors.white,
              margin: EdgeInsets.symmetric(vertical: 5.0),
              padding: EdgeInsets.symmetric(vertical: 4.0),
              child:_buildListView()
            ),
          ]
        )
      )
    );
  }

  Widget _buildListView() {
    return Consumer<GroupUtil>(builder: (context, groupUtil, child) {
      if (groupUtil.friendRequests == null || 
        groupUtil.friendRequests.isEmpty) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Center(
              child: Text(translate("friend.request.no.data"))
            ),
            Center(
              child: Lottie.asset(Assets.animNoData,
                  alignment: Alignment.center,
                  width: DeviceUtils.getScaledSize(context, 0.5)),
            )
          ]
        );
      }
      return ListView.builder(
        shrinkWrap: true,
        padding: const EdgeInsets.all(0),
        itemCount: groupUtil.friendRequests == null ? 
          0 : groupUtil.friendRequests.length,
        itemBuilder: (context, index) {
          return Slidable(
            actionPane: SlidableDrawerActionPane(),
            actionExtentRatio: 0.25,
            child: _buildListItem(groupUtil.friendRequests[index], index),
            secondaryActions: <Widget>[
              IconSlideAction(
                color: Colors.red,
                icon: Icons.delete,
                onTap: () {
                  _deleteRequest(groupUtil.friendRequests[index], index);
                },
              ),
            ],
          );
        },
      );
    });
  }

  Widget _buildListItem(Result item, int index) => Container(
      decoration: BoxDecoration(color: Colors.white),
      margin: const EdgeInsets.only(bottom: 8.0),
      child: item != null
          ? UserListItem(
              title: item.fromUser.nickName,
              subtitle: item.fromUser.stateMessage,
              profileImage: item.fromUser.profileImage,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        ProfileScreen(userProfile: item.fromUser),
                  ),
                );
              },
              trailing: Container(
                child: FlatButton(
                  height: 25.0,
                  color: Colors.white,
                  onPressed: () {
                    if (!item.accepted) {
                      setState(() {
                        isLoading = true;
                      });
                      _acceptFriendRequest(item, index);
                    }
                  },
                  shape: item.accepted ? null : RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: kPrimaryColor, width: 1.5)),
                  child: Text(
                    translate( item.accepted ?
                      "friend.request.confirmed": "friend.request.confirm"),
                    style: TextStyle(color: item.accepted ? 
                      Colors.grey.shade400 : kPrimaryColor),
                  ),
                ),
              ),
            )
          : SizedBox()
    );

  void dispose() {
    searchController.dispose();

    super.dispose();
  }

  Future<String> _acceptFriendRequest(Result item, int index) async {
    final dio = Dio();
    try {
      var url = ServerSettings.getAcceptFriendRequestURL;
      Map<String, dynamic> data = {
        "from_user": item.fromUser.id, 
        "to_user": item.toUser.id
      };
      final response = await DioClient(dio).post(url, data: data);

      Provider.of<GroupUtil>(context, listen: false)
        .friendRequests[index].accepted = true;
      _willRefresh = true;
      setState(() {
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<String> _deleteRequest(Result item, int index) async {
    final dio = Dio();
    String fromuser = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getDenyFriendRequestURL + 
        "?from_user=${item.fromUser.id}" +
        "&to_user=${item.toUser.id}";
      final response = await DioClient(dio).delete(url);

      Provider.of<GroupUtil>(context, listen: false)
        .friendRequests.removeAt(index);
      setState(() {
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
      setState(() {
        isLoading = false;
      });
    }
  }
}
