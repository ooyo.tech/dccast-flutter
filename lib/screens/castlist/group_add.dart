import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:dio/dio.dart' as Dio;
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

import '../../components/group/group_members.dart';
import '../../components/user_list_item.dart';
import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/group_list.dart';
import '../../model/group_members.dart' as GroupMemberModel;
import '../../screens/castlist/group_members.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../../utils/group.dart';

class GroupAddScreen extends StatefulWidget {
  static const String id = 'group_add_screen';

  @override
  _GroupAddScreenState createState() => _GroupAddScreenState();
}

enum Profile { CAMERA, GALLERY }

class _GroupAddScreenState extends State<GroupAddScreen> {
  final storage = FlutterSecureStorage();
  final titleController = TextEditingController();
  final descriptionController = TextEditingController();
  List<int> memberIDs;
  bool isLoading = false;
  Group _selectedGroup;

  /// Variables
  File imageFile;

  final _formKey = GlobalKey<FormState>();

  /// Get from gallery
  _getFromGallery() async {
    PickedFile pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      setState(() {
        imageFile = File(pickedFile.path);
      });
    }
  }

  /// Get from camera
  _getFromCamera() async {
    PickedFile pickedFile = await ImagePicker().getImage(
      source: ImageSource.camera,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      setState(() {
        imageFile = File(pickedFile.path);
      });
    }
  }

  @override
  void initState() {
    super.initState();
    // call get json data function
    // selectedPage = Selec
    _selectedGroup = Provider.of<GroupUtil>(context, listen: false).chosenGroup;
    if (_selectedGroup.id != null) {
      titleController.text = _selectedGroup?.name;
      descriptionController.text = _selectedGroup?.message;
      this.fetchMembersList();
    }
  }

  void dispose() {
    titleController.dispose();
    descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        color: Colors.white,
        child: ModalProgressHUD(
          progressIndicator: Lottie.asset(Assets.animLoad,
              width: DeviceUtils.getScaledSize(context, 0.3),
              height: DeviceUtils.getScaledSize(context, 0.3)),
          inAsyncCall: isLoading,
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                GestureDetector(
                  child: Container(
                    padding: EdgeInsets.all(16.0),
                    child: imageFile != null
                        ? Container(
                            width: DeviceUtils.getScaledWidth(context, 0.3),
                            height: DeviceUtils.getScaledWidth(context, 0.3),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: FileImage(imageFile),
                                    fit: BoxFit.fill)),
                          )
                        : Container(
                            width: DeviceUtils.getScaledWidth(context, 0.3),
                            height: DeviceUtils.getScaledWidth(context, 0.3),
                            decoration: _selectedGroup.id != null && 
                                  _selectedGroup.profileImg != null ?
                            BoxDecoration(
                              shape: BoxShape.circle,
                              color: kGreyColor,
                              image: 
                                DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(_selectedGroup.profileImg)
                                )
                              ) : null,
                            child: _selectedGroup.id == null || 
                            _selectedGroup.profileImg == null ?
                            ClipOval (
                              child: Container(
                              width: DeviceUtils.getScaledWidth(context, 0.3),
                              height: DeviceUtils.getScaledWidth(context, 0.3),
                              color: kGreyColor,
                              child: Icon(
                                FontAwesomeIcons.users,
                                color: kBlack50Color,
                                size: DeviceUtils.getScaledWidth(context, 0.15),
                            )
                          )
                        ) : null
                    )
                  ),
                  onTap: () {
                    _askUser();
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return translate("castlist.group.add.empty.title");
                      }
                      return null;
                    },
                    controller: titleController,
                    style: TextStyle(color: kBlackColor, fontSize: 16),
                    maxLength: 40,
                    decoration: InputDecoration(
                      labelText: translate("castlist.group.add.name"),
                      focusColor: kBlackColor,
                      enabledBorder: const UnderlineInputBorder(
                        borderSide:
                            const BorderSide(color: kGreyColor, width: 1.0),
                      ),
                      focusedBorder: const UnderlineInputBorder(
                        borderSide:
                            const BorderSide(color: kGreyColor, width: 1.0),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return translate("castlist.group.add.description");
                      }
                      return null;
                    },
                    maxLines: null,
                    keyboardType: TextInputType.multiline,
                    controller: descriptionController,
                    style: TextStyle(color: kBlackColor, fontSize: 16),
                    decoration: InputDecoration(
                      labelText: translate("castlist.group.add.description"),
                      enabledBorder: const UnderlineInputBorder(
                        borderSide:
                            const BorderSide(color: kGreyColor, width: 1.0),
                      ),
                      focusedBorder: const UnderlineInputBorder(
                        borderSide:
                            const BorderSide(color: kGreyColor, width: 1.0),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 24.0),
                ListTile(
                  tileColor: kGreyColor,
                  title: Text(
                    translate("castlist.group.members"),
                    style: kTextStyle,
                  ),
                  trailing: FlatButton(
                    child: Text(
                      translate("castlist.group.add-member"),
                      style: kTextPrimaryStyle,
                    ),
                    onPressed: () async {
                      await Navigator.pushNamed(context, GroupMembers.id);
                      setState(() {
                        
                      });
                    },
                  ),
                ),
                Expanded(
                  child:
                      Consumer<GroupUtil>(builder: (context, members, child) {
                    memberIDs = List<int>();

                    return //_selectedGroup.id != null
                        //? GroupMembersComponent() : 
                        ListView.builder(
                            padding: const EdgeInsets.all(0),
                            itemCount: members.selectedMembers == null
                                ? 0
                                : members.selectedMembers.length,
                            itemBuilder: (context, index) {
                              memberIDs.add(members.selectedMembers[index].id);
                              return Container(
                                decoration: BoxDecoration(color: Colors.white),
                                margin: const EdgeInsets.only(bottom: 8.0),
                                child: UserListItem(
                                    title: members
                                        .selectedMembers[index].user.username,
                                    subtitle:
                                        members.selectedMembers[index].nickName,
                                    profileImage: members
                                        .selectedMembers[index].profileImage),
                              );
                            });
                  }),
                ),
                _selectedGroup.id != null
                    ? FlatButton(
                        onPressed: () {
                          _showMyDialog(
                              context,
                              translate("group.delete.title"),
                              TextButton(
                                child: Text(translate("alert.button.ok")),
                                onPressed: () {
                                  Navigator.pop(context);
                                  _leave();
                                },
                              ));
                        },
                        color: kRedColor,
                        minWidth: DeviceUtils.getScaledWidth(context, 0.8),
                        child: Text(
                          translate("castlist.group.leave"),
                          style: kSideTitleStyle,
                        ))
                    : SizedBox(),
                FlatButton(
                    onPressed: () {
                      _submit();
                    },
                    color: kPrimaryColor,
                    minWidth: DeviceUtils.getScaledWidth(context, 0.8),
                    child: Text(
                      _selectedGroup.id != null
                      ? translate("castlist.group.edit-group")
                      : translate("castlist.group.add-group"),
                      style: kSideTitleStyle,
                    )),
                SizedBox(height: 16.0),
              ],
            ),
          ),
        ),
      ),
      resizeToAvoidBottomInset: false,
    );
  }

  void _submit() async {
    if (_formKey.currentState.validate()) {
      final dio = Dio.Dio();
      try {
        String userID = await storage.read(key: Preferences.user_id);
        memberIDs.add(int.parse(userID));
        setState(() {
          isLoading = true;
        });
        var data = {
          "name": titleController.text,
          "message": descriptionController.text,
          "contact_person": int.parse(userID),
          "membersJson": jsonEncode(memberIDs)
        };
        if (imageFile != null) {
          data["profile_img"] = await 
            Dio.MultipartFile.fromFile(imageFile.path);
        }
        Dio.FormData formData = Dio.FormData.fromMap(data);

        String submitURL = _selectedGroup.id != null
            ? "${ServerSettings.getGroupsURL}/${_selectedGroup.id}/update_group/"
            : "${ServerSettings.getGroupsURL}/";
        await DioClient(dio)
            .post(
          submitURL,
          data: formData,
        )
        .then((value) {
          setState(() {
            isLoading = false;
            Provider.of<GroupUtil>(context, listen: false).clearMembers;
            if (_selectedGroup.id != null) {
              var respGroup = Group.fromJson(value);
              respGroup.members = memberIDs;
              var index = Provider.of<GroupUtil>(context, listen: false).
                userGroupList.results.indexWhere((element) => element.id == respGroup.id);
              Provider.of<GroupUtil>(context, listen: false).
                userGroupList.results[index] = respGroup;
              Provider.of<GroupUtil>(context, listen: false).selectGroup(respGroup);
              Provider.of<GroupUtil>(context, listen: false).notifyListeners();
              Navigator.pop(context, "refreshDetail");
            } else {
              Navigator.pop(context, "refreshList");
            }
          });
        });
      } on Dio.DioError catch (e) {
        setState(() {
          isLoading = false;
        });
        _showMyDialog(context, e.response.toString(), null);
      }
    }
  }

  void _leave() async {
    _selectedGroup = Provider.of<GroupUtil>(context, listen: false).chosenGroup;
    final dio = Dio.Dio();
    try {
      String userID = await storage.read(key: Preferences.user_id);

      setState(() {
        isLoading = true;
      });

      Map<String, dynamic> data = {"member_id": int.parse(userID)};
      var url = ServerSettings.getGroupsURL +
                  "/" +
                  _selectedGroup.id.toString() +
                  "/members/?member_id=$userID";
      print(url);

      await DioClient(dio)
          .delete(url)
              // queryParameters: data)
          .then((value) {
        setState(() {
          isLoading = false;
        });
        Navigator.of(context).pop('refreshList');
      });
    } on Dio.DioError catch (e) {
      setState(() {
        isLoading = false;
      });
      _showMyDialog(context, e.response.toString(), null);
    }
  }

  Future<void> fetchMembersList() async {
    _selectedGroup = Provider.of<GroupUtil>(context, listen: false).chosenGroup;
    if (_selectedGroup.id != null) {
      final dio = Dio.Dio();
      try {
        final response = await DioClient(dio).get(ServerSettings.getGroupsURL +
            "/" +
            _selectedGroup.id.toString() +
            "/members");
        print("found");
        print(response);

        var jsonResponse = GroupMemberModel.fromJson(response);
        Provider.of<GroupUtil>(context, listen: false)
            .selectGroupMembers(jsonResponse.results);
      } on Dio.DioError catch (error) {
        _showMyDialog(context, error.response.toString(), null);
      }
    }
  }

  Future _askUser() async {
    switch (await showDialog(
        context: context,
        builder: (context) => SimpleDialog(
              title: Text(translate("vod.upload.select-from")),
              elevation: 4,
              children: <Widget>[
                SimpleDialogOption(
                  child: Text(translate("vod.upload.select-gallery")),
                  onPressed: () {
                    Navigator.pop(context, Profile.GALLERY);
                  },
                ),
                SimpleDialogOption(
                  child: Text(translate("vod.upload.select-camera")),
                  onPressed: () {
                    Navigator.pop(context, Profile.CAMERA);
                  },
                ),
              ],
            ))) {
      case Profile.GALLERY:
        _getFromGallery();
        break;
      case Profile.CAMERA:
        _getFromCamera();
        break;
    }
  }
}

Future<void> _showMyDialog(
    BuildContext context, String message, Widget extraBtn) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(translate("alert.title.warning")),
        content: SingleChildScrollView(
          child: Text(message),
        ),
        actions: <Widget>[
          TextButton(
            child: extraBtn != null
                ? Text(translate("group.delete.cancel"))
                : Text(translate("alert.button.ok")),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          extraBtn
        ].where((obj) => obj != null).toList(),
      );
    },
  );
}
