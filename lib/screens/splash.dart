import 'dart:async';

import 'package:flutter/material.dart';

import '../constants/assets.dart';
import '../constants/styles.dart';
import 'home.dart';

class SplashScreen extends StatefulWidget {
  static const String id = 'splash_screen';
  @override
  State<StatefulWidget> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation animation;

  @override
  void initState() {
    super.initState();
    startTimer();
    controller =
        AnimationController(duration: Duration(seconds: 3), vsync: this);
    animation =
        ColorTween(begin: Colors.blue, end: kPrimaryColor).animate(controller);
    controller.forward();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: animation.value,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
            child: Hero(
              tag: 'logo',
              child: Container(
                height: 60.0,
                child: Image.asset(Assets.appLogo),
              ),
            ),
          )
        ],
      ),
    );
  }

  startTimer() {
    var _duration = Duration(milliseconds: 2000);
    return Timer(_duration, navigate);
  }

  navigate() async {
    Navigator.popAndPushNamed(context, HomeScreen.id);
    // Navigator.pushNamed(context, HomeScreen.id);
  }
}
