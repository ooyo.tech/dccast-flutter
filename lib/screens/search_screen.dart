import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pagewise/flutter_pagewise.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import '../components/video/videolistitem.dart';
import '../constants/assets.dart';
import '../constants/server_settings.dart';
import '../constants/styles.dart';
import '../model/user_media.dart';
import '../utils/device.dart';
import '../utils/dio_client.dart';
import '../utils/media.dart';
import '../utils/pagewise.dart';
import '../utils/toast.dart';
import 'video_detail.dart';

class SearchScreen extends StatefulWidget {
  static const String id = 'search_screen';
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final searchController = TextEditingController();
  int lastPageId = 1;
  UserMedia searchVodModel;
  EditablePagewiseLoadController _pageLiveLoadController, _pageVodLoadController;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
    length: 2,
    child: Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            Expanded(
              child: TextFormField(
                controller: searchController,
                onFieldSubmitted: (text) => {_fetchList(text)},
                style: TextStyle(color: Colors.white, fontSize: 16),
                decoration: InputDecoration(
                  hintText: translate("search"),
                  hintStyle: TextStyle(color: kGreyColor),
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                ),
              ),
            ),
            if (searchController.text != "")
            IconButton(
              icon: Icon(Icons.close_rounded),
              onPressed: () {
                setState(() {
                  searchController.clear();
                });
                Provider.of<MediaUtils>(context, listen: false).clearSearch;
              },
            )
          ],
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
        bottom: searchController.text == "" ? null :
          PreferredSize(
          preferredSize: Size.fromHeight(30.0),
          child: Container(
            height: 40.0,
            padding: EdgeInsets.symmetric(vertical: 0.0),
            color: Colors.white,
            child: TabBar(
              labelColor: kPrimaryColor,
              unselectedLabelColor: kBlackColor,
              labelStyle: kTextPrimaryStyle,
              indicator: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: kPrimaryColor,
                    width: 2.0
                  )
                )
              ),
              tabs: [
                Tab(child: 
                  Text(translate("live"),
                    style: TextStyle(
                      fontWeight: FontWeight.bold
                    )
                  )
                ),
                Tab(child: 
                  Text(translate("vod"),
                    style: TextStyle(
                      fontWeight: FontWeight.bold
                    )
                  )
                ),
              ],
            )
          )
        ),
      ),
      body: TabBarView(children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
          color: Colors.white,
          child: Column(children: [
            Expanded(child: 
                Consumer<MediaUtils>(builder: (context, media, child) {
                  return _buildListView(_pageLiveLoadController);
                }
              )
            ),
            ]
          )
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
          color: Colors.white,
          child: Column(children: [
            Expanded(child: 
                  Consumer<MediaUtils>(builder: (context, media, child) {
                    return _buildListView(_pageVodLoadController);
                  }
                )
              ),
            ]
          )
        ),
      ])
    )
    );
  }

  Widget _buildListView(EditablePagewiseLoadController _pageController) {
    var history = Provider.of<MediaUtils>(context, listen: false).searchHistoryModel;
    return searchController.text == "" ? 
    history != null && history.results.isNotEmpty ?
      ListView.builder(
        padding: const EdgeInsets.all(0),
        itemCount: history?.results?.length,
        itemBuilder: (context, index) {
          return _buildHistoryItem(history?.results[index], index);
        },
      )
    : Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(translate("search_term"), style: kTextStyle)
        ],
      ),
    )
    : PagewiseListView(
        pageLoadController: _pageController,
        itemBuilder: (context, entry, index) {
          return _buildListItem(entry, index);
        },
        noItemsFoundBuilder: (context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Lottie.asset(Assets.animNoData,
                  alignment: Alignment.center,
                  width: DeviceUtils.getScaledSize(context, 0.5)),
              Text("\“${searchController.text}\“ " + 
                translate("no_search_result"), style: kTextStyle)
            ],
          );
        },
        loadingBuilder: (context) => Center(
          child: Lottie.asset(Assets.animLoad,
            width: DeviceUtils.getScaledSize(context, 0.3),
            height: DeviceUtils.getScaledSize(context, 0.3)),
        ),
        showRetry: false,
        errorBuilder: (context, error) {
          return Text('Error: $error');
        }
    );
  }

  Widget _buildListItem(Media item, int index) => Container(
      decoration: BoxDecoration(color: Colors.white),
      margin: const EdgeInsets.only(bottom: 8.0),
      child: item != null
          ? VideoListItem(
              id: item?.id,
              title: item?.title,
              subtitle: item?.explanation,
              kinds: item?.kinds,
              dates: item?.created,
              crown: item?.isHit,
              duration: secToMinConverter(item?.duration ?? 0),
              views: item?.views,
              thumnail: item?.mediaThumbnail,
              media: item,
              onPressed: () => _detailScreen(context, item),
              onDelete: () {
                setState(() {
                  Provider.of<MediaUtils>(context, listen: false)
                    .searchLiveModel.results.removeAt(index);
                  Provider.of<MediaUtils>(context, listen: false)
                    .searchLiveModel.count--;
                });
              },
            )
          : SizedBox());

  Widget _buildHistoryItem(SearchHistory item, int index) => Container(
      decoration: BoxDecoration(color: Colors.white),
      margin: const EdgeInsets.only(bottom: 0.0),
      padding: const EdgeInsets.all(0.0),
      child: item != null
          ? ListTile(
            onTap: () {
              searchController.text = item.text;
              _fetchList(item.text);
            },
            title: Text(item.text, style: TextStyle(fontSize: 12)),
            trailing: GestureDetector(
              onTap: () {
                Provider.of<MediaUtils>(context, listen: false)
                  .deleteSearchHistory(index);
              },
              child: Icon(
                Icons.close,
                size: 18),
              )
            )
          : SizedBox());

  void _detailScreen(BuildContext context, Media item) {
    if (item.category == "LIVE" && !item.alive) {
      showToast(translate("live.ended"));
      return;
    }
    var contentType = item.category == "LIVE" ? eContent.live : eContent.vod;
    Provider.of<MediaUtils>(context, listen: false)
        .mediaDetail(item, contentType);
    Future.delayed(Duration(milliseconds: 250), () {
      Navigator.push(
          context,//VideoDetailScreen.id
          MaterialPageRoute(
            builder: (context) => VideoDetailScreen(content: contentType),
          )
          );
    });
  }

  void _fetchList(String searchText) {
    if (searchText == "") {
      setState(() {
        searchController.clear();
        Provider.of<MediaUtils>(context, listen: false).clearSearch;
      });
      return;
    }
    /*var profile = Provider.of<Auth>(context, listen: false).userProfile;
    if (profile != null && profile.stopRecentSearch) {
      Provider.of<MediaUtils>(context, listen: false)
        .createSeachHistory(profile.id, searchText);
    }*/
    _pageVodLoadController.reset();
    _pageLiveLoadController.reset();
    Provider.of<MediaUtils>(context, listen: false).fetchSearchMedias(
      text: searchText,
      success: () {
      },
      error: (String error) {
        Scaffold.of(context).showSnackBar(
          SnackBar(
            behavior: SnackBarBehavior.floating,
            margin: EdgeInsets.only(bottom: 60.0, left: 10.0, right: 10.0),
            content: Text(error),
          ),
        );
      });
  }

  void initState() {
    Provider.of<MediaUtils>(context, listen: false).fetchSearchHistory();
    _pageVodLoadController = EditablePagewiseLoadController(
      pageSize: 20,
      pageFuture: (pi) async {
        var result = await getData('VOD', pi);
        setState(() { });
        return result;
      }
    );
    _pageLiveLoadController = EditablePagewiseLoadController(
      pageSize: 20,
      pageFuture: (pi) async {
        var result = await getData('LIVE', pi);
        setState(() { });
        return result;
      }
    );
    super.initState();
  }

  Future<List<Media>> getData(String category, int pageID) async {
    final dio = Dio();
    try {
      String url = "${ServerSettings.getSearchByTitle}" + 
        "${searchController.text}" + 
        "&category=$category&ordering=-created&limit=20&page=${pageID+1}";
      var response = await DioClient(dio).get(url);
      var medias = UserMedia.fromJson(response);
      return medias.results;
    } on Exception catch (error) {
      print(error);
    }
    return [];
  }

  void dispose() {
    _pageVodLoadController?.dispose();
    _pageLiveLoadController?.dispose();
    searchController.dispose();

    super.dispose();
  }
}
