import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:http/http.dart' as http;
import 'package:package_info/package_info.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:provider/provider.dart';

import '../constants/server_settings.dart';
import '../constants/styles.dart';
import '../utils/auth.dart';
import '../utils/device.dart';
import '../utils/dio_client.dart';
import '../utils/toast.dart';
import 'home.dart';
import 'login.dart';
import 'settings/history_privacy.dart';
import 'settings/notice_settings.dart';
import 'settings/oss_licenses.dart';
import 'settings/privacy_policy.dart';
import 'settings/question.dart';
import 'settings/terms_of_use.dart';

class SettingsScreen extends StatefulWidget {
  static const String id = 'settings_screen';

  @override
  SettingsScreenState createState() => SettingsScreenState();

}

class SettingsScreenState extends State<SettingsScreen> {
  String _version;
  PackageInfo packageInfo;
  final TextEditingController _pinPutController = TextEditingController();
  final FocusNode _pinPutFocusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    var profile = Provider.of<Auth>(context, listen: false).userProfile;
    return Scaffold(
      appBar: AppBar(
        title: Text(translate('general')),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SwitchListTile(
              title: Text(
                translate('auto_login'),
                style: kNotificationTitle,
              ),
              value: profile != null ? profile.autoLogin : false,
              onChanged: (val) async {
                if (profile == null) {
                  Navigator.pushNamed(context, LoginScreen.id);
                  return;
                }
                var status = await _updateProfile(context, "auto_login", val);
                if (status == 200) {
                  Provider.of<Auth>(context, listen: false).autLogin(val);
                  setState(() {
                    Provider.of<Auth>(context, listen: false)
                      .userProfile.autoLogin = val;
                  });
                }
              },
            ),
            Container(
              color: kGreyColor,
              width: DeviceUtils.getScaledWidth(context, 1.0),
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              child: Text(
                translate('setting_saved'),
                style: kNotificationTitle,
              ),
            ),
            SwitchListTile(
              title: Text(
                translate('mobile_data_usage'),
                style: kNotificationTitle,
              ),
              subtitle: Text(
                translate('mobile_data_usage_desc'),
                style: kNotificationTitle,
              ),
              value: Provider.of<Auth>(context, listen: false).lteStatus,
              onChanged: (val) async {
                setState(() {
                  Provider.of<Auth>(context, listen: false).lteStatus = val;
                });
                showToast(val
                    ? "모바일 데이터 사용 제한이 되었습니다.\nWiFi에서만 자동 재생됩니다."
                    : "모바일 데이터 사용 제한이 해제되었습니다.\n3G/LTE/WiFi 에서 자동 재생 됩니다");
              },
            ),
            ListTile(
              title: Text(
                translate('notification_setting'),
                style: kNotificationTitle,
              ),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: kPrimaryColor,
              ),
              onTap: () {
                if (profile == null) {
                  Navigator.pushNamed(context, LoginScreen.id);
                  return;
                }
                Navigator.pushNamed(context, NoticeSettingScreen.id);
              },
            ),
            ListTile(
              title: Text(
                translate('history_privacy'),
                style: kNotificationTitle,
              ),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: kPrimaryColor,
              ),
              onTap: () {
                if (profile == null) {
                  Navigator.pushNamed(context, LoginScreen.id);
                  return;
                }
                Navigator.pushNamed(context, HistoryPrivacyScreen.id);
              },
            ),
            SwitchListTile(
              title: Text(
                translate('passsword_lock'),
                style: kNotificationTitle,
              ),
              value: profile != null ? profile.setPass : false,
              onChanged: (val) {
                if (profile == null) {
                  Navigator.pushNamed(context, LoginScreen.id);
                  return;
                }
                _pin1 = '';
                _setPinLock(val, translate('passsword_lock'));
              },
            ),
            Container(
              color: kGreyColor,
              width: DeviceUtils.getScaledWidth(context, 1.0),
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              child: Text(
                translate('service_central'),
                style: kNotificationTitle,
              ),
            ),
            ListTile(
              title: Text(
                translate('contact_us'),
                style: kNotificationTitle,
              ),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: kPrimaryColor,
              ),
              onTap: () {
                if (profile == null) {
                  Navigator.pushNamed(context, LoginScreen.id);
                  return;
                }
                Navigator.pushNamed(context, QuestionScreen.id);
              },
            ),
            ListTile(
              title: Text(
                translate("app_version") + 
                "(${packageInfo?.version ?? ' '})",
//                _version != null ? "($_version)" : "",
                style: kNotificationTitle,
              ),
              onTap: null,
              trailing: (_version != null && packageInfo != null 
              && packageInfo.version != _version) ? 
                FlatButton(
                onPressed: () {},
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(color: Colors.green)),
                color: Colors.green,
                child: Text(
                  translate("alert.button.update"),
                  style: TextStyle(color: Colors.white, fontSize: 12.0),
                ),
              ) : FlatButton(),
            ),
            Container(
              color: kGreyColor,
              width: DeviceUtils.getScaledWidth(context, 1.0),
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              child: Text(
                translate('terms_policy'),
                style: kNotificationTitle,
              ),
            ),
            ListTile(
              title: Text(
                translate('terms_policy'),
                style: kNotificationTitle,
              ),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: kPrimaryColor,
              ),
              onTap: () {
                Navigator.pushNamed(context, TermsOfUseScreen.id);
              },
            ),
            ListTile(
              title: Text(
                translate('privacy_policy'),
                style: kNotificationTitle,
              ),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: kPrimaryColor,
              ),
              onTap: () {
                Navigator.pushNamed(context, PrivacyPolicyScreen.id);
              },
            ),
            ListTile(
              title: Text(
                translate('opensource_license'),
                style: kNotificationTitle,
              ),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: kPrimaryColor,
              ),
              onTap: () {
                Navigator.push(context, 
                MaterialPageRoute(builder: (context) => OssLicensesPage()));
              },
            ),
            Container(
              color: kGreyColor,
              width: DeviceUtils.getScaledWidth(context, 1.0),
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              child: SizedBox(
                height: 16.0,
              ),
            ),
            ListTile(
              title: Text(
                translate('log_out'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: kRedColor,
                ),
              ),
              onTap: () {
                Provider.of<Auth>(context, listen: false).signout(success: () {
                  Navigator.pushNamed(context, HomeScreen.id);
                });
              },
            ),
            SizedBox(
              height: 20.0,
            ),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    _fetchVersion();
    super.initState();
  }

  Future<void> _fetchVersion() async {
    packageInfo = await PackageInfo.fromPlatform();
    final dio = Dio();
    try {
      var url = ServerSettings.getVersionURL;

      final response = await DioClient(dio).get(url);
      _version = response["version"];

      setState(() {
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
      });
    }
  }

  BoxDecoration get _pinPutDecoration {
    return BoxDecoration(
      border: Border.all(color: kBlackColor),
      borderRadius: BorderRadius.circular(10.0),
    );
  }

  String _pin1 = '';
  void _setPinLock(bool value, _title) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: false,
      builder: (BuildContext bc) {
        // return 
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          //heightFactor: 0.5,
          child: Wrap(
            children: <Widget>[
              ListTile(
                title: Text(_title),
                trailing: Icon(
                  Icons.close_rounded,
                  color: kBlackColor,
                ),
                onTap: () => {Navigator.of(context).pop()},
              ),
              Divider(
                color: kBlack50Color,
                height: 1.0,
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3.0),
                  border: Border.all(color: Colors.white),
                ),
                padding: const EdgeInsets.all(20.0),
                child: PinPut(
                  fieldsCount: 4,
                  preFilledWidget: Text("*"),
                  autofocus: true,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  withCursor: true,
                  textStyle:
                      const TextStyle(fontSize: 25.0, color: kPrimaryColor),
                  eachFieldWidth: 50.0,
                  eachFieldHeight: 50.0,
                  onSubmit: (var pin) async {
                    _pinPutController.text = '';
                    var _setPass = Provider.of<Auth>(context, listen: false)
                            .userProfile.setPass;
                    if (_setPass) {
                      var _pass = Provider.of<Auth>(context, listen: false)
                              .userProfile.passString;
                      if (_pass == pin) {
                        var status = await
                          _updateProfile(context, "set_pass", false);
                        if (status == 200) {
                          setState(() {
                            Provider.of<Auth>(context, listen: false)
                              .userProfile.setPass = false;
                          });
                          Navigator.of(context).pop();
                        }
                      } else {
                        _showMyDialog(context, 
                          translate("passsword_lock_wrong"));
                        setState(() {
                        });
                      }
                    } else {
                      if (_pin1 == '') {
                        _pin1 = pin;
                        Navigator.of(context).pop();
                        _setPinLock(value, translate('passsword_lock_re'));
                        setState(() {
                        });
                      } else {
                        if (_pin1 == pin) {
                          var status = await _setPass2(context, pin);
                          if (status == 200) {
                            setState(() {
                              Provider.of<Auth>(context, listen: false)
                                .userProfile.setPass = true;
                            });
                            Navigator.of(context).pop();
                          }
                        } else {
                          _showMyDialog(context, 
                            translate("passsword_lock_wrong"));
                        }
                      }
                    }
                  },
                  focusNode: _pinPutFocusNode,
                  controller: _pinPutController,
                  submittedFieldDecoration: _pinPutDecoration,
                  selectedFieldDecoration: _pinPutDecoration.copyWith(
                    color: Colors.white,
                    border: Border.all(
                      width: 2,
                      color: kPrimaryColor,
                    ),
                  ),
                  followingFieldDecoration: _pinPutDecoration,
                ),
              ),
            ],
          ),
        );
      });
  }
  
  Future<void> _showMyDialog(BuildContext context, String title) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          /*content: SingleChildScrollView(
            child: Text(message),
          ),*/
          actions: <Widget>[
            TextButton(
              child: Text(translate("alert.button.ok")),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<int> _updateProfile (context, key, dynamic value) async {
    var userID = Provider.of<Auth>(context, listen: false).userProfile.id;
    var authToken = Provider.of<Auth>(context, listen: false).token;

    try {
      var url = ServerSettings.updateProfileURL + userID.toString();
      var data = {
        "user_no": userID,
        "user": userID,
        key: value
      };
      var _statusCode = 500;

      await http.put(Uri.parse("$url/"), body: jsonEncode(data), headers: {
          "Authorization" : "Token $authToken",
          "Content-Type": "application/json"
        }).then((result) {
        print(result.statusCode);
        print(result.body);
        _statusCode = result.statusCode;
        return result.statusCode;
      });
      return _statusCode;

    } on Exception {
      return 500;
    }
  }

  Future<int> _setPass2 (context, dynamic value) async {
    var userID = Provider.of<Auth>(context, listen: false).userProfile.id;
    var authToken = Provider.of<Auth>(context, listen: false).token;

    try {
      var url = ServerSettings.updateProfileURL + userID.toString();
      var data = {
        "user_no": userID,
        "user": userID,
        "set_pass": true,
        "pass_string": value
      };
      var _statusCode = 500;

      await http.put(Uri.parse("$url/"), body: jsonEncode(data), headers: {
          "Authorization" : "Token $authToken",
          "Content-Type": "application/json"
        }).then((result) {
        print(result.statusCode);
        print(result.body);
        _statusCode = result.statusCode;
        return result.statusCode;
      });
      return _statusCode;

    } on Exception {
      return 500;
    }
  }
}
