import 'package:dccast/constants/preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../constants/assets.dart';
import '../constants/server_settings.dart';
import '../constants/styles.dart';
import '../utils/auth.dart';

class LoginScreen extends StatelessWidget {
  static String id = 'login_screen';

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SignInForm(),
      bottomSheet: Container(
        width: screenWidth,
        height: 60.0,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: kPrimaryColor,
          image: DecorationImage(
              image: AssetImage(Assets.halfEllipse),
              fit: BoxFit.cover,
              alignment: Alignment.topCenter),
        ),
        child: FlatButton(
          onPressed: () => _launchRegisterURL(),
          child: Text(
            translate("register"),
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );
  }

  _launchRegisterURL() async {
    if (await canLaunch(ServerSettings.getSignUpURL)) {
      await launch(ServerSettings.getSignUpURL, forceWebView: true);
    } else {
      throw 'Could not launch ' + ServerSettings.getSignUpURL;
    }
  }
}

class SignInForm extends StatefulWidget {
  @override
  SignInFormState createState() {
    return SignInFormState();
  }
}

class SignInFormState extends State<SignInForm> {
  final storage = FlutterSecureStorage();
  final emailController = TextEditingController(text: "a3234");
  final passwordController = TextEditingController(text: "Moonbreak");

  final _formKey = GlobalKey<FormState>();
  void _submit() {
    if (_formKey.currentState.validate()) {
      Provider.of<Auth>(context, listen: false).signin(
          data: {
            'username': emailController.text,
            'password': passwordController.text
          },
          success: () {
            Navigator.of(context).pop();
          },
          error: (String error) {
            Scaffold.of(context).showSnackBar(
              SnackBar(
                behavior: SnackBarBehavior.floating,
                margin: EdgeInsets.only(bottom: 60.0, left: 10.0, right: 10.0),
                content: Text(error),
              ),
            );
          });
    }
  }

  void initState() {
    super.initState();
    checkAutoLogin();
  }

  Future<void> checkAutoLogin() async {
    String autoLogin = await storage.read(key: Preferences.auth_login);
    String username = await storage.read(key: Preferences.username);
    String password = await storage.read(key: Preferences.password);
    if (autoLogin != null && username != null && password != null) {
      emailController.text = username;
      passwordController.text = password;
    }
  }

  void dispose() {
    emailController.dispose();
    passwordController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Container(
        color: kPrimaryColor,
        padding: EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                Assets.appLogo,
                width: 80.0,
                height: 80.0,
              ),
              SizedBox(height: Metrics.quadrupleBase),
              TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return translate("empty_fields");
                  }
                  return null;
                },
                controller: emailController,
                style: TextStyle(color: Colors.white, fontSize: 16),
                decoration: InputDecoration(
                  hintText: translate('username'),
                  hintStyle: TextStyle(color: kGreyColor),
                  labelText: translate('username'),
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  labelStyle: TextStyle(color: Colors.grey),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey, width: 1.0),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey, width: 1.0),
                  ),
                ),
              ),
              TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return translate("empty_fields");
                  }
                  return null;
                },
                obscureText: true,
                controller: passwordController,
                style: TextStyle(color: Colors.white, fontSize: 16),
                decoration: InputDecoration(
                  hintText: translate("password"),
                  hintStyle: TextStyle(color: kGreyColor),
                  labelText: translate("password"),
                  labelStyle: TextStyle(color: Colors.grey),
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey, width: 1.0),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey, width: 1.0),
                  ),
                ),
              ),
              Container(
                width: screenSize.width,
                margin: EdgeInsets.only(
                    top: Metrics.tripleBase, bottom: Metrics.singleBase),
                child: RaisedButton(
                    padding: EdgeInsets.symmetric(vertical: Metrics.doubleBase),
                    child: Text(
                      translate("login"),
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: _submit,
                    color: kDarkBlueColor),
              ),
              FlatButton(
                  onPressed: _launchForgetPasswordURL,
                  child: Text(
                    translate("forget_password"),
                    style: TextStyle(color: kWhite50Color),
                  ))
            ],
          ),
        ));
  }

  _launchForgetPasswordURL() async {
    if (await canLaunch(ServerSettings.getForgetPasswordURL)) {
      await launch(ServerSettings.getForgetPasswordURL, forceWebView: false);
    } else {
      throw 'Could not launch ${ServerSettings.getForgetPasswordURL}';
    }
  }
}
