import 'package:dccast/constants/styles.dart';
import 'package:dccast/screens/video_detail.dart';
import 'package:dccast/utils/media.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

import '../../components/notificationitem.dart';
import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../model/notification/notice.dart';
import '../../model/user.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';

class NotificationVodScreen extends StatefulWidget {
  static const String id = 'notification_vod_screen';
  @override
  _NotificationVodScreenState createState() => _NotificationVodScreenState();
}

class _NotificationVodScreenState extends State<NotificationVodScreen> {
  Notice notice;
  int lastPageId = 1;
  bool isLoading = false;
  User user;
  final storage = FlutterSecureStorage();
  @override
  Widget build(BuildContext context) {
    return _buildPaginatedListView();
  }

  Widget _buildPaginatedListView() {
    return ModalProgressHUD(
      progressIndicator: Lottie.asset(Assets.animLoad,
          width: DeviceUtils.getScaledSize(context, 0.3),
          height: DeviceUtils.getScaledSize(context, 0.3)),
      inAsyncCall: isLoading,
      child: Column(
        children: <Widget>[
          Expanded(
            child: NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (!isLoading &&
                    scrollInfo.metrics.pixels ==
                        scrollInfo.metrics.maxScrollExtent) {
                  this.getJSONData();
                  setState(() {
                    isLoading = true;
                  });
                }
              },
              child: notice?.next == null
                  ? Center(
                      child: Lottie.asset(Assets.animNoData,
                          alignment: Alignment.center,
                          width: DeviceUtils.getScaledSize(context, 0.5)),
                    )
                  : _buildListView(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListView() {
    return ListView.builder(
      padding: const EdgeInsets.all(0),
      itemCount: notice == null ? 0 : notice.results.length,
      itemBuilder: (context, index) {
        return _buildImageColumn(notice.results[index]);
      },
    );
  }

  Widget _buildImageColumn(Result item) => Container(
        decoration: BoxDecoration(color: Colors.white),
        margin: const EdgeInsets.only(bottom: 8.0),
        child: GestureDetector(
          onTap: () {
            setState(() {
              isLoading = true;
            });
            Provider.of<MediaUtils>(context, listen: false)
              .fetchMedia(mediaID: int.parse(item.contentId))
              .then((value) {
                setState(() {
                  isLoading = false;
                });
                if (value) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) =>
                      VideoDetailScreen(content: eContent.vod),
                  ));
                }
              });
          },
          child: Column(
          children: <Widget>[
            NotificationItem(
                title: item.title,
                subtitle: item.text,
                date:
                    DateFormat("yyyy/MM/dd HH:mm:ss").format(item.sendDatetime))
          ],
        )),
      );

  @override
  void initState() {
    super.initState();
    // call get json data function
    // selectedPage = Selec
    this.getJSONData();
  }

  Future<String> getJSONData() async {
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.notificationVodURL +
          userID +
          "&page=" +
          lastPageId.toString();

      final response = await DioClient(dio).get(url);
      print(response);
      var jsonResponse = Notice.fromJson(response);
      setState(() {
        Notice newItems = jsonResponse;
        if (notice == null) {
          notice = newItems;
        } else {
          notice.results.addAll(newItems.results);
        }
        lastPageId = lastPageId + 1;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }

  void dispose() {
    super.dispose();
  }
}
