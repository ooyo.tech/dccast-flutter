import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

import '../../components/notificationitem.dart';
import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/arguments/selectedtabarguments.dart';
import '../../model/notification/notice.dart';
import '../../model/user.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import 'notification_live.dart';
import 'notification_vod.dart';

class NotificationsScreen extends StatefulWidget {
  static const String id = 'notifications_screen';
  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen>
 with SingleTickerProviderStateMixin {
  Notice notice;
  int lastPageId = 1;
  bool isLoading = false;
  User user;
  final storage = FlutterSecureStorage();
  bool _newNotice = false;
  bool _newLive = false;
  bool _newVod = false;
  TabController _tabController;

  @override
  Widget build(BuildContext context) {
    final SelectedTabArguments args = ModalRoute.of(context).settings.arguments;

    return DefaultTabController(
      initialIndex: args.selectedTab ?? 0,
      length: 3,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(translate("notification_title")),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.of(context).pop(),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(50.0),
            child: Container(
              height: 50.0,
              color: Colors.white,
              child: TabBar(
                  controller: _tabController,
                  labelColor: kBlackColor,
                  unselectedLabelColor: kBlackColor,
                  labelStyle: TextStyle(fontWeight: FontWeight.normal),
                  indicatorColor: Colors.white,
                  indicator: UnderlineTabIndicator(
                    borderSide: BorderSide(width: 3.0, color: kPrimaryColor),
                  ),
                  tabs: [
                    Tab(
                      child: Stack(
                        clipBehavior: Clip.none,
                        children: <Widget>[
                          Text(translate("notification_notice")),
                          if (_newNotice)
                          Positioned(
                            right: -25,
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                horizontal: 4,
                                vertical: 1
                              ),
                              decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.circular(6),
                              ),
                              constraints: BoxConstraints(
                                minWidth: 12,
                                minHeight: 12,
                              ),
                              child: Text(
                                'new',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 8,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          )
                        ],
                      )
                    ),
                    Tab(
                      child: Stack(
                        clipBehavior: Clip.none,
                        children: <Widget>[
                          Text(translate("notification_live")),
                          if( _newLive)
                          Positioned(
                            right: -25,
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                horizontal: 4,
                                vertical: 1
                              ),
                              decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.circular(6),
                              ),
                              constraints: BoxConstraints(
                                minWidth: 12,
                                minHeight: 12,
                              ),
                              child: Text(
                                'new',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 8,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          )
                        ],
                      )
                    ),
                    Tab(
                      child: Stack(
                        clipBehavior: Clip.none,
                        children: <Widget>[
                          Text(translate("notification_vod")),
                          if (_newVod)
                          Positioned(
                            right: -25,
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                horizontal: 4,
                                vertical: 1
                              ),
                              decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.circular(6),
                              ),
                              constraints: BoxConstraints(
                                minWidth: 12,
                                minHeight: 12,
                              ),
                              child: Text(
                                'new',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 8,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          )
                        ],
                      )
                    ),
                  ]),
            ),
          ),
        ),
        body: TabBarView(
          controller: _tabController,
          children: [
            _buildPaginatedListView(),
            NotificationLiveScreen(),
            NotificationVodScreen()
          ],
        ),
      ),
    );
  }

  Widget _buildPaginatedListView() {
    return ModalProgressHUD(
      progressIndicator: Lottie.asset(Assets.animLoad,
          width: DeviceUtils.getScaledSize(context, 0.3),
          height: DeviceUtils.getScaledSize(context, 0.3)),
      inAsyncCall: isLoading,
      child: Column(
        children: <Widget>[
          Expanded(
            child: NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (!isLoading &&
                    scrollInfo.metrics.pixels ==
                        scrollInfo.metrics.maxScrollExtent) {
                  this.getJSONData();
                  setState(() {
                    isLoading = true;
                  });
                }
              },
              child: notice?.results == null
                  ? Center(
                      child: Lottie.asset(Assets.animNoData,
                          alignment: Alignment.center,
                          width: DeviceUtils.getScaledSize(context, 0.5)),
                    )
                  : _buildListView(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListView() {
    return ListView.builder(
      padding: const EdgeInsets.all(0),
      itemCount: notice == null ? 0 : notice.results.length,
      itemBuilder: (context, index) {
        return _buildImageColumn(notice.results[index]);
      },
    );
  }

  Widget _buildImageColumn(Result item) => Container(
        decoration: BoxDecoration(color: Colors.white),
        margin: const EdgeInsets.only(bottom: 8.0),
        child: Column(
          children: <Widget>[
            NotificationItem(
                title: item.title,
                subtitle: item.text,
                date:
                    DateFormat("yyyy/MM/dd HH:mm:ss").format(item.sendDatetime))
          ],
        ),
      );

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);

    _tabController.addListener(_detectTabChange);
    // call get json data function
    // selectedPage = Selec
    getHasNew();
    getJSONData();
  }

  Future<String> getJSONData() async {
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);

    try {
      var url = ServerSettings.notificationNoticeURL +
          userID +
          "&page=" +
          lastPageId.toString();

      final response = await DioClient(dio).get(url);
      var jsonResponse = Notice.fromJson(response);
      readNotice("notice");
      setState(() {
        var newItems = jsonResponse;
        if (notice == null) {
          notice = newItems;
        } else {
          notice.results.addAll(newItems.results);
        }
        lastPageId = lastPageId + 1;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  Future<void> getHasNew() async {
    final dio = Dio();
    try {
      var url = ServerSettings.notificationHasNew;

      final response = await DioClient(dio).get(url);
      setState(() {
        print(response);
        isLoading = false;
        _newNotice = response["notice"];
        _newLive = response["live"];
        _newVod = response["vod"];
      });
      return "sucessful";
    } on Exception {
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<void> readNotice(String type) async {
    final dio = Dio();
    try {
      var url = "${ServerSettings.notificationRead}$type";

      final response = await DioClient(dio).get(url);
      setState(() {
        if (response["results"]) {
          if (type == "notice") {
            _newNotice = false;
          } else if (type == "live") {
            _newLive = false;
          } else if (type == "vod") {
            _newVod = false;
          }
        }
      });
      return "sucessful";
    } on Exception {
      setState(() {
        isLoading = false;
      });
    }
  }

  void _detectTabChange() {
    switch (_tabController.index) {
      case 0:
        if (_newNotice) {
          readNotice("notice");
        }
        break;
      case 1:
        if (_newLive) {
          readNotice("live");
        }
        break;
      case 2:
        if (_newVod) {
          readNotice("vod");
        }
        break;
    }
  }
}
