import 'package:dccast/utils/push_notification_service.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../constants/styles.dart';
import 'home_vod.dart';
import 'live/home_live.dart';
import 'search_screen.dart';
import 'sidemenu.dart';

class HomeScreen extends StatelessWidget {
  static const String id = 'home_screen';
  //final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  
  @override
  Widget build(BuildContext context) {
    //final pushNotificationService = PushNotificationService();
    //pushNotificationService.initialise(context);
    return DefaultTabController(
      initialIndex: 1,
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: TabBar(
              unselectedLabelColor: Colors.white,
              indicatorPadding: EdgeInsets.symmetric(vertical: 5),
              indicator: BoxDecoration(
                  borderRadius: BorderRadius.circular(25),
                  color: kBlack50Color),
              tabs: [
                Tab(
                  child: Container(
                    padding: EdgeInsets.zero,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        border:
                            Border.all(color: Colors.transparent, width: 1)),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(translate('live')),
                    ),
                  ),
                ),
                Tab(
                  child: Container(
                    padding: EdgeInsets.zero,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        border:
                            Border.all(color: Colors.transparent, width: 1)),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(translate('vod')),
                    ),
                  ),
                ),
              ]),
          actions: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, SearchScreen.id);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12.0),
                child: Icon(
                  Icons.search,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
        body: TabBarView(
          children: [
            Container(child: HomeLiveScreen()),
            Container(child: HomeVodScreen()),
          ],
        ),
        drawer: Drawer(
          child: Container(child: SideMenu()),
        ),
      ),
    );
  }
}
