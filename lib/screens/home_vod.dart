import 'package:dccast/screens/live_test.dart';
import 'package:dccast/screens/video_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';

import '../components/categories_list.dart';
import '../components/home/home_grid.dart';
import '../components/home/home_list.dart';
import '../components/home/home_swiper.dart';
import '../constants/styles.dart';
import '../utils/media.dart';

class HomeVodScreen extends StatefulWidget {
  @override
  _HomeVodScreenState createState() => _HomeVodScreenState();
}

class _HomeVodScreenState extends State<HomeVodScreen> {
  bool isLoading = false;
  double aspectRatio = 16 / 9;

  @override
  Widget build(BuildContext context) {
    return Consumer<MediaUtils>(
      builder: (context, media, child) {
        return RefreshIndicator(
          onRefresh: _fetchData,
          child: ListView(
            physics: AlwaysScrollableScrollPhysics(),
            children: <Widget>[
              // FlatButton(
              //     onPressed: () {
              //       Navigator.pushNamed(context, VideoLiveTestScreen.id);
              //     },
              //     child: Text("Live test")),
              CategoryListComponent(content: eContent.vod),
              AspectRatio(
                  aspectRatio: 16 / 9,
                  child: HomeSwiperComponent(eContent.vod)),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 8.0, vertical: 12.0),
                      child:
                          Text(translate("top_vod"), style: kTextBoldStyle),
                    ),
                  ),
                  FlatButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => VideoListScreen(
                                  title: translate("top_vod"),
                                  contentType: eContent.vod,
                                  listType: eListType.popular),
                            ));
                      },
                      child:
                          Text(translate("more") + ">", style: kTextGreyStyle))
                ],
              ),
              HomeListComponent(eContent.vod, false),
              Row(
                children: [
                  Expanded(
                      child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 8.0, vertical: 6.0),
                    child: Text(translate("last_vod"), style: kTextBoldStyle),
                  )),
                  FlatButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => VideoListScreen(
                                  title: translate("last_vod"),
                                  contentType: eContent.vod,
                                  listType: eListType.recent),
                            ));
                      },
                      child:
                          Text(translate("more") + ">", style: kTextGreyStyle))
                ],
              ),
              HomeGridComponent(content: eContent.vod)
            ],
          ),
        );
      },
    );
  }

  Future<void> _fetchSwiper() async {
    Provider.of<MediaUtils>(context, listen: false).fetchHitVodMedia(
        success: () {
      setState(() {
        isLoading = false;
      });
    }, error: (String error) {
      Scaffold.of(context).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          margin: EdgeInsets.only(bottom: 60.0, left: 10.0, right: 10.0),
          content: Text(error),
        ),
      );
    });
  }

  Future<void> _fetchList() async {
    Provider.of<MediaUtils>(context, listen: false).fetchMedias(
        options: "category=VOD&limit=30&ordering=-views&page=1",
        success: () {
          setState(() {
            isLoading = false;
          });
        },
        error: (String error) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              margin: EdgeInsets.only(bottom: 60.0, left: 10.0, right: 10.0),
              content: Text(error),
            ),
          );
        });
  }

  Future<void> _fetchRecentList() async {
    Provider.of<MediaUtils>(context, listen: false).fetchRecentMedias(
        options:
            "category=VOD&limit=20&is_hit_active=0&is_popular=0&ordering=-created",
        // options: "category=LIVE&limit=10&is_popular=1&ordering=-views",
        type: eContent.vod,
        success: () {
          setState(() {
            isLoading = false;
          });
        },
        error: (String error) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              margin: EdgeInsets.only(bottom: 60.0, left: 10.0, right: 10.0),
              content: Text(error),
            ),
          );
        });
  }

  Future<String> _fetchData() async {
    await _fetchSwiper();
    await _fetchList();
    await _fetchRecentList();
    await Future.delayed(Duration(seconds: 3));
    return "success";
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    _fetchSwiper();
    _fetchList();
    _fetchRecentList();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }
}
