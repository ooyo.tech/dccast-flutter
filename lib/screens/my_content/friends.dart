import 'package:flutter/material.dart';

import '../castlist/friends.dart';

class FriendsScreen extends StatelessWidget {
  static const String id = 'friends_screen';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: CastListFriends(),
    );
  }
}
