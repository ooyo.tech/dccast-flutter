import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';

import '../../components/profile/profile_recent.dart';
import '../../constants/styles.dart';
import '../../model/user.dart';
import '../../model/user_media.dart';
import '../../utils/auth.dart';

class RecentScreen extends StatefulWidget {
  static const String id = 'recent_screen';
  final Profile userProfile;

  // ignore: public_member_api_docs
  const RecentScreen({Key key, this.userProfile}) : super(key: key);
  @override
  _RecentScreenState createState() => _RecentScreenState();
}

class _RecentScreenState extends State<RecentScreen> {

  final searchController = TextEditingController();
  bool isLoading = false;
  UserMedia userMedias;
  int lastPageId = 1;
  String searchText;
  final storage = FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    var user = widget.userProfile != null
        ? widget.userProfile
        : Provider.of<Auth>(context, listen: false).userProfile;
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(translate("recent_livevod")),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(
                text: translate("live"),
              ),
              Tab(
                text: translate("vod"),
              ),
            ]
          )
        ),
        body: TabBarView(
          children: [
            ProfileRecent(searchText: searchText,
              userProfile: user, category: "LIVE"),
            ProfileRecent(searchText: searchText,
              userProfile: user, category: "VOD"),
          ],
        )
      )
    );
  }

  @override
  void dispose() {
    searchController?.dispose();
    super.dispose();
  }

}
