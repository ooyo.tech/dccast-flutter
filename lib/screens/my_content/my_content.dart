import 'package:dccast/screens/my_content/subscriptions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../components/profile/profile_recent.dart';
import '../../constants/assets.dart';
import '../../constants/styles.dart';
import '../favorites.dart';
import '../profile.dart';
import 'recent.dart';

class MyContentScreen extends StatelessWidget {
  static const String id = 'mycontent_screen';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(translate("my_content")),
        ),
        body: Column(
          children: [
            ListTile(
              leading: Image.asset(
                Assets.tvIcon,
                color: kBlackColor,
              ),
              title: Text(translate("profile")),
              onTap: () {
                Navigator.pushNamed(context, ProfileScreen.id);
              },
            ),
            ListTile(
              leading: Image.asset(
                Assets.heartIcon,
                color: kBlackColor,
              ),
              title: Text(translate("favorite")),
              onTap: () {
                Navigator.pushNamed(context, FavoritesScreen.id);
              },
            ),
            ListTile(
              leading: Image.asset(
                Assets.profileIcon,
                color: kBlackColor,
              ),
              title: Text(translate("subscribe")),
              onTap: () {
                Navigator.pushNamed(context, SubscriptionScreen.id);
              },
            ),
            ListTile(
              leading: Image.asset(
                Assets.videoIcon,
                color: kBlackColor,
              ),
              title: Text(translate("my_live_vod")),
              onTap: () {
                Navigator.pushNamed(context, RecentScreen.id);
              },
            ),
            //Expanded(child: ProfileRecent()),
          ],
        ));
  }
}
