import 'package:dccast/constants/assets.dart';
import 'package:dccast/model/subscribe.dart';
import 'package:dccast/utils/device.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';

import '../../components/user_list_item.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/user.dart';
import '../../model/user_media.dart';
import '../../utils/dio_client.dart';
import '../profile.dart';


class SubscriptionScreen extends StatefulWidget {
  static const String id = 'subscription_screen';
  @override
  _SubscriptionScreenState createState() => _SubscriptionScreenState();
}

class _SubscriptionScreenState extends State<SubscriptionScreen> {
  ScrollController scrollController = new ScrollController();

  final searchController = TextEditingController();
  bool isLoading = false;
  Subscribe subscribes;
  int lastPageId = 1;
  int _totalPages = 2;
  String searchText = '';
  String ordering = '-created';
  final storage = FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(translate("subscribe")),
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  child: TextFormField(
                    controller: searchController,
                    style: kTextStyle,
                    onChanged: onSearchTextChanged,
                    textAlignVertical: TextAlignVertical.center,
                    decoration: InputDecoration(
                      hintText: translate('search'),
                      hintStyle: TextStyle(color: kBlack50Color),
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      prefixIcon: Padding(
                        padding: EdgeInsets.all(0.0),
                        child: Icon(
                          Icons.search,
                          color: kBlackColor,
                        ), // icon is 48px widget.
                      ),
                    ),
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    _settingModalBottomSheet(context);
                  },
                  child: Row(
                    children: [
                      Text(ordering == "-created"
                        ? translate("subscribe.sort.recent")
                        : ordering == "-on_air"
                        ? translate("subscribe.sort.onair")
                        : translate("subscribe.sort.name")
                        , style: kTextGreyStyle),
                      Icon(Icons.keyboard_arrow_down_sharp)
                    ],
                  ),
                )
              ],
            ),
            Expanded(
              child: RefreshIndicator (
                onRefresh: refresh,
                child: isLoading ? 
                  Center(
                    child: Lottie.asset(Assets.animLoad,
                      width: DeviceUtils.getScaledSize(context, 0.3),
                      height: DeviceUtils.getScaledSize(context, 0.3))
                  )
                  : (subscribes?.results == null
                    ? Center(
                        child: Lottie.asset(Assets.animNoData,
                          alignment: Alignment.center,
                          width: DeviceUtils.getScaledSize(context, 0.5)),
                      )
                    : _buildListView()
                  ),
              )
            )
          ],
        )
      ),
    );
  }

  Widget _buildListView() {
    return ListView.builder(
      controller: scrollController,
      padding: const EdgeInsets.all(0),
      itemCount: subscribes == null ? 0 : subscribes.results.length,
      itemBuilder: (context, index) {
        return Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: subscribes != null && subscribes.results.isNotEmpty
          ? _buildImageColumn(subscribes.results[index].toUser)
          : Center(
            child: Lottie.asset(Assets.animNoData,
                alignment: Alignment.center,
                width: DeviceUtils.getScaledSize(context, 0.5)),
          ),
          secondaryActions: <Widget>[
            IconSlideAction(
              color: Colors.red,
              icon: Icons.delete,
              onTap: () {
                _unsubscribe(subscribes.results[index], index);
              },
            ),
          ],
        );
      },
    );
  }

  Widget _buildImageColumn(Profile item) => Container(
        decoration: BoxDecoration(color: Colors.white),
        margin: const EdgeInsets.only(bottom: 8.0),
        child: Column(
          children: <Widget>[
            UserListItem(
              title: item.nickName,
              subtitle: item.stateMessage,
              profileImage: item.profileImage,
              trailing: item.onAir ? 
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 4,
                    vertical: 1
                  ),
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  constraints: BoxConstraints(
                    minWidth: 16,
                    minHeight: 12,
                  ),
                  child: Text("ON AIR",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                    ),
                    textAlign: TextAlign.center)
               ) : null,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        ProfileScreen(userProfile: item),
                  ),
                );
              },
            ),
          ],
        ),
      );

  @override
  void initState() {
    // call get json data function
    // selectedPage = Selec
    super.initState();
    _getJSONData();
    scrollController.addListener(() {
      if (!isLoading &&
          scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        lastPageId = lastPageId + 1;
        _getJSONData();
      }
    });
  }

  Future<String> refresh() {
    subscribes = null;
    lastPageId = 1;
    _totalPages = 2;
    return _getJSONData();
  }

  Future<String> _getJSONData() async {
    if (lastPageId > _totalPages) {
      return "success";
    }
    setState(() {
      isLoading = true;
    });
    final dio = Dio();
    var userID = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getDeleteSubURL +
          "?from_user=$userID&ordering=$ordering&limit=20&page=" +
          lastPageId.toString();
      var tmp = searchText;
      print(tmp);
      if (searchText != null) {
        url += "&search=$searchText";
      }

      final response = await DioClient(dio).get(url);
      var jsonResponse = Subscribe.fromJson(response);

      setState(() {
        if (tmp == searchText) {
          var newItems = jsonResponse;

          if (subscribes == null) {
            subscribes = newItems;
          } else {
            subscribes.results.addAll(newItems.results);
          }
        }
        isLoading = false;
      });
      return "sucessful";
    } on DioError catch (error) {
      print("error: ${error.response}");
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<String> _unsubscribe(Result sub, int index) async {
    setState(() {
      isLoading = true;
    });
    final dio = Dio();
    try {
      var url = "${ServerSettings.getCreateSubscribeMediaURL}${sub.id}/";
      print(url);

      final response = await DioClient(dio).delete(url);

      setState(() {
        subscribes.results.removeAt(index);
        subscribes.count--;
        isLoading = false;
      });
      return "sucessful";
    } on DioError catch (error) {
      print("error: ${error.response}");
      setState(() {
        isLoading = false;
      });
    }
  }


  Future<void> onSearchTextChanged(String text) async {
    searchText = text;
    refresh();
  }

  @override
  void dispose() {
    searchController?.dispose();
    scrollController.dispose();
    super.dispose();
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                  title: Text(translate("subscribe.sort.recent")),
                  onTap: () {
                    Navigator.of(context).pop();
                    setState(() {
                      ordering = "-created";
                      refresh();
                    });
                  }),
                ListTile(
                  title: Text(translate("subscribe.sort.onair")),
                  onTap: () {
                    Navigator.of(context).pop();
                    setState(() {
                      ordering = "on_air";
                      refresh();
                    });
                  }),
                ListTile(
                  title: Text(translate("subscribe.sort.name")),
                  onTap: () {
                    Navigator.of(context).pop();
                    setState(() {
                      ordering = "to_user__nick_name";
                      refresh();
                    });
                  }),
              ],
            ),
          );
        });
  }
}
