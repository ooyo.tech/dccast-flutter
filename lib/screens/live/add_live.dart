import 'dart:io';

import 'package:dio/dio.dart' as Dio;
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:provider/provider.dart';

import '../../components/live/add_live_list_tile.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/category.dart' as CategoryModel;
import '../../model/live_upload.dart';
import '../../model/vod/upload_thumb.dart';
import '../../utils/category.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../../utils/toast.dart';
import 'start_live.dart';

enum Answers { CAMERA, GALLERY }
enum AppState { free, picked, cropped }

class AddLiveScreen extends StatefulWidget {
  static const String id = 'add_live_screen';
  int groupId;

  AddLiveScreen({Key key, this.groupId}) : super(key: key);

  @override
  _AddLiveScreenState createState() => _AddLiveScreenState();
}

class _AddLiveScreenState extends State<AddLiveScreen> {
  int maxUserLock = 5000;
  final titleController = TextEditingController();
  final TextEditingController _pinPutController = TextEditingController();
  final FocusNode _pinPutFocusNode = FocusNode();
  final storage = FlutterSecureStorage();

  Dio.CancelToken token;
  double progressPercentage = 0.0;
  AppState state;
  File imageFile;

  bool isLoading = false;

  bool _pincode = false;
  bool _chat_restrict = false;
  bool _age_restrict = false;
  bool flashLight = false;
  LiveUpload _liveUpload;
  final _formKey = GlobalKey<FormState>();
  VodThumbUpload thumbUploaded;

  String setPinCode;
  String selectedDistribute;
  List<String> distributes = [
    translate("live.upload.distribute-normal"),
    translate("live.upload.distribute.castline"),
    translate('live.upload.distribute.subscriber'),
  ];

  String selectedUploadType;
  List<String> uploadTypes = [
    translate("live.upload.source.camera"),
    translate("live.upload.source.vod"),
    translate('live.upload.source.album'),
  ];

  String selectedOrientation;
  List<String> orientations = [
    translate("live.upload.orientation.portrait"),
    translate("live.upload.orientation.landscape"),
  ];

  int setLockUser;

  String selectedQuality;
  List<String> videoQualities = [
    translate("live.upload.quality.high"),
    translate("live.upload.quality.medium"),
    translate('live.upload.quality.low'),
  ];

  CategoryModel.Result selectedCategory;
  CategoryModel.Category _category;

  int countSub, countFollow1, countFollow2, countFollow3;

  @override
  void initState() {
    super.initState();
    if (widget.groupId != null) {
      selectedDistribute = "그룹 Live";
    } else {
      selectedDistribute = distributes[0];
    }
    selectedUploadType = uploadTypes[0];
    selectedOrientation = orientations[0];
    setLockUser = 100;
    selectedQuality = videoQualities[0];
    state = AppState.free;
    getFollowerStat();
  }

  @override
  Widget build(BuildContext context) {
    _category = Provider.of<CategoryUtils>(context, listen: false).categories;

    return Scaffold(
      appBar: AppBar(
        title: Text(translate("castlist.group.start-live")),
      ),
      body: ModalProgressHUD(
          opacity: 0.6,
          progressIndicator: Container(
              width: DeviceUtils.getScaledWidth(context, 0.8),
              height: DeviceUtils.getScaledHeight(context, 0.5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: CircularPercentIndicator(
                radius: 100.0,
                lineWidth: 5.0,
                percent: progressPercentage,
                center: new Text(
                    (progressPercentage * 100.0).toStringAsFixed(0) + " %"),
                progressColor: kPrimaryColor,
                backgroundColor: Colors.white,
              )),
          inAsyncCall: isLoading,
          child: ListView(
            children: [
              AddLiveListTile(
                leading: Icon(FontAwesomeIcons.alignRight, size: 20.0),
                title: Text(translate("live.upload.title"), style: kTextStyle),
                trailing: Form(
                  key: _formKey,
                  child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: TextFormField(
                      controller: titleController,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "제목을 입력해 주시기 바랍니다.";
                        }
                        return null;
                      },
                      maxLength: 40,
                      textAlign: TextAlign.right,
                      style: kTextStyle,
                      decoration:
                        kEmptyBorderInput(translate("live.upload.title"), true)),
                  )
                ),
              ),
              ExpandablePanel(
                  headerAlignment: ExpandablePanelHeaderAlignment.center,
                  header: GestureDetector(
                    onTap: _setDistribute,
                    child: Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: 16.0, vertical: 12.0),
                      child: Row(
                        children: [
                          Icon(FontAwesomeIcons.shareAlt, size: 20.0),
                          SizedBox(width: 14.0),
                          Expanded(
                            child: Text(translate("live.upload.distribute"),
                                style: kTextStyle),
                          ),
                          Text(
                            selectedDistribute,
                            style: kTextGreyStyle,
                            textAlign: TextAlign.right,
                          )
                        ],
                      ),
                    ),
                  ),
                  tapHeaderToExpand: false,
                  hasIcon: (widget.groupId == null) ? true : false,
                  expanded: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: (selectedDistribute == translate("live.upload.distribute-normal"))
                      ? SizedBox()
                      : Row(
                      children: [
                        Text(translate("live.upload.distribute")),
                        Expanded(
                          child: SizedBox(),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (selectedDistribute == translate("live.upload.distribute.subscriber"))
                            Text("${translate("live.upload.subscribers-count")} $countSub"),
                            if (selectedDistribute == translate("live.upload.distribute.castline"))
                            Text("${translate("live.upload.castline.first")} $countFollow1"),
                            if (selectedDistribute == translate("live.upload.distribute.castline"))
                            Text("${translate("live.upload.castline.second")} $countFollow2"),
                            if (selectedDistribute == translate("live.upload.distribute.castline"))
                            Text("${translate("live.upload.castline.third")} $countFollow3"),
                          ],
                        )
                      ],
                    )
                  )),
              Divider(),
              AddLiveListTile(
                leading: Icon(FontAwesomeIcons.wifi, size: 20.0),
                title: Text(translate("live.upload.type"), style: kTextStyle),
                trailing: Text(selectedUploadType, style: kTextGreyStyle),
                onPressed: _setUploadType,
              ),
              Container(
                color: kGreyColor,
                padding: EdgeInsets.all(16.0),
                child: Text(
                  translate("setting_saved"),
                  style: kTextStyle,
                ),
              ),
              AddLiveListTile(
                leading: Icon(FontAwesomeIcons.mobileAlt, size: 20.0),
                title: Text(translate("live.upload.orientation"),
                    style: kTextStyle),
                trailing: Text(selectedOrientation, style: kTextGreyStyle),
                onPressed: _setOrientation,
              ),
              AddLiveListTile(
                leading: Icon(FontAwesomeIcons.thLarge, size: 20.0),
                title:
                    Text(translate("live.upload.category"), style: kTextStyle),
                trailing: Text(
                    selectedCategory?.name == null
                        ? _category.results[0].name
                        : selectedCategory.name,
                    style: kTextGreyStyle),
                onPressed: _setCategory,
              ),
              AddLiveListTile(
                leading: Icon(FontAwesomeIcons.userAlt, size: 20.0),
                title:
                    Text(translate("live.upload.setlock"), style: kTextStyle),
                trailing: Text(setLockUser.toString(), style: kTextGreyStyle),
                onPressed: _setUserLock,
              ),
              /*AddLiveListTile(
                leading: Icon(FontAwesomeIcons.solidImage, size: 20.0),
                title:
                    Text(translate("live.upload.quality"), style: kTextStyle),
                trailing: Text(selectedQuality, style: kTextGreyStyle),
                onPressed: _setUploadQuality,
              ),*/
              SwitchListTile(
                title: Text(translate("live.upload.age-restrict"),
                    style: kTextStyle),
                value: _age_restrict,
                onChanged: (bool value) {
                  setState(() {
                    _age_restrict = value;
                  });
                },
                secondary: const Icon(
                  FontAwesomeIcons.exclamationCircle,
                  color: kBlackColor,
                ),
              ),
              SwitchListTile(
                title:
                    Text(translate("live.upload.chat-lock"), style: kTextStyle),
                value: _chat_restrict,
                onChanged: (bool value) {
                  setState(() {
                    _chat_restrict = value;
                  });
                },
                secondary: const Icon(
                  FontAwesomeIcons.solidComment,
                  color: kBlackColor,
                ),
              ),
              SwitchListTile(
                title:
                    Text(translate("live.upload.live-lock"), style: kTextStyle),
                value: _pincode,
                onChanged: (bool value) {
                  pin1 = 'test';
                  _setPinCode(value);
                },
                secondary: const Icon(
                  Icons.lock_rounded,
                  color: kBlackColor,
                ),
              ),
              Container(
                margin: EdgeInsets.all(12.0),
                child: RaisedButton(
                  padding: EdgeInsets.all(16.0),
                  onPressed: _askUser,
                  color: kPrimaryColor,
                  child: Text(
                    translate("live.upload.start"),
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ],
          )),
    );
  }

  void _setDistribute() {
    if (widget.groupId != null) {
      return;
    }
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                  trailing: Icon(
                    Icons.close_rounded,
                    color: kBlackColor,
                  ),
                  onTap: () => {Navigator.of(context).pop()},
                ),
                Divider(
                  color: kBlack50Color,
                  height: 1.0,
                ),
                ...List.generate(distributes.length, (index) {
                  return ListTile(
                      title: Text(distributes[index], style: kTextStyle),
                      trailing: Icon(
                        Icons.check,
                        color: selectedDistribute == distributes[index]
                            ? kPrimaryColor
                            : kGreyColor,
                      ),
                      onTap: () => {
                            setState(() {
                              selectedDistribute = distributes[index];
                            }),
                            Navigator.of(context).pop()
                          });
                }),
              ],
            ),
          );
        });
  }

  void _setUploadType() {
    showModalBottomSheet(
        context: context,
        isDismissible: false,
        builder: (BuildContext bc) {
          return Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                  trailing: Icon(
                    Icons.close_rounded,
                    color: kBlackColor,
                  ),
                  onTap: () => {Navigator.of(context).pop()},
                ),
                Divider(
                  color: kBlack50Color,
                  height: 1.0,
                ),
                ...List.generate(uploadTypes.length, (index) {
                  return ListTile(
                      title: Text(uploadTypes[index], style: kTextStyle),
                      trailing: Icon(
                        Icons.check,
                        color: selectedUploadType == uploadTypes[index]
                            ? kPrimaryColor
                            : kGreyColor,
                      ),
                      onTap: () => {
                            setState(() {
                              selectedUploadType = uploadTypes[index];
                            }),
                            Navigator.of(context).pop()
                          });
                }),
              ],
            ),
          );
        });
  }

  void _setOrientation() {
    showModalBottomSheet(
        context: context,
        isDismissible: false,
        builder: (BuildContext bc) {
          return Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                  trailing: Icon(
                    Icons.close_rounded,
                    color: kBlackColor,
                  ),
                  onTap: () => {Navigator.of(context).pop()},
                ),
                Divider(
                  color: kBlack50Color,
                  height: 1.0,
                ),
                ...List.generate(orientations.length, (index) {
                  return ListTile(
                      title: Text(orientations[index], style: kTextStyle),
                      trailing: Icon(
                        Icons.check,
                        color: selectedOrientation == orientations[index]
                            ? kPrimaryColor
                            : kGreyColor,
                      ),
                      onTap: () => {
                            setState(() {
                              selectedOrientation = orientations[index];
                            }),
                            Navigator.of(context).pop()
                          });
                }),
              ],
            ),
          );
        });
  }

  void _setCategory() {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        isDismissible: false,
        builder: (BuildContext bc) {
          return Container(
            padding: EdgeInsets.only(top: 30.0),
            child: ListView(
              children: <Widget>[
                ListTile(
                  trailing: Icon(
                    Icons.close_rounded,
                    color: kBlackColor,
                  ),
                  onTap: () => {Navigator.of(context).pop()},
                ),
                Divider(
                  color: kBlack50Color,
                  height: 1.0,
                ),
                ..._category.results.map((i) {
                  return ListTile(
                      title: Text(i.name, style: kTextStyle),
                      trailing: Icon(
                        Icons.check,
                        color:
                            selectedCategory == i ? kPrimaryColor : kGreyColor,
                      ),
                      onTap: () {
                        setState(() {
                          selectedCategory = i;
                        });
                        Navigator.of(context).pop();
                      });
                }),
              ],
            ),
          );
        });
  }

  void _setUserLock() {
    final userLockNumbers = <Widget>[];
    for (var i = 100; i <= maxUserLock; i+=100) {
      userLockNumbers.add(
        ListTile(
            title: Text(i.toString(), style: kTextStyle),
            trailing: Icon(
              Icons.check,
              color: setLockUser == i ? kPrimaryColor : kGreyColor,
            ),
            onTap: () => {
                  setState(() {
                    setLockUser = i;
                  }),
                  Navigator.of(context).pop()
                }),
      );
    }

    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        isDismissible: false,
        builder: (BuildContext bc) {
          return Container(
            padding: EdgeInsets.only(top: 30.0),
            child: ListView(
              children: <Widget>[
                ListTile(
                  trailing: Icon(
                    Icons.close_rounded,
                    color: kBlackColor,
                  ),
                  onTap: () => {Navigator.of(context).pop()},
                ),
                Divider(
                  color: kBlack50Color,
                  height: 1.0,
                ),
                ...userLockNumbers
              ],
            ),
          );
        });
  }

  void _setUploadQuality() {
    showModalBottomSheet(
        context: context,
        isDismissible: false,
        builder: (BuildContext bc) {
          return Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                  trailing: Icon(
                    Icons.close_rounded,
                    color: kBlackColor,
                  ),
                  onTap: () => {Navigator.of(context).pop()},
                ),
                Divider(
                  color: kBlack50Color,
                  height: 1.0,
                ),
                ...List.generate(videoQualities.length, (index) {
                  return ListTile(
                      title: Text(videoQualities[index], style: kTextStyle),
                      trailing: Icon(
                        Icons.check,
                        color: selectedQuality == videoQualities[index]
                            ? kPrimaryColor
                            : kGreyColor,
                      ),
                      onTap: () => {
                            setState(() {
                              selectedQuality = videoQualities[index];
                            }),
                            Navigator.of(context).pop()
                          });
                }),
              ],
            ),
          );
        });
  }

  BoxDecoration get _pinPutDecoration {
    return BoxDecoration(
      border: Border.all(color: kBlackColor),
      borderRadius: BorderRadius.circular(10.0),
    );
  }

  String pin1;
  void _setPinCode(bool value) {
    setState(() {
      _pinPutController.text = '';
    });
    showModalBottomSheet(
        context: context,
        isScrollControlled: false,
        isDismissible: false,
        builder: (BuildContext bc) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Wrap(
              children: <Widget>[
                ListTile(
                  trailing: Icon(
                    Icons.close_rounded,
                    color: kBlackColor,
                  ),
                  onTap: () => {Navigator.of(context).pop()},
                ),
                Divider(
                  color: kBlack50Color,
                  height: 1.0,
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(3.0),
                    border: Border.all(color: Colors.white),
                  ),
                  padding: const EdgeInsets.all(20.0),
                  child: PinPut(
                    fieldsCount: 4,
                    preFilledWidget: Text("*"),
                    autofocus: true,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    withCursor: true,
                    textStyle:
                        const TextStyle(fontSize: 25.0, color: kPrimaryColor),
                    eachFieldWidth: 50.0,
                    eachFieldHeight: 50.0,
                    onSubmit: (String pin) {
                      if (_pincode) {
                        print("new pin");
                        if (setPinCode == pin) {
                          setState(() {
                            // Navigator.of(context).pop();
                            _pincode = false;
                            setPinCode = "";
                          });
                        } else {
                          setState(() {
                            _pinPutController.text = '';
                            _pinPutFocusNode.requestFocus();
                          });
                          showToast("잘못된 핀");
                          return;
                        }
                      } else {
                        print("new ping");
                        if (pin1 == 'test') {
                          pin1 = pin;
                          setState(() {
                            _pinPutController.text = '';
                          });
                          Navigator.of(context).pop();
                          _setPinCode(value);
                          // return;
                        } else {
                          if (pin1 == pin) {
                            _pincode = value;
                            setPinCode = pin;
                            Navigator.of(context).pop();
                            return;
                          } else {
                            Navigator.of(context).pop();
                            showToast("잘못된 핀");
                            return;
                          }
                        }
                      }
                      // setState(() {
                      // });
                    },
                    focusNode: _pinPutFocusNode,
                    controller: _pinPutController,
                    submittedFieldDecoration: _pinPutDecoration,
                    selectedFieldDecoration: _pinPutDecoration.copyWith(
                      color: Colors.white,
                      border: Border.all(
                        width: 2,
                        color: kPrimaryColor,
                      ),
                    ),
                    followingFieldDecoration: _pinPutDecoration,
                  ),
                ),
              ],
            ),
          );
        });
  }

  Future _askUser() async {
    if (_formKey.currentState.validate()) {
      await Future.delayed(Duration(milliseconds: 50));

      switch (await showDialog(
          context: context,
          builder: (context) => SimpleDialog(
                title: Text(translate('question.question')),
                elevation: 4,
                children: <Widget>[
                  SimpleDialogOption(
                    child: Text(translate('question.gallery')),
                    onPressed: () {
                      Navigator.pop(context, Answers.GALLERY);
                    },
                  ),
                  SimpleDialogOption(
                    child: Text(translate('question.camera')),
                    onPressed: () {
                      Navigator.pop(context, Answers.CAMERA);
                    },
                  ),
                ],
              ))) {
        case Answers.GALLERY:
          _pickImageFromGallery();
          break;
        case Answers.CAMERA:
          _pickImageFromCamera();
          break;
      }
    }
  }

  Future<Null> _pickImageFromGallery() async {
    PickedFile pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      setState(() {
        imageFile = File(pickedFile.path);
        _cropImage();
      });
    }
  }

  Future<Null> _pickImageFromCamera() async {
    PickedFile pickedFile = await ImagePicker().getImage(
      source: ImageSource.camera,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      setState(() {
        imageFile = File(pickedFile.path);
        _cropImage();
      });
    }
  }

  Future<Null> _cropImage() async {
    print("image:" + imageFile.path);
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: imageFile.path,
        aspectRatioPresets: Platform.isAndroid
            ? [
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio16x9
              ]
            : [
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio5x3,
                CropAspectRatioPreset.ratio5x4,
                CropAspectRatioPreset.ratio7x5,
                CropAspectRatioPreset.ratio16x9
              ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          title: 'Cropper',
        ));
    if (croppedFile != null) {
      imageFile = croppedFile;
      _submit();
    }
  }

  void _submit() {
    if (imageFile != null) {
      setState(() {
        isLoading = true;
      });
      postUploadMedia();
    }
  }

  String videoFileExt(String path) {
    return path.replaceAll(".jpg", ".mp4");
  }

  Future<String> postUploadMedia() async {
    String username = await storage.read(key: Preferences.username);
    String userID = await storage.read(key: Preferences.user_id);
    //if groupID is exist add in data
    token = Dio.CancelToken();
    double tsLong = DateTime.now().millisecondsSinceEpoch / 1000;
    String ts = tsLong.toStringAsFixed(0);

    final dio = Dio.Dio();
    try {
      Dio.FormData thumData = Dio.FormData.fromMap({
        "thumbnail": await Dio.MultipartFile.fromFile(imageFile.path),
      });
      _category = Provider.of<CategoryUtils>(context, listen: false).categories;
      print("data:" + imageFile.path + ServerSettings.uploadVODThumbURL);
      final thumbResponse = await DioClient(dio).post(
        ServerSettings.uploadVODThumbURL,
        data: thumData,
        cancelToken: token,
        onSendProgress: showPostProgress,
      );

      thumbUploaded = VodThumbUpload.fromJson(thumbResponse);
      print("thumb:" + thumbUploaded.path);

      if (thumbUploaded != null) {
        Map<String, dynamic> createLive = {
          "user": userID,
          "title": titleController.text,
          "live_deploy": selectedDistribute,
          "category": "LIVE",
          "media_category": selectedCategory?.name == null
              ? _category.results[0].id
              : selectedCategory.id,
          "live_member": setLockUser,
          "live_resolution": selectedQuality,
          "kinds": _age_restrict ? translate("vod.upload.share.under19") : null,
          "orientation": selectedOrientation ==
                  translate("live.upload.orientation.portrait")
              ? "portrait"
              : "landscape",
          "live_chat_disable": _chat_restrict ? "1" : "0",
          "live_password": _pincode ? setPinCode : null,
          "live_setpass": _pincode ? 1 : 0,
          "media_id": username + "_" + ts,
          "stream_name": username + "_" + ts,
          "live_type":
              selectedUploadType == translate("live.upload.source.camera")
                  ? 1
                  : selectedUploadType == translate("live.upload.source.vod")
                      ? 3
                      : 2,
          "media_thumbnail": thumbUploaded.path
              .replaceAll("public", ServerSettings.videoThumbUrlChangeURL),
          "media_type": selectedUploadType
        };
        if (widget.groupId != null) {
          createLive["group_id"] = widget.groupId;
        }

        print(createLive.toString());
        final uploadResp = await DioClient(dio)
            .post(ServerSettings.createVODMediaURL, data: createLive);
        print("RESPONSE LIVE START:" + uploadResp.toString());
        _liveUpload = LiveUpload.fromJson(uploadResp);
        setState(() {
          isLoading = false;
        });
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => StartLiveScreen(upload: _liveUpload),
          ),
        );
      } else {
        print("media create faild, something is null");
      }
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
    }
  }

  int showPostProgress(received, total) {
    if (total != -1) {
      setState(() {
        print((received / total * 100).toStringAsFixed(0) + "%");
        progressPercentage = received / total;
      });
    }
  }

  Future<void> getFollowerStat() async {
    final dio = Dio.Dio();
    String userID = await storage.read(key: Preferences.user_id);

    try {
      var response = await DioClient(dio)
        .get(ServerSettings.getFollowerStat + userID);
        print(response);

      setState(() {
        countSub = response["subscribers"];
        countFollow1 = response["followers"];
        countFollow2 = response["second_followers"];
        countFollow3 = response["third_followers"];
      });
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }
}
