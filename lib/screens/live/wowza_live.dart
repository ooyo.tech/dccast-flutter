import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_wowza/gocoder/wowza_gocoder.dart';

class WowzaLiveScreen extends StatefulWidget {
  @override
  _WowzaLiveScreenState createState() => _WowzaLiveScreenState();
}

class _WowzaLiveScreenState extends State<WowzaLiveScreen> {
  WOWZCameraController controller = WOWZCameraController();

  bool flashLight = false;
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      controller.setWOWZConfig(
          hostAddress: "121.125.77.40",
          portNumber: 1935,
          applicationName: "live",
          streamName: "my_stream_name",
          username: "dccast",
          password: "dccast",
          scaleMode: ScaleMode.FILL_VIEW);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Wowza live'),
        ),
        body: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              SizedBox(
                height: 720,
                width: 1280,
                child: WOWZCameraView(
                  androidLicenseKey: "GOSK-1C46-010C-FBCA-3D42-BADA",
                  iosLicenseKey: "GOSK-1147-010C-7AAA-0DC8-8AD4",
                  controller: controller,
                  statusCallback: (status) {
                    print(
                        "status: ${status.mState} | ${status.isStarting()} | ${status.isReady()}");
                  },
                  broadcastStatusCallback: (broadcastStatus) {
                    print(
                        "status: ${broadcastStatus.state.toString()} | ${broadcastStatus.message}");
                  },
                ),
              ),
              Wrap(
                children: <Widget>[
                  _action('start preview', () {
                    controller.startPreview();
                  }),
                  _action('resume preview', () {
                    controller.continuePreview();
                  }),
                  _action('pause preview', () {
                    controller.pausePreview();
                  }),
                  _action('switch camera', () {
                    controller.switchCamera();
                  }),
                  _action('flashlight', () {
                    controller.flashLight(!flashLight);
                    flashLight = !flashLight;
                  }),
                  _action('Start Live Stream', () {
                    controller.startBroadcast();
                  }),
                  _action('End Live Stream', () {
                    controller.endBroadcast();
                  }),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  _action(text, Function function) =>
      RaisedButton(child: Text(text), onPressed: function);
}
