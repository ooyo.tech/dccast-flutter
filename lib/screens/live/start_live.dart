import 'dart:async';
import 'dart:convert';

import 'package:dccast/utils/dio_client.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:flutter_wowza/gocoder/wowza_gocoder.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';
import 'package:timer_count_down/timer_count_down.dart';
import 'package:wakelock/wakelock.dart';

import '../../components/comment/comment_dccon.dart';
import '../../components/live/chat_list.dart';
import '../../constants/assets.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/category.dart' as CategoryModel;
import '../../model/comments.dart';
import '../../model/live_upload.dart';
import '../../utils/auth.dart';
import '../../utils/category.dart';
import '../../utils/device.dart';
import '../../utils/toast.dart';

class StartLiveScreen extends StatefulWidget {
  static const String id = 'start_live_screen';
  final LiveUpload upload;

  const StartLiveScreen({Key key, this.upload}) : super(key: key);

  @override
  _StartLiveScreenState createState() => _StartLiveScreenState();
}

class _StartLiveScreenState extends State<StartLiveScreen> {
  final textController = TextEditingController();
  WOWZCameraController controller = WOWZCameraController();
  final storage = FlutterSecureStorage();
  DateTime _lastButtonPress;
  String _liveDuration = "00:00:00";
  bool flashLight = false;
  bool muteStatus = false;
  bool countDown = false;
  Timer _ticker;
  CategoryModel.Category _category;
  int maxUserLock = 5000;
  ChatListComponent chatList;
  bool _showComment = false;
  Function setSettingsState = () {};

  FocusNode chatFocus = FocusNode();
  TextEditingController chatController = TextEditingController();
  final GlobalKey<ChatListComponentState> _chatKey
   = GlobalKey<ChatListComponentState>();
  final TextEditingController _pinPutController = TextEditingController();
  final FocusNode _pinPutFocusNode = FocusNode();
  List<String> distributes = [
    translate("live.upload.distribute-normal"),
    translate("live.upload.distribute.castline"),
    translate('live.upload.distribute.subscriber'),
  ];

  @override
  void initState() {
    if (widget.upload?.orientation == "landscape") {
      SystemChrome.setPreferredOrientations([
          DeviceOrientation.landscapeRight,
          DeviceOrientation.landscapeLeft,
      ]);
    }
    super.initState();
    initializeWowza();
  }

  @override
  Widget build(BuildContext context) {
    _category = Provider.of<CategoryUtils>(context, listen: false).categories;
    chatList = ChatListComponent(
      key: _chatKey,
      roomID: widget.upload.id,
      fullscreen: true,
      owner: true
    );
    return WillPopScope(
      onWillPop: () {
        return Future(() => showAlertDialog(context, () {
          Navigator.of(context).pop();
          return false;
        }, () {
          _endLive();
          Navigator.of(context).pop();
          return true;
        }));
      },
      child: Scaffold(
      backgroundColor: kBlackColor,
      body: ModalProgressHUD(
        inAsyncCall: countDown,
        progressIndicator: Center(
            child: Countdown(
          seconds: 3,
          build: (BuildContext context, double time) => Text(
              time.toInt().toString(),
              style: TextStyle(
                  fontSize: DeviceUtils.getScaledWidth(context, 0.5),
                  color: Colors.white)),
          onFinished: _startLive,
        )),
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              SizedBox(
                height: DeviceUtils.getScaledHeight(context, 1.0),
                width: DeviceUtils.getScaledWidth(context, 1.0),
                child: WOWZCameraView(
                  androidLicenseKey: "GOSK-1C46-010C-FBCA-3D42-BADA",
                  iosLicenseKey: "GOSK-1147-010C-7AAA-0DC8-8AD4",
                  controller: controller,
                  statusCallback: (status) {
                    print(
                        "statusCallback: ${status.mState} | ${status.isStarting()} | ${status.isReady()}");
                  },
                  broadcastStatusCallback: (broadcastStatus) {
                    print(
                        "broadcastStatusCallback: ${broadcastStatus.state.toString()} | ${broadcastStatus.message}");
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: Metrics.sextupleBase,
                    left: 16.0,
                    right: 16.0,
                    bottom: 16.0),
                child: Wrap(
                  children: <Widget>[
                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 8.0, vertical: 4.0),
                          decoration: BoxDecoration(
                              color: kRedColor,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(5.0),
                                  bottomLeft: Radius.circular(5.0))),
                          child: Text(
                            translate("live"),
                            style: kSideTitleStyle,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 8.0, vertical: 4.0),
                          decoration: BoxDecoration(
                              color: kBlack50Color,
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(5.0),
                                  bottomRight: Radius.circular(5.0))),
                          child: Text(
                            _liveDuration,
                            style: kSideTitleStyle,
                          ),
                        ),
                        SizedBox(width: 12.0),
                        Image.asset(
                          Assets.profileImage,
                          color: Colors.white,
                          width: 16.0,
                        ),
                        SizedBox(width: 4.0),
                        (_chatKey == null || _chatKey.currentState == null) ?
                        Text("0",
                          style: kSideTitleStyle) :
                        Text("${_chatKey?.currentState?.membersCount ?? 0}",
                          style: kSideTitleStyle),
                        SizedBox(width: 12.0),
                        Image.asset(
                          Assets.manduIcon,
                          width: 16.0,
                        ),
                        SizedBox(width: 4.0),
                        Text("0", style: kSideTitleStyle),
                        Expanded(
                          child: Container(),
                        ),
                        IconButton(
                          icon: Icon(Icons.settings),
                          color: Colors.white,
                          onPressed: () {
                            _settingModalBottomSheet(context);
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.close_rounded),
                          color: Colors.white,
                          onPressed: () {
                            showAlertDialog(context, () {
                                // Navigator.of(context).pop();
                                Navigator.of(context).pop();
                              }, _endLive);
                          },
                        )
                      ],
                    ),
                    Row(
                      children: [
                        Text(widget.upload?.title != null
                            ? widget.upload?.title
                            : "제목을 입력해 주시기 바랍니다.",
                            style: TextStyle(
                              color: Colors.white, 
                              shadows: [
                                Shadow( // bottomLeft
                                  offset: Offset(-1.5, -1.5),
                                  color: Colors.black
                                ),
                                Shadow( // bottomRight
                                  offset: Offset(1.5, -1.5),
                                  color: Colors.black
                                ),
                                Shadow( // topRight
                                  offset: Offset(1.5, 1.5),
                                  color: Colors.black
                                ),
                                Shadow( // topLeft
                                  offset: Offset(-1.5, 1.5),
                                  color: Colors.black
                                ),
                              ]
                            )
                            ),
                        IconButton(
                          icon: Icon(
                            Icons.edit_outlined,
                            color: Colors.white,
                            size: 16.0,
                          ),
                          onPressed: () => updateLiveTitle(context),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              Positioned(
                  bottom: 16.0,
                  left: 16.0,
                  right: 16.0,
                  top: 200,
                  // width: DeviceUtils.getScaledWidth(context, 0.9),
                  child: Column(
                    children: [
                      Expanded(child: chatList),
                      SizedBox(height: 20),
                      if (_showComment)
                      _liveChatComponent(context),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            width: 48,
                            child: IconButton(
                              padding: EdgeInsets.symmetric(
                                horizontal: muteStatus ? 0 : 19
                              ),
                              iconSize: 24,
                              icon: Icon(
                                  muteStatus
                                      ? FontAwesomeIcons.microphoneAltSlash
                                      : FontAwesomeIcons.microphoneAlt,
                                  color: Colors.white),
                              onPressed: () {
                                controller.setMuted(!muteStatus);
                                setState(() {
                                  muteStatus = !muteStatus;
                                });
                                showToast(muteStatus
                                    ? "마이크 중지"
                                    : "마이크 중지 해제");
                              },
                            )
                          ),
                          IconButton(
                            icon: Icon(Icons.flip_camera_ios_rounded,
                                color: Colors.white),
                            onPressed: () {
                              controller.switchCamera();
                            },
                          ),
                          IconButton(
                            icon: Icon(
                                flashLight
                                    ? Icons.flash_on_rounded
                                    : Icons.flash_off_rounded,
                                color: Colors.white),
                            onPressed: () {
                              controller.flashLight(!flashLight);
                              setState(() {
                                flashLight = !flashLight;
                              });
                              showToast(muteStatus
                                  ? "플래시 켜기"
                                  : "플래시 끄기");
                            },
                          ),
                          IconButton(
                            icon: FaIcon(FontAwesomeIcons.share,
                                color: Colors.white),
                            onPressed: () {
                              Share.share(ServerSettings.getShareURL +
                                  widget.upload.id.toString());
                            },
                          ),
                          IconButton(
                            icon: Icon(Icons.comment,
                                color: Colors.white),
                            onPressed: () {
                              setState(() {
                                _showComment = !_showComment;
                              });
                            },
                          )
                        ],
                      ),
                      _liveDuration == "00:00:00"
                          ? Center(
                              child: FlatButton(
                                  padding: EdgeInsets.symmetric(vertical: 16.0),
                                  textColor: Colors.white,
                                  minWidth:
                                      DeviceUtils.getScaledWidth(context, 1.0),
                                  color: kPrimaryColor,
                                  child: Text(
                                      translate("castlist.group.start-live")),
                                  onPressed: _startCountDown),
                            )
                          : Container(),
                    ],
                  ))
            ],
          ),
        ),
      ),
    ));
  }

  Future<void> initializeWowza() async {
    if (widget.upload != null) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        controller.setWOWZConfig(
            hostAddress: ServerSettings.serverIP,
            portNumber: 1935,
            applicationName: "live",
            streamName: widget.upload?.mediaId,
            username: "dccast",
            password: "dccast",
            scaleMode: ScaleMode.FILL_VIEW);
      });

      print("CONFIG: " + widget.upload?.mediaId);
    } else {
      // set up the AlertDialog
      AlertDialog alert = AlertDialog(
        title: Text(translate("live.upload.alert.leaving")),
        content: Text(translate("live.upload.leaving-message")),
        actions: [
          FlatButton(
            child: Text("Wowza error"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          )
        ],
      );

      // show the dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }
  }

  @override
  void dispose() {
    if (_ticker != null) {
      _ticker.cancel();
      _ticker = null;
    }
    _endLive();
    chatController.dispose();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }

  void _startCountDown() {
    setState(() {
      countDown = true;
    });
  }

  void _startLive() {
    setState(() {
      countDown = false;
    });
    _lastButtonPress = DateTime.now();
    controller.startBroadcast();
    _ticker = Timer.periodic(Duration(seconds: 1), (_) => _updateTimer());
    Wakelock.enable();
    lastUpdateMedia(false, 0);
  }

  void _updateTimer() {
    final duration = DateTime.now().difference(_lastButtonPress);
    final newDuration = _formatDuration(duration);
    setState(() {
      _liveDuration = newDuration;
    });
    if (duration.inSeconds % 60 == 0) {
      lastUpdateMedia(false, duration.inSeconds);
    }
  }

  void lastUpdateMedia(is_stop, duration) {
    final dio = Dio();
    var user = Provider.of<Auth>(context, listen: false)
      .authenticatedUser;
    try {
      var data = {
        "kind": "lastupdate_media",
        "media_id": widget.upload.id,
        "is_stop": is_stop,
        "duration": duration,
        "views": _chatKey?.currentState?.membersCount ?? 0
      };
      print(data);

      DioClient(dio).post(ServerSettings.lastUpdateURL, data: data);
    } on Exception catch (error) {
    }
  }

  String _formatDuration(Duration duration) {
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
  }

  void _endLive() {
    Wakelock.disable();
    if (_ticker != null) {
      _ticker.cancel();
      _ticker = null;
    }
    lastUpdateMedia(true,
      DateTime.now().difference(_lastButtonPress).inSeconds);
    Future.microtask(() {
      controller.endBroadcast();
      Navigator.of(context).pop();
      Navigator.of(context).pop();
      toast(translate("live.end.success"));
    });
  }

  showAlertDialog(BuildContext context, Function cancel, Function pass) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(translate("alert_button_cancel")),
      onPressed: cancel,
    );
    Widget continueButton = FlatButton(
      child: Text(translate("alert_button_yes")),
      onPressed: pass,
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(translate("live.upload.alert.leaving")),
      content: Text(translate("live.upload.leaving-message")),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  updateLiveTitle(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
      return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Container(
          padding: EdgeInsets.all(Metrics.singleBase),
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(Metrics.halfBase),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 8.0, top: 8.0),
                child: Text(
                  "Live 방송 제목 변경",
                  style: kTextPrimaryStyle,
                ),
              ),
              SizedBox(height: 16.0),
              TextFormField(
                controller: textController,
                maxLength: 40,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.only(left: 8.0),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                  counterText: "",
                ),
              ),
              SizedBox(height: 16.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: FlatButton(
                      child: Text(translate("cancel"),
                          style: TextStyle(color: Colors.white)),
                      color: Colors.grey,
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  SizedBox(width: 8.0),
                  Expanded(
                    child: FlatButton(
                      color: kPrimaryColor,
                      child: Text(translate("confirm"),
                          style: TextStyle(color: Colors.white)),
                      onPressed: () async {
                        int statusCode = await _updateLive(context, "title", textController.text);
                        Navigator.of(context).pop();
                        if (statusCode == 200) {
                          setState(() {
                            widget.upload.title = textController.text;
                          });
                        }
                        textController.text = "";
                      },
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
    );
    });
  }

  Future<int> _updateLive(context, key, dynamic value) async {
    var authToken = Provider.of<Auth>(context, listen: false).token;

    try {
      var url = ServerSettings.createVODMediaURL + widget.upload.id.toString();
      var data = {
        "live_set_pass": widget.upload.liveSetpass,
        key: value
      };
      var _statusCode = 500;

      await http.put(Uri.parse("$url/"), body: jsonEncode(data), headers: {
          "Authorization" : "Token $authToken",
          "Content-Type": "application/json"
        }).then((result) {
        print(result.statusCode);
        print(result.body);
        _statusCode = result.statusCode;
        return result.statusCode;
      });
      return _statusCode;

    } on Exception {
      return 500;
    }
  }

  // OPTIONS

  bool _age_restrict = false;
  String old_kind = "";

  void _settingModalBottomSheet(context) async {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (BuildContext bc) {
        return StatefulBuilder(
        builder: (BuildContext context, StateSetter setModalState) {
          setSettingsState = setModalState;
          return Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                  leading: Icon(
                    FontAwesomeIcons.shareAlt,
                    color: kBlackColor),
                  title: Row(
                    children: [
                      Text(translate("live.upload.distribute"),
                        style: kTextStyle),
                      Spacer(),
                      Text(widget.upload.liveDeploy.toString(),
                        style: kTextGreyStyle),
                    ],
                  ),
                  onTap: () {
                    Navigator.of(context).pop();
                    _setDistribute();
                  }
                ),
                ListTile(
                  leading: Icon(
                    FontAwesomeIcons.thLarge,
                    color: kBlackColor),
                  title: Row(
                    children: [
                      Text(translate("live.upload.category"),
                        style: kTextStyle),
                      Spacer(),
                      Text(_category.results.firstWhere((element) =>
                        element.id == widget.upload.mediaCategory).name,
                        style: kTextGreyStyle),
                    ],
                  ),
                  onTap: () {
                    Navigator.of(context).pop();
                    _setCategory();
                  }
                ),
                ListTile(
                  leading: Icon(
                    Icons.supervised_user_circle,
                    color: kBlackColor),
                  title: Row(
                    children: [
                      Text(translate("live.upload.setlock"),
                        style: kTextStyle),
                      Spacer(),
                      Text(widget.upload.liveMember.toString(),
                        style: kTextGreyStyle),
                    ],
                  ),
                  onTap: () {
                    Navigator.of(context).pop();
                    _setUserLock();
                  }
                ),
                SwitchListTile(
                  title: Text(translate("live.upload.age-restrict"),
                      style: kTextStyle),
                  value: _age_restrict,
                  onChanged: (bool value) async {
                    if (value) {
                      _ask19(context);
                    } else {
                      /*var status = await _updateLive(context, "kinds",old_kind);
                      if (status == 200) {
                        setModalState(() {
                          _age_restrict = value;
                        });
                        toast(translate("alert.share.un-under19.result"));
                      }*/
                      _updateLive(context, "kinds",old_kind);
                      setModalState(() {
                        _age_restrict = value;
                      });
                      toast(translate("alert.share.un-under19.result"));
                    }
                  },
                  secondary: const Icon(
                    FontAwesomeIcons.exclamationCircle,
                    color: kBlackColor,
                  ),
                ),
                SwitchListTile(
                  title: Text(translate("live.upload.chat-lock"),
                      style: kTextStyle),
                  value: widget.upload.liveChatDisable,
                  onChanged: (bool value) async {
                    var status = await _updateLive(context,
                      "live_chat_disable", value);
                    if (status == 200) {
                      setModalState(() {
                        widget.upload.liveChatDisable = value;
                      });
                      _chatKey.currentState.toggleMute(value);
                      if (value) {
                        toast(translate("live.comment.chat_lock"));
                      } else {
                        toast(translate("live.comment.un_chat_lock"));
                      }
                    }
                  },
                  secondary: const Icon(
                    FontAwesomeIcons.solidComment,
                    color: kBlackColor,
                  ),
                ),
                SwitchListTile(
                  title: Text(translate("live.upload.live-lock"),
                      style: kTextStyle),
                  value: widget.upload.liveSetpass,
                  onChanged: (bool value) async {
                    _setPinCode(value);
                  },
                  secondary: const Icon(
                    Icons.lock_rounded,
                    color: kBlackColor,
                  ),
                ),
              ],
            ),
          );
        });
      });
  }

  void _setDistribute() {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: false,
      builder: (BuildContext bc) {
        return FractionallySizedBox(
          heightFactor: 0.5,
          child: ListView(
            children: <Widget>[
              ListTile(
                trailing: Icon(
                  Icons.close_rounded,
                  color: kBlackColor,
                ),
                onTap: () => {Navigator.of(context).pop()},
              ),
              Divider(
                color: kBlack50Color,
                height: 1.0,
              ),
              ...distributes.map((i) {
                return ListTile(
                  title: Text(i, style: kTextStyle),
                  trailing: Icon(
                    Icons.check,
                    color: widget.upload.liveDeploy == i
                      ? kPrimaryColor : kGreyColor,
                  ),
                  onTap: () async {
                    var status =
                      await _updateLive(context, "live_deploy", i);
                    if (status == 200) {
                      Navigator.of(context).pop();
                      setState(() {
                        widget.upload.liveDeploy = i;
                      });
                    }
                  }
                );
              }),
            ],
          ),
        );
      });
  }

  void _setCategory() {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: false,
      builder: (BuildContext bc) {
        return FractionallySizedBox(
          heightFactor: 0.5,
          child: ListView(
            children: <Widget>[
              ListTile(
                trailing: Icon(
                  Icons.close_rounded,
                  color: kBlackColor,
                ),
                onTap: () => {Navigator.of(context).pop()},
              ),
              Divider(
                color: kBlack50Color,
                height: 1.0,
              ),
              ..._category.results.map((i) {
                return ListTile(
                  title: Text(i.name, style: kTextStyle),
                  trailing: Icon(
                    Icons.check,
                    color: widget.upload.mediaCategory == i.id
                      ? kPrimaryColor : kGreyColor,
                  ),
                  onTap: () async {
                    var status =
                      await _updateLive(context, "media_category", i.id);
                    if (status == 200) {
                      Navigator.of(context).pop();
                      setState(() {
                        widget.upload.mediaCategory = i.id;
                      });
                    }
                  }
                );
              }),
            ],
          ),
        );
      });
  }

  void _setUserLock() {
    final userLockNumbers = <Widget>[];
    for (var i = 100; i <= maxUserLock; i+=100) {
      userLockNumbers.add(
        ListTile(
          title: Text(i.toString(), style: kTextStyle),
          trailing: Icon(
            Icons.check,
            color: widget.upload.liveMember == i.toString() ? kPrimaryColor : kGreyColor,
          ),
          onTap: () async {
            var status = await _updateLive(context, "live_member", i);
            if (status == 200) {
              Navigator.of(context).pop();
              setState(() {
                widget.upload.liveMember = "$i";
              });
            }
          }),
      );
    }

    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      builder: (BuildContext bc) {
        return FractionallySizedBox(
          heightFactor: 0.5,
          child: ListView(
            children: <Widget>[
              ListTile(
                trailing: Icon(
                  Icons.close_rounded,
                  color: kBlackColor,
                ),
                onTap: () => {Navigator.of(context).pop()},
              ),
              Divider(
                color: kBlack50Color,
                height: 1.0,
              ),
              ...userLockNumbers
            ],
          ),
        );
      });
  }

  Future<void> _ask19(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(translate("alert.message.under-19-title")),
          content: SingleChildScrollView(
            child: Text(translate("alert.message.under-19-description")),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(translate("group.delete.cancel")),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(translate("alert.button.ok")),
              onPressed: () async {
                old_kind = widget.upload.kinds;
                Navigator.of(context).pop();
                var status = await _updateLive(context, "kinds",
                  translate("vod.upload.share.under19"));
                if (status == 200) {
                  setSettingsState(() {
                    _age_restrict = true;
                  });
                  toast(translate("alert.share.under19.result"));
                }
              },
            ),
          ],
        );
      },
    );
  }

  BoxDecoration get _pinPutDecoration {
    return BoxDecoration(
      border: Border.all(color: kBlackColor),
      borderRadius: BorderRadius.circular(10.0),
    );
  }

  String pin1;
  void _setPinCode(bool value) {
    setState(() {
      _pinPutController.text = '';
    });
    showModalBottomSheet(
        context: context,
        isScrollControlled: false,
        isDismissible: false,
        builder: (BuildContext bc) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Wrap(
              children: <Widget>[
                ListTile(
                  trailing: Icon(
                    Icons.close_rounded,
                    color: kBlackColor,
                  ),
                  onTap: () => {Navigator.of(context).pop()},
                ),
                Divider(
                  color: kBlack50Color,
                  height: 1.0,
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(3.0),
                    border: Border.all(color: Colors.white),
                  ),
                  padding: const EdgeInsets.all(20.0),
                  child: PinPut(
                    fieldsCount: 4,
                    preFilledWidget: Text("*"),
                    autofocus: true,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    withCursor: true,
                    textStyle:
                        const TextStyle(fontSize: 25.0, color: kPrimaryColor),
                    eachFieldWidth: 50.0,
                    eachFieldHeight: 50.0,
                    onSubmit: (String pin) async {
                      if (!value) {
                        if (widget.upload.livePassword == pin) {
                          widget.upload.liveSetpass = value;
                          var status = await _updateLive(context,
                            "live_password", value);
                          if (status == 200) {
                            setState(() {
                              widget.upload.liveSetpass = false;
                              widget.upload.livePassword = "";
                            });
                          }
                        } else {
                          setState(() {
                            _pinPutController.text = '';
                            _pinPutFocusNode.requestFocus();
                          });
                          toast(translate("password_lock_wrong"));
                          return;
                        }
                      } else {
                        if (pin1 == '') {
                          pin1 = pin;
                          setState(() {
                            _pinPutController.text = '';
                            Navigator.of(context).pop();
                          });
                          _setPinCode(value);
                          // return;
                        } else {
                          if (pin1 == pin) {
                            widget.upload.liveSetpass = value;
                            var status = await _updateLive(context,
                              "live_password", value);
                            if (status == 200) {
                              setState(() {
                                widget.upload.liveChatDisable = value;
                                widget.upload.livePassword = "";
                              });
                            }
                            Navigator.of(context).pop();
                            return;
                          } else {
                            Navigator.of(context).pop();
                            toast("잘못된 핀");
                            return;
                          }
                        }
                      }
                      // setState(() {
                      // });
                    },
                    focusNode: _pinPutFocusNode,
                    controller: _pinPutController,
                    submittedFieldDecoration: _pinPutDecoration,
                    selectedFieldDecoration: _pinPutDecoration.copyWith(
                      color: Colors.white,
                      border: Border.all(
                        width: 2,
                        color: kPrimaryColor,
                      ),
                    ),
                    followingFieldDecoration: _pinPutDecoration,
                  ),
                ),
              ],
            ),
          );
        });
  }

  void toast(msg) {
    showToast(msg);
  }

  // LIVE chat

  Widget _liveChatComponent(BuildContext context) {
    return TextFormField(
        focusNode: chatFocus,
        style: TextStyle(color: kBlackColor, fontSize: 16),
        controller: chatController,
        onFieldSubmitted: (val) => 
          _sendChat(chatController.text),
        decoration: InputDecoration(
          filled: true,
          fillColor: kGreyColor,
          hintText: translate("live_comment_add"),
          contentPadding:
              const EdgeInsets.only(left: 14.0, bottom: 0.0, top: 0.0),
          enabledBorder: OutlineInputBorder(
            borderSide: const
              BorderSide(color: kGreyColor, width: 1.0),
            borderRadius: BorderRadius.circular(25.7)
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: const 
              BorderSide(color: kGreyColor, width: 1.0),
            borderRadius: BorderRadius.circular(25.7)
          ),
          suffixIcon: Container(
            width: 100,
            child: Row(
              children: [
                IconButton(
                  icon: Icon(FontAwesomeIcons.smile),
                  color: Colors.grey,
                  onPressed: () {
                    Future.delayed(Duration.zero, chatFocus.unfocus);
                    showEmojis();
                  }
                ),
                IconButton(
                  icon: Icon(FontAwesomeIcons.solidPaperPlane),
                  color: kPrimaryColor,
                  onPressed: () {
                    Future.delayed(Duration.zero, chatFocus.unfocus);
                    if (chatController.text != "")  {
                      _sendChat(chatController.text);
                    }
                  }
                ),
              ])
          )
        ),
      );
  }
  
  void showEmojis() {
    showModalBottomSheet(
      isScrollControlled: false,
      isDismissible: true,
      backgroundColor: Colors.white,
      context: context,
      builder: (BuildContext bc) {
        return CommentDcconScreen(sendEmoji: (context, Dccon dccon) {
        _sendChat("dcconUrl:${dccon.img}");
      });
    });
  }

  void _sendChat(String messageText) {
    /*if (widget.upload.liveChatDisable) {
      toast(translate("live.comment.chat_lock"));
    } else {*/
      _chatKey.currentState.sendChat(messageText);
      chatController.text = "";
    // }
    if (messageText.startsWith("dcconUrl")) {
      Navigator.of(context).pop();
    }
  }
}
