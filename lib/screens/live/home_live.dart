import 'package:dccast/components/home/home_swiper_live.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';

import '../../components/categories_list.dart';
import '../../components/home/home_grid.dart';
import '../../components/home/home_list.dart';
import '../../constants/styles.dart';
import '../../utils/media.dart';
import '../video_list.dart';

class HomeLiveScreen extends StatefulWidget {
  @override
  _HomeLiveScreenState createState() => _HomeLiveScreenState();
}

class _HomeLiveScreenState extends State<HomeLiveScreen> {
  bool isLoading = false;
  bool isEnabled = false;
  var videoIndexSelected = -1;

  @override
  Widget build(BuildContext context) {
    return Consumer<MediaUtils>(builder: (context, media, child) {
      return RefreshIndicator(
        onRefresh: _fetchData,
        child: ListView(
          children: <Widget>[
            CategoryListComponent(content: eContent.live),
            AspectRatio(
                aspectRatio: 16 / 9,
                child: HomeSwiperLiveComponent(eContent.live)),
            Row(
              children: [
                Expanded(
                  child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 8.0, vertical: 12.0),
                    child:
                        Text(translate("top_live"), style: kTextBoldStyle),
                  ),
                ),
                FlatButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => VideoListScreen(
                                title: translate("top_live"),
                                contentType: eContent.live,
                                listType: eListType.popular),
                          ));
                    },
                    child:
                        Text(translate("more") + " >", style: kTextGreyStyle))
              ],
            ),
            HomeListComponent(eContent.live, false),
            Row(
              children: [
                Expanded(
                    child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 6.0),
                  child: Text(translate("last_live"), style: kTextBoldStyle),
                )),
                FlatButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => VideoListScreen(
                                title: translate("last_live"),
                                contentType: eContent.live,
                                listType: eListType.recent),
                          ));
                    },
                    child:
                        Text(translate("more") + " >", style: kTextGreyStyle))
              ],
            ),
            HomeGridComponent(content: eContent.live)
          ],
        ),
      );
    });
  }

  Future<void> _fetchList() async {
    Provider.of<MediaUtils>(context, listen: false).fetchMedias(
        options: "category=LIVE&limit=30&is_popular=0&is_hit_active=0&ordering=-views",
        type: eContent.live,
        success: () {
          setState(() {
            isLoading = false;
          });
        },
        error: (String error) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              margin: EdgeInsets.only(bottom: 60.0, left: 10.0, right: 10.0),
              content: Text(error),
            ),
          );
        });
  }

  Future<void> _fetchRecentList() async {
    Provider.of<MediaUtils>(context, listen: false).fetchRecentMedias(
        options:
            "category=LIVE&limit=20&is_hit_active=0&is_popular=0&ordering=-alive",
        // options: "category=LIVE&limit=10&is_popular=1&ordering=-views",
        type: eContent.live,
        success: () {
          setState(() {
            isLoading = false;
          });
        },
        error: (String error) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              margin: EdgeInsets.only(bottom: 60.0, left: 10.0, right: 10.0),
              content: Text(error),
            ),
          );
        });
  }

  Future<void> _fetchSwiper() async {
    Provider.of<MediaUtils>(context, listen: false).fetchHitLiveMedia(
        success: () {
      setState(() {
        isLoading = false;
      });
    }, error: (String error) {
      Scaffold.of(context).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          margin: EdgeInsets.only(bottom: 60.0, left: 10.0, right: 10.0),
          content: Text(error),
        ),
      );
    });
  }

  Future<String> _fetchData() async {
    await _fetchSwiper();
    await _fetchList();
    await _fetchRecentList();
    await Future.delayed(Duration(seconds: 3));
    return "success";
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    _fetchList();
    _fetchRecentList();
    _fetchSwiper();
    super.initState();
  }

  void dispose() {
    super.dispose();
  }
}
