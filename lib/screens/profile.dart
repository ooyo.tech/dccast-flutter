import 'package:dccast/screens/profile_edit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';

import '../components/profile/profile_header.dart';
import '../components/profile/profile_media.dart';
import '../constants/styles.dart';
import '../model/user.dart';
import '../model/user_media.dart';
import '../utils/auth.dart';

class ProfileScreen extends StatefulWidget {
  static const String id = 'profile_screen';
  final Profile userProfile;

  const ProfileScreen({Key key, this.userProfile}) : super(key: key);
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final searchController = TextEditingController();
  bool isLoading = false;
  UserMedia userMedias;
  int lastPageId = 1;
  String searchText;
  final storage = FlutterSecureStorage();
  String selectedValue = '-created';
  final GlobalKey<ProfileMediaState> _mediaKey = GlobalKey<ProfileMediaState>();

  @override
  Widget build(BuildContext context) {
    Profile user = widget.userProfile != null
        ? widget.userProfile
        : Provider.of<Auth>(context, listen: false).userProfile;
    return Scaffold(
      appBar: AppBar(
        title: widget.userProfile == null
          ? Text(translate("my.content.my.channel.title"))
          : Text(widget.userProfile?.nickName),
        actions: [
          IconButton(
            icon: Icon(Icons.edit),
            onPressed: () async {
              final result = await 
              Navigator.pushNamed(context, ProfileEditScreen.id);
              if (result == 'refresh') {
                setState(() {
                  user = Provider.of<Auth>(context, listen: false).userProfile;
                  Navigator.of(context).pop();
                });
              }
            }
          )
        ]
      ),
      body: Container(
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ProfileHeader(userProfile: user),
              Row(
                children: [
                  Expanded(
                    child: TextFormField(
                      controller: searchController,
                      style: kTextStyle,
                      //onChanged: onSearchTextChanged,
                      textAlignVertical: TextAlignVertical.center,
                      onFieldSubmitted: onSearchTextChanged,
                      decoration: InputDecoration(
                        hintText: translate('search'),
                        hintStyle: TextStyle(color: kBlack50Color),
                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        prefixIcon: Padding(
                          padding: EdgeInsets.all(0.0),
                          child: Icon(
                            Icons.search,
                            color: kBlackColor,
                          ), // icon is 48px widget.
                        ),
                      ),
                    ),
                  ),
                  FlatButton(
                    onPressed: () {
                      _settingModalBottomSheet(context);
                    },
                    child: Row(
                      children: [
                        Text(selectedValue == "-created"
                          ? translate("my_content_recent")
                          : translate("my_content_popular")
                          , style: kTextGreyStyle),
                        Icon(Icons.keyboard_arrow_down_sharp)
                      ],
                    ),
                  )
                ],
              ),
              Expanded(
                child: ProfileMedia(key: _mediaKey,
                      searchText: searchText,
                      userProfile: user,
                      ordering: selectedValue),
              )
            ],
          )),
    );
  }

  onSearchTextChanged(String text) async {
    // _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _mediaKey.currentState.setSearchText(text);
    setState(() {
      searchText = text;
    });
  }

  @override
  void dispose() {
    searchController?.dispose();
    super.dispose();
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                    title: Text(translate("my_content_recent")),
                    onTap: () {
                      Navigator.of(context).pop();
                      setState(() {
                        selectedValue = "-created";
                        _mediaKey.currentState.setOrdering(selectedValue);
                      });
                    }),
                ListTile(
                  title: Text(translate("my_content_popular")),
                  onTap: () {
                    Navigator.of(context).pop();
                    setState(() {
                      selectedValue = "views";
                        _mediaKey.currentState.setOrdering(selectedValue);
                    });
                  },
                ),
              ],
            ),
          );
        });
  }
}
