import 'package:dccast/components/home/home_grid.dart';
import 'package:dccast/components/home/home_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../constants/styles.dart';

class VideoListScreen extends StatefulWidget {
  static const String id = 'video_list_screen';
  final String title;
  final eContent contentType;
  final eListType listType;

  const VideoListScreen({Key key, this.title, this.contentType, this.listType})
      : super(key: key);

  @override
  _VideoListScreenState createState() => _VideoListScreenState();
}

class _VideoListScreenState extends State<VideoListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: widget.title != null ?
            Text(widget.title) : Text(widget.contentType == eContent.vod
              ? translate("vod")
              : translate("live")),
        ),
        body: widget.listType == eListType.popular
            ? HomeListComponent(widget.contentType, true)
            : HomeGridComponent(content: widget.contentType,
              scrollable: true,));
  }
}
