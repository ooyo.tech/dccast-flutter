import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/point_transaction.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';

class PointTransactionScreen extends StatefulWidget {
  @override
  _PointTransactionScreenState createState() => _PointTransactionScreenState();
}

class _PointTransactionScreenState extends State<PointTransactionScreen> {
  PointTransaction _pointTransaction;

  int lastPageId = 1;
  bool isLoading = false;
  final storage = FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    return Container(child: _buildPaginatedListView());
  }

  Widget _buildPaginatedListView() {
    return ModalProgressHUD(
      progressIndicator: Lottie.asset(Assets.animLoad,
          width: DeviceUtils.getScaledSize(context, 0.3),
          height: DeviceUtils.getScaledSize(context, 0.3)),
      inAsyncCall: isLoading,
      child: NotificationListener<ScrollNotification>(
        onNotification: (ScrollNotification scrollInfo) {
          if (!isLoading &&
              scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
            _fetchHistory();
            setState(() {
              isLoading = true;
            });
          }
        },
        child: _pointTransaction?.results == null
            ? Center(
                child: Lottie.asset(Assets.animNoData,
                    alignment: Alignment.center,
                    width: DeviceUtils.getScaledSize(context, 0.5)),
              )
            : Column(
                children: [
                  ListTile(
                    selected: true,
                    selectedTileColor: Colors.grey[300],
                    contentPadding: EdgeInsets.zero,
                    title: Row(
                      children: [
                        Expanded(
                            child: Text(translate('date'),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: kBlack50Color))),
                        Expanded(
                            child: Text(translate('division'),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: kBlack50Color))),
                        Expanded(
                            child: Text(translate('point'),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: kBlack50Color)))
                      ],
                    ),
                  ),
                  _buildListView(),
                  SizedBox(height: 20.0)
                ],
              ),
      ),
    );
  }

  Widget _buildListView() {
    return Expanded(
      child: ListView.builder(
        padding: const EdgeInsets.all(0),
        itemCount:
            _pointTransaction == null ? 0 : _pointTransaction.results.length,
        itemBuilder: (context, index) {
          return _buildImageColumn(_pointTransaction.results[index]);
        },
      ),
    );
  }

  Widget _buildImageColumn(Transactions item) => Container(
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(bottom: BorderSide(color: kBlack50Color))),
        child: ListTile(
          contentPadding: EdgeInsets.zero,
          title: Row(
            children: [
              Expanded(
                  child: Text(
                DateFormat("yy.MM.dd").format(item.created),
                textAlign: TextAlign.center,
                style: kTextGreyStyle,
              )),
              Expanded(
                child: Text(item?.media?.category + " " + item?.media?.title,
                    textAlign: TextAlign.center, style: kTextGreyStyle),
              ),
              Expanded(
                child: Text(item.quantity.toString(),
                    textAlign: TextAlign.center, style: kTextGreyStyle),
              ),
            ],
          ),
        ),
      );

  @override
  void initState() {
    super.initState();
    // call get json data function
    // selectedPage = Selec
    _fetchHistory();
  }

  Future<String> _fetchHistory() async {
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getPointTransactionURL +
          userID +
          "&page=" +
          lastPageId.toString();

      final response = await DioClient(dio).get(url);
      var jsonResponse = PointTransaction.fromJson(response);
      setState(() {
        PointTransaction newItems = jsonResponse;
        if (_pointTransaction == null) {
          _pointTransaction = newItems;
        } else {
          _pointTransaction.results.addAll(newItems.results);
        }
        lastPageId = lastPageId + 1;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  void dispose() {
    super.dispose();
  }
}
