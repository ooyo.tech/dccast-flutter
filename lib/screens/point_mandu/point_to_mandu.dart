import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/user_mandu.dart';
import '../../model/user_point.dart';
import '../../utils/auth.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../../utils/toast.dart';

class PointToManduScreen extends StatefulWidget {
  @override
  _PointToManduScreenState createState() => _PointToManduScreenState();
}

class _PointToManduScreenState extends State<PointToManduScreen> {

  String defaultValue = "* 포인트를 선택해 주십시오.";
  UserPoint userPoint;
  UserMandu userMandu;
  String toChangePoint;
  List<String> options;
  int changeRate = 1;
  int lastPageId = 1;
  bool isLoading = false;
  final _formKey = GlobalKey<FormState>();
  final titleController = TextEditingController();
  final passwordController = TextEditingController();
  final storage = FlutterSecureStorage();

  void _submit() async {
    setState(() {
      isLoading = true;
    });
    var password = await storage.read(key: Preferences.password);
    if (password != passwordController.text) {
      setState(() {
        isLoading = false;
      });
      showToast("잘못된 비밀번호");
      return;
    }
    if (_formKey.currentState.validate()) {
      final dio = Dio();
      try {
        var encodedUserID = Provider.of<Auth>(context, listen: false)
          .authenticatedUser.dcinside.userId;
        var userID = await storage.read(key: Preferences.user_id);
        var appID = await storage.read(key: Preferences.appID);
        print(appID);
        print(password);
        print(encodedUserID);
        encodedUserID = "djJ1bTNnK2NTUTAzNnl6MGpXUmpKRHE4cU1xbnRQcTU1OUx0MU9NTmlyOD0=";

        var data = {
          "kind": "change_point",
          "encoded_user_id": encodedUserID,
          "user_id": userID,
          "app_id": appID,
          "point": num.parse(toChangePoint)
        };
        print(data);

        final response = await DioClient(dio).post(
          ServerSettings.getApiUpdateURL,
          data: data
        ).then((value) {
          print(value);
          setState(() {
            isLoading = false;
          });
          var msg = "success";
          if (value["results"] != true) {
            if (value["results"] == false) {
              msg = value["cause"];
            } else if (value["results"][0]["result"] == "success") {
              msg = "success";
            } else {
              msg = value["results"][0]["cause"];
            }
          }
          if (msg == "success") {
            msg = "만두 교환이 완료되었습니다.";
            setState(() {
              double current = double.parse(userPoint.point)
                - double.parse(toChangePoint);
              userPoint.point = current;
              Provider.of<Auth>(context, listen: false).userPoint.point
                = current;
              double currentMandu = double.parse(userMandu.cause.toString()) + 
                double.parse(toChangePoint) / changeRate;
              userMandu.cause = currentMandu.toString();
              Provider.of<Auth>(context, listen: false).userMandu.cause
               = currentMandu.toString();
            });
          }
          showToast(msg);
        });

        print(response.toString());
      } on DioError catch (e) {
        setState(() {
          isLoading = false;
        });
        showToast("응용 프로그램 오류");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: isLoading ? 
        Center(
          child: Lottie.asset(Assets.animLoad,
              alignment: Alignment.center, width: 40.0, height: 40.0),
        )
        : Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: Row(
              children: [
                Flexible(
                  child: Row(
                    children: [
                      SizedBox(
                        width: 20.0,
                      ),
                      Image.asset(
                        Assets.manduIcon,
                        width: 30.0,
                        height: 30.0,
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Text(
                        userMandu == null ? '0' : userMandu.cause.toString(),
                        style: TextStyle(
                            color: kPrimaryColor,
                            fontSize: 16.0,
                            fontWeight: FontWeight.normal),
                      )
                    ],
                  ),
                ),
                Flexible(
                  child: Row(
                    children: [
                      Image.asset(
                        Assets.pointIcon,
                        width: 30.0,
                        height: 30.0,
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      userPoint == null
                          ? Container(
                              width: 20.0,
                              height: 20.0,
                              child: CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(
                                        Colors.white),
                                strokeWidth: 2.0,
                              ))
                          : Text(
                              userPoint.point,
                              style: TextStyle(
                                  color: kPrimaryColor,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.normal),
                            )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(20.0),
            width: double.infinity,
            color: Colors.grey[300],
            child: Text(
              "포인트 대 만두는 $changeRate : 1 비율로 교환 가능합니다.\n" + 
              "($changeRate포인트 당 만두 1개)\n" + 
              "최소 $changeRate 포인트(만두 $changeRate개) 부터 교환 가능합니다.",
              style: TextStyle(color: kPrimaryColor,
              fontWeight: FontWeight.w500)
            )
          ),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 20.0),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("포인트"),
                      Container(
                        width: DeviceUtils.getScaledWidth(context, 0.5),
                        child: DropdownButtonFormField<String>(
                          validator: (value) {
                            if (value.isEmpty || value == defaultValue) {
                              return translate(
                                'insert.inquiry.warning-empty-data');
                            }
                            return null;
                          },
                          dropdownColor: kGreyColor,
                          isExpanded: true,
                          icon: Icon(
                            Icons.keyboard_arrow_down,
                            color: kPrimaryColor,
                          ),
                          value: toChangePoint,
                          iconSize: 20.0,
                          elevation: 8,
                          style: TextStyle(color: kBlackColor),
                          onChanged: (String newValue) {
                            setState(() {
                              toChangePoint = newValue;
                            });
                          },
                          items: options
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("교환 될  만두"),
                      Text((toChangePoint != defaultValue
                       && num.tryParse(toChangePoint.toString()) != null) ?
                        (double.parse(toChangePoint) / changeRate).toString()
                         : "0",
                        style: TextStyle(color: kPrimaryColor)
                      )
                    ],
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: DeviceUtils.getScaledWidth(context, 1.0),
                    child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return translate('insert.inquiry.warning-empty-data');
                        }
                        return null;
                      },
                      obscureText: true,
                      controller: passwordController,
                      style: TextStyle(color: kBlackColor, fontSize: 16),
                      decoration: InputDecoration(
                        labelText: '비밀번호',
                        enabledBorder: const UnderlineInputBorder(
                          borderSide:
                            BorderSide(color: Colors.grey, width: 1.0),
                        ),
                        focusedBorder: const UnderlineInputBorder(
                          borderSide:
                            BorderSide(color: Colors.grey, width: 1.0),
                        ),
                      ),
                    )
                  ),
                  SizedBox(height: 15),
                  FlatButton(
                      color: kPrimaryColor,
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          _submit();
                        }
                      },
                      child: Text(
                        "교환하기",
                        style: TextStyle(color: Colors.white),
                      ))
                ],
              ),
            )
          )
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    // call get json data function
    // selectedPage = Selec
    toChangePoint = defaultValue;
    options = <String>[];
    options.add(toChangePoint);
    _fetchConfig();
  }

  Future<String> _fetchConfig() async {
    userPoint = Provider.of<Auth>(context, listen: false).userPoint;
    userMandu = Provider.of<Auth>(context, listen: false).userMandu;
    final dio = Dio();
    options = <String>[];
    options.add(defaultValue);
    try {
      var url = ServerSettings.getConfigURL;

      final response = await DioClient(dio).get(url);
      setState(() {
        changeRate = response['results'][0]['point_change_rate'];
        var opt = double.parse(userPoint.point) / changeRate;
        for (var i=1; i<=opt; i++) {
          var rate = i * changeRate;
          options.add(rate.toString());
        }
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  void dispose() {
    super.dispose();
  }
}
