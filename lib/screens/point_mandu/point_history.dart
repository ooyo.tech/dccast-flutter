import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/point_history.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';

class PointHistoryScreen extends StatefulWidget {
  @override
  _PointHistoryScreenState createState() => _PointHistoryScreenState();
}

class _PointHistoryScreenState extends State<PointHistoryScreen> {
  PointHistory _pointHistory;

  int lastPageId = 1;
  bool isLoading = false;
  final storage = FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    return Container(child: _buildPaginatedListView());
  }

  Widget _buildPaginatedListView() {
    return ModalProgressHUD(
      progressIndicator: Lottie.asset(Assets.animLoad,
          width: DeviceUtils.getScaledSize(context, 0.3),
          height: DeviceUtils.getScaledSize(context, 0.3)),
      inAsyncCall: isLoading,
      child: NotificationListener<ScrollNotification>(
        onNotification: (ScrollNotification scrollInfo) {
          if (!isLoading &&
              scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
            _fetchHistory();
            setState(() {
              isLoading = true;
            });
          }
        },
        child: _pointHistory?.results == null
            ? Center(
                child: Lottie.asset(Assets.animNoData,
                    alignment: Alignment.center,
                    width: DeviceUtils.getScaledSize(context, 0.5)),
              )
            : Column(
                children: [
                  ListTile(
                    selected: true,
                    selectedTileColor: Colors.grey[300],
                    contentPadding: EdgeInsets.zero,
                    title: Row(
                      children: [
                        Expanded(
                            child: Text(translate('date'),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: kBlack50Color))),
                        Expanded(
                            child: Text(translate('division'),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: kBlack50Color))),
                        Expanded(
                            child: Text(translate('point'),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: kBlack50Color)))
                      ],
                    ),
                  ),
                  _buildListView(),
                ],
              ),
      ),
    );
  }

  Widget _buildListView() {
    return Expanded(
      child: ListView.builder(
        padding: const EdgeInsets.all(0),
        itemCount: _pointHistory == null ? 0 : _pointHistory.results.length,
        itemBuilder: (context, index) {
          return _buildImageColumn(_pointHistory.results[index]);
        },
      ),
    );
  }

  Widget _buildImageColumn(Histories item) => Container(
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(bottom: BorderSide(color: kBlack50Color))),
        child: ListTile(
          contentPadding: EdgeInsets.zero,
          title: Row(
            children: [
              Expanded(
                  child: Text(
                DateFormat("yy.MM.dd").format(item.created),
                textAlign: TextAlign.center,
                style: kTextGreyStyle,
              )),
              Expanded(
                child: Text(translate("mandu_exchange"),
                    textAlign: TextAlign.center, style: kTextGreyStyle),
              ),
              Expanded(
                child: Text(item.points.toString(),
                    textAlign: TextAlign.center, style: kTextGreyStyle),
              ),
            ],
          ),
        ),
      );

  @override
  void initState() {
    super.initState();
    // call get json data function
    // selectedPage = Selec
    _fetchHistory();
  }

  Future<String> _fetchHistory() async {
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getPointHistoryURL +
          userID +
          "&page=" +
          lastPageId.toString();

      final response = await DioClient(dio).get(url);
      var jsonResponse = PointHistory.fromJson(response);
      setState(() {
        PointHistory newItems = jsonResponse;
        if (_pointHistory == null) {
          _pointHistory = newItems;
        } else {
          _pointHistory.results.addAll(newItems.results);
        }
        lastPageId = lastPageId + 1;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  void dispose() {
    super.dispose();
  }
}
