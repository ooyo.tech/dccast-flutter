import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

import '../components/video/videolistitem.dart';
import '../constants/assets.dart';
import '../constants/preferences.dart';
import '../constants/server_settings.dart';
import '../constants/styles.dart';
import '../model/user.dart';
import '../model/user_favorites.dart';
import '../utils/device.dart';
import '../utils/dio_client.dart';

class FavoritesScreen extends StatefulWidget {
  static const String id = 'favorites_screen';
  @override
  _FavoritesScreenState createState() => _FavoritesScreenState();
}

class _FavoritesScreenState extends State<FavoritesScreen> {
  UserFavorites favorites;
  int lastPageId = 1;
  bool isLoading = false;
  User user;
  final storage = FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(translate("favorite")),
        ),
        body: Container(
          padding: EdgeInsets.all(8.0),
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(8.0),
                child: Text(
                    favorites?.results == null
                        ? translate("favorite") + ": (0)"
                        : translate("favorite") +
                            ": (" +
                            favorites.results.length.toString() +
                            ")",
                    style: kNotificationTitle),
              ),
              Expanded(child: _buildFavoritesListView())
            ],
          ),
        ));
  }

  Widget _buildFavoritesListView() {
    return ModalProgressHUD(
      progressIndicator: Lottie.asset(Assets.animLoad,
          width: DeviceUtils.getScaledSize(context, 0.3),
          height: DeviceUtils.getScaledSize(context, 0.3)),
      inAsyncCall: isLoading,
      child: Column(
        children: <Widget>[
          Expanded(
            child: NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (!isLoading &&
                    scrollInfo.metrics.pixels ==
                        scrollInfo.metrics.maxScrollExtent) {
                  this.getJSONData();
                  setState(() {
                    isLoading = true;
                  });
                }
              },
              child: favorites?.results == null
                  ? Center(
                      child: Lottie.asset(Assets.animNoData,
                          alignment: Alignment.center,
                          width: DeviceUtils.getScaledSize(context, 0.5)),
                    )
                  : _buildListView(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListView() {
    return ListView.builder(
      padding: const EdgeInsets.all(0),
      itemCount: favorites == null ? 0 : favorites.results.length,
      itemBuilder: (context, index) {
        return _buildImageColumn(favorites.results[index]);
      },
    );
  }

  Widget _buildImageColumn(Result item) => Container(
        decoration: BoxDecoration(color: Colors.white),
        margin: const EdgeInsets.only(bottom: 8.0),
        child: Column(
          children: <Widget>[
            item?.media != null
                ? VideoListItem(
                    id: item?.media?.id,
                    title: item?.media?.title,
                    subtitle: item?.media?.explanation,
                    kinds: item?.media?.kinds,
                    dates: item?.media?.created,
                    crown: item?.media?.isHit,
                    duration: secToMinConverter(item?.media?.duration ?? 0),
                    views: item?.media?.views,
                    thumnail: item?.media?.mediaThumbnail,
                    media: item?.media,
                    favorite: item?.id
                  )
                : SizedBox()
          ],
        ),
      );

  @override
  void initState() {
    super.initState();
    // call get json data function
    // selectedPage = Selec
    this.getJSONData();
  }

  Future<String> getJSONData() async {
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getUserFavoritesURL +
          userID +
          "&page=" +
          lastPageId.toString();

      final response = await DioClient(dio).get(url);
      var jsonResponse = UserFavorites.fromJson(response);
      print(json.encode(response.toString()));
      setState(() {
        UserFavorites newItems = jsonResponse;

        if (favorites == null) {
          favorites = newItems;
        } else {
          favorites.results.addAll(newItems.results);
        }
        lastPageId = lastPageId + 1;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }

  void dispose() {
    super.dispose();
  }
}
