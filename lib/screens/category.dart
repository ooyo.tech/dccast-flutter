import 'package:dccast/screens/video_detail.dart';
import 'package:dccast/utils/media.dart';
import 'package:dccast/utils/pagewise.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pagewise/flutter_pagewise.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

import '../components/video/videolistitem.dart';
import '../constants/assets.dart';
import '../constants/preferences.dart';
import '../constants/server_settings.dart';
import '../constants/styles.dart';
import '../model/category.dart';
import '../model/user_media.dart';
import '../utils/category.dart';
import '../utils/device.dart';
import '../utils/dio_client.dart';
import 'home.dart';

class CategoryScreen extends StatefulWidget {
  static const String id = 'category_screen';
  final eContent content;
  final int selectedTab;

  const CategoryScreen({Key key, this.content, this.selectedTab})
      : super(key: key);
  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen>
    with SingleTickerProviderStateMixin {
  UserMedia _categoryListData;
  EditablePagewiseLoadController _pageLoadController;
  bool isLoading = false;
  int lastPageId = 1;
  int prevCategory;
  final storage = FlutterSecureStorage();
  @override
  Widget build(BuildContext context) {
    return Consumer<CategoryUtils>(builder: (context, category, child) {
      return DefaultTabController(
        initialIndex: widget.selectedTab ?? 0,
        length: category.categoryResults.length,
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.of(context).pop(),
            ),
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(30.0),
              child: Container(
                height: 40.0,
                padding: EdgeInsets.symmetric(vertical: 5.0),
                color: Colors.white,
                child: TabBar(
                  onTap: (index) {
                    setState(() {
                      _categoryListData = null;
                      prevCategory = category.categoryResults[index].id;
                      lastPageId = 1;
                    });
                    _fetchCategoryList(
                        type: widget.content,
                        categoryID: category.categoryResults[index].id);
                  },
                  isScrollable: true,
                  labelColor: Colors.white,
                  unselectedLabelColor: kPrimaryColor,
                  indicator: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: kPrimaryColor),
                  tabs: List<Widget>.generate(category.categoryResults.length,
                      (index) {
                    return Tab(
                      child: Container(
                        height: 20.0,
                        padding: EdgeInsets.zero,
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(category.categoryResults[index].name,
                              style: kCategoryTitle),
                        ),
                      ),
                    );
                  }),
                ),
              ),
            ),
            title: Text(eContent.vod == widget.content
                ? translate("vod")
                : translate("live")),
          ),
          body: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            children:
                List<Widget>.generate(category.categoryResults.length, (index) {
              return Container(
                padding: EdgeInsets.all(8.0),
                child: PagewiseListView(
                    // padding: EdgeInsets.all(15.0),
                    itemBuilder: (context, entry, index) {
                      return _buildListItem(entry, index);
                    },
                    noItemsFoundBuilder: (context) {
                      return Center(
                        child: Text(eContent.vod == widget.content
                            ? translate("no.vod.data.category")
                            : translate("no.live.data.category")),
                      );
                    },
                    pageLoadController: _pageLoadController,
                    loadingBuilder: (context) => Center(
                          child: Lottie.asset(Assets.animLoad,
                              width: DeviceUtils.getScaledSize(context, 0.3),
                              height: DeviceUtils.getScaledSize(context, 0.3)),
                        ),
                )
                // _buildPaginatedListView()
              );
            }),
          ),
        ),
      );
    });
  }

  Widget _buildPaginatedListView() {
    return ModalProgressHUD(
      progressIndicator: Lottie.asset(Assets.animLoad,
          width: DeviceUtils.getScaledSize(context, 0.4),
          height: DeviceUtils.getScaledSize(context, 0.4)),
      inAsyncCall: isLoading,
      child: Column(
        children: <Widget>[
          Expanded(
            child: NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (!isLoading &&
                    scrollInfo.metrics.pixels ==
                        scrollInfo.metrics.maxScrollExtent) {
                  _fetchCategoryList(
                      type: widget.content, categoryID: prevCategory);
                  setState(() {
                    isLoading = true;
                  });
                }
              },
              child: isLoading? 
                  Center()
                  : _categoryListData == null &&
                      _categoryListData?.results == null
                  ? Center(
                      child: Column(children: [
                        Lottie.asset(Assets.animNoData,
                          alignment: Alignment.center,
                          width: DeviceUtils.getScaledSize(context, 0.5)),
                        Text(translate("category.no.data"))
                      ])
                    )
                  : _buildListView(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListView() {
    return (_categoryListData?.results?.length > 0)
        ? ListView.builder(
            padding: const EdgeInsets.all(0),
            itemCount: _categoryListData == null
                ? 0
                : _categoryListData?.results?.length,
            itemBuilder: (context, index) {
              return _categoryListData?.results == null
                  ? SizedBox()
                  : _buildListItem(_categoryListData?.results[index], index);
            },
          )
        : _categoryListData != null
            ? Center(
                child: Text("해당 카테고리에 현재 진행중인 Live가 없습니다"),
              )
            : Container();
  }

  Widget _buildListItem(Media item, int index) => Container(
        decoration: BoxDecoration(color: Colors.white),
        margin: const EdgeInsets.only(bottom: 8.0),
        child: item != null
            ? VideoListItem(
                id: item?.id,
                title: item?.title,
                subtitle: item?.explanation,
                kinds: item?.kinds,
                dates: item?.created,
                crown: item?.isHit,
                duration: secToMinConverter(item?.duration ?? 0),
                views: item?.views,
                thumnail: item?.mediaThumbnail,
                media: item,
                onPressed: () {
                  Provider.of<MediaUtils>(context, listen: false)
                      .mediaDetail(item, widget.content);
                  Future.delayed(Duration(milliseconds: 250), () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              VideoDetailScreen(content: widget.content),
                        ));
                  });
                },
                onDelete: () {
                  setState(() {
                    _pageLoadController.removeAt(index);
                   });
                },
              )
            : SizedBox(),
      );

  @override
  void initState() {
    Category categories =
        Provider.of<CategoryUtils>(context, listen: false).availableCategories;
    prevCategory = categories.results[widget.selectedTab].id;
    // _fetchCategoryList(
    //     type: widget.content,
    //     categoryID: categories.results[widget.selectedTab].id);
    _pageLoadController = EditablePagewiseLoadController(
      pageSize: 15,
      pageFuture: (pageIndex) => _fetchData(
        type: widget.content,
        categoryID: prevCategory,
        page: pageIndex + 1
      )
    );
    super.initState();
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  void dispose() {
    _pageLoadController?.dispose();
    super.dispose();
  }

  Future<dynamic> _fetchCategoryList({eContent type, int categoryID}) async {
    setState(() {
      isLoading = true;
    });
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      String optionUser = userID != null ? "&user_id=" + userID : "";
      String contentType = eContent.vod == type ? "vod" : "live";
      String url = ServerSettings.getMediaURL +
          "?limit=30&" +
          "category=" +
          contentType +
          "&media_category=" +
          categoryID.toString() +
          "&is_hit_active=0&is_popular=0" +
          optionUser +
          "&page=" +
          lastPageId.toString();

      final response = await DioClient(dio).get(url);
      var jsonResponse = UserMedia.fromJson(response);
      setState(() {
        UserMedia newItems = jsonResponse;
        if (_categoryListData == null) {
          _categoryListData = newItems;
        } else {
          _categoryListData.results.addAll(newItems.results);
        }
        lastPageId = lastPageId + 1;
        prevCategory = categoryID;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (e) {
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<List<Media>> _fetchData({eContent type, int categoryID, page}) async {
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      String optionUser = userID != null ? "&user_id=" + userID : "";
      String contentType = eContent.vod == type ? "vod" : "live";
      String url = ServerSettings.getMediaURL +
          "?category=$contentType" +
          "&media_category=$categoryID" +
          "&is_hit_active=0&is_popular=0" +
          optionUser +
          "&page=$page";

      print("hatnaa = $url");
      final response = await DioClient(dio).get(url);
      var jsonResponse = UserMedia.fromJson(response);
      UserMedia newItems = jsonResponse;
      return newItems.results;
    } on Exception catch (e) {}
  }
}
