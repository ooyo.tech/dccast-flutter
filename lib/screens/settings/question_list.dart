import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/questions.dart';
import '../../model/user.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import 'question_detail.dart';

class QuestionListScreen extends StatefulWidget {
  static const String id = 'question_list_screen';
  @override
  _QuestionListScreenState createState() => _QuestionListScreenState();
}

class _QuestionListScreenState extends State<QuestionListScreen> {
  ScrollController scrollController = ScrollController();
  Questions questions;
  int lastPageId = 1;
  bool isLoading = false;
  User user;
  final storage = FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    return _buildPaginatedListView();
  }

  Widget _buildPaginatedListView() {
    return ModalProgressHUD(
      progressIndicator: Lottie.asset(Assets.animLoad,
          width: DeviceUtils.getScaledSize(context, 0.3),
          height: DeviceUtils.getScaledSize(context, 0.3)),
      inAsyncCall: isLoading,
      child: Column(
        children: <Widget>[
          Expanded(
            child: RefreshIndicator (
              onRefresh: refresh,
              child: questions?.results == null
                  ? (isLoading ? Container() : Center(
                      child: Lottie.asset(Assets.animNoData,
                          alignment: Alignment.center,
                          width: DeviceUtils.getScaledSize(context, 0.5)),
                    ))
                  : _buildListView(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListView() {
    return ListView.builder(
      padding: const EdgeInsets.all(0),
      controller: scrollController,
      itemCount: questions == null ? 0 : questions.results.length,
      itemBuilder: (context, index) {
        return _buildImageColumn(questions.results[index]);
      },
    );
  }

  Widget _buildImageColumn(Question item) => Container(
        decoration: BoxDecoration(color: Colors.white),
        margin: const EdgeInsets.only(bottom: 8.0),
        child: Column(
          children: <Widget>[
            ListTile(
              title: Text(item.title != null ? item.title : ""),
              subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    item.created != null ? 
                  DateFormat("yyyy-MM-dd HH:mm").format(item.created.toLocal())
                        : "",
                    style: TextStyle(color: kBlackColor),
                  ),
                  Text(item.kinds != null ? item.kinds : "",
                      style: TextStyle(color: kBlackColor))
                ],
              ),
              onTap: () {
                // Navigator.pushNamed(context, QuestionDetailScreen.id);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            QuestionDetailScreen(question: item)));
              },
            ),
            Divider(
              color: kBlackColor,
              height: 1.0,
            )
          ],
        ),
      );

  @override
  void initState() {
    super.initState();
    // call get json data function
    // selectedPage = Selec
    this.getJSONData();
    scrollController.addListener(() {
      if (!isLoading &&
          scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        this.getJSONData();
        setState(() {
          isLoading = true;
        });
      }
    });
  }

  Future<String> refresh() {
    lastPageId = 1;
    return getJSONData();
  }

  Future<String> getJSONData() async {
    setState(() {
      isLoading = true;
    });
    final dio = Dio();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      var url = ServerSettings.getMyQandAURL +
          userID +
          "&page=" +
          lastPageId.toString();

      final response = await DioClient(dio).get(url);
      var jsonResponse = Questions.fromJson(response);
      setState(() {
        Questions newItems = jsonResponse;
        if (questions == null) {
          questions = newItems;
        } else {
          questions.results.addAll(newItems.results);
        }
        lastPageId = lastPageId + 1;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }

  void dispose() {
    super.dispose();
  }
}
