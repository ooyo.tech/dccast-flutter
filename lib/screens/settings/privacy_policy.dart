import 'dart:convert';

import 'package:dccast/constants/assets.dart';
import 'package:dccast/constants/server_settings.dart';
import 'package:dccast/model/user.dart';
import 'package:dccast/utils/auth.dart';
import 'package:dccast/utils/device.dart';
import 'package:dccast/utils/dio_client.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PrivacyPolicyScreen extends StatefulWidget {
  static const String id = 'privacy_policy_screen';
  @override
  _PrivacyPolicyScreenState createState() => _PrivacyPolicyScreenState();
}

class _PrivacyPolicyScreenState extends State<PrivacyPolicyScreen> {
  final storage = FlutterSecureStorage();
  String htmlResponse;
  bool isLoading = true;
  WebViewController _controller;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(translate("privacy_policy")),
      ),
      body: isLoading
          ? Center(
              child: Lottie.asset(Assets.animManduPreload,
                  alignment: Alignment.center,
                  width: DeviceUtils.getScaledSize(context, 0.5)),
            )
          : WebView(
              initialUrl: 'about:blank',
              onWebViewCreated: (WebViewController webViewController) {
                _controller = webViewController;
                _loadHtmlRespone();
              },
            ),
    );
  }

  @override
  void initState() {
    super.initState();
    // call get json data function
    // selectedPage = Selec
    this.getJSONData();
  }

  Future<String> getJSONData() async {
    final dio = Dio();
    dio.options.headers['content-type'] = 'application/x-www-form-urlencoded';
    dio.options.headers['user-agent'] = 'dcinside.castapp';

    try {
      final response = await DioClient(dio).get(ServerSettings.termofUseURL);
      setState(() {
        htmlResponse = response;
        isLoading = false;
      });
      return "sucessful";
    } on Exception catch (error) {
      setState(() {
        isLoading = true;
      });
    }
  }

  _loadHtmlRespone() {
    _controller.loadUrl(Uri.dataFromString(htmlResponse,
            mimeType: 'text/html', encoding: Encoding.getByName('utf-8'))
        .toString());
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  void dispose() {
    super.dispose();
  }
}
