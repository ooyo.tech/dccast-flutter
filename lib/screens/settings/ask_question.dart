import 'dart:io';

import 'package:dio/dio.dart' as Dio;
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:image_picker/image_picker.dart';

import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../../utils/toast.dart';

class AskQuestionScreen extends StatefulWidget {
  @override
  _AskQuestionScreenState createState() => _AskQuestionScreenState();
}

enum Answers { CAMERA, GALLERY }

class _AskQuestionScreenState extends State<AskQuestionScreen> {
  final storage = FlutterSecureStorage();
  final titleController = TextEditingController();
  final questionController = TextEditingController();
  String selectedKind = '저작권';

  /// Variables
  File imageFile;

  final _formKey = GlobalKey<FormState>();

  void _submit() async {
    if (_formKey.currentState.validate()) {
      final dio = Dio.Dio();
      try {
        var userID = await storage.read(key: Preferences.user_id);
        var fileName = imageFile != null ? imageFile.path.split('/').last : "empty";

        var map = {
          "img_path": imageFile != null ?
              await Dio.MultipartFile.fromFile(
                imageFile.path,
                filename: fileName,
              ) : null,
          "file_name": fileName,
          "title": titleController.text,
          "kinds": selectedKind,
          "question": questionController.text,
          "user": userID.toString(),
        };
        if (imageFile == null) {
          map.removeWhere((key, value) => key == "img_path");
        }
        var data = Dio.FormData.fromMap(map);

        final response = await DioClient(dio).post(
          ServerSettings.getCreateQuestionURL,
          data: data,
          /*options: Dio.Options(
            headers: {
              "Authorization": Preferences.auth_token
            },
          ),*/
        );
        showToast(translate("insert.inquiry.success"));
        titleController.text = '';
        questionController.text = '';
        selectedKind = '저작권';
        imageFile = null;
        Navigator.of(context).pop();
      } on Dio.DioError catch (e) {
        showToast(translate("error.upload.image"));
        Scaffold.of(context).showSnackBar(
          SnackBar(
            behavior: SnackBarBehavior.floating,
            margin: EdgeInsets.only(bottom: 60.0, left: 10.0, right: 10.0),
            content: Text(e.toString()),
          ),
        );
        Navigator.of(context).pop();
      }
    }
  }

  void dispose() {
    titleController.dispose();
    questionController.dispose();
    super.dispose();
  }

  /// Get from gallery
  _getFromGallery() async {
    PickedFile pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      setState(() {
        imageFile = File(pickedFile.path);
      });
    }
  }

  /// Get from camera
  _getFromCamera() async {
    PickedFile pickedFile = await ImagePicker().getImage(
      source: ImageSource.camera,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      setState(() {
        imageFile = File(pickedFile.path);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("서비스 관련 문의를 남겨주세요. "),
          Text("문의에 대한 답변은 '내 문의 내역' 에서 확인하실 수 있습니다."),
          Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                TextFormField(
                  validator: (value) {
                    if (value.isEmpty) {
                      return translate('insert.inquiry.warning-empty-data');
                    }
                    return null;
                  },
                  controller: titleController,
                  maxLength: 40,
                  style: TextStyle(color: kBlackColor, fontSize: 16),
                  decoration: InputDecoration(
                    labelText: '문의 제목',
                    focusColor: kBlackColor,
                    enabledBorder: const UnderlineInputBorder(
                      borderSide:
                          const BorderSide(color: Colors.grey, width: 1.0),
                    ),
                    focusedBorder: const UnderlineInputBorder(
                      borderSide:
                          const BorderSide(color: Colors.grey, width: 1.0),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("문의 종류"),
                    Container(
                      width: DeviceUtils.getScaledWidth(context, 0.5),
                      child: DropdownButton<String>(
                        dropdownColor: kGreyColor,
                        isExpanded: true,
                        value: selectedKind,
                        icon: Icon(
                          Icons.keyboard_arrow_down,
                          color: kPrimaryColor,
                        ),
                        iconSize: 20.0,
                        elevation: 8,
                        style: TextStyle(color: kBlackColor),
                        underline: Container(
                          height: 1.0,
                          color: kGreyColor,
                        ),
                        onChanged: (String newValue) {
                          setState(() {
                            selectedKind = newValue;
                          });
                        },
                        items: <String>['저작권', '만두', '화질/해상도', '기타', '청소년 유해신고']
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                ),
                TextFormField(
                  validator: (value) {
                    if (value.isEmpty) {
                      return translate('insert.inquiry.warning-empty-data');
                    }
                    return null;
                  },
                  keyboardType: TextInputType.multiline,
                  maxLines: 3,// when user presses enter it will adapt to it
                  controller: questionController,
                  style: TextStyle(color: kBlackColor, fontSize: 16),
                  decoration: InputDecoration(
                    labelText: '문의 내용',
                    enabledBorder: const UnderlineInputBorder(
                      borderSide:
                          const BorderSide(color: Colors.grey, width: 1.0),
                    ),
                    focusedBorder: const UnderlineInputBorder(
                      borderSide:
                          const BorderSide(color: Colors.grey, width: 1.0),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("첨부 이미지를 업로드하세요."),
                    FlatButton(
                      color: kPrimaryColor,
                      child: Text(
                        '파일 업로드',
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        _askUser();
                      },
                    ),
                  ],
                ),
                imageFile != null
                    ? Container(
                        child: Image.file(
                          imageFile,
                          fit: BoxFit.cover,
                        ),
                      )
                    : SizedBox(
                        height: 1.0,
                      ),
                /*imageFile != null
                    ? Text("Path: " + imageFile.path) : */
                    SizedBox(
                        height: 0.0,
                      ),
                FlatButton(
                    color: kPrimaryColor,
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _showMyDialog(context);
                      }
                    },
                    child: Text(
                      translate('insert.inquiry.send'),
                      style: TextStyle(color: Colors.white),
                    ))
              ],
            ),
          )
        ],
      ),
    );
  }

  Future<void> _showMyDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(translate("insert.inquiry.confirmation")),
          /*content: SingleChildScrollView(
            child: Text(message),
          ),*/
          actions: <Widget>[
            TextButton(
              child: Text(translate("group.delete.cancel")),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(translate("alert.button.ok")),
              onPressed: () {
                _submit();
              },
            ),
          ],
        );
      },
    );
  }
  
  Future _askUser() async {
    switch (await showDialog(
        context: context,
        builder: (context) => new SimpleDialog(
              title: Text(translate("question.question")),
              elevation: 4,
              children: <Widget>[
                SimpleDialogOption(
                  child: Text(translate("question.gallery")),
                  onPressed: () {
                    Navigator.pop(context, Answers.GALLERY);
                  },
                ),
                SimpleDialogOption(
                  child: Text(translate("question.camera")),
                  onPressed: () {
                    Navigator.pop(context, Answers.CAMERA);
                  },
                ),
              ],
            ))) {
      case Answers.GALLERY:
        _getFromGallery();
        break;
      case Answers.CAMERA:
        _getFromCamera();
        break;
    }
  }
}
