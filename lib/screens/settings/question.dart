import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';

import 'ask_question.dart';
import 'question_list.dart';

class QuestionScreen extends StatefulWidget {
  static const String id = 'questions_screen';
  @override
  _QuestionScreenState createState() => _QuestionScreenState();
}

class _QuestionScreenState extends State<QuestionScreen> {
  final storage = FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            title: Text(translate('contact_us')),
            bottom: TabBar(tabs: [
              Tab(
                text: translate("insert.inquiry.send"),
              ),
              Tab(
                text: translate("settings.contactus.inquery-list"),
              ),
            ])),
        body: TabBarView(
          children: [
            AskQuestionScreen(),
            QuestionListScreen(),
          ],
        ),
      ),
    );
  }
}
