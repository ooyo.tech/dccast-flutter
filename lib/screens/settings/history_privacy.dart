import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:http/http.dart' as http;
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import '../../constants/assets.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../utils/auth.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../../utils/media.dart';
import '../../utils/toast.dart';

class HistoryPrivacyScreen extends StatefulWidget {
  static const String id = 'history_privacy_screen';
  @override
  _HistoryPrivacyScreenState createState() => _HistoryPrivacyScreenState();
}

class _HistoryPrivacyScreenState extends State<HistoryPrivacyScreen> {
  final storage = FlutterSecureStorage();
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(translate("history_privacy")),
      ),
      body: SingleChildScrollView(
        child: 
          _isLoading ?
          Center(
            child: Lottie.asset(Assets.animLoad,
                alignment: Alignment.center,
                width: DeviceUtils.getScaledSize(context, 0.5)),
          ) : Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ListTile(
              title: Text(
                translate('clear_recent_live_history'),
                style: kNotificationTitle,
              ),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: kPrimaryColor,
              ),
              onTap: () {
                _showMyDialog(context, 1,
                translate("clear_recent_live_history"),
                translate("clear_recent_live_history_question"));
              },
            ),
            ListTile(
              title: Text(
                translate('clear_recent_vod_history'),
                style: kNotificationTitle,
              ),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: kPrimaryColor,
              ),
              onTap: () {
                _showMyDialog(context, 2,
                translate("clear_recent_vod_history"),
                translate("clear_recent_vod_history_question"));
              },
            ),
            ListTile(
              title: Text(
                translate('clear_recent_search_history'),
                style: kNotificationTitle,
              ),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: kPrimaryColor,
              ),
              onTap: () {
                _showMyDialog(context, 3,
                translate("clear_recent_search_history"),
                translate("clear_recent_search_history_question"));
              },
            ),
            SwitchListTile(
              title: Text(
                translate('stop_recent_history'),
                style: kNotificationTitle,
              ),
              value: Provider.of<Auth>(context, listen: false).
                userProfile.stopRecentView,
              onChanged: (val) async {
                var status = await _updateProfile(context, 
                "stop_recent_view", val);
                if (status == 200) {
                  setState(() {
                    Provider.of<Auth>(context, listen: false)
                      .userProfile.stopRecentView = val;
                    showToast(val
                          ? "최근보기 기록이 일시 중지."
                          : "최근보기 기록 일시 중지가 해제.");
                  });
                }
              },
            ),
            SwitchListTile(
              title: Text(
                translate('stop_search_history'),
                style: kNotificationTitle,
              ),
              value: Provider.of<Auth>(context, listen: false).
                userProfile.stopRecentSearch,
              onChanged: (val) async {
                var status = await _updateProfile(context,
                "stop_recent_search", val);
                if (status == 200) {
                  setState(() {
                    Provider.of<Auth>(context, listen: false)
                      .userProfile.stopRecentSearch = val;
                    showToast(val
                          ? "검색 기록이 일시 중지 되었습니다."
                          : "검색 기록 일시 중지가 해제 되었습니다.");
                  });
                }
              },
            ),
          ],
        ),
      )
    );
  }
  
  Future<void> _showMyDialog(BuildContext context, int no,
  String title, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: Text(message),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(translate("group.delete.cancel")),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(translate("alert.button.ok")),
              onPressed: () {
                setState(() {
                  _isLoading = true;
                });
                if (no == 1) {
                  _clearRecent("LIVE");
                } else if (no == 2) {
                  _clearRecent("VOD");
                } else if (no == 3) {
                  _clearSearch("");
                }
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
  
  void _clearRecent(mediaCategory) async {
    final dio = Dio();
    var userID = Provider.of<Auth>(context, listen: false)
      .userProfile.id.toString();
    try {
      // ignore: lines_longer_than_80_chars
      await DioClient(dio).delete("${ServerSettings.getUserRecentMediaURL}$userID&media__category=$mediaCategory")
          .then((value) {
            setState(() {
              _isLoading = false;
            });
            showToast(mediaCategory == "LIVE" ?
              translate("cleared_recent_live_history")
              : translate("cleared_recent_vod_history"));
      });
    } on Exception catch (error) {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _clearSearch(mediaCategory) async {
    final dio = Dio();
    var userID = Provider.of<Auth>(context, listen: false)
      .userProfile.id.toString();
    try {
      // ignore: lines_longer_than_80_chars
      await DioClient(dio).delete("${ServerSettings.getUserRecentSearchURL}")//?user=$userID
          .then((value) {
            setState(() {
              _isLoading = false;
            });
            Provider.of<MediaUtils>(context, listen: false)
              .searchHistoryModel = null;
            showToast(translate("cleared_recent_search_history"));
      });
    } on Exception catch (error) {
      setState(() {
        _isLoading = false;
      });
    }
  }

  Future<int> _updateProfile (context, key, dynamic value) async {
    var userID = Provider.of<Auth>(context, listen: false).userProfile.id;
    var authToken = Provider.of<Auth>(context, listen: false).token;

    try {
      var url = ServerSettings.updateProfileURL + userID.toString();
      var data = {
        "user_no": userID,
        "user": userID,
        key: value
      };
      var _statusCode = 500;

      await http.put(Uri.parse("$url/"), body: jsonEncode(data), headers: {
          "Authorization" : "Token $authToken",
          "Content-Type": "application/json"
        }).then((result) {
        _statusCode = result.statusCode;
      });
      return _statusCode;

    } on Exception {
      return 500;
    }
  }
}
