import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:intl/intl.dart';

import '../../constants/styles.dart';
import '../../model/questions.dart';

class QuestionDetailScreen extends StatelessWidget {
  static const String id = 'question_detail_screen';
  final Question question;

  // In the constructor, require a Todo.
  QuestionDetailScreen({Key key, @required this.question}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(translate("문의하기")),
      ),
      body: Column(
        children: [
          ListTile(
            title: Text(translate("vod.upload.title")),
            subtitle: Text(
              question.title,
              style: TextStyle(color: kBlackColor),
            ),
          ),
          ListTile(
            title: Text("문의 종류"),
            subtitle: Text(
              question.kinds,
              style: TextStyle(color: kBlackColor),
            ),
          ),
          ListTile(
            title: Text("문의일"),
            subtitle: Text(
              question.created != null
                  ? DateFormat("yyyy-MM-dd HH:mm")
                  .format(question.created.toLocal())
                  : "",
              style: TextStyle(color: kBlackColor),
            ),
          ),
          ListTile(
            title: Text("처리상태"),
            subtitle: Text(
              question.state,
              style: TextStyle(color: kBlackColor),
            ),
          ),
          ListTile(
            title: Text("문의 내용"),
            subtitle: Text(question.question),
          ),
          ListTile(
            title: Text("답변 내용"),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(question.answerDate != null
                    ? DateFormat("yyyy-MM-dd HH:mm")
                    .format(question.answerDate.toLocal())
                    : ""),
                question.answer == "" ? 
                Text("대답이 없다")
                : Text(question.answer)
              ],
            ),
          ),
        ],
      ),
    );
  }
}
