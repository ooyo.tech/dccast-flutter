import 'dart:convert';

import 'package:dccast/constants/server_settings.dart';
import 'package:dccast/constants/styles.dart';
import 'package:dccast/utils/auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

class NoticeSettingScreen extends StatefulWidget {
  static const String id = 'notice_settings_screen';
  @override
  _NoticeSettingScreenState createState() => _NoticeSettingScreenState();
}

class _NoticeSettingScreenState extends State<NoticeSettingScreen> {
  final storage = FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(translate("notification_setting")),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SwitchListTile(
              title: Text(
                translate('notice_notice'),
                style: kNotificationTitle,
              ),
              value: Provider.of<Auth>(context, listen: false).
                userProfile.noticeNotice,
              onChanged: (val) async {
                var status = await _updateProfile(context,"notice_notice", val);
                if (status == 200) {
                  setState(() {
                    Provider.of<Auth>(context, listen: false)
                      .userProfile.noticeNotice = val;
                  });
                }
              },
            ),
            SwitchListTile(
              title: Text(
                translate('notice_live'),
                style: kNotificationTitle,
              ),
              value: Provider.of<Auth>(context, listen: false).
                userProfile.noticeLive,
              onChanged: (val) async {
                var status = await _updateProfile(context,"notice_live", val);
                if (status == 200) {
                  setState(() {
                    Provider.of<Auth>(context, listen: false)
                      .userProfile.noticeLive = val;
                  });
                }
              },
            ),
            SwitchListTile(
              title: Text(
                translate('notice_vod'),
                style: kNotificationTitle,
              ),
              value: Provider.of<Auth>(context, listen: false).
                userProfile.noticeVod,
              onChanged: (val) async {
                var status = await _updateProfile(context,"notice_vod", val);
                if (status == 200) {
                  setState(() {
                    Provider.of<Auth>(context, listen: false)
                      .userProfile.noticeVod = val;
                  });
                }
              },
            )
          ],
        ),
      )
    );
  }

  Future<int> _updateProfile (context, key, dynamic value) async {
    var userID = Provider.of<Auth>(context, listen: false).userProfile.id;
    var authToken = Provider.of<Auth>(context, listen: false).token;

    try {
      var url = ServerSettings.updateProfileURL + userID.toString();
      var data = {
        "user_no": userID,
        "user": userID,
        key: value
      };
      var _statusCode = 500;

      await http.put(Uri.parse("$url/"), body: jsonEncode(data), headers: {
          "Authorization" : "Token $authToken",
          "Content-Type": "application/json"
        }).then((result) {
        print(result.statusCode);
        print(result.body);
        _statusCode = result.statusCode;
      });
      return _statusCode;

    } on Exception {
      return 500;
    }
  }
}
