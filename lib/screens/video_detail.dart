
import 'package:dccast/components/live/chat_list.dart';
import 'package:dccast/components/live_stack.dart';
import 'package:dccast/utils/auth.dart';
import 'package:dccast/utils/overlay_handler.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:timer_button/timer_button.dart';
import 'package:video_player/video_player.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../components/detail_bottom.dart';
import '../components/video/live_player.dart';
import '../components/video/vod_player.dart';
import '../constants/assets.dart';
import '../constants/preferences.dart';
import '../constants/server_settings.dart';
import '../constants/styles.dart';
import '../model/advertisement.dart';
import '../model/user_media.dart';
import '../utils/device.dart';
import '../utils/dio_client.dart';
import '../utils/media.dart';

class VideoDetailScreen extends StatefulWidget {
  static const String id = 'video_detail_screen';
  final eContent content;

  const VideoDetailScreen({Key key, this.content}) : super(key: key);

  @override
  VideoDetailScreenState createState() => VideoDetailScreenState();
}

enum adStatus { LOADING, SHOW, SKIPPED }

class VideoDetailScreenState extends State<VideoDetailScreen> {
  double aspectRatio = 16 / 9;
  Media media;
  VideoPlayerController _adPlayerController;
  LiveStackComponent liveStackComponent;
  adStatus currentAdStatus = adStatus.LOADING;
  Advertisement currentAd;
  bool isLoading = true;
  bool _allowBack = true;
  bool isAdAvailable = true;
  bool liveFullscreen = false;//false
  final storage = FlutterSecureStorage();

  // IjkMediaController adPlayer = IjkMediaController();
  VodPlayer vodPlayer;
  LivePlayer livePlayer;

  int roomID;
  ChatListComponent chatList;
  final GlobalKey<ChatListComponentState> _chatKey
   = GlobalKey<ChatListComponentState>();
  final GlobalKey<VodPlayerState> _vodKey
   = GlobalKey<VodPlayerState>();
  
  @override
  Widget build(BuildContext context) {
    liveStackComponent = LiveStackComponent(fullScreen: false);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: kPrimaryColor
    ));
    media = Provider.of<MediaUtils>(context, listen: false).chosenMedia;
    print("chosenMedia id:${media.id}");
    print("chosenMedia mediaId:${media.mediaId}");
    return WillPopScope(
      onWillPop: () {
        print("trying to pop");
        return Future(() => _allowBack);
      },
      child: Scaffold(
        appBar: null/*liveFullscreen
          ? null
          : AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () => _allowBack ? Navigator.of(context).pop() : {},
          ),
          title: Text(media?.title.toString(), style: kSwiperStyle),
        )*/,
        resizeToAvoidBottomInset: media.category != "LIVE" || liveFullscreen,
        body: Consumer<OverlayHandlerProvider>(
          builder: (context, overlayProvider, _) {
          return /*(overlayProvider.inPipMode) 
          ? _buildOverlayVideoPlayer()
          :*/ isLoading
          ? Center(
              child: Lottie.asset(Assets.animLoad,
                  alignment: Alignment.center,
                  width: DeviceUtils.getScaledSize(context, 0.5)),
            )
          : SafeArea(
            child: liveFullscreen
              ? _renderFullLive()
              : Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                /*GestureDetector(
                  onVerticalDragDown: (DragDownDetails details) {
                    Provider.of<OverlayHandlerProvider>(context, listen: false)
                      .enablePip(aspectRatio);
                  },
                  child: media.category == "LIVE"
                  ? _renderLive()
                  : _renderVod()
                ),*/
                  media.category == "LIVE"
                  ? _renderLive()
                  : _renderVod(),
                media.category == "LIVE"
                  ? Expanded(child: LiveStackComponent(
                      fullScreen: false,
                      chatListComponentKey: _chatKey,
                      chatListComponent: ChatListComponent(
                        key: _chatKey,
                        roomID: roomID,
                        fullscreen: false),
                  ))
                  : Expanded(child: VideoDetailBottomComponent())
              ],
            )
          );
        }
      ),
      ),
    );
  }

  Widget _renderVod() {
    vodPlayer = VodPlayer(media: media);
    return AspectRatio(
      aspectRatio: aspectRatio,
      child: Center(
        child:  
            currentAdStatus == adStatus.SHOW && isAdAvailable ? 
            _renderAd()
            : vodPlayer
      ),
    );
  }

  Widget _renderFullLive() {
    livePlayer = LivePlayer(media: media, detailState: this);
    return Stack(
      clipBehavior: Clip.none,
      children: <Widget>[
        livePlayer,
        LiveStackComponent(
          fullScreen: true,
          chatListComponentKey: _chatKey,
          chatListComponent: ChatListComponent(
            key: _chatKey,
            roomID: roomID,
            fullscreen: true
        )
      )
    ]);
  }

  Widget _renderLive() {
    livePlayer = LivePlayer(media: media, detailState: this);
    return AspectRatio(
      aspectRatio: aspectRatio,
      child: Center(
        child:  
            currentAdStatus == adStatus.SHOW && isAdAvailable ? 
            _renderAd()
            : livePlayer
      ),
    );
  }

  Widget _renderAd() {
    return Container(
      color: kBlackColor,
      child: Stack(children: [
        InkWell(
          onTap: _showModelSheet,
          child: VideoPlayer(_adPlayerController)
            // IjkPlayer(mediaController: adPlayer)
            //Container()
        ),
        Positioned(
          bottom: 8.0,
          right: -1.0,
          child: TimerButton(
            timeOutInSeconds: currentAd?.result?.skipDuration ?? 15,
            disabledTextStyle: kSwiperStyle,
            buttonType: ButtonType.FlatButton,
            onPressed: skipAdAction,
            color: kBlack50Color,
            label: "Skip",
          ),
        )
      ]),
    );
  }

  Widget _buildOverlayVideoPlayer() {
    return Consumer<OverlayHandlerProvider>(
      builder: (context, overlayProvider, _) {
        return Stack(
          children: <Widget>[
            Row(
              children: <Widget>[
                AnimatedContainer(
                  duration: Duration(milliseconds: 250),
                  width: /*overlayProvider.inPipMode 
                    ? (80*aspectRatio)
                    :*/ MediaQuery.of(context).size.width,
                  color: Colors.black,
                  constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width,
                  ),
                  child: AspectRatio(
                    aspectRatio: aspectRatio,
                    child: 
                      // _vodKey?.currentState?.player ?? 
                      SizedBox()
                    ,
                  ),
                ),
                if(overlayProvider.inPipMode)
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        Provider.of<OverlayHandlerProvider>(context, listen: false).disablePip();
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(media?.title),
                            Text(media?.user?.nickName,
                              style: Theme.of(context).textTheme.caption,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                if(overlayProvider.inPipMode)
                  IconButton(
                    icon: Icon(
                     Icons.pause,
                    ),
                    onPressed: () {
                      /*if(_videoPlayerController.value.isPlaying) {
                        _videoPlayerController.pause();
                      } else {
                        _videoPlayerController.play();
                      }*/
                    },
                  ),
                if(overlayProvider.inPipMode)
                  IconButton(
                    icon: Icon(
                      Icons.close
                    ),
                    onPressed: () {
                      Provider.of<OverlayHandlerProvider>(context, listen: false).removeOverlay(context);
                    },
                  )
              ],
            ),
          ],
        );
      }
    );
  }

  void skipAdAction() {
    var profile = Provider.of<Auth>(context, listen: false).userProfile;
    if (profile != null) {
      final dio = Dio();
      var data = {
        "media": media.id,
        "user": profile.id,
        "ad": currentAd.result.id
      };

      DioClient(dio)
      .post(ServerSettings.watchAdURL, data: data);
    }

    setState(() {
      currentAdStatus = adStatus.SKIPPED;
      _allowBack = true;
    });
  }

  @override
  void initState() {
    roomID = Provider.of<MediaUtils>
      (context, listen: false).chosenMedia.id;
    fetchAd().then((value) async {
      if (value != null) {
        currentAd = value;
        await initializeAdPlayer().then((value) {
          vodPlayer?.pause();
          livePlayer?.pause();
          setState(() {
            _allowBack = false;
            currentAdStatus = adStatus.SHOW;
          });
        });
      }
    });
    super.initState();
    createRecentView();
  }

  @override
  void dispose() {
    _adPlayerController?.dispose();
    // adPlayer?.dispose();
    vodPlayer?.dispose();
    livePlayer?.dispose();
    super.dispose();
  }

  String videoFormatRemove(String mediaID) {
    return mediaID.replaceAll(".mp4", "");
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  Future<dynamic> createRecentView() async {
    final dio = Dio();
    try {
      var profile = Provider.of<Auth>(context, listen: false).userProfile;
      if (profile == null || profile.stopRecentView) {
        return false;
      }

      final response = await DioClient(dio).post(
        ServerSettings.getUserCreateRecentURL,
        data: {
        "user": profile.id,
        "media": Provider.of<MediaUtils>(context, listen: false).chosenMedia.id,
        }
      );
      return true;
    } on Exception catch (e) {
      return false;
    }
  }

  Future<dynamic> fetchAd() async {
    final dio = Dio();
    try {
      Map data = {
        "kind": "get_rand_adver",
        "locate_id":
            widget.content == eContent.vod ? "VOD_movie" : "LIVE_movie",
      };

      final response =
        await DioClient(dio).post(ServerSettings.getAdsURL, data: data);
      print(response);

      if (response['result'] == false) {
        setState(() {
          isAdAvailable = false;
          _allowBack = true;
          isLoading = false;
          currentAdStatus = adStatus.SKIPPED;
        });
        return null;
      } else {
        setState(() {
          currentAdStatus = adStatus.LOADING;
          isAdAvailable = true;
          isLoading = false;
          currentAd = Advertisement.fromJson(response);
        });
        return currentAd;
      }
    } on Exception catch (e) {
      setState(() {
        currentAdStatus = adStatus.SKIPPED;
        isAdAvailable = false;
      });
    }
  }

  Future<void> initializeAdPlayer() async {
    if (isAdAvailable) {
      var adPlayURL = 
        ServerSettings.wowZaUrl +
        "/vod/_definst_/smil:" +
        currentAd.result.file.replaceAll(".mp4", "") +
        ".smil/playlist.m3u8";
        print(adPlayURL);
      // await adPlayer.setNetworkDataSource(adPlayURL, autoPlay: true);
      _adPlayerController = VideoPlayerController.network(adPlayURL);
      await _adPlayerController.initialize();
      _adPlayerController.addListener(() {
        setState(() {
          if (!_adPlayerController.value.isPlaying
            &&_adPlayerController.value.isInitialized &&
    (_adPlayerController.value.duration ==_adPlayerController.value.position)) {
              skipAdAction();
          }
        });
      });
    }
  }

  void _showModelSheet() {
    //TODO: increase counter
    showModalBottomSheet(
      context: context,
      builder: (builder) {
        return Container(
          child: WebView(initialUrl: currentAd.result.affil.site),
        );
      },
    );
  }
}
