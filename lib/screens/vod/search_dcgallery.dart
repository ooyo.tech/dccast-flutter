import 'package:dccast/constants/assets.dart';
import 'package:dccast/constants/preferences.dart';
import 'package:dccast/constants/server_settings.dart';
import 'package:dccast/constants/styles.dart';
import 'package:dccast/model/category.dart';
import 'package:dccast/model/media.dart';
import 'package:dccast/utils/auth.dart';
import 'package:dccast/utils/device.dart';
import 'package:dccast/utils/dio_client.dart';
import 'package:dccast/utils/media.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

class SearchDcGalleryScreen extends StatefulWidget {
  static const String id = 'search_dc_gallery_screen';
  @override
  _SearchDcGalleryScreenState createState() => _SearchDcGalleryScreenState();
}

class _SearchDcGalleryScreenState extends State<SearchDcGalleryScreen> {
  final storage = FlutterSecureStorage();
  final searchController = TextEditingController();
  int lastPageId = 1;
  bool isLoading = false;
  DcGallery dcGallery;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            Expanded(
              child: TextFormField(
                controller: searchController,
                onFieldSubmitted: (text) => {_fetchList(text)},
                style: TextStyle(color: Colors.white, fontSize: 16),
                decoration: InputDecoration(
                  hintText: translate("search"),
                  hintStyle: TextStyle(color: kGreyColor),
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                ),
              ),
            ),
            IconButton(
              icon: Icon(Icons.close_rounded),
              onPressed: () {
                setState(() {
                  searchController.clear();
                });
                Provider.of<MediaUtils>(context, listen: false).clearSearch;
              },
            )
          ],
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Container(
        color: Colors.white,
        child: isLoading
            ? Center(
                child: Lottie.asset(Assets.animLoad,
                    alignment: Alignment.center,
                    width: DeviceUtils.getScaledSize(context, 0.5)),
              )
            : _buildListView(),
      ),
    );
  }

  Widget _buildListView() {
    return searchController.text == ""
    ? Center(child: Text("갤러리를 검색해 주세요"),)
    : dcGallery == null || dcGallery.count == 0
    ? Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Lottie.asset(Assets.animNoData,
                alignment: Alignment.center,
                width: DeviceUtils.getScaledSize(context, 0.5)),
            Text(translate("no_search_result"), style: kTextStyle)
          ],
        ),
      )
    : 
    SingleChildScrollView(
      physics: ScrollPhysics(),
      child: Column(
        children: [
          Container(
            color: kGreyColor,
            width: DeviceUtils.getScaledWidth(context, 1.0),
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
            child: Text(translate("vod.upload.gallery.main"),
                style: kNotificationSubtitle),
          ),
          if (dcGallery.mainGall.isNotEmpty)
          ListView.builder(
            shrinkWrap: true,
            padding: const EdgeInsets.all(0),
            physics: NeverScrollableScrollPhysics(),
            itemCount: dcGallery.mainGall.length,
            itemBuilder: (context, index) {
              return _buildListItem(dcGallery.mainGall[index]);
            },
          ),
          Container(
            color: kGreyColor,
            width: DeviceUtils.getScaledWidth(context, 1.0),
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
            child: Text(translate("vod.upload.gallery.minor"),
                style: kNotificationSubtitle),
          ),
          if (dcGallery.minorGall.isNotEmpty)
          ListView.builder(
            shrinkWrap: true,
            padding: const EdgeInsets.all(0),
            physics: NeverScrollableScrollPhysics(),
            itemCount: dcGallery.minorGall.length,
            itemBuilder: (context, index) {
              return _buildListItem(dcGallery.minorGall[index]);
            },
          ),
          if (dcGallery.miniGall.isNotEmpty)
          ListView.builder(
            shrinkWrap: true,
            padding: const EdgeInsets.all(0),
            physics: NeverScrollableScrollPhysics(),
            itemCount: dcGallery.miniGall.length,
            itemBuilder: (context, index) {
              return _buildListItem(dcGallery.miniGall[index]);
            },
          ),
          if (dcGallery.mainRecommGall.isNotEmpty)
          ListView.builder(
            shrinkWrap: true,
            padding: const EdgeInsets.all(0),
            physics: NeverScrollableScrollPhysics(),
            itemCount: dcGallery.mainRecommGall.length,
            itemBuilder: (context, index) {
              return _buildListItem(dcGallery.mainRecommGall[index]);
            },
          ),
          if (dcGallery.minorRecomGall.isNotEmpty)
          ListView.builder(
            shrinkWrap: true,
            padding: const EdgeInsets.all(0),
            physics: NeverScrollableScrollPhysics(),
            itemCount: dcGallery.minorRecomGall.length,
            itemBuilder: (context, index) {
              return _buildListItem(dcGallery.minorRecomGall[index]);
            },
          ),
        ]
      )
    );
  }

  Widget _buildListItem(DcGalleryItem item) => Container(
    decoration: BoxDecoration(color: Colors.white),
    margin: const EdgeInsets.all(10.0),
    child: GestureDetector(
      child: Text(item.title),
      onTap: () {
        Navigator.of(context).pop(item.title);
      },
    )
  );

  Future<void> _fetchList(String searchText) async {
    final dio = Dio();
    var appID = await storage.read(key: Preferences.appID);
    setState(() {
      isLoading = true;
    });
    try {
      var data = {
        "kind": "gallery_search",
        "keyword": searchText,
        "app_id": appID,
        "search_type": "gall_name",
      };

      print(data);
      final response = await DioClient(dio)
      .post(ServerSettings.getApiUpdateURL, data: data);
      print(response);
      setState(() {
        isLoading = false;
        dcGallery = DcGallery.fromJson(response);
        dcGallery.count = dcGallery.minorGall.length
          + dcGallery.mainGall.length
          + dcGallery.miniGall.length
          + dcGallery.mainRecommGall.length
          + dcGallery.minorRecomGall.length;
      });
    } on Exception catch (error) {
      setState(() {
      });
    }
  }

  void dispose() {
    searchController.dispose();

    super.dispose();
  }
}
