// import 'package:flutter/material.dart';
// import 'package:flutter_ijkplayer/flutter_ijkplayer.dart';
// // import 'package:flutter_vlc_player/flutter_vlc_player.dart';
// import 'package:provider/provider.dart';
//
// import '../../components/categories_list.dart';
// import '../../components/detail_bottom.dart';
// import '../../components/home/home_grid.dart';
// import '../../components/home/home_list.dart';
// import '../../components/home/home_swiper.dart';
// import '../../constants/server_settings.dart';
// import '../../constants/styles.dart';
// import '../../model/user_media.dart';
// import '../../utils/easy_pip.dart';
// import '../../utils/media.dart';
// import '../../utils/overlay_handler.dart';
//
// class HomeVodPipScreen extends StatefulWidget {
//   @override
//   _HomeVodPipScreenState createState() => _HomeVodPipScreenState();
// }
//
// class _HomeVodPipScreenState extends State<HomeVodPipScreen> {
//   bool isLoading = false;
//   IjkMediaController controller = IjkMediaController();
//   VlcPlayerController _videoViewController;
//   double aspectRatio = 16 / 9;
//   bool visibleControl;
//   bool isPlaying = false;
//   String position = "";
//   String duration = "";
//   double sliderValue = 0.0;
//   bool isBuffering = true;
//
//   @override
//   Widget build(BuildContext context) {
//     return Consumer<MediaUtils>(
//       builder: (context, media, child) {
//         return PIPStack(
//           animationDuration: Duration(milliseconds: 500),
//           backgroundWidget: ListView(
//             children: <Widget>[
//               CategoryListComponent(content: eContent.vod),
//               AspectRatio(
//                   aspectRatio: 16 / 9,
//                   child: HomeSwiperComponent(eContent.vod)),
//               HomeListComponent(eContent.vod),
//               HomeGridComponent(content: eContent.vod)
//             ],
//           ),
//           pipWidget: media.inPipMode ? videoPlayComponent() : Container(),
//           pipExpandedContent: Container(
//               color: Colors.white, child: VideoDetailBottomComponent()),
//           onClosed: () {
//             media.disablePip();
//           },
//           pipEnabled: media.inPipMode,
//           // pipMiniDetail: Column(
//           //   crossAxisAlignment: CrossAxisAlignment.start,
//           //   mainAxisSize: MainAxisSize.min,
//           //   children: <Widget>[
//           //     Text(
//           //       media?.chosenMedia?.title.toString(),
//           //       maxLines: 2,
//           //     ),
//           //     Text(
//           //       media?.chosenMedia?.user?.nickName.toString(),
//           //       style: Theme.of(context).textTheme.caption,
//           //       maxLines: 1,
//           //     ),
//           //   ],
//           // ),
//         );
//       },
//     );
//   }
//
//   void _fetchSwiper() {
//     Provider.of<MediaUtils>(context, listen: false).fetchHitVodMedia(
//         success: () {
//       setState(() {
//         isLoading = false;
//       });
//     }, error: (String error) {
//       Scaffold.of(context).showSnackBar(
//         SnackBar(
//           behavior: SnackBarBehavior.floating,
//           margin: EdgeInsets.only(bottom: 60.0, left: 10.0, right: 10.0),
//           content: Text(error),
//         ),
//       );
//     });
//   }
//
//   void _fetchList() {
//     Provider.of<MediaUtils>(context, listen: false).fetchMedias(
//         options: "category=VOD&ordering=-views&limit=10",
//         success: () {
//           setState(() {
//             isLoading = false;
//           });
//         },
//         error: (String error) {
//           Scaffold.of(context).showSnackBar(
//             SnackBar(
//               behavior: SnackBarBehavior.floating,
//               margin: EdgeInsets.only(bottom: 60.0, left: 10.0, right: 10.0),
//               content: Text(error),
//             ),
//           );
//         });
//   }
//
//   Widget videoPlayComponent() {
//     _videoViewController = VlcPlayerController(onInit: () {
//       _videoViewController.play();
//     });
//     _videoViewController.addListener(() {
//       if (!this.mounted) return;
//       if (_videoViewController.initialized) {
//         var oPosition = _videoViewController.position;
//         var oDuration = _videoViewController.duration;
//         if (oDuration.inHours == 0) {
//           var strPosition = oPosition.toString().split('.')[0];
//           var strDuration = oDuration.toString().split('.')[0];
//           position =
//               "${strPosition.split(':')[1]}:${strPosition.split(':')[2]}";
//           duration =
//               "${strDuration.split(':')[1]}:${strDuration.split(':')[2]}";
//         } else {
//           position = oPosition.toString().split('.')[0];
//           duration = oDuration.toString().split('.')[0];
//         }
//         sliderValue = _videoViewController.position.inSeconds.toDouble();
//
//         switch (_videoViewController.playingState) {
//           case PlayingState.PAUSED:
//             setState(() {
//               isBuffering = false;
//             });
//             break;
//
//           case PlayingState.STOPPED:
//             setState(() {
//               isPlaying = false;
//               isBuffering = false;
//             });
//             break;
//           case PlayingState.BUFFERING:
//             setState(() {
//               isBuffering = true;
//             });
//             break;
//           case PlayingState.PLAYING:
//             setState(() {
//               isBuffering = false;
//             });
//             break;
//           case PlayingState.ERROR:
//             setState(() {});
//             print("VLC encountered error");
//             break;
//           default:
//             setState(() {});
//             break;
//         }
//       }
//     });
//     Media media = Provider.of<MediaUtils>(context, listen: true).chosenMedia;
//     String mediaPlayURL = ServerSettings.wowZaUrl +
//         "/vod/_definst_/smil:" +
//         media.mediaId.replaceAll(".mp4", "") +
//         ".smil/playlist.m3u8";
//     controller.setNetworkDataSource(mediaPlayURL, autoPlay: false);
//     return Stack(clipBehavior: Clip.none, children: <Widget>[
//       AnimatedContainer(
//         duration: Duration(milliseconds: 250),
//         width: MediaQuery.of(context).size.width,
//         color: Colors.black,
//         constraints: BoxConstraints(
//           maxWidth: MediaQuery.of(context).size.width,
//         ),
//         child: IjkPlayer(mediaController: controller),
//       ),
//       // AspectRatio(
//       //   aspectRatio: 13 / 5,
//       //   child: Container(
//       //     color: Colors.transparent,
//       //   ),
//       // )
//     ]);
//   }
//
//   Widget playerControl() {
//     return !Provider.of<MediaUtils>(context, listen: false).isInSmallMode
//         ? InkWell(
//             onTap: () {},
//             child: AspectRatio(
//               aspectRatio: aspectRatio,
//               child: AnimatedOpacity(
//                 duration: Duration(milliseconds: 500),
//                 opacity: visibleControl ? 1 : 0,
//                 child: Container(
//                   color: Colors.black45,
//                   padding: EdgeInsets.all(8.0),
//                   child: Column(
//                     children: [
//                       Row(
//                         children: [
//                           IconButton(
//                             padding: EdgeInsets.zero,
//                             icon: Icon(Icons.picture_in_picture_alt_rounded,
//                                 size: 24.0, color: Colors.white),
//                             onPressed: () {
//                               Provider.of<OverlayHandlerProvider>(context,
//                                       listen: false)
//                                   .enablePip(aspectRatio);
//                             },
//                           ),
//                           Expanded(
//                             child: Row(
//                               crossAxisAlignment: CrossAxisAlignment.end,
//                               mainAxisAlignment: MainAxisAlignment.end,
//                               children: [
//                                 IconButton(
//                                   padding: EdgeInsets.zero,
//                                   icon: Icon(Icons.settings_rounded,
//                                       size: 24.0, color: Colors.white),
//                                 ),
//                                 IconButton(
//                                   padding: EdgeInsets.zero,
//                                   icon: Icon(Icons.close_rounded,
//                                       size: 24.0, color: Colors.white),
//                                   onPressed: () {
//                                     Provider.of<OverlayHandlerProvider>(context,
//                                             listen: false)
//                                         .removeOverlay(context);
//                                   },
//                                 ),
//                               ],
//                             ),
//                           )
//                         ],
//                       ),
//                       Expanded(
//                         child: IconButton(
//                             icon: isPlaying
//                                 ? Icon(
//                                     Icons.pause_rounded,
//                                     color: Colors.white,
//                                     size: 50.0,
//                                   )
//                                 : Icon(
//                                     Icons.play_arrow_rounded,
//                                     color: Colors.white,
//                                     size: 50.0,
//                                   ),
//                             onPressed: playOrPauseVideo),
//                       ),
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         mainAxisSize: MainAxisSize.max,
//                         children: [
//                           Text(
//                             position,
//                             style: kSwiperStyle,
//                           ),
//                           Expanded(
//                             child: SliderTheme(
//                               data: SliderTheme.of(context).copyWith(
//                                 activeTrackColor: Colors.white,
//                                 inactiveTrackColor: Colors.white38,
//                                 trackShape: RectangularSliderTrackShape(),
//                                 trackHeight: 2.0,
//                                 thumbColor: Colors.white,
//                                 thumbShape: RoundSliderThumbShape(
//                                     enabledThumbRadius: 5.0),
//                                 overlayShape:
//                                     RoundSliderOverlayShape(overlayRadius: 6.0),
//                               ),
//                               child: Slider(
//                                 value: sliderValue,
//                                 min: 0.0,
//                                 max: _videoViewController.duration == null
//                                     ? 1.0
//                                     : _videoViewController.duration.inSeconds
//                                         .toDouble(),
//                                 onChanged: (progress) {
//                                   // setState(() {
//                                   //   sliderValue = progress.floor().toDouble();
//                                   // });
//                                   //convert to Milliseconds since VLC requires MS to set time
//                                   _videoViewController
//                                       .setTime(sliderValue.toInt() * 1000);
//                                 },
//                               ),
//                             ),
//                           ),
//                           Text(
//                             duration,
//                             style: kSwiperStyle,
//                           ),
//                         ],
//                       ),
//                       SizedBox(height: 5.0)
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//           )
//         : Container();
//   }
//
//   @override
//   void setState(fn) {
//     if (mounted) {
//       super.setState(fn);
//     }
//   }
//
//   void playOrPauseVideo() {
//     String state = _videoViewController.playingState.toString();
//
//     if (state == "PlayingState.PLAYING") {
//       _videoViewController.pause();
//       setState(() {
//         isPlaying = false;
//       });
//     } else {
//       _videoViewController.play();
//       setState(() {
//         isPlaying = true;
//       });
//     }
//   }
//
//   @override
//   void initState() {
//     _fetchSwiper();
//     _fetchList();
//     super.initState();
//   }
//
//   @override
//   void dispose() {
//     print("dispose on home vod");
//     controller.dispose();
//     _videoViewController.dispose();
//     super.dispose();
//   }
// }
