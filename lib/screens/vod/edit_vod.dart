import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart' as Dio;
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/category.dart';
import '../../model/user_media.dart';
import '../../model/vod/upload_thumb.dart';
import '../../model/vod/upload_video.dart';
import '../../utils/auth.dart';
import '../../utils/category.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../../utils/toast.dart';

enum Answers { CAMERA, GALLERY }
enum Progress { UPLOAD, LOADING, SUCCESS }

class EditVodScreen extends StatefulWidget {
  static const String id = 'edit_vod_screen';
  Media media;

  EditVodScreen({
    @required this.media
  });

  @override
  _EditVodScreenState createState() => _EditVodScreenState();
}

class _EditVodScreenState extends State<EditVodScreen> {
  final storage = FlutterSecureStorage();

  String selectedKind = translate('vod.upload.share.public');

  /// Variables
  File _videoFile;
  File _imageFile;
  ImagePicker picker = ImagePicker();
  final titleController = TextEditingController();
  final descriptionController = TextEditingController();
  bool isLoading = false;
  Progress STEPS = Progress.UPLOAD;
  Category _categories;
  int selectedCategory;
  double progressPercentage = 0.0;
  Dio.CancelToken token;
  VodVideoUpload mediaUploaded;
  VodThumbUpload thumbUploaded;

  SnackBar _snackBar;

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _categories = Provider.of<CategoryUtils>(context, listen: false).categories;
    titleController.text = widget.media.title;
    descriptionController.text = widget.media.explanation;
    selectedKind = widget.media.kinds;
    selectedCategory = widget.media.mediaCategory?.id;
  }

  @override
  Widget build(BuildContext context) {
    //   = _categories.results[0].id;
    return Scaffold(
      appBar: AppBar(
        title: Text(translate("upload-vod")),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: ModalProgressHUD(
        opacity: 0.6,
        progressIndicator: Container(
            width: DeviceUtils.getScaledWidth(context, 0.8),
            height: DeviceUtils.getScaledHeight(context, 0.5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.white,
            ),
            child: progressIndic()),
        inAsyncCall: isLoading,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return translate("field_required");
                              }
                              return null;
                            },
                            controller: titleController,
                            maxLines: 1,
                            maxLength: 40,
                            maxLengthEnforced: true,
                            style: TextStyle(color: kBlackColor, fontSize: 16),
                            decoration: InputDecoration(
                              hintText: translate("vod.upload.title"),
                              enabledBorder: const UnderlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1.0),
                              ),
                              focusedBorder: const UnderlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1.0),
                              ),
                            ),
                          ),
                          TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return translate("field_required");
                              }
                              return null;
                            },
                            maxLines: 3,
                            minLines: 1,
                            controller: descriptionController,
                            style:
                                TextStyle(color: kBlack50Color, fontSize: 16),
                            decoration: InputDecoration(
                              hintText: translate("vod.upload.description"),
                              enabledBorder: const UnderlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1.0),
                              ),
                              focusedBorder: const UnderlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1.0),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: 10.0),
                      _imageFile != null
                        ? SizedBox(
                            width: DeviceUtils.getScaledWidth(context, 0.3),
                            child: Image.file(_imageFile, fit: BoxFit.contain),
                          )
                        : 
                        (widget.media.mediaThumbnail != null ?
                        SizedBox(
                            width: DeviceUtils.getScaledWidth(context, 0.3),
                            height: DeviceUtils.getScaledWidth(context, 0.3),
                            child: GestureDetector(
                              onTap: _askUser,
                              child: Image.network(widget.media.mediaThumbnail, fit: BoxFit.contain)
                            ),
                          ) :
                        SizedBox(
                            width: DeviceUtils.getScaledWidth(context, 0.3),
                            height: DeviceUtils.getScaledWidth(context, 0.3),
                            child: GestureDetector(
                              onTap: _askUser,
                              child: Container(
                                decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Colors.redAccent)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(Icons.camera_alt_rounded),
                                    Text("image")
                                  ],
                                ),
                              ),
                            ))
                        ),
                  ],
                ),
                SizedBox(
                  height: 20.0,
                ),
                Row(children: [
                  Container(
                    width: DeviceUtils.getScaledWidth(context, 0.3),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      translate("vod.upload.category"),
                    ),
                  ),
                  Expanded(
                    child: DropdownButtonFormField(
                      isExpanded: true,
                      value: selectedCategory,
                      hint: Text(translate("vod.upload.choose-cat")),
                      iconEnabledColor: kPrimaryColor,
                      items:
                          List.generate(_categories.results.length, (int item) {
                        return DropdownMenuItem(
                          value: _categories.results[item]?.id,
                          child: Text(_categories.results[item]?.name),
                        );
                      }),
                      onChanged: (value2) {
                        setState(() {
                          selectedCategory = value2;
                        });
                      },
                      validator: (value) => value == null
                          ? translate("vod.upload.choose-cat")
                          : null,
                    ),
                  )
                ]),
                Row(children: [
                  Container(
                    width: DeviceUtils.getScaledWidth(context, 0.3),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      translate("vod.upload.share"),
                    ),
                  ),
                  Expanded(
                    child: DropdownButtonFormField(
                      iconEnabledColor: kPrimaryColor,
                      isExpanded: true,
                      hint: Text(translate("vod.upload.select-share")),
                      items: [
                        DropdownMenuItem<String>(
                          value: "",
                          child: Text("-"),
                        ),
                        DropdownMenuItem<String>(
                          value: translate('vod.upload.share.public'),
                          child: Text(translate('vod.upload.share.public')),
                        ),
                        DropdownMenuItem<String>(
                          value: translate('vod.upload.share.private'),
                          child: Text(translate('vod.upload.share.private')),
                        ),
                        DropdownMenuItem<String>(
                          value: translate('vod.upload.share.under19'),
                          child: Text(translate('vod.upload.share.under19')),
                        ),
                      ],
                      value: selectedKind,
                      validator: (value) => value == null
                          ? translate("vod.upload.select-share")
                          : null,
                      onChanged: (value) {
                        if (value == translate('vod.upload.share.under19')) {
                          _showMyDialog(
                            context,
                            translate("alert.message.under-19-title"),
                            translate("alert.message.under-19-description"),
                            TextButton(
                              child: Text(translate("alert.button.ok")),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          );
                        }
                        setState(() {
                          selectedKind = value;
                        });
                      },
                    ),
                  )
                ]),
                Expanded(
                  child: Container(),
                ),
                RaisedButton(
                  color: kPrimaryColor,
                  child: Text(
                    translate("vod.upload.post"),
                    style: kSwiperStyle,
                  ),
                  onPressed: _submit,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _thumbnailWidget(String videoPath) async {
    final uint8list = await VideoThumbnail.thumbnailFile(
      video: videoPath,
      imageFormat: ImageFormat.PNG,
      maxWidth: 300,
      quality: 50,
    );

    setState(() {
      _imageFile = File(uint8list);
    });
  }

  Future<void> _submit() async {
    if (_formKey.currentState.validate()) {
      setState(() {
        isLoading = true;
      });
      final dio = Dio.Dio();
      token = Dio.CancelToken();
      String userID = await storage.read(key: Preferences.user_id);
      var authToken = Provider.of<Auth>(context, listen: false).token;
      try {
        var data = {
          "title": titleController.text,
          "explanation": descriptionController.text,
          "kinds": selectedKind,
          "media_category": selectedCategory,
        };
        
        if (_imageFile != null) {
          Dio.FormData thumData = Dio.FormData.fromMap({
            "thumbnail": await Dio.MultipartFile.fromFile(_imageFile.path),
          });
          final thumbResponse = await DioClient(dio).post(
              ServerSettings.uploadVODThumbURL,
              data: thumData,
              cancelToken: token);

          thumbUploaded = VodThumbUpload.fromJson(thumbResponse);
          data["media_thumbnail"] = thumbUploaded.path
              .replaceAll("public", ServerSettings.videoThumbUrlChangeURL);
        }
        print(data);

        var url = ServerSettings.createVODMediaURL + widget.media.id.toString();
        await http.put(Uri.parse("$url/"), body: jsonEncode(data), headers: {
            "Authorization" : "Token $authToken",
            "Content-Type": "application/json"
          }).then((result) {
          setState(() {
            isLoading = false;
          });
          Navigator.of(context).pop("refresh");
        });
        return "sucessful";
      } on Exception catch (error) {
        print(error.toString());
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future _askUser() async {
    switch (await showDialog(
        context: context,
        builder: (context) => SimpleDialog(
              title: Text(translate("vod.upload.select-from")),
              elevation: 4,
              children: <Widget>[
                SimpleDialogOption(
                  child: Text(translate("vod.upload.select-gallery")),
                  onPressed: () {
                    Navigator.pop(context, Answers.GALLERY);
                  },
                ),
                SimpleDialogOption(
                  child: Text(translate("vod.upload.select-camera")),
                  onPressed: () {
                    Navigator.pop(context, Answers.CAMERA);
                  },
                ),
              ],
            ))) {
      case Answers.GALLERY:
        _getFromGallery();
        break;
      case Answers.CAMERA:
        _getFromCamera();
        break;
    }
  }

  /// Get from gallery
  _getFromGallery() async {
    PickedFile pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      setState(() {
        _imageFile = File(pickedFile.path);
      });
    }
  }

  /// Get from camera
  _getFromCamera() async {
    PickedFile pickedFile = await ImagePicker().getImage(
      source: ImageSource.camera,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      setState(() {
        _imageFile = File(pickedFile.path);
      });
    }
  }

  Widget progressIndic() {
    if (STEPS == Progress.SUCCESS) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Lottie.asset(Assets.animSuccess,
              width: DeviceUtils.getScaledSize(context, 0.3),
              height: DeviceUtils.getScaledSize(context, 0.3)),
          FlatButton(
            child: Text("OK"),
            onPressed: () {
              setState(() {
                isLoading = false;
              });
              Navigator.of(context).pop();
            },
          )
        ],
      );
    } else if (STEPS == Progress.UPLOAD) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircularPercentIndicator(
            radius: 100.0,
            lineWidth: 5.0,
            percent: progressPercentage,
            center: new Text(
                (progressPercentage * 100.0).toStringAsFixed(0) + " %"),
            progressColor: kPrimaryColor,
            backgroundColor: Colors.white,
          ),
          FlatButton(
            child: Text(translate("cancel")),
            onPressed: () {
              showToast("VOD 등록이 취소되었습니다");
              setState(() {
                Timer(Duration(milliseconds: 500), () {
                  token.cancel("cancelled");
                });
                isLoading = false;
              });
            },
          )
        ],
      );
    } else {
      return Lottie.asset(Assets.animLoad,
          width: DeviceUtils.getScaledSize(context, 0.3),
          height: DeviceUtils.getScaledSize(context, 0.3));
    }
  }

  String videoFileExt(String path) {
    return path.replaceAll(".jpg", ".mp4");
  }

  int showPostProgress(received, total) {
    if (total != -1) {
      setState(() {
        print((received / total * 100).toStringAsFixed(0) + "%");
        progressPercentage = received / total;
      });
    }
  }

  Future<void> _showMyDialog(BuildContext context, String title, String message,
      Widget extraBtn) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: Text(message),
          ),
          actions: <Widget>[extraBtn],
        );
      },
    );
  }

  @override
  void dispose() {
    titleController.dispose();
    descriptionController.dispose();
    super.dispose();
  }
}
