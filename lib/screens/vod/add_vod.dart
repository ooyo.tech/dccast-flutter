import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart' as Dio;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:flutter_uploader/flutter_uploader.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:video_thumbnail/video_thumbnail.dart';
import 'package:video_trimmer/video_trimmer.dart';

import '../../constants/assets.dart';
import '../../constants/preferences.dart';
import '../../constants/server_settings.dart';
import '../../constants/styles.dart';
import '../../model/category.dart';
import '../../model/vod/upload_thumb.dart';
import '../../model/vod/upload_video.dart';
import '../../utils/category.dart';
import '../../utils/device.dart';
import '../../utils/dio_client.dart';
import '../../utils/media.dart';
import '../../utils/toast.dart';
import '../video_detail.dart';
import 'search_dcgallery.dart';
import 'trimmer_view.dart';

enum Answers { CAMERA, GALLERY }
enum Progress { UPLOAD, LOADING, SUCCESS }

class StartVodScreen extends StatefulWidget {
  static const String id = 'start_vod_screen';
  int groupId;

  StartVodScreen({Key key, this.groupId}): super(key: key);

  @override
  _StartVodScreenState createState() => _StartVodScreenState();
}

class _StartVodScreenState extends State<StartVodScreen> {
  final storage = FlutterSecureStorage();

  String selectedKind = translate('vod.upload.share.public');
  String selectedGallery = "";
  final uploader = FlutterUploader();

  /// Variables
  File _videoFile;
  File _imageFile;
  ImagePicker picker = ImagePicker();
  final Trimmer _trimmer = Trimmer();
  final titleController = TextEditingController();
  final descriptionController = TextEditingController();
  bool isLoading = false;
  Progress STEPS = Progress.UPLOAD;
  Category _categories;
  int selectedCategory;
  double progressPercentage = 0.0;
  Dio.CancelToken token;
  VodVideoUpload mediaUploaded;
  VodThumbUpload thumbUploaded;
  int uploadMediaResponseId;
  PickedFile pickedFile;
  File trimmedFile;
  String trimmedPath;

  SnackBar _snackBar;

  // This funcion will helps you to pick a Video File
  _pickVideo() async {
    pickedFile = await picker.getVideo(source: ImageSource.gallery);
    if (pickedFile != null) {
      File picked = File(pickedFile.path);
      await _trimmer.loadVideo(videoFile: picked);
      var _trimmedPath = await Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) {
        return TrimmerView(_trimmer);
      }));
      if (_trimmedPath == null) {
        if (trimmedFile == null) {
          Navigator.of(context).pop();
        }
      } else {
        trimmedPath = _trimmedPath;
        trimmedFile = File(trimmedPath);
        _thumbnailWidget(pickedFile.path);
        // _thumbnailWidget(trimmedPath);
      }
      picked?.delete();
    } else {
      Navigator.of(context).pop();
    }
  }

  // This funcion will helps you to pick a Video File from Camera
  _pickVideoFromCamera() async {
    pickedFile = await picker.getVideo(source: ImageSource.camera);
    if (pickedFile != null) {
      File picked = File(pickedFile.path);
      // _thumbnailWidget(pickedFile.path);
      await _trimmer.loadVideo(videoFile: picked);
      var _trimmedPath = await Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) {
        return TrimmerView(_trimmer);
      }));
      if (_trimmedPath == null) {
        if (trimmedFile == null) {
          Navigator.of(context).pop();
        }
      } else {
        trimmedPath = _trimmedPath;
        trimmedFile = File(trimmedPath);
        _thumbnailWidget(trimmedPath);
      }
      picked?.delete();
    } else {
      Navigator.of(context).pop();
    }
  }

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    _askUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _categories = Provider.of<CategoryUtils>(context, listen: false).categories;
    //   = _categories.results[0].id;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(translate("upload-vod")),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: ModalProgressHUD(
        opacity: 0.6,
        progressIndicator: Container(
            width: DeviceUtils.getScaledWidth(context, 0.8),
            height: DeviceUtils.getScaledHeight(context, 0.5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.white,
            ),
            child: progressIndic()),
        inAsyncCall: isLoading,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return "제목을 입력해 주세요";
                              }
                              return null;
                            },
                            controller: titleController,
                            maxLines: 1,
                            maxLength: 40,
                            maxLengthEnforcement: MaxLengthEnforcement.enforced,
                            style: TextStyle(color: kBlackColor, fontSize: 16),
                            decoration: InputDecoration(
                              hintText: translate("vod.upload.title"),
                              enabledBorder: const UnderlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1.0),
                              ),
                              focusedBorder: const UnderlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1.0),
                              ),
                            ),
                          ),
                          TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return "설명을 입력해 주세요";
                              }
                              return null;
                            },
                            maxLines: 3,
                            minLines: 1,
                            controller: descriptionController,
                            style:
                                TextStyle(color: kBlack50Color, fontSize: 16),
                            decoration: InputDecoration(
                              hintText: translate("vod.upload.description"),
                              enabledBorder: const UnderlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1.0),
                              ),
                              focusedBorder: const UnderlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1.0),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: 10.0),
                    _imageFile != null
                        ? SizedBox(
                            width: DeviceUtils.getScaledWidth(context, 0.3),
                            child: Image.file(_imageFile, fit: BoxFit.contain),
                          )
                        : SizedBox(
                            width: DeviceUtils.getScaledWidth(context, 0.3),
                            height: DeviceUtils.getScaledWidth(context, 0.3),
                            child: GestureDetector(
                              onTap: _askUser,
                              child: Container(
                                decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Colors.redAccent)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(Icons.camera_alt_rounded),
                                    Text("image")
                                  ],
                                ),
                              ),
                            )),
                  ],
                ),
                SizedBox(
                  height: 20.0,
                ),
                if (widget.groupId == null)
                Row(children: [
                  Container(
                    width: DeviceUtils.getScaledWidth(context, 0.3),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      translate("vod.upload.category"),
                    ),
                  ),
                  Expanded(
                    child: DropdownButtonFormField(
                      isExpanded: true,
                      value: selectedCategory,
                      hint: Text(translate("vod.upload.choose-cat")),
                      iconEnabledColor: kPrimaryColor,
                      items:
                          List.generate(_categories.results.length, (int item) {
                        return DropdownMenuItem(
                          value: _categories.results[item]?.id,
                          child: Text(_categories.results[item]?.name),
                        );
                      }),
                      onChanged: (value2) {
                        setState(() {
                          selectedCategory = value2;
                        });
                      },
                      validator: (value) => value == null
                          ? translate("vod.upload.choose-cat")
                          : null,
                    ),
                  )
                ]),
                if (widget.groupId == null)
                Row(children: [
                  Container(
                    width: DeviceUtils.getScaledWidth(context, 0.3),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      translate("vod.upload.share"),
                    ),
                  ),
                  Expanded(
                    child: DropdownButtonFormField(
                      iconEnabledColor: kPrimaryColor,
                      isExpanded: true,
                      hint: Text(translate("vod.upload.select-share")),
                      items: [
                        DropdownMenuItem<String>(
                          value: translate('vod.upload.share.public'),
                          child: Text(translate('vod.upload.share.public')),
                        ),
                        DropdownMenuItem<String>(
                          value: translate('vod.upload.share.private'),
                          child: Text(translate('vod.upload.share.private')),
                        ),
                        DropdownMenuItem<String>(
                          value: translate('vod.upload.share.under19'),
                          child: Text(translate('vod.upload.share.under19')),
                        ),
                      ],
                      value: selectedKind,
                      validator: (value) => value == null
                          ? translate("vod.upload.select-share")
                          : null,
                      onChanged: (value) {
                        if (value == translate('vod.upload.share.under19')) {
                          _showMyDialog(
                            context,
                            translate("alert.message.under-19-title"),
                            translate("alert.message.under-19-description"),
                            TextButton(
                              child: Text(translate("alert.button.ok")),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          );
                        }
                        setState(() {
                          selectedKind = value;
                        });
                      },
                    ),
                  )
                ]),
                if (widget.groupId == null)
                Row(children: [
                  Container(
                    width: DeviceUtils.getScaledWidth(context, 0.3),
                    alignment: Alignment.centerLeft,
                    child: Text("dcinside\n동시 등록"),
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () async {
                        var result = await Navigator.of(context).
                          pushNamed(SearchDcGalleryScreen.id);
                        if (result != null) {
                          setState(() {
                            selectedGallery = result;
                          });
                        }
                      },
                      child: DropdownButtonFormField(
                        iconEnabledColor: kPrimaryColor,
                        isExpanded: true,
                        hint: Text(translate("vod.upload.select-gallery")),
                        items: [DropdownMenuItem<String>(
                          value: selectedGallery,
                          child: Text(selectedGallery == "" 
                            ? translate("vod.upload.select-gallery") 
                            : selectedGallery),
                        )],
                        value: selectedGallery,
                      ),
                    ),
                  )
                ]),
                RaisedButton(
                  color: kPrimaryColor,
                  child: Text(
                    translate("vod.upload.post"),
                    style: kSwiperStyle,
                  ),
                  onPressed: _submit,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future _askUser() async {
    await Future.delayed(Duration(milliseconds: 50));

    switch (await showDialog(
        context: context,
        builder: (context) => SimpleDialog(
              title: Text(translate("vod.upload.select-from.vod")),
              elevation: 4,
              children: <Widget>[
                SimpleDialogOption(
                  child: Row(children: [
                    Icon(FontAwesomeIcons.photoVideo),
                    SizedBox(width: 15),
                    Text(translate("vod.upload.select-vod")),
                  ]),
                  onPressed: () {
                    Navigator.pop(context, Answers.GALLERY);
                  },
                ),
                SimpleDialogOption(
                  child:Row(children: [
                    Icon(FontAwesomeIcons.camera),
                    SizedBox(width: 15),
                    Text(translate("vod.upload.select-camera")),
                  ]),
                  onPressed: () {
                    Navigator.pop(context, Answers.CAMERA);
                  },
                ),
              ],
            ))) {
      case Answers.GALLERY:
        _pickVideo();
        break;
      case Answers.CAMERA:
        _pickVideoFromCamera();
        break;
    }
  }

  void _thumbnailWidget(String videoPath) async {
    final uint8list = await VideoThumbnail.thumbnailFile(
      video: videoPath,
      imageFormat: ImageFormat.PNG,
      maxWidth: 300,
      quality: 50,
    );

    setState(() {
      _imageFile = File(uint8list);
    });
  }

  void _submit() {
    if (_formKey.currentState.validate()) {
      FocusScopeNode currentFocus = FocusScope.of(context);
      if (!currentFocus.hasPrimaryFocus) {
        currentFocus.unfocus();
      }
      if (trimmedFile != null) {
        postUploadMedia();
        setState(() {
          isLoading = true;
        });
      }
    }
  }

  Widget progressIndic() {
    if (STEPS == Progress.SUCCESS) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Lottie.asset(Assets.animSuccess,
              width: DeviceUtils.getScaledSize(context, 0.3),
              height: DeviceUtils.getScaledSize(context, 0.3)),
          FlatButton(
            child: Text("OK"),
            onPressed: () {
              setState(() {
                isLoading = false;
              });
              Provider.of<MediaUtils>(context, listen: false)
                .fetchMedia(mediaID: uploadMediaResponseId).then((value) => {
                Navigator.of(context).pop(),
                Navigator.push(
                  context, //VideoDetailScreen.id
                  MaterialPageRoute(builder: 
                    (context) => VideoDetailScreen(content: eContent.vod),
                  ))
              });
            },
          )
        ],
      );
    } else if (STEPS == Progress.UPLOAD) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircularPercentIndicator(
            radius: 100.0,
            lineWidth: 5.0,
            percent: progressPercentage,
            center: new Text(
                (progressPercentage * 100.0).toStringAsFixed(0) + " %"),
            progressColor: kPrimaryColor,
            backgroundColor: Colors.white,
          ),
          FlatButton(
            child: Text(translate("cancel")),
            onPressed: () {
              showToast("VOD 등록이 취소되었습니다");
              setState(() {
                Timer(Duration(milliseconds: 500), () {
                  token.cancel("cancelled");
                });
                isLoading = false;
              });
            },
          )
        ],
      );
    } else {
      return Lottie.asset(Assets.animLoad,
          width: DeviceUtils.getScaledSize(context, 0.3),
          height: DeviceUtils.getScaledSize(context, 0.3));
    }
  }

  String videoFileExt(String path) {
    return path.replaceAll(".jpg", ".mp4");
  }

  Future<String> postUploadMedia() async {
    final dio = Dio.Dio();
    token = Dio.CancelToken();
    String userID = await storage.read(key: Preferences.user_id);
    try {
      Dio.FormData thumData = Dio.FormData.fromMap({
        "thumbnail": await Dio.MultipartFile.fromFile(_imageFile.path),
      });
      final thumbResponse = await DioClient(dio).post(
          ServerSettings.uploadVODThumbURL,
          data: thumData,
          cancelToken: token);

      thumbUploaded = VodThumbUpload.fromJson(thumbResponse);

      // Upload VOD
      List<String> dir = trimmedPath.split('/');
      var fileName = dir.last;
      dir.removeLast();
      var filePath = dir.join('/');
      File(trimmedPath).exists().then((value) => print(value));
      var response = await uploader.enqueue(
        url: ServerSettings.uploadVODVideoURL,
        files: [FileItem(
          filename: fileName,
          savedDir: filePath,
          fieldname: "avatar"
        )],
        method: UploadMethod.POST,
        showNotification: false,
        tag: titleController.text);
      var setOuterState = setState;
      final subscriptionProgress = uploader.progress.listen((progress) {
        setOuterState(() {
          progressPercentage = progress.progress / 100;
        });
      });

      final subscriptionResult = uploader.result.listen((result) async {
        mediaUploaded = VodVideoUpload.fromJson(jsonDecode(result.response));
        if (mediaUploaded != null && thumbUploaded != null) {
          Map createMedia = {
            "user": userID,
            "title": titleController.text,
            "explanation": descriptionController.text,
            "category": "VOD",
            "kinds": selectedKind,
            "duration": mediaUploaded.duration,
            "orientation": mediaUploaded.orientation.toString(),
            "media_id": mediaUploaded.filename,
            "media_thumbnail": thumbUploaded.path
                .replaceAll("public", ServerSettings.videoThumbUrlChangeURL),
            "media_category": selectedCategory,
            "dc_gallery": "dccon",
            "dc_gallery_name": selectedGallery
          };
          if (widget.groupId != null) {
            createMedia["group_id"] = widget.groupId;
          }

          var response = await DioClient(dio).post(
            ServerSettings.createVODMediaURL,
            data: createMedia,
            cancelToken: token,
          );
          uploadMediaResponseId = response["id"];
          trimmedFile?.delete();
          setOuterState(() {
            STEPS = Progress.SUCCESS;
            // isLoading = false;
          });
        } else {
          print("media create faild, something is null");
        }
      }, onError: (ex, stacktrace) {
        print(ex);
        trimmedFile?.delete();
          // ... code to handle error
      });
      return "sucessful";
    } on Exception catch (error) {
      print(error.toString());
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<void> _showMyDialog(BuildContext context, String title, String message,
      Widget extraBtn) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: Text(message),
          ),
          actions: <Widget>[extraBtn],
        );
      },
    );
  }

  @override
  void dispose() {
    _videoFile = null;
    pickedFile = null;
    trimmedFile?.delete();
    titleController.dispose();
    descriptionController.dispose();
    super.dispose();
  }
}
