// import 'package:chewie/chewie.dart';
// import 'package:dccast/components/video/video_player_chewie.dart';
// import 'package:dio/dio.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_ijkplayer/flutter_ijkplayer.dart';
// // import 'package:flutter_vlc_player/flutter_vlc_player.dart';
// import 'package:lottie/lottie.dart';
// import 'package:provider/provider.dart';
// import 'package:timer_button/timer_button.dart';
// import 'package:video_player/video_player.dart';
// import 'package:webview_flutter/webview_flutter.dart';
//
// import '../components/detail_bottom.dart';
// import '../components/video/video_player_ijk.dart';
// import '../constants/assets.dart';
// import '../constants/server_settings.dart';
// import '../constants/styles.dart';
// import '../model/advertisement.dart';
// import '../model/user_media.dart';
// import '../utils/device.dart';
// import '../utils/dio_client.dart';
// import '../utils/media.dart';
//
// // const String liveURL = ServerSettings.wowZaUrl +
// //     "/live/ngrp:" +
// //     "spinaweb_1610830631" +
// //     "_all" +
// //     "/playlist.m3u8";
//
// // const String liveURL =
// //     "http://demo.unified-streaming.com/video/tears-of-steel/tears-of-steel.ism/.m3u8";
//
// const String liveURL = ServerSettings.wowZaUrl +
//     "/live/" +
//     "spinaweb_1610882454" +
//     "/playlist.m3u8";
//
// class VideoLiveTestScreen extends StatefulWidget {
//   static const String id = 'video_live_test_screen';
//   final eContent content;
//
//   const VideoLiveTestScreen({Key key, this.content}) : super(key: key);
//
//   @override
//   _VideoLiveTestScreenState createState() => _VideoLiveTestScreenState();
// }
//
// class _VideoLiveTestScreenState extends State<VideoLiveTestScreen> {
//   double aspectRatio = 16 / 9;
//   VideoPlayerController _adPlayerController;
//   VideoPlayerController _mainPlayerController;
//   ChewieController _chewieController;
//
//   bool isLoading = true;
//   bool _allowBack = false;
//   @override
//   Widget build(BuildContext context) {
//     print("LIVE URL: " + liveURL);
//     return Scaffold(
//         appBar: AppBar(
//           leading: IconButton(
//             icon: Icon(Icons.arrow_back_ios),
//             onPressed: () => _allowBack ? Navigator.of(context).pop() : {},
//           ),
//           title: Text("Title"),
//         ),
//         body: Column(
//           children: [VLCPlayer(), IjkLivePlayer(), ChewieDemo()],
//         ));
//   }
// }
//
// class VLCPlayer extends StatefulWidget {
//   @override
//   _VLCPlayerState createState() => _VLCPlayerState();
// }
//
// class _VLCPlayerState extends State<VLCPlayer> {
//   final String urlToStreamVideo = liveURL;
//   VlcPlayerController vlcController;
//
//   final double playerWidth = 640;
//   final double playerHeight = 360;
//
//   @override
//   void initState() {
//     vlcController = new VlcPlayerController(onInit: () {
//       vlcController.play();
//     });
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//         color: Colors.green,
//         height: playerHeight,
//         width: playerWidth,
//         child: new VlcPlayer(
//           aspectRatio: 16 / 9,
//           url: urlToStreamVideo,
//           controller: vlcController,
//           placeholder: Center(child: CircularProgressIndicator()),
//         ));
//   }
// }
//
// class IjkLivePlayer extends StatefulWidget {
//   @override
//   _IjkLivePlayerState createState() => _IjkLivePlayerState();
// }
//
// class _IjkLivePlayerState extends State<IjkLivePlayer> {
//   IjkMediaController ijkController = IjkMediaController();
//
//   DataSource source = DataSource.network(liveURL);
//
//   @override
//   void initState() {
//     super.initState();
//     ijkController.setDataSource(source, autoPlay: true);
//     OptionUtils.addDefaultOptions(ijkController);
//   }
//
//   @override
//   void dispose() {
//     ijkController?.dispose();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//         height: 200,
//         width: DeviceUtils.getScaledWidth(context, 1.0),
//         color: Colors.blue,
//         child: AspectRatio(
//           aspectRatio: 1280 / 720,
//           child: IjkPlayer(mediaController: ijkController),
//         ));
//   }
// }
//
// class OptionUtils {
//   static void addDefaultOptions(IjkMediaController controller) {
//     controller.addIjkPlayerOptions(
//       [TargetPlatform.iOS, TargetPlatform.android],
//       createIJKOptions(),
//     );
//   }
//
//   static Set<IjkOption> createIJKOptions() {
//     return <IjkOption>[
//       IjkOption(IjkOptionCategory.player, "mediacodec", 1),
//       IjkOption(IjkOptionCategory.player, "mediacodec-hevc", 1),
//       IjkOption(IjkOptionCategory.player, "videotoolbox", 1),
//       IjkOption(IjkOptionCategory.player, "opensles", 0),
//       IjkOption(IjkOptionCategory.player, "overlay-format", 0x32335652),
//       IjkOption(IjkOptionCategory.player, "framedrop", 1),
//       IjkOption(IjkOptionCategory.player, "start-on-prepared", 0),
//       IjkOption(IjkOptionCategory.format, "http-detect-range-support", 0),
//       IjkOption(IjkOptionCategory.codec, "skip_loop_filter", 48),
//     ].toSet();
//   }
// }
//
// class ChewieDemo extends StatefulWidget {
//   // ignore: use_key_in_widget_constructors
//   const ChewieDemo({this.title = 'Chewie Demo'});
//
//   final String title;
//
//   @override
//   State<StatefulWidget> createState() {
//     return _ChewieDemoState();
//   }
// }
//
// class _ChewieDemoState extends State<ChewieDemo> {
//   TargetPlatform _platform;
//   VideoPlayerController _videoPlayerController1;
//   VideoPlayerController _videoPlayerController2;
//   ChewieController _chewieController;
//
//   @override
//   void initState() {
//     super.initState();
//     initializePlayer();
//   }
//
//   @override
//   void dispose() {
//     _videoPlayerController1.dispose();
//     _chewieController.dispose();
//     super.dispose();
//   }
//
//   Future<void> initializePlayer() async {
//     _videoPlayerController1 = VideoPlayerController.network(liveURL);
//     await _videoPlayerController1.initialize();
//
//     _chewieController = ChewieController(
//       videoPlayerController: _videoPlayerController1,
//       autoPlay: true,
//       looping: true,
//       // Try playing around with some of these other options:
//
//       // showControls: false,
//       // materialProgressColors: ChewieProgressColors(
//       //   playedColor: Colors.red,
//       //   handleColor: Colors.blue,
//       //   backgroundColor: Colors.grey,
//       //   bufferedColor: Colors.lightGreen,
//       // ),
//       // placeholder: Container(
//       //   color: Colors.grey,
//       // ),
//       // autoInitialize: true,
//     );
//     setState(() {});
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       color: Colors.yellow,
//       child: Center(
//         child: _chewieController != null &&
//                 _chewieController.videoPlayerController.value.initialized
//             ? Chewie(
//                 controller: _chewieController,
//               )
//             : Column(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: const [
//                   CircularProgressIndicator(),
//                   SizedBox(height: 20),
//                   Text('Loading'),
//                 ],
//               ),
//       ),
//     );
//   }
// }
