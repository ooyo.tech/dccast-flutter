import 'dart:ui';

import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF525EAA);
const kBlackColor = Color(0xFF383A40);
const kBlack50Color = Color(0x80383A40);
const kRedColor = Color(0xFFE53935);
const kGreyColor = Color(0xFFE9EAEB);
const kMessageColor = Color(0xFF727A82);
const kDarkBlueColor = Color(0xFF313867);
const kWhite50Color = Color(0x80FFFFFF);
const kWhiteColor = Color(0xFFFFFFFF);
const matPrimaryColor = MaterialColor(0xFF525EAA, {});
// static double ratio = screenWidth / 360;
const kTextStyle = TextStyle(
  color: kBlackColor,
  fontWeight: FontWeight.normal,
  fontSize: 14.0,
);

const kTextBoldStyle = TextStyle(
  color: kBlackColor,
  fontWeight: FontWeight.bold,
  fontSize: 16.0,
);

const kTextPrimaryStyle = TextStyle(
  color: kPrimaryColor,
  fontWeight: FontWeight.normal,
  fontSize: 14.0,
);

const kSmallTextStyle = TextStyle(
  color: kBlackColor,
  fontWeight: FontWeight.normal,
  fontSize: 12.0,
);

const kTextGreyStyle = TextStyle(
  color: kBlack50Color,
  fontWeight: FontWeight.normal,
  fontSize: 14.0,
);

const kSideTitleStyle = TextStyle(
  fontFamily: 'Noto Sans',
  color: Colors.white,
  fontWeight: FontWeight.normal,
  fontSize: 16.0,
);

const kSwiperStyle = TextStyle(
  fontFamily: 'Noto Sans',
  color: Colors.white,
  fontWeight: FontWeight.normal,
  fontSize: 12.0,
);

const kNotificationTitle = TextStyle(
  fontFamily: 'Noto Sans',
  color: kBlackColor,
  fontWeight: FontWeight.normal,
  fontSize: 12.0,
);

const kNotificationSubtitle = TextStyle(
  fontFamily: 'Noto Sans',
  color: kBlack50Color,
  fontWeight: FontWeight.normal,
  fontSize: 12.0,
);

const kSmallText = TextStyle(
  fontFamily: 'Noto Sans',
  color: kBlack50Color,
  fontWeight: FontWeight.normal,
  fontSize: 10.0,
);

const kDurationText = TextStyle(
  fontFamily: 'Noto Sans',
  color: kGreyColor,
  fontWeight: FontWeight.normal,
  fontSize: 10.0,
);

InputDecoration kEmptyBorderInput(text, [bool errorStyle = false]) {
  return InputDecoration(
    hintText: text,
    border: InputBorder.none,
    focusedBorder: InputBorder.none,
    enabledBorder: InputBorder.none,
    errorBorder: InputBorder.none,
    disabledBorder: InputBorder.none,
    errorStyle: errorStyle ? TextStyle(
      fontSize: 10.0,
      height: 0.3,
    ) : TextStyle(),
    counterStyle: TextStyle(height: double.minPositive,),
    counterText: "",
  );
}

const kCategoryTitle = TextStyle(
    fontFamily: 'Gothic A1', fontSize: 12.0, fontWeight: FontWeight.w600);

enum eContent { vod, live }

enum eListType { hit, popular, recent }

enum eReport { COMMENT, VOD, LIVE }

class Metrics {
  static double screenWidth = window.physicalSize.width;

  static double halfBase = 4;
  static double singleBase = 8;
  static double doubleBase = 16;
  static double tripleBase = 24;
  static double quadrupleBase = 32;
  static double quintupleBase = 40;
  static double sextupleBase = 48;

  static const double BOTTOM_PADDING_PIP = 16;
  static const double VIDEO_HEIGHT_PIP = 200;
  static const double VIDEO_TITLE_HEIGHT_PIP = 70;
}

String secToMinConverter(int seconds) {
  int minutes = (seconds / 60).truncate();
  String minutesStr = (minutes % 60).toString().padLeft(2, '0');
  int remSec = seconds - (minutes * 60);
  String secStr = remSec.toString().padLeft(2, '0');

  return minutesStr + " : " + secStr;
}
