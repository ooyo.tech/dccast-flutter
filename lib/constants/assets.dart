class Assets {
  // splash screen assets
  static const String appLogo = "assets/images/logo_white.png";

  // login screen assets
  static const String carBackground = "assets/images/img_login.jpg";
  static const String videoThumbnail = "assets/images/img_place_holder.png";
  static const String sideMenuHome = "assets/images/side_home_black.png";
  static const String sideMenuSettings = 'assets/images/ic_live_settings.png';
  static const String sideMenuChannel = 'assets/images/side_channel_icon.png';
  static const String sideMenuLive = 'assets/images/side_live_icon.png';
  static const String sideMenuVod = 'assets/images/side_vod_icon.png';
  static const String sideMenuFavorite = 'assets/images/side_fav_icon.png';
  static const String sideMenuCast = 'assets/images/side_castlist_icon.png';
  static const String sideMenuProfile =
      'assets/images/ic_my_content_subscribe.png';

  ///Sidemenu categories
  static const String sideMenuSport = 'assets/images/side_sport.png';
  static const String sideMenuStock = 'assets/images/side_stock_icon.png';
  static const String sideMenuTravel = 'assets/images/side_travel.png';
  static const String sideMenuBeauty = 'assets/images/side_beauty.png';
  static const String sideMenuFood = 'assets/images/side_food.png';
  static const String sideMenuEntertainment =
      'assets/images/side_entertainment.png';
  static const String sideMenuGame = 'assets/images/side_game.png';

  static const String halfEllipse = 'assets/images/ellipse.png';

  static const String profileImage =
      'assets/images/ic_my_content_subscribe.png';
  static const String crownImage = 'assets/images/ic_crown.png';
  static const String lockImage = 'assets/images/ic_lock.png';
  static const String notifCheck =
      'assets/images/ic_notification_notice_check.png';
  static const String videoCommentIcon = 'assets/images/ic_player_comment.png';
  static const String shareIcon = 'assets/images/ic_video_share.png';
  static const String videoManduIcon = 'assets/images/ic_video_anim.png';

  /// SVGs
  static const String ellipseSvg = 'assets/svg/ellipse.svg';

  /// Icons
  static const String loading = "assets/images/loading.gif";
  static const String manduIcon = "assets/images/buuz_icon.png";
  static const String pointIcon = "assets/images/point_icon.png";
  static const String tvIcon = "assets/images/ic_my_content_mychannel.png";
  static const String heartIcon = "assets/images/ic_my_content_favorite.png";
  static const String profileIcon = "assets/images/ic_my_content_subscribe.png";
  static const String videoIcon = "assets/images/ic_my_content_recent.png";

  /// Lottie animations
  static const String animDumpling = "assets/animations/dc_dumpling.json";
  static const String animLoad = "assets/animations/dc_load.json";
  static const String animFollow = "assets/animations/follow.json";
  static const String animFollowNo = "assets/animations/follow_no.json";
  static const String animManduPreload = "assets/animations/mandu-preload.json";
  static const String animManduGift = "assets/animations/mandu_gift.json";
  static const String animNoData = "assets/animations/no_data.json";
  static const String animSuccess = "assets/animations/success.json";
}
