class Preferences {
  static const String is_logged_in = "isLoggedIn";
  static const String auth_token = "authToken";
  static const String is_dark_mode = "is_dark_mode";
  static const String current_language = "current_language";
  static const String user_id = "user_id";
  static const String auth_login = "auto_login";
  static const String username = "username";
  static const String password = "password";
  static const String connectivity = "connectivity";
  static const String SIGNATURE =
      "Ah54t065GYTinrJ2kzeXKjNDhHvqamsbLNPrawKJ3ds=";
  static const String APPLICATION_ID = "notq.dccast";
  static const String VERSION_NAME = "1.5.1";
  static const String appID = "app_id";
  static const String appIDTime = "app_id_time";
}
