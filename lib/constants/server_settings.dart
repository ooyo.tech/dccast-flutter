class ServerSettings {
  /// Base url setting for DCCast app
  static const String serverIP = "121.125.77.40";
  static const String baseUrl = "http://121.125.77.40:8080";
  static const String baseUploadUrl = "http://121.125.77.40:5050";
  static const String adminUrl = "https://cast.dcinside.com";
  static const String dcIdUrl = "https://dcid.dcinside.com";
  static const String mobileDcIdUrl = "http://m.dcinside.com";

  /// Base web socket url
  static const String socketUrl = "121.125.77.40:5050";

  /// Base wowZa client server url
  static const String wowZaUrl = "http://121.125.77.40:1935";
  static const String wowZaLiveUrl = "rtsp://121.125.77.40:1935/live/";

  // receiveTimeout
  static const int receiveTimeout = 5000;

  // connectTimeout
  static const int connectionTimeout = 3000;

  /// Authentication urls
  static const String loginURL = "$baseUrl/api/auth/login";
  static const String signUpURL = "$baseUrl/api/auth/signup";
  static const String logoutURL = "$baseUrl/api/auth/logout";
  static const String meURL = "$baseUrl/api/auth/me";
  static const String findidURL = "$baseUrl/api/find_id";
  static const String updateProfileURL = "$baseUrl/api/profile_list/";
  static const String dcInsideLoginURL =
      "$dcIdUrl/join/mobile_app_login.php/";
  static const String dcInsideGenerateKey =
      "$dcIdUrl/join/mobile_app_key_verification_3rd.php/";
  static const String manduHistoryURL = "$mobileDcIdUrl/api/dccast/webview";
  static const String termofUseURL = "https://nstatic.dcinside.com/dc/m/policy/policy.html";
  static const String privacyPolicyURL = "https://nstatic.dcinside.com/dc/m/policy/privacy.html";

  /// Notifications
  static const String notificationHasNew =
      "$baseUrl/api/notifications/has_new";
  static const String notificationRead =
      "$baseUrl/api/notifications/read?category=";
  static const String notificationNoticeURL =
      "$baseUrl/api/notifications/notice?user_id=";
  static const String notificationLiveURL =
      "$baseUrl/api/notifications/live?user_id=";
  static const String notificationVodURL =
      "$baseUrl/api/notifications/vod?user_id=";

  /// Points
  static const String getConfigURL = "$baseUrl/config/dcast_config";
  static const String getUserPointsURL = "$baseUrl/api/point/?user=";

  static const String getUserManduURL = "$baseUrl/api/mandu/?user=";

  /// User related
  static const String getUserFavoritesURL = "$baseUrl/api/favorite?user=";
  static const String getUserFavoriteRemoveURL = "$baseUrl/api/favorite/";
  static const String getUserMediaURL = "$baseUrl/media/media?user=";
  static const String getUserRecentMediaURL = "$baseUrl/api/recent/?user=";
  static const String getUserCreateRecentURL = "$baseUrl/api/recent_list/";
  static const String getDeleteRecentMediaURL = "$baseUrl/api/recent/";
  static const String getUserRecentSearchURL = "$baseUrl/api/search/";
  static const String deleteUserRecentSearchURL = "$baseUrl/api/search_list/";
  static const String watchAdURL = "$baseUrl/media/adwatch_list/";
  static const String getPointHistoryURL =
      "$baseUrl/api/point_to_mandu/?user=";
  static const String getPointTransactionURL =
      "$baseUrl/api/point_transaction/?to_user=";

  static const String getFollowersSearchURL =
      "$baseUrl/api/follow/followers_search/";
  static const String getFollowersURL =
      "$baseUrl/api/follow/followers?user_id=";
  static const String getFollowingURL =
      "$baseUrl/api/follow/following?user_id=";
  static const String getFollowingSearchURL =
      "$baseUrl/api/follow/following_search/";
  static const getUnfollowUserURL =
      "$baseUrl/api/follow/unfollowing?user_id=";
  static const String deleteFollowUserURL =
      "$baseUrl/api/follow/delete/?user_id=";
  static const String getFollowUserURL = "$baseUrl/api/follow/";
  static const String getFollowSearchURL =
      "$baseUrl/api/follow/follow_search/";

  static const String getFriendSearchURL =
      "$baseUrl/api/friends/avail_friends_search/";
  static const String getSendFriendRequestURL =
      "$baseUrl/api/friends_request/";
  static const String getAcceptFriendRequestURL =
      "$baseUrl/api/friends_request/accept/";
  static const String getDenyFriendRequestURL =
      "$baseUrl/api/friends_request/delete/";
  static const String getUnfriendRequestURL =
      "$baseUrl/api/friends_request/delete/?from_user=";
  static const String getListFriendsURL = "$baseUrl/api/friends/";

  static const String getStatCountURL = "$baseUrl/api/profiles/stat?user_id=";

  /// Q&A related
  static const String getCreateQuestionURL =
      "$baseUrl/property/question_list/";
  static const String getMyQandAURL = "$baseUrl/property/question/?user=";

  /// Media
  static const String getHitVodURL = "$baseUrl/media/media/hit_vod_list";
  static const String getHitLiveURL = "$baseUrl/media/media/hit_live_list";
  static const String getCategoryURL = "$baseUrl/property/mediacategory/";
  static const String getSearchByTitle = "$baseUrl/media/media/?search=";
  static const String getSearchHistory = "$baseUrl/api/search/?user_id=";
  static const String getMediaURL = "$baseUrl/media/media";
  static const String getApiUpdateURL = "$baseUrl/api/update";
  static const String lastUpdateURL = "$baseUrl/media/update";
  static const String getFavoriteStatusMediaURL = "$baseUrl/api/update";
  static const String getFavoriteMediaURL = "$baseUrl/api/favorite_list/";
  static const String getMediaLikeURL = "$baseUrl/media/like_list/";
  static const String getCommentListURL = "$baseUrl/media/comment";
  static const String getCommentLikeURL = "$baseUrl/media/commentlike_list/";

  static const String getCreateCommentURL = "$baseUrl/media/comment_list/";
  static const String getUpdateCommentURL = "$baseUrl/media/comment_list/";
  static const String getDeleteCommentURL = "$baseUrl/media/comment/";

  static const String getReportURL = "$baseUrl/property/declaration_list/";
  static const String getDeclarationURL = "$baseUrl/property/declaration/";

  static const String getSubscribeStatusURL = "$baseUrl/api/update";
  static const String getCreateSubscribeMediaURL =
      "$baseUrl/api/subscribe_list/";
  static const String getDeleteSubURL = "$baseUrl/api/subscribe/";

  /// VOD
  static const String uploadVODVideoURL = "$baseUploadUrl/upload";
  static const String uploadVODThumbURL = "$baseUploadUrl/upload_thumbnail";
  static const String createVODMediaURL = "$baseUrl/media/media_list/";
  static const String videoThumbUrlChangeURL = "https://cast.dcinside.com";

  /// Ads
  static const String getAdsURL = "$baseUrl/adver/update";
  // Random ad {kind:get_rand_adver, locate_id: vod_comment, media_category_id}
  // Affil counter{ "kind": "click_adver", "adver_id": 76 }

  // liveGetData
  // createThumbnail
  // getMediaOrientation

  static const String getForgetPasswordURL =
      "https://dcid.dcinside.com/join_mobile/member_find_id.php";
  static const String getSignUpURL =
      "https://dcid.dcinside.com/join_mobile/join_dcinside.php";

  static const String getFollowerStat = "$baseUrl/api/follow/followers_stat?user_id=";

  static const String getShareURL = "$adminUrl/dc_live/";

  static const String getGroupsURL = "$baseUrl/api/group";
  static const String getGroupsSearchURL = "$baseUrl/api/group/search/";

  static const String getVersionURL = "$baseUrl/config/version";
}
