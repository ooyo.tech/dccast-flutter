import 'package:better_player/better_player.dart';

final BetterPlayerTranslations ko_translation = BetterPlayerTranslations(
  languageCode: "ko",
  generalDefaultError: "비디오를 재생할 수 없음",
  generalNone: "없음",
  generalDefault: "기본값",
  generalRetry: "재시도",
  playlistLoadingNextVideo: "다음 동영상로드 중",
  controlsLive: "라이브",
  controlsNextVideoIn: "다음 비디오 입력",
  overflowMenuPlaybackSpeed: "재생 속도",
  overflowMenuSubtitles: "자막",
  overflowMenuQuality: "품질",
  overflowMenuAudioTracks: "오디오",
  qualityAuto: "자동"
);